#!/bin/sh
# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

# Run or rerun conda setup
s4m_exec miniconda::setup -e ./scpipe_scripts_env.yml -m "$1" "$2"
exit $?

