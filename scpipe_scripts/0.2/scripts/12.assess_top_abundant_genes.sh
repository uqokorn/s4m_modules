#!/bin/sh
## Assess the degree to which top most abundant genes in each library contribute
## to overall expression (by proportion)

DSID=XXXX
DSDIR=/path/to/data/datasets/$DSID
GENOME=%GENOME%
PROCDIR="$DSDIR/source/processed.$GENOME"
RAWFILE="$PROCDIR/counts/gene_count.txt"
## e.g. Ensembl_ID\tEntrez_ID\tSymbol\tDescription\tChromosome\tStart\tEnd\tStrand 
ANNOTFILE=%ANNOTATION_TSV%
TARGETS="$DSDIR/source/raw/targets_cells.txt"
OUTDIR="$PROCDIR/qc_gene_abundance"
mkdir -p "$OUTDIR"

CONDA_ENV=scpipe_scripts_c6d14fbba5c56782d05a2cc3df34cdd7
if [ -z "$MINICONDA_HOME" ]; then
  MINICONDA_HOME=~/.s4m/moduledata/miniconda/miniconda2
fi
source $MINICONDA_HOME/bin/activate $CONDA_ENV
if [ $? -ne 0 ]; then
  echo "Error: Failed to load conda environment '$CONDA_ENV'!"
  exit 1
fi

cd scripts
R --slave -f ./assess_top_abundant_genes.R --args -input="$RAWFILE" -outdir="$OUTDIR" -targets="$TARGETS"
if [ $? -ne 0 ]; then
  echo "Error: R script 'assess_top_abundant_genes.R' exited with non-zero status!"
  exit 1
fi
cd $OLDPWD

echo "Annotating.."
## attempt to annotate output top genes TSV table
head -1 "$OUTDIR/topgenes_data.tsv" > "$OUTDIR/.topgenes_header"
head -1 "$ANNOTFILE" | cut -f 3,5-9 > "$OUTDIR/.annot_header"
pr -Jtm "$OUTDIR/.topgenes_header" "$OUTDIR/.annot_header" > "$OUTDIR/topgenes_data_annotated.tsv"
tail -n+2 "$OUTDIR/topgenes_data.tsv" | while read line
do
  ensid=`echo "$line" | cut -f 2`
  ensannot=`grep -F "$ensid" "$ANNOTFILE" | cut -f 3,5-9`
  /bin/echo -e "$line\t$ensannot"
done >> "$OUTDIR/topgenes_data_annotated.tsv"
echo "Done"; echo

