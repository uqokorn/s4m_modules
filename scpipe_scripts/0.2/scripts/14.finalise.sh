#!/bin/sh
## Set up for Stemformatics loading
GENOME="$1"
COUNTDIR="counts"
## Default is "counts" but sometimes "normalised" required for pipelines starting with raw counts input
if [ ! -z "$2" ]; then
  COUNTDIR="$2"
fi

Usage () {
  echo; echo "Usage:  $0 <genome> <countdir>

  NOTE: default countdir is 'counts' if not supplied
"
}

if [ -z "$GENOME" -o -z "$COUNTDIR" ]; then
  Usage
  exit 1
fi

## NOTE: This isn't actually used for anything, but the file must exist for s4m.sh purposes!
RAW_TABLE="gene_count_log2_CPM+1.txt"
NORM_TABLE="gene_count_normalised_log2_CPM+1.txt"

echo "Checking inputs.."
if [ ! -f "./source/processed.$GENOME/$COUNTDIR/$RAW_TABLE" ]; then
  echo "Error: Input file './source/processed.$GENOME/$COUNTDIR/$RAW_TABLE' not found
!" 1>&2
  exit 1
fi
if [ ! -f "./source/processed.$GENOME/$COUNTDIR/$NORM_TABLE" ]; then
  echo "Error: Input file './source/processed.$GENOME/$COUNTDIR/$NORM_TABLE' not found
!" 1>&2
  exit 1
fi
echo "  done"; echo

echo "Linking expression files (for s4m.sh --finalize and --load to work) .."
(cd "./source/normalized" && [ ! -L raw_expression.txt ] && ln -s "../processed.$GENOME/$COUNTDIR/$RAW_TABLE" raw_expression.txt)
(cd "./source/normalized" && [ ! -L normalized_expression.txt ] && ln -s "../processed.$GENOME/$COUNTDIR/$NORM_TABLE" normalized_expression.txt)
echo "  done"

echo "All Done"; echo

