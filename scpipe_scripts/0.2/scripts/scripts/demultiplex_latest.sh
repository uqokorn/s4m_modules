#!/bin/sh
DSDIR="$1"
GENOME="$2"
R_BIN="$3"

procdir="$DSDIR/source/processed.$GENOME"
countout="$procdir/counts"
fastqdir="$DSDIR/source/raw"
bamout_reannotated="$procdir/aligned_reannotated"
## - FASTQ/sample targets
## NOTE: Requires "BarcodeCSV" column with .csv file name that contains the cell_id,barcode info (one per FASTQ)
targets_reformatted="$fastqdir/targets_reformatted.txt"

targetsheader="$countout/.targets_header"
head -1 "$targets_reformatted" > "$targetsheader"
targetsbody="$countout/.targets_body"
tail -n+2 "$targets_reformatted" > "$targetsbody"

bccol=`head -1 "$targets_reformatted" | tr "\t" "\n" | grep -n BarcodeCSV | cut -d':' -f 1`

## NOTE: Expects that we asked for N+1 CPUs (N = 1:1 CPU/BAM)
while read line; do
  sid=`echo "$line" | cut -f 1`
  ## NOTE: See 'bccol' var defined previously
  bcanno=`echo "$line" | cut -f $bccol`
  if [ ! -f "$fastqdir/$bcanno" ]; then echo "Error: Barcode annotation file '$fastqdir/$bcanno' for input '$sid' not found!" 1>&2; exit 1; fi
  ## Make temp targets.txt file for our current BAM
  targetssid="$countout/.targets_$sid.txt"
  echo "$line" | cat "$targetsheader" - > "$targetssid"

  echo "Starting demultiplexing for BAM '$sid'.."
  ##    Runtime: Approx 4.5 hours for 1,200M reads total, in parallel
  nohup $R_BIN --slave -f ./demultiplex_bam_single.R --args  -inputdir="$bamout_reannotated" -outdir="$countout" -targets="$targetssid" -barcode_csv="$fastqdir/$bcanno" 2>&1 | tee "$countout/demultiplex_bam_single_$sid.log" &
done < "$targetsbody"
## Wait for all spawned R subprocesses to finish
wait

## Post-demultiplex merge of gene counts (prep first and then actual)
statdirs=""

while read line; do
  sid=`echo "$line" | cut -f 1`
  if [ ! -d "$countout/$sid" ]; then echo "Error: Demultiplexed counts for input '$sid' not found!" 1>&2; exit 1; fi
  ## add per-cell count CSV files to merged path
  cd "$countout/$sid/count/"
  mkdir -p "$countout/count"
  cp -p *csv "$countout/count/"
  cd $OLDPWD
  ## create list of stat dirs to merge later
  if [ -z "$statdirs" ]; then statdirs="$countout/$sid/stat"; else statdirs="$statdirs,$countout/$sid/stat"; fi
done < "$targetsbody"

## skip merge and do simple symlink for N=1
if ! echo "$statdirs" | grep "," > /dev/null; then
  dname=`dirname "$statdirs"`
  sid=`basename "$dname"`
  ## symlink single stat output to canonical location
  (cd "$countout" && ln -s "$sid/stat" .)

## merge stats from input comma-separated list of stat dir paths
else
  mkdir -p "$countout/stat"
  $R_BIN --slave -f ./post_demultiplex_merge_stat_csv.R --args -statdirs="$statdirs" -outdir="$countout/stat"
fi

