## ============================================================================
## File:    post_demultiplex_merge_stat_csv.R
## Author:  O.Korn
## Created: 2018-03-31
##
## Merge 'N' stat data (from scPipe demultiplexing) i.e. when demultiplexing
## was run separately on multiple BAM inputs
##
## ============================================================================

source("./inc/util.R")

library(assertthat)

## ============================================================================
##                     ARGS PROCESSING AND OPTIONS
## ============================================================================

## Comma-separated list of paths to 'stat' diretories to merge
statdirs <- ARGV.get("statdirs", required=T)
## Output 'stat' directory to write merged CSV summary files 
outdir <- ARGV.get("outdir", required=T)

writeLines(paste("DEBUG: Got input statdirs=[",statdirs,"]",sep=""))

statdir_list <- strsplit(as.character(statdirs), ",")[[1]]
assert_that(length(statdir_list) >= 2)


## ============================================================================
##                                  MAIN 
## ============================================================================

overall_stat_merged <- NULL
chr_stat_merged <- NULL
cell_stat_merged <- NULL

overall_stat_names <- c()

for (statdir in statdir_list) {
  ## *** merge 'overall_stat.csv' data ***

  writeLines(paste("DEBUG: Loading CSV=[",paste(statdir,"/overall_stat.csv",sep=""),"]",sep=""))
  ## load data for current statdir
  overall <- read.table(paste(statdir,"/overall_stat.csv",sep=""), sep=",", header=T, as.is=T, check.names=F, quote="", stringsAsFactors=F, colClasses=c("character","numeric"))

  ## DEBUG
  #print(overall)

  if (is.null(overall_stat_merged)) {
    overall_stat_merged <- overall
    overall_stat_names <- as.character(overall$status)
  } else {
    ## if data points are the same and in order, we can just add the counts
    assert_that(identical(as.character(overall$status), overall_stat_names))
    overall_stat_merged$count <- overall_stat_merged$count + overall$count
  }

  ## *** merge 'chr_stat.csv' data ***
  writeLines(paste("DEBUG: Loading CSV=[",paste(statdir,"/chr_stat.csv",sep=""),"]",sep=""))
  ## Use read.table() to stop "chromosome name" header changing to "chromosome.name" etc
  chr_stat <- read.table(paste(statdir,"/chr_stat.csv",sep=""), sep=",", header=T, as.is=T, check.names=F, quote="", stringsAsFactors=F, colClasses=c("character","numeric"))
  if (is.null(chr_stat_merged)) {
    chr_stat_merged <- chr_stat
  } else {
    ## we don't assume that all demutiplexed subsets have equal chromosomal representation
    apply(chr_stat, 1, function(row) {
      if (row["chromosome name"] %in% chr_stat_merged$`chromosome name`) {
        chr_stat_merged[ chr_stat_merged$`chromosome name` == row["chromosome name"], "count" ] <- 
          chr_stat_merged[ chr_stat_merged$`chromosome name` == row["chromosome name"], "count" ] + as.numeric(row["count"])
      } else {
        chr_stat_merged <- rbind(chr_stat_merged, row)
      }
    })
  }

  ## *** merge 'cell_stat.csv' data ***
  writeLines(paste("DEBUG: Loading CSV=[",paste(statdir,"/cell_stat.csv",sep=""),"]",sep=""))
  cell_stat <- read.table(paste(statdir,"/cell_stat.csv",sep=""), sep=",", header=T, as.is=T, check.names=F, quote="", stringsAsFactors=F)
  cell_stat$cell_id <- as.character(cell_stat$cell_id)

  if (is.null(cell_stat_merged)) {
    cell_stat_merged <- cell_stat
  } else {
    cell_stat_merged <- rbind(cell_stat_merged, cell_stat)
  }
}

writeLines("Writing merged 'overall_stat.csv'..")
## write merged overall_stat.csv
write.csv(overall_stat_merged, file=paste(outdir,"/overall_stat.csv",sep=""), quote=F, row.names=F)
writeLines("Writing merged 'chr_stat.csv'..")
## write merged chr_stat.csv
write.csv(chr_stat_merged, file=paste(outdir,"/chr_stat.csv",sep=""), quote=F, row.names=F)
writeLines("Writing merged 'cell_stat.csv'..")
## write merged cell_stat.csv
write.csv(cell_stat_merged, file=paste(outdir,"/cell_stat.csv",sep=""), quote=F, row.names=F)

writeLines(capture.output(sessionInfo()), paste(outdir,"/sessionInfo.txt",sep=""))
writeLines("Merge done")
