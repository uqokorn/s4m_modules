#!/bin/sh
DSDIR="$1"
if [ ! -d "$DSDIR" ]; then
  echo "Error: Dataset directory '$DSDIR' not found!" 1>&2
  exit 1
fi
GENOME="$2"
if [ -z "$GENOME" ]; then
  echo "Error: Genome not given!" 1>&2
  exit 1
fi
COUNTSDIR="$3"
if [ -z "$COUNTSDIR" ]; then
  echo "Error: Counts directory not given (usually 'counts' or sometimes 'normalised')!" 1>&2
  exit 1
fi

echo "Generating t-SNE and PCA data.."
date
## Generate t-SNE and PCA data (post-QC and ISCANDAR inputs)
##
## NOTE: Requires at least 4 CPUs (raw, norm, norm filtered, norm filtered sans outliers)
##
./ISCANDAR/generate_tSNE_PCA_data.sh "$DSDIR" "$GENOME" "$COUNTSDIR"
if [ $? -ne 0 ]; then
  echo "Error: Script './ISCANDAR/generate_tSNE_PCA_data.sh' exited with non-zero status!"
  exit 1
fi
echo "Finished t-SNE and PCA data generation"
date

