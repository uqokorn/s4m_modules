#!/bin/sh
DSDIR="$1"
if [ ! -d "$DSDIR" ]; then
  echo "Error: Dataset directory '$DSDIR' not found!"
  exit 1
fi
GENOME="$2"
if [ -z "$GENOME" ]; then
  echo "Error: Genome not given!"
  exit 1
fi
TSNE_PERPLEXITY="$3"
if [ -z "$TSNE_PERPLEXITY" ]; then
  TSNE_PERPLEXITY=20
  echo "NOTE: Setting default t-SNE perplexity for ISCANDAR=$TSNE_PERPLEXITY"; echo
fi


## Run ISCANDAR proper (report generation)
##
date
echo "Generating ISCANDAR reports.."

./ISCANDAR/generate_ISCANDAR_reports.sh "$DSDIR" "$GENOME" "$TSNE_PERPLEXITY"
if [ $? -ne 0 ]; then
  echo "Error: Script './ISCANDAR/generate_ISCANDAR_reports.sh' exited with non-zero status!"
  exit 1
fi
echo "Finished generating ISCANDAR reports"
date
