#!/bin/sh
DSDIR="$1"
if [ ! -d "$DSDIR" ]; then
  echo "$0: Error: Dataset directory '$DSDIR' not found!"
  exit 1
fi
GENOME="$2"
if [ -z "$GENOME" ]; then
  echo "$0: Error: Genome not given!"
  exit 1
fi
PERPLEXITY="$3"
if [ -z "$PERPLEXITY" ]; then
  echo "$0: Error: Must supply t-SNE perplexity for ISCANDAR report generation!"
  exit 1
fi

PROCDIR="$DSDIR/source/processed.$GENOME"

OUTDIR_RAW="$PROCDIR/post_qc/raw"
OUTDIR_RAW_PLUS1="$PROCDIR/post_qc/raw_plus1"
OUTDIR_NORM="$PROCDIR/post_qc/normalized"
OUTDIR_NORM_PLUS1="$PROCDIR/post_qc/normalized_plus1"
OUTDIR_FILT="$PROCDIR/post_qc/normalized_filtered"
OUTDIR_FILT_PLUS1="$PROCDIR/post_qc/normalized_filtered_plus1"
OUTDIR_FILT_SANS_OUTLIERS="$PROCDIR/post_qc/normalized_filtered_sans_outliers"
OUTDIR_FILT_SANS_OUTLIERS_PLUS1="$PROCDIR/post_qc/normalized_filtered_sans_outliers_plus1"

MINICONDA_HOME=~/.s4m/moduledata/miniconda/miniconda2
CONDA_ENV=iscandar
CUSTOMDIR="$DSDIR/source/custom"
ISCANDAR_GIT="$CUSTOMDIR/iscandar"
ISCANDAR=iscandar

echo "Loading ISCANDAR conda environment.."
source $MINICONDA_HOME/bin/activate $CONDA_ENV
if [ $? -ne 0 ]; then
  echo "Error: Failed to load conda environment '$CONDA_ENV'!"
  exit 1
fi

echo; echo "Setting up ISCANDAR input/output paths.."

rm -rf "$OUTDIR_RAW/$ISCANDAR" "$OUTDIR_NORM/$ISCANDAR" "$OUTDIR_FILT/$ISCANDAR" "$OUTDIR_FILT_SANS_OUTLIERS/$ISCANDAR" \
  "$OUTDIR_RAW_PLUS1/$ISCANDAR" "$OUTDIR_NORM_PLUS1/$ISCANDAR" "$OUTDIR_FILT_PLUS1/$ISCANDAR" "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/$ISCANDAR" \

## copy iscandar to output path
cp -Rp $ISCANDAR_GIT "$OUTDIR_RAW/"
cp -Rp $ISCANDAR_GIT "$OUTDIR_RAW_PLUS1/"
cp -Rp $ISCANDAR_GIT "$OUTDIR_NORM/"
cp -Rp $ISCANDAR_GIT "$OUTDIR_NORM_PLUS1/"
cp -Rp $ISCANDAR_GIT "$OUTDIR_FILT/"
cp -Rp $ISCANDAR_GIT "$OUTDIR_FILT_PLUS1/"
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS" ]; then
  cp -Rp $ISCANDAR_GIT "$OUTDIR_FILT_SANS_OUTLIERS/"
fi
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1" ]; then
  cp -Rp $ISCANDAR_GIT "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/"
fi

rm -rf "$OUTDIR_RAW/$ISCANDAR/input/"*.txt \
  "$OUTDIR_RAW_PLUS1/$ISCANDAR/input/"*.txt \
  "$OUTDIR_NORM/$ISCANDAR/input/"*.txt \
  "$OUTDIR_NORM_PLUS1/$ISCANDAR/input/"*.txt \
  "$OUTDIR_FILT/$ISCANDAR/input/"*.txt \
  "$OUTDIR_FILT_PLUS1/$ISCANDAR/input/"*.txt \
  "$OUTDIR_FILT_SANS_OUTLIERS/$ISCANDAR/input/"*.txt \
  "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/$ISCANDAR/input/"*.txt
## set up iscandar inputs
cp -p "$OUTDIR_RAW"/*.txt "$OUTDIR_RAW/$ISCANDAR/input/"
cp -p "$OUTDIR_RAW_PLUS1"/*.txt "$OUTDIR_RAW_PLUS1/$ISCANDAR/input/"
cp -p "$OUTDIR_NORM"/*.txt "$OUTDIR_NORM/$ISCANDAR/input/"
cp -p "$OUTDIR_NORM_PLUS1"/*.txt "$OUTDIR_NORM_PLUS1/$ISCANDAR/input/"
cp -p "$OUTDIR_FILT"/*.txt "$OUTDIR_FILT/$ISCANDAR/input/"
cp -p "$OUTDIR_FILT_PLUS1"/*.txt "$OUTDIR_FILT_PLUS1/$ISCANDAR/input/"
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS" ]; then
  cp -p "$OUTDIR_FILT_SANS_OUTLIERS"/*.txt "$OUTDIR_FILT_SANS_OUTLIERS/$ISCANDAR/input/"
fi
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1" ]; then
  cp -p "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1"/*.txt "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/$ISCANDAR/input/"
fi

## link selected t-SNE plot for ISCANDAR to use
(cd "$OUTDIR_RAW/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
(cd "$OUTDIR_RAW_PLUS1/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
(cd "$OUTDIR_NORM/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
(cd "$OUTDIR_NORM_PLUS1/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
(cd "$OUTDIR_FILT/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
(cd "$OUTDIR_FILT_PLUS1/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS" ]; then
  (cd "$OUTDIR_FILT_SANS_OUTLIERS/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
fi
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1" ]; then
  (cd "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/$ISCANDAR/input/" && unlink tsne.txt 2>/dev/null; ln -s tsne_perplexity_$PERPLEXITY.txt ./tsne.txt)
fi

ts=`date +"%Y%m%d-%H%M%S"`

echo; echo "Running ISCANDAR (raw and plus1 counts).."
(cd "$OUTDIR_RAW/$ISCANDAR" && python create_data_model.py)
if [ $? -ne 0 ]; then
  echo "Error: ISCANDAR exited with non-zero status!"
  exit 1
else
  (cd "$OUTDIR_RAW/$ISCANDAR/output" && zip -r iscandar_raw_${ts}.zip js img Report.html)
  echo "Done"
fi
(cd "$OUTDIR_RAW_PLUS1/$ISCANDAR" && python create_data_model.py)
if [ $? -ne 0 ]; then
  echo "Error: ISCANDAR exited with non-zero status!"
  exit 1
else
  (cd "$OUTDIR_RAW_PLUS1/$ISCANDAR/output" && zip -r iscandar_raw_plus1_${ts}.zip js img Report.html)
  echo "Done"
fi


echo; echo "Running ISCANDAR (normalized and plus1 counts).."
(cd "$OUTDIR_NORM/$ISCANDAR" && python create_data_model.py)
if [ $? -ne 0 ]; then
  echo "Error: ISCANDAR exited with non-zero status!"
  exit 1
else
  (cd "$OUTDIR_NORM/$ISCANDAR/output" && zip -r iscandar_normalized_${ts}.zip js img Report.html)
  echo "Done"
fi
(cd "$OUTDIR_NORM_PLUS1/$ISCANDAR" && python create_data_model.py)
if [ $? -ne 0 ]; then
  echo "Error: ISCANDAR exited with non-zero status!"
  exit 1
else
  (cd "$OUTDIR_NORM_PLUS1/$ISCANDAR/output" && zip -r iscandar_normalized_plus1_${ts}.zip js img Report.html)
  echo "Done"
fi

echo; echo "Running ISCANDAR (normalized FILTERED and plus1 counts).."
(cd "$OUTDIR_FILT/$ISCANDAR" && python create_data_model.py)
if [ $? -ne 0 ]; then
  echo "Error: ISCANDAR exited with non-zero status!"
  exit 1
else
  (cd "$OUTDIR_FILT/$ISCANDAR/output" && zip -r iscandar_normalized_filtered_${ts}.zip js img Report.html)
  echo "Done"
fi
(cd "$OUTDIR_FILT_PLUS1/$ISCANDAR" && python create_data_model.py)
if [ $? -ne 0 ]; then
  echo "Error: ISCANDAR exited with non-zero status!"
  exit 1
else
  (cd "$OUTDIR_FILT_PLUS1/$ISCANDAR/output" && zip -r iscandar_normalized_filtered_plus1_${ts}.zip js img Report.html)
  echo "Done"
fi

echo; echo "Running ISCANDAR (normalized FILTERED, OUTLIER REMOVED and plus1 counts *** if required ***) .."
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS" ]; then
  (cd "$OUTDIR_FILT_SANS_OUTLIERS/$ISCANDAR" && python create_data_model.py)
  if [ $? -ne 0 ]; then
    echo "Error: ISCANDAR exited with non-zero status!"
    exit 1
  else
  (cd "$OUTDIR_FILT_SANS_OUTLIERS/$ISCANDAR/output" && zip -r iscandar_normalized_filtered_sans_outliers_${ts}.zip js img Report.html)
    echo "Done"
  fi
fi
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1" ]; then
  (cd "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/$ISCANDAR" && python create_data_model.py)
  if [ $? -ne 0 ]; then
    echo "Error: ISCANDAR exited with non-zero status!"
    exit 1
  else
  (cd "$OUTDIR_FILT_SANS_OUTLIERS_PLUS1/$ISCANDAR/output" && zip -r iscandar_normalized_filtered_sans_outliers_plus1_${ts}.zip js img Report.html)
    echo "Done"
  fi
fi

echo "All done"; echo
