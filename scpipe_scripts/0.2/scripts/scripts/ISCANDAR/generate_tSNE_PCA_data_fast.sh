#!/bin/sh
DSDIR="$1"
if [ ! -d "$DSDIR" ]; then
  echo "Error: Dataset directory '$DSDIR' not found!"
  exit 1
fi
GENOME="$2"
if [ -z "$GENOME" ]; then
  echo "Error: Genome not given, aborting SCE QC!" 1>&2
  exit 1
fi
## Usually "counts" but sometimes "normalised" if being overriden
COUNTSDIR="$3"
if [ -z "$COUNTSDIR" ]; then
  echo "Error: Counts directory not given (usually 'counts' or sometimes 'normalised'), aborting t-SNE/PCA generation!"
  exit 1
fi
## Number of threads per run - max should be: TOTAL CPU / 4
NCPU="$4"
## Number of CPU per run (raw, norm, norm filtered, sans outliers)
NTHREADS=1
if [ ! -z "$NCPU" ]; then
  ## NCPU must be divisible by 4
  MOD=`expr $NCPU % 4`
  if [ $MOD -ne 0 ]; then
    echo "Error: NCPU must be divisible by 4!" 1>&2
    exit 1
  fi
  NTHREADS=`expr $NCPU "/" 4`
fi

MINICONDA_HOME=~/.s4m/moduledata/miniconda/miniconda2
source $MINICONDA_HOME/bin/activate scpipe_scripts_c6d14fbba5c56782d05a2cc3df34cdd7
if [ $? -ne 0 ]; then
  echo "Error: Failed to load environment 'scpipe_scripts_c6d14fbba5c56782d05a2cc3df34cdd7'!"
  exit 1
fi

PROCDIR="$DSDIR/source/processed.$GENOME"

targets_cells="$DSDIR/source/raw/targets_cells.txt"

## t-SNE data and plots will be created for each value here
perplexity="20
50
100"

### Generate t-SNE and PCA data ###

## ============================================================================
##                                   RAW CPM+1
## ============================================================================
raw_plus1_data_for_iscandar () {
  RAW_OUTDIR="$PROCDIR/post_qc/raw_plus1"
  mkdir -p "$RAW_OUTDIR"
  RAW_INPUTDIR="$PROCDIR/$COUNTSDIR"
  RAW_EXPR_TABLE="gene_count_log2_CPM+1.txt"

  if [ ! -f "$RAW_INPUTDIR/$RAW_EXPR_TABLE" ]; then
    echo "Error: Input file '$RAW_INPUTDIR/$RAW_EXPR_TABLE' not found!"
    exit 1
  fi

  echo "Generating PCA (RSVD) data on RAW CPM+1 counts.." 
  time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$RAW_INPUTDIR/$RAW_EXPR_TABLE" -outdir="$RAW_OUTDIR"
  if [ $? -ne 0 ]; then
    echo "Error: Script 'do_rpca.R' exited with non-zero status!"
    exit 1
  fi

  echo "Generating t-SNE data on RAW CPM+1 counts.."
  for perp in $perplexity
  do
    time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$RAW_INPUTDIR/$RAW_EXPR_TABLE" -outdir="$RAW_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
      exit 1
    fi
  done
  (
    echo "Bundling t-SNE plots into PDF.."
    cd $RAW_OUTDIR;
    tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
    convert $tsne_plots  tsne_ALL.PDF
  )
  return $?
}


## ============================================================================
##                                   RAW 
## ============================================================================
raw_data_for_iscandar () {
  RAW_OUTDIR="$PROCDIR/post_qc/raw"
  mkdir -p "$RAW_OUTDIR"
  RAW_INPUTDIR="$PROCDIR/$COUNTSDIR"
  RAW_EXPR_TABLE="gene_count_log2_CPM.txt"

  if [ ! -f "$RAW_INPUTDIR/$RAW_EXPR_TABLE" ]; then
    echo "Error: Input file '$RAW_INPUTDIR/$RAW_EXPR_TABLE' not found!"
    exit 1
  fi

  echo "Generating PCA (RSVD) data on RAW CPM counts.." 
  time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$RAW_INPUTDIR/$RAW_EXPR_TABLE" -outdir="$RAW_OUTDIR"
  if [ $? -ne 0 ]; then
    echo "Error: Script 'do_rpca.R' exited with non-zero status!"
    exit 1
  fi

  echo "Generating t-SNE data on RAW CPM counts.."
  for perp in $perplexity
  do
    time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$RAW_INPUTDIR/$RAW_EXPR_TABLE" -outdir="$RAW_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
      exit 1
    fi
  done
  (
    echo "Bundling t-SNE plots into PDF.."
    cd $RAW_OUTDIR;
    tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
    convert $tsne_plots  tsne_ALL.PDF
  )
  return $?
}


## ============================================================================
##                               NORMALISED CPM+1
## ============================================================================
normalised_plus1_data_for_iscandar () {
  NORM_OUTDIR="$PROCDIR/post_qc/normalized_plus1"
  mkdir -p "$NORM_OUTDIR"
  NORM_INPUTDIR="$PROCDIR/$COUNTSDIR"
  NORM_EXPR_TABLE="gene_count_normalised_log2_CPM+1.txt"

  if [ ! -f "$NORM_INPUTDIR/$NORM_EXPR_TABLE" ]; then
    echo "Error: Input file '$NORM_INPUTDIR/$NORM_EXPR_TABLE' not found!"
    exit 1
  fi

  echo "Generating PCA (RSVD) data on NORMALISED CPM+1 counts.."
  time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$NORM_INPUTDIR/$NORM_EXPR_TABLE" -outdir="$NORM_OUTDIR"
  if [ $? -ne 0 ]; then
    echo "Error: Script 'do_rpca.R' exited with non-zero status!"
    exit 1
  fi

  echo "Generating t-SNE data on NORMALISED CPM+1 counts.."
  for perp in $perplexity
  do
    time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$NORM_INPUTDIR/$NORM_EXPR_TABLE" -outdir="$NORM_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
      exit 1
    fi
  done
  (
    echo "Bundling t-SNE plots into PDF.."
    cd $NORM_OUTDIR;
    tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
    convert $tsne_plots  tsne_ALL.PDF
  )
  return $?
}


## ============================================================================
##                               NORMALISED
## ============================================================================
normalised_data_for_iscandar () {
  NORM_OUTDIR="$PROCDIR/post_qc/normalized"
  mkdir -p "$NORM_OUTDIR"
  NORM_INPUTDIR="$PROCDIR/$COUNTSDIR"
  NORM_EXPR_TABLE="gene_count_normalised_log2_CPM.txt"

  if [ ! -f "$NORM_INPUTDIR/$NORM_EXPR_TABLE" ]; then
    echo "Error: Input file '$NORM_INPUTDIR/$NORM_EXPR_TABLE' not found!"
    exit 1
  fi

  echo "Generating PCA (RSVD) data on NORMALISED CPM counts.."
  time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$NORM_INPUTDIR/$NORM_EXPR_TABLE" -outdir="$NORM_OUTDIR"
  if [ $? -ne 0 ]; then
    echo "Error: Script 'do_rpca.R' exited with non-zero status!"
    exit 1
  fi

  echo "Generating t-SNE data on NORMALISED CPM counts.."
  for perp in $perplexity
  do
    time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$NORM_INPUTDIR/$NORM_EXPR_TABLE" -outdir="$NORM_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
      exit 1
    fi
  done
  (
    echo "Bundling t-SNE plots into PDF.."
    cd $NORM_OUTDIR;
    tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
    convert $tsne_plots  tsne_ALL.PDF
  )
  return $?
}


## ============================================================================
##                          NORMALISED FILTERED CPM+1
## ============================================================================
normalised_filtered_plus1_data_for_iscandar () {
  NORMF_OUTDIR="$PROCDIR/post_qc/normalized_filtered_plus1"
  mkdir -p "$NORMF_OUTDIR"
  NORMF_INPUTDIR="$PROCDIR/$COUNTSDIR"
  NORMF_EXPR_TABLE="gene_count_normalised_filtered_log2_CPM+1.txt"

  if [ ! -f "$NORMF_INPUTDIR/$NORMF_EXPR_TABLE" ]; then
    echo "Error: Input file '$NORMF_INPUTDIR/$NORMF_EXPR_TABLE' not found!"
    exit 1
  fi

  echo "Generating PCA (RSVD) data on NORMALISED FILTERED CPM+1 counts.."
  time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$NORMF_INPUTDIR/$NORMF_EXPR_TABLE" -outdir="$NORMF_OUTDIR"
  if [ $? -ne 0 ]; then
    echo "Error: Script 'do_rpca.R' exited with non-zero status!"
    exit 1
  fi

  echo "Generating t-SNE data on NORMALISED FILTERED CPM+1 counts.."
  for perp in $perplexity
  do
    time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$NORMF_INPUTDIR/$NORMF_EXPR_TABLE" -outdir="$NORMF_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
      exit 1
    fi
  done
  (
    echo "Bundling t-SNE plots into PDF.."
    cd $NORMF_OUTDIR;
    tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
    convert $tsne_plots  tsne_ALL.PDF
  )
  return $?
}


## ============================================================================
##                          NORMALISED FILTERED
## ============================================================================
normalised_filtered_data_for_iscandar () {
  NORMF_OUTDIR="$PROCDIR/post_qc/normalized_filtered"
  mkdir -p "$NORMF_OUTDIR"
  NORMF_INPUTDIR="$PROCDIR/$COUNTSDIR"
  NORMF_EXPR_TABLE="gene_count_normalised_filtered_log2_CPM.txt"

  if [ ! -f "$NORMF_INPUTDIR/$NORMF_EXPR_TABLE" ]; then
    echo "Error: Input file '$NORMF_INPUTDIR/$NORMF_EXPR_TABLE' not found!"
    exit 1
  fi

  echo "Generating PCA (RSVD) data on NORMALISED FILTERED CPM counts.."
  time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$NORMF_INPUTDIR/$NORMF_EXPR_TABLE" -outdir="$NORMF_OUTDIR"
  if [ $? -ne 0 ]; then
    echo "Error: Script 'do_rpca.R' exited with non-zero status!"
    exit 1
  fi

  echo "Generating t-SNE data on NORMALISED FILTERED CPM counts.."
  for perp in $perplexity
  do
    time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$NORMF_INPUTDIR/$NORMF_EXPR_TABLE" -outdir="$NORMF_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
      exit 1
    fi
  done
  (
    echo "Bundling t-SNE plots into PDF.."
    cd $NORMF_OUTDIR;
    tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
    convert $tsne_plots  tsne_ALL.PDF
  )
  return $?
}


## ============================================================================
##                  NORMALISED FILTERED - SANS OUTLIER SAMPLES CPM+1
## ============================================================================
normalised_filtered_sans_plus1_data_for_iscandar () {
  NORMFS_OUTDIR="$PROCDIR/post_qc/normalized_filtered_sans_outliers_plus1"
  NORMFS_INPUTDIR="$PROCDIR/counts_sans_outliers"
  NORMFS_EXPR_TABLE="gene_count_normalised_filtered_log2_CPM+1.txt"

  if [ -d "$NORMFS_INPUTDIR" ]; then
    mkdir -p "$NORMFS_OUTDIR"

    if [ ! -f "$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE" ]; then
      echo "Error: Input file '$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE' not found!"
      exit 1
    fi

    echo "Generating PCA (RSVD) data on NORMALISED FILTERED & OUTLIER SAMPLE REMOVED CPM+1 counts.."
    time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE" -outdir="$NORMFS_OUTDIR"
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_rpca.R' exited with non-zero status!"
      exit 1
    fi

    echo "Generating t-SNE data on NORMALISED FILTERED & OUTLIER SAMPLE REMOVED CPM+1 counts.."
    for perp in $perplexity
    do
      time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE" -outdir="$NORMFS_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
      if [ $? -ne 0 ]; then
	echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
	exit 1
      fi
    done
    (
      echo "Bundling t-SNE plots into PDF.."
      cd $NORMFS_OUTDIR;
      tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
      convert $tsne_plots  tsne_ALL.PDF
    )
    return $?

  else
    echo "WARNING: Outlier removed data not found, skipping PCA/t-SNE.."
  fi
}


## ============================================================================
##                  NORMALISED FILTERED - SANS OUTLIER SAMPLES
## ============================================================================
normalised_filtered_sans_data_for_iscandar () {
  NORMFS_OUTDIR="$PROCDIR/post_qc/normalized_filtered_sans_outliers"
  NORMFS_INPUTDIR="$PROCDIR/counts_sans_outliers"
  NORMFS_EXPR_TABLE="gene_count_normalised_filtered_log2_CPM.txt"

  if [ -d "$NORMFS_INPUTDIR" ]; then
    mkdir -p "$NORMFS_OUTDIR"

    if [ ! -f "$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE" ]; then
      echo "Error: Input file '$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE' not found!"
      exit 1
    fi

    echo "Generating PCA (RSVD) data on NORMALISED FILTERED & OUTLIER SAMPLE REMOVED CPM counts.."
    time R --slave -f ./ISCANDAR/do_rpca.R --args -input="$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE" -outdir="$NORMFS_OUTDIR"
    if [ $? -ne 0 ]; then
      echo "Error: Script 'do_rpca.R' exited with non-zero status!"
      exit 1
    fi

    echo "Generating t-SNE data on NORMALISED FILTERED & OUTLIER SAMPLE REMOVED CPM counts.."
    for perp in $perplexity
    do
      time R --slave -f ./ISCANDAR/do_tsne_fast.R --args -input="$NORMFS_INPUTDIR/$NORMFS_EXPR_TABLE" -outdir="$NORMFS_OUTDIR" -targets="$targets_cells" -perplexity=$perp -ncpu=$NTHREADS
      if [ $? -ne 0 ]; then
	echo "Error: Script 'do_tsne_fast.R' exited with non-zero status!"
	exit 1
      fi
    done
    (
      echo "Bundling t-SNE plots into PDF.."
      cd $NORMFS_OUTDIR;
      tsne_plots=`ls -1 *tsne_perp*png | sort -V | tr "\n" " "`;
      convert $tsne_plots  tsne_ALL.PDF
    )
    return $?

  else
    echo "WARNING: Outlier removed data not found, skipping PCA/t-SNE.."
  fi
}

## START ##
##
logbase=`basename $0 .sh`
LOGDIR="$PROCDIR/post_qc"
mkdir -p "$LOGDIR"

(raw_data_for_iscandar > "$LOGDIR/${logbase}_RAW.log" 2>&1 && raw_plus1_data_for_iscandar > "$LOGDIR/${logbase}_RAW_plus1.log" 2>&1)&
(normalised_data_for_iscandar > "$LOGDIR/${logbase}_NORMALISED.log" 2>&1 && normalised_plus1_data_for_iscandar > "$LOGDIR/${logbase}_NORMALISED_plus1.log" 2>&1)&
(normalised_filtered_data_for_iscandar > "$LOGDIR/${logbase}_NORMALISED_FILTERED.log" 2>&1 && normalised_filtered_plus1_data_for_iscandar > "$LOGDIR/${logbase}_NORMALISED_FILTERED_plus1.log" 2>&1)&
(normalised_filtered_sans_data_for_iscandar > "$LOGDIR/${logbase}_NORMALISED_FILTERED_SANS_OUTLIERS.log" 2>&1 && normalised_filtered_sans_plus1_data_for_iscandar > "$LOGDIR/${logbase}_NORMALISED_FILTERED_SANS_OUTLIERS_plus1.log" 2>&1)&
wait

grep -iP "error.*non-zero status" "$LOGDIR/${logbase}"*log > /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "Error: One or more t-SNE/PCA generations failed!
" 1>&2
  exit 1
fi

exit 0
