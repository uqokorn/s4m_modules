## File:    do_rpca.R 
## Desc:    Fast (SVD) implementation of PCA for large sample N
##          NOTE: PC1 and PC2 only - intended for ISCANDAR report only
## Author:  O.Korn
## Created: 2019-01-23 from 'do_pca.R'
## Updated:
##      2019-04-03: Added islog2 flag in order to prevent NA values breaking rpca
##
## ============================================================================

source("./ISCANDAR/inc/util.R")

library(rsvd)

## Path to expression table (TSV)
input <- ARGV.get("input", required=T)
## Path to output directory
outdir <- ARGV.get("outdir", required=T)

## Is input log2? Need to undo log2 for rpca to not break due to NA values
islog2 <- ARGV.get("islog2")
if (! is.na(islog2)) {
  islog2 <- as.logical(ARGV.get("islog2"))
} else {
  islog2 <- FALSE
}

## Load expression data
dat <- read.table(input, sep="\t", row.names=1, header=T, check.names=F, as.is=T, stringsAsFactors=F, quote="")

## Replace NA values with -Inf assuming input must be log2
if (any(is.na(dat)) && islog2 == TRUE) {
  writeLines("WARNING:  NA values detected in log2 input, setting all NA to -Inf..")
  dat[is.na(dat)] <- -Inf
}
## Undo log2 transform so Rtsne doesn't complain about missing values
if (islog2 == TRUE) {
  writeLines("Un-transforming log2 so the floor will be zero..")
  datraw <- dat
  dat <- 2^dat
  writeLines("DEBUG: First 5 rows:")
  head(dat, 5)
}

## PCA data
print("Calculating PCA (random SVD) data..")
## k = number of top PC to retain
pca_output <- rpca(t(dat), k = 2)

pca = data.frame(x=xy.coords(pca_output$x)$x, y=xy.coords(pca_output$x)$y)
rownames(pca) = colnames(dat)
colnames(pca) <- c("x","y")
print("Writing PCA (random SVD) data..")
write.table(pca, file=paste(outdir,"/pca.txt",sep=""), sep="\t", quote=F)

## Write sessionInfo() for R and library version info
writeLines(capture.output(sessionInfo()), paste(outdir,"/sessionInfo_PCA.txt",sep=""))
