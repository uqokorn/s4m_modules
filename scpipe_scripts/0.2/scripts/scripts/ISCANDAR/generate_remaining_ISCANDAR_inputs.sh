#!/bin/sh
## Generate the remaining input files required by ISCANDAR
DSDIR="$1"
if [ ! -d "$DSDIR" ]; then
  echo "Error: $0: Dataset directory '$DSDIR' not found!"
  exit 1
fi
GENOME="$2"
if [ -z "$GENOME" ]; then
  echo "Error: $0: Genome not given!"
  exit 1
fi
PERPLEXITY="$3"
if [ -z "$PERPLEXITY" ]; then
  echo "Error: $0: Perplexity not given!"
  exit 1
fi
COUNTSDIR="$4"
if [ -z "$COUNTSDIR" ]; then
  echo "Error: $0: Counts directory not given (usually 'counts' or 'normalised')!"
  exit 1
fi


if [ -z "$PIPELINE_DATA" ]; then
  PIPELINE_DATA="."
fi
PROCDIR="$DSDIR/source/processed.$GENOME"

COUNTDIR="$PROCDIR/$COUNTSDIR"
COUNTDIR_SANS_OUTLIERS="$PROCDIR/counts_sans_outliers"

OUTDIR_RAW="$PROCDIR/post_qc/raw"
OUTDIR_RAW_plus1="$PROCDIR/post_qc/raw_plus1"
OUTDIR_NORM="$PROCDIR/post_qc/normalized"
OUTDIR_NORM_plus1="$PROCDIR/post_qc/normalized_plus1"
OUTDIR_FILT="$PROCDIR/post_qc/normalized_filtered"
OUTDIR_FILT_plus1="$PROCDIR/post_qc/normalized_filtered_plus1"
OUTDIR_FILT_SANS_OUTLIERS="$PROCDIR/post_qc/normalized_filtered_sans_outliers"
OUTDIR_FILT_SANS_OUTLIERS_plus1="$PROCDIR/post_qc/normalized_filtered_sans_outliers_plus1"
mkdir -p "$OUTDIR_RAW" "$OUTDIR_RAW_plus1" "$OUTDIR_NORM" "$OUTDIR_NORM_plus1" "$OUTDIR_FILT" "$OUTDIR_FILT_plus1"
if [ -d "$COUNTDIR_SANS_OUTLIERS" ]; then
  mkdir -p "$OUTDIR_FILT_SANS_OUTLIERS" "$OUTDIR_FILT_SANS_OUTLIERS_plus1"
fi

TARGETS="$DSDIR/source/raw/targets_cells.txt"
if [ ! -f "$TARGETS" ]; then
  echo "Error: Targets file '$TARGETS' not found!" 1>&2
  exit 1
fi
## exit early if cell cycle predictions not found
if [ ! -f "$COUNTDIR/cell_cycle.txt" ]; then
  echo "Error: No cell cycle predictions found!" 1>&2
  exit 1
fi

###############################################################################
#                          EXPRESSION TABLES                                  #
###############################################################################

echo "Creating inputs for ISCANDAR.."
##    _____
##   | RAW |
##    -----
/bin/echo -e -n "  expression.txt (raw)\t\t\t..."
if [ -f "$COUNTDIR/gene_count_log2_CPM.txt" ]; then
  cp "$COUNTDIR/gene_count_log2_CPM.txt" "$OUTDIR_RAW/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_RAW/expression.txt"
  echo "done"
else
  echo "FAILED (file '$COUNTDIR/gene_count_log2_CPM.txt' not found)"
  exit 1
fi
raw_expression_ncols=`head -1 "$COUNTDIR/gene_count_log2_CPM.txt" | tr "\t" "\n" | wc -l`

##    __________
##   | RAW CPM+1|
##    ----------
/bin/echo -e -n "  expression.txt (raw CPM+1)\t\t\t..."
if [ -f "$COUNTDIR/gene_count_log2_CPM+1.txt" ]; then
  cp "$COUNTDIR/gene_count_log2_CPM+1.txt" "$OUTDIR_RAW_plus1/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_RAW_plus1/expression.txt"
  echo "done"
else
  echo "FAILED (file '$COUNTDIR/gene_count_log2_CPM+1.txt' not found)"
  exit 1
fi

##    ______
##   | NORM |
##    ------
/bin/echo -e -n "  expression.txt (norm)\t\t\t..."
if [ -f "$COUNTDIR/gene_count_normalised_log2_CPM.txt" ]; then
  cp "$COUNTDIR/gene_count_normalised_log2_CPM.txt" "$OUTDIR_NORM/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_NORM/expression.txt"
  echo "done"
else
  echo "FAILED (file '$COUNTDIR/gene_count_normalised_log2_CPM.txt' not found)"
  exit 1
fi

##    ____________
##   | NORM CPM+1 |
##    ------------
/bin/echo -e -n "  expression.txt (norm CPM+1)\t\t\t..."
if [ -f "$COUNTDIR/gene_count_normalised_log2_CPM+1.txt" ]; then
  cp "$COUNTDIR/gene_count_normalised_log2_CPM+1.txt" "$OUTDIR_NORM_plus1/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_NORM_plus1/expression.txt"
  echo "done"
else
  echo "FAILED (file '$COUNTDIR/gene_count_normalised_log2_CPM+1.txt' not found)"
  exit 1
fi

##    _______________
##   | NORM FILTERED |
##    ---------------
/bin/echo -e -n "  expression.txt (norm, filtered)\t..."
if [ -f "$COUNTDIR/gene_count_normalised_filtered_log2_CPM.txt" ]; then
  cp "$COUNTDIR/gene_count_normalised_filtered_log2_CPM.txt" "$OUTDIR_FILT/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_FILT/expression.txt"
  echo "done"
else
  echo "FAILED (file '$COUNTDIR/gene_count_normalised_filtered_log2_CPM.txt' not found)"
  exit 1
fi

##    _____________________
##   | NORM FILTERED CPM+1 |
##    ---------------------
/bin/echo -e -n "  expression.txt (norm, filtered CPM+1)\t..."
if [ -f "$COUNTDIR/gene_count_normalised_filtered_log2_CPM+1.txt" ]; then
  cp "$COUNTDIR/gene_count_normalised_filtered_log2_CPM+1.txt" "$OUTDIR_FILT_plus1/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_FILT_plus1/expression.txt"
  echo "done"
else
  echo "FAILED (file '$COUNTDIR/gene_count_normalised_filtered_log2_CPM+1.txt' not found)"
  exit 1
fi

##    ___________________________
##   | FILTERED, OUTLIER REMOVED |
##    ---------------------------
/bin/echo -e -n "  expression.txt (norm, filtered, outliers removed)\t..."
if [ -f "$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM.txt" ]; then
  cp "$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM.txt" "$OUTDIR_FILT_SANS_OUTLIERS/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_FILT_SANS_OUTLIERS/expression.txt"
  echo "done"
else
  echo "SKIPPED (file '$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM.txt' not found)"
fi

##    ________________________________
##   | FILTERED, OUTLIER REMOVED CPM+1|
##    --------------------------------
/bin/echo -e -n "  expression.txt (norm, filtered, outliers removed CPM+1)\t..."
if [ -f "$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM+1.txt" ]; then
  cp "$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM+1.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/expression.txt"
  sed -i -r -e '1 s|^|\t|' "$OUTDIR_FILT_SANS_OUTLIERS_plus1/expression.txt"
  echo "done"
else
  echo "SKIPPED (file '$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM+1.txt' not found)"
fi


###############################################################################
#                                  SAMPLES                                    #
###############################################################################

/bin/echo -e -n "  samples.txt\t\t\t\t..."
## create "samples.txt"
##    _____
##   | RAW |
##    -----
cut -f 1,2 "$TARGETS" | sed '1 s|^.*$|sample_id\tgroup|' > "$OUTDIR_RAW/samples.txt"
cp "$OUTDIR_RAW/samples.txt" "$OUTDIR_RAW_plus1/"
##    ______
##   | NORM |
##    ------
## Get actual samples used from expression data
TARGETS_NORM="$OUTDIR_NORM/targets.txt"
head -1 "$TARGETS" > "$TARGETS_NORM"
head -1 "$COUNTDIR/gene_count_normalised_log2_CPM.txt" | tr "\t" "\n" > "$OUTDIR_NORM/.sample_filter.txt"
grep -w -f "$OUTDIR_NORM/.sample_filter.txt" "$TARGETS" >> "$TARGETS_NORM"
cut -f 1,2 "$TARGETS_NORM" | sed '1 s|^.*$|sample_id\tgroup|' > "$OUTDIR_NORM/samples.txt"
cp "$OUTDIR_NORM/targets.txt" "$OUTDIR_NORM/.sample_filter.txt" "$OUTDIR_NORM/samples.txt" "$OUTDIR_NORM_plus1/"
##    _______________
##   | NORM FILTERED |
##    ---------------
## Get actual samples used from expression data
TARGETS_FILT="$OUTDIR_FILT/targets.txt"
head -1 "$TARGETS" > "$TARGETS_FILT"
head -1 "$COUNTDIR/gene_count_normalised_filtered_log2_CPM.txt" | tr "\t" "\n" > "$OUTDIR_FILT/.sample_filter.txt"
grep -w -f "$OUTDIR_FILT/.sample_filter.txt" "$TARGETS" >> "$TARGETS_FILT"
cut -f 1,2 "$TARGETS_FILT" | sed '1 s|^.*$|sample_id\tgroup|' > "$OUTDIR_FILT/samples.txt"
cp "$OUTDIR_FILT/targets.txt" "$OUTDIR_FILT/.sample_filter.txt" "$OUTDIR_FILT/samples.txt" "$OUTDIR_FILT_plus1/"
##    ___________________________
##   | FILTERED, OUTLIER REMOVED |
##    ---------------------------
if [ -f "$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM.txt" ]; then
  ## get filtered sample list directly from expression data to prevent errors
  filt_samples=`head -1 "$COUNTDIR_SANS_OUTLIERS/gene_count_normalised_filtered_log2_CPM.txt" | tr "\t" "\n"`
  echo "$filt_samples" > "$OUTDIR_FILT_SANS_OUTLIERS/.sample_filter.txt"
  ## create filtered targets and samples list while we're here
  TARGETS_FILT_SANS_OUTLIERS="$OUTDIR_FILT_SANS_OUTLIERS/targets.txt"
  head -1 "$TARGETS" > "$TARGETS_FILT_SANS_OUTLIERS"
  ## Note on "-w" flag - sub-strings must match whole words (anchored by ^, $ or non-alphanumeric e.g. first column full match)
  grep -w -f "$OUTDIR_FILT_SANS_OUTLIERS/.sample_filter.txt" "$TARGETS" >> "$TARGETS_FILT_SANS_OUTLIERS"
  cut -f 1,2 "$TARGETS_FILT_SANS_OUTLIERS" | sed '1 s|^.*$|sample_id\tgroup|' > "$OUTDIR_FILT_SANS_OUTLIERS/samples.txt"
  ## validate filtered targets sample count matches expected
  expect_sampleN=`echo "$filt_samples" | wc -l`
  filtered_sampleN=`tail -n+2 "$OUTDIR_FILT_SANS_OUTLIERS/samples.txt" | wc -l`
  if [ $expect_sampleN -ne $filtered_sampleN ]; then
    echo "Internal Error: Filtered sample count ($filtered_sampleN) does not match expected sample count ($expect_sampleN)!" 1>&2
    exit 1
  fi
  cp "$OUTDIR_FILT_SANS_OUTLIERS/.sample_filter.txt" "$OUTDIR_FILT_SANS_OUTLIERS/targets.txt" "$OUTDIR_FILT_SANS_OUTLIERS/samples.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/"
fi
echo "done"


###############################################################################
#                                  GENESETS                                   #
###############################################################################

colours="#3E3B8F,#C73201,#0E703F,#E09F35,#9FD6F3"

/bin/echo -e -n "  genesets.txt\t\t\t\t..."
##    _____
##   | RAW |
##    -----
/bin/echo -e "name\tgenes\tmeanExpression" > "$OUTDIR_RAW/genesets.txt"
cp "$OUTDIR_RAW/genesets.txt" "$OUTDIR_RAW_plus1/"
##    ______
##   | NORM |
##    ------
cp "$OUTDIR_RAW/genesets.txt" "$OUTDIR_NORM/genesets.txt"
cp "$OUTDIR_NORM/genesets.txt" "$OUTDIR_NORM_plus1/"
##    _______________
##   | NORM FILTERED |
##    ---------------
cp "$OUTDIR_RAW/genesets.txt" "$OUTDIR_FILT/genesets.txt"
cp "$OUTDIR_FILT/genesets.txt" "$OUTDIR_FILT_plus1/"
##    ___________________________
##   | FILTERED, OUTLIER REMOVED |
##    ---------------------------
if [ -d "$OUTDIR_FILT_SANS_OUTLIERS" ]; then
  cp "$OUTDIR_RAW/genesets.txt" "$OUTDIR_FILT_SANS_OUTLIERS/genesets.txt"
  cp "$OUTDIR_FILT_SANS_OUTLIERS/genesets.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/"
fi
echo "done (empty)"


###############################################################################
#                              SAMPLE GROUPS                                  #
###############################################################################

/bin/echo -e -n "  sampleGroupItems.txt\t\t\t..."
## create 'sampleGroupItems.txt'
##    _____
##   | RAW |
##    -----
/bin/echo -e "sampleGroup\titems\tcolours" > "$OUTDIR_RAW/sampleGroupItems.txt"
values=`tail -n+2 "$TARGETS" | cut -f 2 | sort -u | tr "\n" "," | sed 's|\,$||'`
/bin/echo -e "group\t$values\t$colours" >> "$OUTDIR_RAW/sampleGroupItems.txt"
cp "$OUTDIR_RAW/sampleGroupItems.txt" "$OUTDIR_RAW_plus1/"
##    ______
##   | NORM |
##    ------
/bin/echo -e "sampleGroup\titems\tcolours" > "$OUTDIR_NORM/sampleGroupItems.txt"
values=`tail -n+2 "$TARGETS_NORM" | cut -f 2 | sort -u | tr "\n" "," | sed 's|\,$||'`
/bin/echo -e "group\t$values\t$colours" >> "$OUTDIR_NORM/sampleGroupItems.txt"
cp "$OUTDIR_NORM/sampleGroupItems.txt" "$OUTDIR_NORM_plus1/"
##    _______________
##   | NORM FILTERED |
##    ---------------
/bin/echo -e "sampleGroup\titems\tcolours" > "$OUTDIR_FILT/sampleGroupItems.txt"
values=`tail -n+2 "$TARGETS_FILT" | cut -f 2 | sort -u | tr "\n" "," | sed 's|\,$||'`
/bin/echo -e "group\t$values\t$colours" >> "$OUTDIR_FILT/sampleGroupItems.txt"
cp "$OUTDIR_FILT/sampleGroupItems.txt" "$OUTDIR_FILT_plus1/"
##    ___________________________
##   | FILTERED, OUTLIER REMOVED |
##    ---------------------------
if [ -d "$COUNTDIR_SANS_OUTLIERS" ]; then
  /bin/echo -e "sampleGroup\titems\tcolours" > "$OUTDIR_FILT_SANS_OUTLIERS/sampleGroupItems.txt"
  values=`tail -n+2 "$TARGETS_FILT_SANS_OUTLIERS" | cut -f 2 | sort -u | tr "\n" "," | sed 's|\,$||'`
  /bin/echo -e "group\t$values\t$colours" >> "$OUTDIR_FILT_SANS_OUTLIERS/sampleGroupItems.txt"
  cp "$OUTDIR_FILT_SANS_OUTLIERS/sampleGroupItems.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/"
fi

echo "done"


###############################################################################
#                                 CLUSTERS                                    #
###############################################################################

/bin/echo -e -n "  clusters.txt\t\t\t\t..."
## create "clusters.txt" adding cell cycle predictions as a baseline
##    _____
##   | RAW |
##    -----
cellcycle_nrows=`tail -n+2 "$COUNTDIR/cell_cycle.txt" | wc -l`
## NOTE: cell cycle is always done on normalised data that might necessarily have had low complexity cells removed,
## so only use this for raw data if the sample count is the same
if [ -f "$COUNTDIR/cell_cycle.txt" ]; then
  ## Replace NA with Unknown so downstream processes don't treat as missing value
  sed -i -r -e 's|NA|Unknown|g' "$COUNTDIR/cell_cycle.txt"
fi
if [ $cellcycle_nrows -eq $raw_expression_ncols ]; then
  cp "$COUNTDIR/cell_cycle.txt" "$OUTDIR_RAW/clusters.txt"
  ## append library size percentiles
  cut -f 2 "$COUNTDIR/library_counts_percentiles.txt" | sed -r -e '1 s|above_percentile|library_counts_percentile|' | pr -Jtm "$OUTDIR_RAW/clusters.txt" - > "$OUTDIR_RAW/clusters.txt.tmp"
  mv "$OUTDIR_RAW/clusters.txt.tmp" "$OUTDIR_RAW/clusters.txt"
else
  /bin/echo -e -n " skipped RAW cell cycle metadata (less samples than data) ..."
  ## Use only library size percentiles
  cat "$COUNTDIR/library_counts_percentiles.txt" | sed -r -e '1 s|above_percentile|library_counts_percentile|' > "$OUTDIR_RAW/clusters.txt"
fi
## Add mt-RNA content (proportion)
if [ -f "$COUNTDIR/MT_counts_proportion_binned.txt" ]; then
  cat "$COUNTDIR/MT_counts_proportion_binned.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_RAW/clusters.txt" - > "$OUTDIR_RAW/clusters.txt.tmp"
  mv "$OUTDIR_RAW/clusters.txt.tmp" "$OUTDIR_RAW/clusters.txt"
fi
## Add # genes percentiles (gene complexity)
if [ -f "$COUNTDIR/library_genes_expressed_percentiles.txt" ]; then
  cat "$COUNTDIR/library_genes_expressed_percentiles.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_RAW/clusters.txt" - > "$OUTDIR_RAW/clusters.txt.tmp"
  mv "$OUTDIR_RAW/clusters.txt.tmp" "$OUTDIR_RAW/clusters.txt"
fi
cp "$OUTDIR_RAW/clusters.txt" "$OUTDIR_RAW_plus1/"

##    ______
##   | NORM |
##    ------
cp "$COUNTDIR/cell_cycle.txt" "$OUTDIR_NORM/clusters.txt"
cut -f 2 "$COUNTDIR/library_counts_percentiles_normalised.txt" | sed -r -e '1 s|above_percentile|library_counts_percentile|' | pr -Jtm "$OUTDIR_NORM/clusters.txt" - > "$OUTDIR_NORM/clusters.txt.tmp"
mv "$OUTDIR_NORM/clusters.txt.tmp" "$OUTDIR_NORM/clusters.txt"
## Add mt-RNA content (proportion)
if [ -f "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" ]; then
  head -1 "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" > "$OUTDIR_NORM/MT_counts_proportion_normalised_binned.txt"
  grep -w -f "$OUTDIR_NORM/.sample_filter.txt" "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" >> "$OUTDIR_NORM/MT_counts_proportion_normalised_binned.txt"
  cat "$OUTDIR_NORM/MT_counts_proportion_normalised_binned.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_NORM/clusters.txt" - > "$OUTDIR_NORM/clusters.txt.tmp"
  mv "$OUTDIR_NORM/clusters.txt.tmp" "$OUTDIR_NORM/clusters.txt"
fi
## Add # genes percentiles (gene complexity)
##  (filter samples in case any were removed during normalisation)
if [ -f "$COUNTDIR/library_genes_expressed_percentiles.txt" ]; then
  head -1 "$COUNTDIR/library_genes_expressed_percentiles.txt" > "$OUTDIR_NORM/library_genes_expressed_percentiles.txt"
  grep -w -f "$OUTDIR_NORM/.sample_filter.txt" "$COUNTDIR/library_genes_expressed_percentiles.txt" >> "$OUTDIR_NORM/library_genes_expressed_percentiles.txt"
  cat "$OUTDIR_NORM/library_genes_expressed_percentiles.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_NORM/clusters.txt" - > "$OUTDIR_NORM/clusters.txt.tmp"
  mv "$OUTDIR_NORM/clusters.txt.tmp" "$OUTDIR_NORM/clusters.txt"
fi
cp "$OUTDIR_NORM/clusters.txt" "$OUTDIR_NORM_plus1/"

##    _______________
##   | NORM FILTERED |
##    ---------------
cp "$COUNTDIR/cell_cycle_filtered.txt" "$OUTDIR_FILT/clusters.txt"
cut -f 2 "$COUNTDIR/library_counts_percentiles_filtered.txt" | sed -r -e '1 s|above_percentile|library_counts_percentile|' | pr -Jtm "$OUTDIR_FILT/clusters.txt" - > "$OUTDIR_FILT/clusters.txt.tmp"
mv "$OUTDIR_FILT/clusters.txt.tmp" "$OUTDIR_FILT/clusters.txt"
## Add mt-RNA content (proportion)
##  (filter samples in case any were removed during normalisation)
if [ -f "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" ]; then
  head -1 "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" > "$OUTDIR_FILT/MT_counts_proportion_normalised_binned.txt"
  grep -w -f "$OUTDIR_FILT/.sample_filter.txt" "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" >> "$OUTDIR_FILT/MT_counts_proportion_normalised_binned.txt"
  cat "$OUTDIR_FILT/MT_counts_proportion_normalised_binned.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_FILT/clusters.txt" - > "$OUTDIR_FILT/clusters.txt.tmp"
  mv "$OUTDIR_FILT/clusters.txt.tmp" "$OUTDIR_FILT/clusters.txt"
fi
## Add # genes percentiles (gene complexity)
##  (filter samples in case any were removed during normalisation)
if [ -f "$COUNTDIR/library_genes_expressed_percentiles.txt" ]; then
  head -1 "$COUNTDIR/library_genes_expressed_percentiles.txt" > "$OUTDIR_FILT/library_genes_expressed_percentiles.txt"
  grep -w -f "$OUTDIR_FILT/.sample_filter.txt" "$COUNTDIR/library_genes_expressed_percentiles.txt" >> "$OUTDIR_FILT/library_genes_expressed_percentiles.txt"
  cat "$OUTDIR_FILT/library_genes_expressed_percentiles.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_FILT/clusters.txt" - > "$OUTDIR_FILT/clusters.txt.tmp"
  mv "$OUTDIR_FILT/clusters.txt.tmp" "$OUTDIR_FILT/clusters.txt"
fi
cp "$OUTDIR_FILT/clusters.txt" "$OUTDIR_FILT_plus1/"

##    ___________________________
##   | FILTERED, OUTLIER REMOVED |
##    ---------------------------
if [ -d "$COUNTDIR_SANS_OUTLIERS" ]; then
  cp "$COUNTDIR/cell_cycle_filtered.txt" "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt"
  cut -f 2 "$COUNTDIR_SANS_OUTLIERS/library_counts_percentiles_filtered.txt" | sed -r -e '1 s|above_percentile|library_counts_percentile|' | pr -Jtm "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt" - > "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp"
  mv "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp" "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt"
  ## Add mt-RNA content (proportion)
  ##  (filter samples in case any were removed during normalisation)
  if [ -f "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" ]; then
    head -1 "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" > "$OUTDIR_FILT_SANS_OUTLIERS/MT_counts_proportion_normalised_binned.txt"
    grep -w -f "$OUTDIR_FILT_SANS_OUTLIERS/.sample_filter.txt" "$COUNTDIR/MT_counts_proportion_normalised_binned.txt"  >> "$OUTDIR_FILT_SANS_OUTLIERS/MT_counts_proportion_normalised_binned.txt"
    cat "$OUTDIR_FILT_SANS_OUTLIERS/MT_counts_proportion_normalised_binned.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt" - > "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp"
    mv "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp" "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt"
  fi
  ## Add # genes percentiles (gene complexity)
  ##  (filter samples in case any were removed during normalisation)
  if [ -f "$COUNTDIR/library_genes_expressed_percentiles.txt" ]; then
    head -1 "$COUNTDIR/library_genes_expressed_percentiles.txt" > "$OUTDIR_FILT_SANS_OUTLIERS/library_genes_expressed_percentiles.txt"
    grep -w -f "$OUTDIR_FILT_SANS_OUTLIERS/.sample_filter.txt" "$COUNTDIR/library_genes_expressed_percentiles.txt" >> "$OUTDIR_FILT_SANS_OUTLIERS/library_genes_expressed_percentiles.txt"
    cat "$OUTDIR_FILT_SANS_OUTLIERS/library_genes_expressed_percentiles.txt" | sed '1s|^|\t|' | cut -f 2 | pr -Jtm "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt" - > "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp"
    mv "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp" "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt"
  fi
  cp "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/"
fi


### START SKIP ###

## If existing, add columns other than SampleID, SampleType, ReadFile1 and ReadFile2 from targets_cells.txt
## NOTE: Skipping this section for now since we're not subsetting targets by samples actually involved at each stage
## TODO: Fix ASAP
if false; then
keepcols=`head -1 "$TARGETS" | tr "\t" "\n" | grep -nvP "(SampleID|SampleType|ReadFile[12])" | cut -d':' -f 1`
if [ ! -z "$keepcols" ]; then
  #echo "DEBUG: keepcols=[$keepcols]"
  keepcolnames=`head -1 "$TARGETS" | tr "\t" "\n" | grep -nvP "(SampleID|SampleType|ReadFile[12])" | cut -d':' -f 2`
  cutcols=`echo "$keepcols" | tr "\n" "," | sed 's|,$||'`
  #echo "DEBUG: cutcols=[$cutcols]"
  cut -f $cutcols "$TARGETS" > "$OUTDIR_RAW/clusters_extra.tmp"
  ##   - raw
  pr -Jtm "$OUTDIR_RAW/clusters.txt" "$OUTDIR_RAW/clusters_extra.tmp" > "$OUTDIR_RAW/clusters.txt.tmp"
  mv "$OUTDIR_RAW/clusters.txt.tmp" "$OUTDIR_RAW/clusters.txt"
  rm -f "$OUTDIR_RAW/clusters_extra.tmp"
  ##   - norm
  cp "$OUTDIR_RAW/clusters.txt" "$OUTDIR_NORM/clusters.txt"
  ##   - filtered
  cp "$OUTDIR_RAW/clusters.txt" "$OUTDIR_FILT/clusters.txt"
  ##   - filtered, sans outliers
  cut -f $cutcols "$TARGETS_FILT_SANS_OUTLIERS" > "$OUTDIR_FILT_SANS_OUTLIERS/clusters_extra.tmp"
  pr -Jtm "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt" "$OUTDIR_FILT_SANS_OUTLIERS/clusters_extra.tmp" > "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp"
  mv "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt.tmp" "$OUTDIR_FILT_SANS_OUTLIERS/clusters.txt"
  rm -f "$OUTDIR_FILT_SANS_OUTLIERS/clusters_extra.tmp"
fi
fi
### END SKIP ###
echo "done"

###############################################################################
#                              CLUSTER ITEMS                                  #
###############################################################################

/bin/echo -e -n "  clusterItems.txt\t\t\t..."
## default colour palette
## create "clusterItems.txt"
##    _____
##   | RAW |
##    -----
/bin/echo -e "cluster\titems\tcolours" > "$OUTDIR_RAW/clusterItems.txt"
## NOTE: cell cycle is always done on normalised data that might necessarily have had low complexity cells removed,
## so only use this for raw data if the sample count is the same
if [ $cellcycle_nrows -eq $raw_expression_ncols ]; then
  ## Colours from J.Choi's ISCANDAR example for cell cycle clustering
  ## G1,G2M,NA,S
  cell_cycle_colours="#1b9e77,#d95f02,#001122,#7570b3"
  #cell_cycle_vals=`tail -n+2 "$COUNTDIR/cell_cycle.txt" | cut -f 2 | sort -u | tr "\n" "," | sed -r -e 's|\,$||'`
  ## Override values to match with colours we want
  cell_cycle_vals="G1,G2M,NA,S"
  /bin/echo -e "cell_cycle\t${cell_cycle_vals}\t${cell_cycle_colours}" >> "$OUTDIR_RAW/clusterItems.txt"
  sed -i -r -e 's|NA|Unknown|g' "$OUTDIR_RAW/clusterItems.txt"
fi
## Library size colours
## Generated at: http://www.perbang.dk/rgbgradient/
lib_size_colours="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
lib_size_vals=`tail -n+2 "$COUNTDIR/library_counts_percentiles.txt" | cut -f 2 | sort -Vu | tr "\n" "," | sed -r -e 's|\,$||'`
/bin/echo -e "library_counts_percentile\t${lib_size_vals}\t${lib_size_colours}" >> "$OUTDIR_RAW/clusterItems.txt"
## Add MT pct vals
if [ -f "$COUNTDIR/MT_counts_proportion_binned.txt" ]; then
  mt_pct_vals="0-5%,5-10%,10-15%,15-20%,20-25%,25-30%,30-35%,35-40%,40-45%,45-50%,50-55%,55-60%,60-65%,65-70%,70-75%,75-80%,80-85%,85-90%,90-95%,95-100%"
  #mt_pct_cols="#15FD08,#4FFB06,#89F905,#89F905,#C3F804,#C3F804,#F6F003,#F6F003,#F4B302,#F4B302,#F37701,#F37701,#F13B00,#F13B00,#F13B00,#F13B00,#EF0000,#EF0000,#EF0000,#EF0000"
  mt_pct_cols="#08FDAB,#15FD08,#89F905,#C3F804,#F6F003,#F4B302,#F37701,#F13B00,#F13B00,#F42C00,#F42C00,#F42C00,#F42C00,#FB0F00,#FB0F00,#FB0F00,#EF0000,#EF0000,#EF0000,#EF0000"
  /bin/echo -e "MT_counts_percentage\t$mt_pct_vals\t$mt_pct_cols" >> "$OUTDIR_RAW/clusterItems.txt"
fi
## Add # genes percentiles
if [ -f "$COUNTDIR/library_genes_expressed_percentiles.txt" ]; then
  gene_pct_vals="0%,5%,10%,25%,50%,75%,90%,95%"
  gene_pct_cols="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
  /bin/echo -e "library_complexity_percentile\t$gene_pct_vals\t$gene_pct_cols" >> "$OUTDIR_RAW/clusterItems.txt"
fi
### START SKIP ###
if false; then
if [ ! -z "$keepcols" ]; then
  ## now add additional groups from targets
  for attrib in $keepcolnames
  do
    colind=`head -1 "$TARGETS" | tr "\t" "\n" | grep -nP "^$attrib$" | cut -d':' -f 1`
    values=`tail -n+2 "$TARGETS" | cut -f $colind | sort -u | tr "\n" "," | sed 's|\,$||'`
    /bin/echo -e "$attrib\t$values\t$colours" >> "$OUTDIR_RAW/clusterItems.txt"
  done
fi
fi
### END SKIP ###
cp "$OUTDIR_RAW/clusterItems.txt" "$OUTDIR_RAW_plus1/"

##    ______
##   | NORM |
##    ------
/bin/echo -e "cluster\titems\tcolours" > "$OUTDIR_NORM/clusterItems.txt"
## Colours from J.Choi's ISCANDAR example for cell cycle clustering
cell_cycle_colours="#1b9e77,#d95f02,#7570b3"
cell_cycle_vals=`tail -n+2 "$COUNTDIR/cell_cycle.txt" | cut -f 2 | sort -u | tr "\n" "," | sed -r -e 's|\,$||'`
/bin/echo -e "cell_cycle\t${cell_cycle_vals}\t${cell_cycle_colours}" >> "$OUTDIR_NORM/clusterItems.txt"
## Library size colours
## Generated at: http://www.perbang.dk/rgbgradient/
lib_size_colours="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
lib_size_vals=`tail -n+2 "$COUNTDIR/library_counts_percentiles_normalised.txt" | cut -f 2 | sort -Vu | tr "\n" "," | sed -r -e 's|\,$||'`
/bin/echo -e "library_counts_percentile\t${lib_size_vals}\t${lib_size_colours}" >> "$OUTDIR_NORM/clusterItems.txt"
## Add MT pct vals
if [ -f "$COUNTDIR/MT_counts_proportion_normalised_binned.txt" ]; then
  mt_pct_vals="0-5%,5-10%,10-15%,15-20%,20-25%,25-30%,30-35%,35-40%,40-45%,45-50%,50-55%,55-60%,60-65%,65-70%,70-75%,75-80%,80-85%,85-90%,90-95%,95-100%"
  #mt_pct_cols="#15FD08,#4FFB06,#89F905,#89F905,#C3F804,#C3F804,#F6F003,#F6F003,#F4B302,#F4B302,#F37701,#F37701,#F13B00,#F13B00,#F13B00,#F13B00,#EF0000,#EF0000,#EF0000,#EF0000"
  mt_pct_cols="#08FDAB,#15FD08,#89F905,#C3F804,#F6F003,#F4B302,#F37701,#F13B00,#F13B00,#F42C00,#F42C00,#F42C00,#F42C00,#FB0F00,#FB0F00,#FB0F00,#EF0000,#EF0000,#EF0000,#EF0000"
  /bin/echo -e "MT_counts_percentage\t$mt_pct_vals\t$mt_pct_cols" >> "$OUTDIR_NORM/clusterItems.txt"
fi
## Add # genes percentiles
if [ -f "$OUTDIR_NORM/library_genes_expressed_percentiles.txt" ]; then
  gene_pct_vals="0%,5%,10%,25%,50%,75%,90%,95%"
  gene_pct_cols="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
  /bin/echo -e "library_complexity_percentile\t$gene_pct_vals\t$gene_pct_cols" >> "$OUTDIR_NORM/clusterItems.txt"
fi
### START SKIP ###
if false; then
if [ ! -z "$keepcols" ]; then
  ## now add additional groups from targets
  for attrib in $keepcolnames
  do
    colind=`head -1 "$TARGETS_NORM" | tr "\t" "\n" | grep -nP "^$attrib$" | cut -d':' -f 1`
    values=`tail -n+2 "$TARGETS_NORM" | cut -f $colind | sort -u | tr "\n" "," | sed 's|\,$||'`
    /bin/echo -e "$attrib\t$values\t$colours" >> "$OUTDIR_NORM/clusterItems.txt"
  done
fi
fi
### END SKIP ###
cp "$OUTDIR_NORM/clusterItems.txt" "$OUTDIR_NORM_plus1/"

##    _______________
##   | NORM FILTERED |
##    ---------------
/bin/echo -e "cluster\titems\tcolours" > "$OUTDIR_FILT/clusterItems.txt"
## Colours from J.Choi's ISCANDAR example for cell cycle clustering
cell_cycle_colours="#1b9e77,#d95f02,#7570b3"
cell_cycle_vals=`tail -n+2 "$COUNTDIR/cell_cycle_filtered.txt" | cut -f 2 | sort -u | tr "\n" "," | sed -r -e 's|\,$||'`
/bin/echo -e "cell_cycle\t${cell_cycle_vals}\t${cell_cycle_colours}" >> "$OUTDIR_FILT/clusterItems.txt"
## Library size colours
## Generated at: http://www.perbang.dk/rgbgradient/
lib_size_colours="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
lib_size_vals=`tail -n+2 "$COUNTDIR/library_counts_percentiles_filtered.txt" | cut -f 2 | sort -Vu | tr "\n" "," | sed -r -e 's|\,$||'`
/bin/echo -e "library_counts_percentile\t${lib_size_vals}\t${lib_size_colours}" >> "$OUTDIR_FILT/clusterItems.txt"
## Add MT pct vals
if [ -f "$OUTDIR_FILT/MT_counts_proportion_normalised_binned.txt" ]; then
  mt_pct_vals="0-5%,5-10%,10-15%,15-20%,20-25%,25-30%,30-35%,35-40%,40-45%,45-50%,50-55%,55-60%,60-65%,65-70%,70-75%,75-80%,80-85%,85-90%,90-95%,95-100%"
  #mt_pct_cols="#15FD08,#4FFB06,#89F905,#89F905,#C3F804,#C3F804,#F6F003,#F6F003,#F4B302,#F4B302,#F37701,#F37701,#F13B00,#F13B00,#F13B00,#F13B00,#EF0000,#EF0000,#EF0000,#EF0000"
  mt_pct_cols="#08FDAB,#15FD08,#89F905,#C3F804,#F6F003,#F4B302,#F37701,#F13B00,#F13B00,#F42C00,#F42C00,#F42C00,#F42C00,#FB0F00,#FB0F00,#FB0F00,#EF0000,#EF0000,#EF0000,#EF0000"
  /bin/echo -e "MT_counts_percentage\t$mt_pct_vals\t$mt_pct_cols" >> "$OUTDIR_FILT/clusterItems.txt"
fi
## Add # genes percentiles
if [ -f "$OUTDIR_FILT/library_genes_expressed_percentiles.txt" ]; then
  gene_pct_vals="0%,5%,10%,25%,50%,75%,90%,95%"
  gene_pct_cols="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
  /bin/echo -e "library_complexity_percentile\t$gene_pct_vals\t$gene_pct_cols" >> "$OUTDIR_FILT/clusterItems.txt"
fi
### START SKIP ###
if false; then
if [ ! -z "$keepcols" ]; then
  ## now add additional groups from targets
  for attrib in $keepcolnames
  do
    colind=`head -1 "$TARGETS_FILT" | tr "\t" "\n" | grep -nP "^$attrib$" | cut -d':' -f 1`
    values=`tail -n+2 "$TARGETS_FILT" | cut -f $colind | sort -u | tr "\n" "," | sed 's|\,$||'`
    /bin/echo -e "$attrib\t$values\t$colours" >> "$OUTDIR_FILT/clusterItems.txt"
  done
fi
fi
### END SKIP ###
cp "$OUTDIR_FILT/clusterItems.txt" "$OUTDIR_FILT_plus1/"

##    ___________________________
##   | FILTERED, OUTLIER REMOVED |
##    ---------------------------
if [ -d "$COUNTDIR_SANS_OUTLIERS" ]; then
  /bin/echo -e "cluster\titems\tcolours" > "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt"
  cell_cycle_vals=`tail -n+2 "$COUNTDIR_SANS_OUTLIERS/cell_cycle_filtered.txt" | cut -f 2 | sort -u | tr "\n" "," | sed -r -e 's|\,$||'`
  /bin/echo -e "cell_cycle\t${cell_cycle_vals}\t${cell_cycle_colours}" >> "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt"
  lib_size_vals=`tail -n+2 "$COUNTDIR_SANS_OUTLIERS/library_counts_percentiles_filtered.txt" | cut -f 2 | sort -Vu | tr "\n" "," | sed -r -e 's|\,$||'`
  /bin/echo -e "library_counts_percentile\t${lib_size_vals}\t${lib_size_colours}" >> "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt"
  ## Add MT pct vals
  if [ -f "$OUTDIR_FILT_SANS_OUTLIERS/MT_counts_proportion_normalised_binned.txt" ]; then
    mt_pct_vals="0-5%,5-10%,10-15%,15-20%,20-25%,25-30%,30-35%,35-40%,40-45%,45-50%,50-55%,55-60%,60-65%,65-70%,70-75%,75-80%,80-85%,85-90%,90-95%,95-100%"
    #mt_pct_cols="#15FD08,#4FFB06,#89F905,#89F905,#C3F804,#C3F804,#F6F003,#F6F003,#F4B302,#F4B302,#F37701,#F37701,#F13B00,#F13B00,#F13B00,#F13B00,#EF0000,#EF0000,#EF0000,#EF0000"
    mt_pct_cols="#08FDAB,#15FD08,#89F905,#C3F804,#F6F003,#F4B302,#F37701,#F13B00,#F13B00,#F42C00,#F42C00,#F42C00,#F42C00,#FB0F00,#FB0F00,#FB0F00,#EF0000,#EF0000,#EF0000,#EF0000"
    /bin/echo -e "MT_counts_percentage\t$mt_pct_vals\t$mt_pct_cols" >> "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt"
  fi
  ## Add # genes percentiles
  if [ -f "$OUTDIR_FILT_SANS_OUTLIERS/library_genes_expressed_percentiles.txt" ]; then
    gene_pct_vals="0%,5%,10%,25%,50%,75%,90%,95%"
    gene_pct_cols="#30FCF8,#2AB8F9,#256DF7,#1F21F5,#6019F2,#A613F0,#EE0EED,#EC099F"
    /bin/echo -e "library_complexity_percentile\t$gene_pct_vals\t$gene_pct_cols" >> "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt"
  fi
  ### START SKIP ###
  if false; then
  if [ ! -z "$keepcols" ]; then
    ## now add additional groups from targets
    for attrib in $keepcolnames
    do
      colind=`head -1 "$TARGETS_FILT_SANS_OUTLIERS" | tr "\t" "\n" | grep -nP "^$attrib$" | cut -d':' -f 1`
      values=`tail -n+2 "$TARGETS_FILT_SANS_OUTLIERS" | cut -f $colind | sort -u | tr "\n" "," | sed 's|\,$||'`
      /bin/echo -e "$attrib\t$values\t$colours" >> "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt"
    done
  fi
  ### END SKIP ###
  cp "$OUTDIR_FILT_SANS_OUTLIERS/clusterItems.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/"
fi

fi
echo "done"


###############################################################################
#                              REPORT METADATA                                #
###############################################################################

/bin/echo -e -n "  analysisMetadata.txt\t\t\t..."
## create "analysisMetadata.txt"
##   - raw
/bin/echo -e "MDS_function\t(R) <a href='https://stat.ethz.ch/R-manual/R-devel/library/stats/html/cmdscale.html'>cmdscale</a> (Note this is actually MDS and not PCA)
TSNE_function\t(R) <a href='https://cran.r-project.org/web/packages/Rtsne/index.html'>Rtsne</a>
TSNE_parameters\tperplexity=$PERPLEXITY (roughly the number of neighbours for each point),<br/> n_iter=1000 (number of iterations),<br/> 
Notes\tNA
gene sets\tNA
genes included\tNA" > "$OUTDIR_RAW/analysisMetadata.txt"
##   - norm
cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_RAW_plus1/"
cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_NORM/"
cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_NORM_plus1/"
##   - filtered
cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_FILT/"
cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_FILT_plus1/"
##   - filtered, sans outliers
if [ -d "$COUNTDIR_SANS_OUTLIERS" ]; then
  cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_FILT_SANS_OUTLIERS/"
fi
if [ -d  "$COUNTDIR_SANS_OUTLIERS_plus1" ]; then
  cp "$OUTDIR_RAW/analysisMetadata.txt" "$OUTDIR_FILT_SANS_OUTLIERS_plus1/"
fi
echo "done"

/bin/echo -e -n "  metadata.txt\t\t\t\t..."
## create "metadata.txt"
##   - raw
echo "name	Dataset `basename $DSDIR` (Raw log2 CPM)
description	Raw, unfiltered
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe.
QC notes	NA" > "$OUTDIR_RAW/metadata.txt"
##   - raw CPM+1
echo "name	Dataset `basename $DSDIR` (Raw log2 CPM+1)
description	Raw, unfiltered
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe.
QC notes	NA" > "$OUTDIR_RAW_plus1/metadata.txt"
##   - norm
echo "name	Dataset `basename $DSDIR` (Normalised log2 CPM)
description	Normalised (scater/scran), some zero read samples and problematic low complexity samples may have been auto-removed
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe.
QC notes	NA" > "$OUTDIR_NORM/metadata.txt"
##   - norm CPM+1
echo "name	Dataset `basename $DSDIR` (Normalised log2 CPM+1)
description	Normalised (scater/scran), some zero read samples and problematic low complexity samples may have been auto-removed
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe.
QC notes	NA" > "$OUTDIR_NORM_plus1/metadata.txt"
##   - filtered
echo "name	Dataset `basename $DSDIR` (Normalised log2 CPM)
description	Normalised (scater/scran), gene filtered (retain only genes having 1 or more counts in at least 10% of phenotype). Some zero read samples and problematic low complexity samples may have been auto-removed.
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe.
QC notes	NA" > "$OUTDIR_FILT/metadata.txt"
##   - filtered CPM+1
echo "name	Dataset `basename $DSDIR` (Normalised log2 CPM+1)
description	Normalised (scater/scran), gene filtered (retain only genes having 1 or more counts in at least 10% of phenotype). Some zero read samples and problematic low complexity samples may have been auto-removed.
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe.
QC notes	NA" > "$OUTDIR_FILT_plus1/metadata.txt"

##   - filtered, sans outliers
if [ -d "$COUNTDIR_SANS_OUTLIERS" ]; then
  input_sampleN=`tail -n+2 $TARGETS | wc -l`
  echo "name	Dataset `basename $DSDIR` (Normalised log2 CPM)
description	Normalised (scater/scran), gene filtered (retain only genes having 1 or more counts in at least 10% of phenotype). Some zero read samples and problematic low complexity samples may have been auto-removed. Outliers removed by user-supplied gene/library size thresholding ($filtered_sampleN of $input_sampleN samples remain).
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe. Samples were filtered (outliers removed) by gene/library size count thresholds.
QC notes	NA" > "$OUTDIR_FILT_SANS_OUTLIERS/metadata.txt"
  ##   - filtered, sans outliers CPM+1
  echo "name	Dataset `basename $DSDIR` (Normalised log2 CPM+1)
description	Normalised (scater/scran), gene filtered (retain only genes having 1 or more counts in at least 10% of phenotype). Some zero read samples and problematic low complexity samples may have been auto-removed. Outliers removed by user-supplied gene/library size thresholding ($filtered_sampleN of $input_sampleN samples remain).
read mapping summary	Reads were aligned using R/Subread and genes were counted using scPipe. Samples were filtered (outliers removed) by gene/library size count thresholds.
QC notes	NA" > "$OUTDIR_FILT_SANS_OUTLIERS_plus1/metadata.txt"
fi
echo "done"

