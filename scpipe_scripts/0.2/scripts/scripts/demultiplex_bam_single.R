## ============================================================================
## File:    demultiplex_bam_single.R
## Author:  O.Korn
## Created: 2017-11-09
## Updated: 2018-03-30 (Creating subdir output based on input BAM name, single row input targets)
##
## Demultiplex reads from UMI/barcode encoded BAMs
## ============================================================================

source("./inc/util.R")

library(limma)
library(scPipe)

## Path to directory containing BAM files
inputdir <- ARGV.get("inputdir", required=T)
## Path to output directory (parent path - subfolders will be created)
outdir <- ARGV.get("outdir", required=T)
## Path to targets file with sample names for BAM input and output
## NOTE: Assumes 1 row only!
targets_file <- ARGV.get("targets", required=T)
## Path to cell -> barcode CSV
barcode_csv <- ARGV.get("barcode_csv", required=T)

if (! file.exists(barcode_csv)) {
  stop(paste("Barcode CSV '",barcode_csv,"' not found!",sep=""))
}
## Targets file usage as per example:
## http://bioinf.wehi.edu.au/RNAseqCaseStudy/
##
## Must have columns: 'SampleID', 'ReadFile1', and if requiring paired-end, 'ReadFile2'
targets <- readTargets(targets_file)

sampleid <- targets$SampleID[1]

writeLines(paste("Demultiplexing BAM '",sampleid,"'",sep=""))

sampleoutdir <- paste(outdir,"/",sampleid,sep="") 

if (! dir.exists(sampleoutdir)) {
  dir.create(sampleoutdir)
}

sc_demultiplex(
    inbam=paste(inputdir,"/",sampleid,".bam",sep=""),
    outdir=sampleoutdir,
    bc_anno=barcode_csv
)
writeLines("  done")
