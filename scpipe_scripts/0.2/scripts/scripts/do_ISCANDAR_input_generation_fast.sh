#!/bin/sh
DSDIR="$1"
if [ ! -d "$DSDIR" ]; then
  echo "Error: Dataset directory '$DSDIR' not found!" 1>&2
  exit 1
fi
GENOME="$2"
if [ -z "$GENOME" ]; then
  echo "Error: Genome not given!" 1>&2
  exit 1
fi
COUNTSDIR="$3"
if [ -z "$COUNTSDIR" ]; then
  echo "Error: Counts directory not given (usually 'counts' or sometimes 'normalised')!" 1>&2
  exit 1
fi
NCPU="$4"
if [ -z "$NCPU" ]; then
  ## Minimum required CPU (raw, norm, norm filtered, sans outliers are run in parallel)
  NCPU=4
fi


echo "Generating t-SNE and PCA (RSVD) data.."
date
## Generate t-SNE and PCA data (post-QC and ISCANDAR inputs)
## Does a range of t-SNE valuse e.g. 20, 50, 100
##
## NOTE: Requires at least 4 CPUs (raw, norm, norm filtered, norm filtered sans outliers)
##
## D#7302: 2019-01-21: Look for input data in the correct place -Othmar
## D#7302: 2019-01-23: Testing RSVD implementation of faster PCAs for large matrices
## D#7302: 2019-01-30: Using irlba (fast PCA) implementation in rtsne, with num_threads

./ISCANDAR/generate_tSNE_PCA_data_fast.sh "$DSDIR" "$GENOME" "$COUNTSDIR" "$NCPU"
if [ $? -ne 0 ]; then
  echo "Error: Script './ISCANDAR/generate_tSNE_PCA_data_fast.sh' exited with non-zero status!"
  exit 1
fi
echo "Finished t-SNE and PCA data generation"
date

