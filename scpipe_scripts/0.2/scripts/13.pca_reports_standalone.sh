#!/bin/sh
DSID=XXXX
DSDIR=/path/to/data/datasets/$DSID
PROCDIR="$DSDIR/source/custom"
COUNTDIR="$PROCDIR/counts"
## It's okay if this doesn't exist, it will be skipped
COUNTDIR_SANS="$PROCDIR/counts_sans_outliers"

## Load subread module env for R to generate PCA data
MINICONDA_HOME=~/.s4m/moduledata/miniconda/miniconda2
CONDA_ENV=subread_83900493ea17ab5ec4f0a4ece422cf98
source $MINICONDA_HOME/bin/activate $CONDA_ENV
SUBREAD=/path/to/data/repo/git-working/s4m_modules/subread/0.14

PCAOUT="$PROCDIR/PCA"
mkdir -p "$PCAOUT"


###########
### RAW ###
###########

inputfile="$COUNTDIR/gene_count_log2_CPM+1.txt"
fbase=`basename $inputfile .txt`
## Must be inside module directory for R includes to be found
cd $SUBREAD
R --slave -f $SUBREAD/scripts/subread_PCA.R --args -input=$inputfile -output="$PCAOUT/$fbase.png" -title="Raw log2(CPM+1)"
if [ $? -ne 0 ]; then
  echo "Error: Script '$SUBREAD/scripts/subread_PCA.R' exited with non-zero status!"
  cd $OLDPWD
  exit 1
fi
cd $OLDPWD

PCANAME="raw_log2_CPM+1"
PCATSV="$PCAOUT/${fbase}_sample_data.tsv"
if [ ! -f "$PCATSV" ]; then
  echo "Error: Required PCA data output '$PCATSV' not found!"
  exit 1
fi
## make header correct for pca_reports module
sed -i -r -e '1 s|^|SampleID|' "$PCATSV"
SCREETABLE="$PCAOUT/${fbase}_screetable_prcomp_variance.tsv"
outdir="$PCAOUT"
mkdir -p "$outdir"
METATSV="$PCAOUT/metadata_$PCANAME.tsv"
## make PCA metadata TSV by using clustering metadata from normalisation
sed -r -e '1s|^.*|SampleID\tSampleType|' "$PROCDIR/post_qc/raw/samples.txt" > "$METATSV.tmp"
sed -r -e '1s|^|\t|' "$PROCDIR/post_qc/raw/clusters.txt" | cut -f 2- | pr -Jtm "$METATSV.tmp" - > "$METATSV"
rm -f "$METATSV.tmp"

s4m pca_reports::pca-html -p "$PCATSV" -s "$SCREETABLE" -m "$METATSV" -o "$outdir" -n "$PCANAME"
if [ $? -ne 0 ]; then
  echo "Error: 'pca-html' returned non-zero status!"
  exit 1
fi

##################
### NORMALISED ###
##################

inputfile="$COUNTDIR/gene_count_normalised_log2_CPM+1.txt"
fbase=`basename $inputfile .txt`
cd $SUBREAD
R --slave -f $SUBREAD/scripts/subread_PCA.R --args -input=$inputfile -output="$PCAOUT/$fbase.png" -title="Normalised log2(CPM+1)"
if [ $? -ne 0 ]; then
  echo "Error: Script '$SUBREAD/scripts/subread_PCA.R' exited with non-zero status!"
  cd $OLDPWD
  exit 1
fi
cd $OLDPWD

PCANAME="norm_log2_CPM+1"
PCATSV="$PCAOUT/${fbase}_sample_data.tsv"
if [ ! -f "$PCATSV" ]; then
  echo "Error: Required PCA data output '$PCATSV' not found!"
  exit 1
fi
## make header correct for pca_reports module
sed -i -r -e '1 s|^|SampleID|' "$PCATSV"
SCREETABLE="$PCAOUT/${fbase}_screetable_prcomp_variance.tsv"
outdir="$PCAOUT"
mkdir -p "$outdir"
METATSV="$PCAOUT/metadata_$PCANAME.tsv"
## make PCA metadata TSV by using clustering metadata from normalisation
sed -r -e '1s|^.*|SampleID\tSampleType|' "$PROCDIR/post_qc/normalized/samples.txt" > "$METATSV.tmp"
sed -r -e '1s|^|\t|' "$PROCDIR/post_qc/normalized/clusters.txt" | cut -f 2- | pr -Jtm "$METATSV.tmp" - > "$METATSV"
rm -f "$METATSV.tmp"

s4m pca_reports::pca-html -p "$PCATSV" -s "$SCREETABLE" -m "$METATSV" -o "$outdir" -n "$PCANAME"
if [ $? -ne 0 ]; then
  echo "Error: 'pca-html' returned non-zero status!"
  exit 1
fi

###########################
### NORMALISED FILTERED ###
###########################

inputfile="$COUNTDIR/gene_count_normalised_filtered_log2_CPM+1.txt"
fbase=`basename $inputfile .txt`
cd $SUBREAD
R --slave -f $SUBREAD/scripts/subread_PCA.R --args -input=$inputfile -output="$PCAOUT/$fbase.png" -title="Normalised, gene filtered log2(CPM+1)"
if [ $? -ne 0 ]; then
  echo "Error: Script '$SUBREAD/scripts/subread_PCA.R' exited with non-zero status!"
  cd $OLDPWD
  exit 1
fi
cd $OLDPWD

PCANAME="norm_filt_log2_CPM+1"
PCATSV="$PCAOUT/${fbase}_sample_data.tsv"
if [ ! -f "$PCATSV" ]; then
  echo "Error: Required PCA data output '$PCATSV' not found!"
  exit 1
fi
## make header correct for pca_reports module
sed -i -r -e '1 s|^|SampleID|' "$PCATSV"
SCREETABLE="$PCAOUT/${fbase}_screetable_prcomp_variance.tsv"
outdir="$PCAOUT"
mkdir -p "$outdir"
METATSV="$PCAOUT/metadata_$PCANAME.tsv"
## make PCA metadata TSV by using clustering metadata from normalisation
## (Note: Using same inputs as "normalised" [no samples removed])
sed -r -e '1s|^.*|SampleID\tSampleType|' "$PROCDIR/post_qc/normalized/samples.txt" > "$METATSV.tmp"
sed -r -e '1s|^|\t|' "$PROCDIR/post_qc/normalized/clusters.txt" | cut -f 2- | pr -Jtm "$METATSV.tmp" - > "$METATSV"
rm -f "$METATSV.tmp"

finalise=""
if [ ! -d "$COUNTDIR_SANS" ]; then
  finalise="-f"
fi
s4m pca_reports::pca-html -p "$PCATSV" -s "$SCREETABLE" -m "$METATSV" -o "$outdir" -n "$PCANAME" $finalise
if [ $? -ne 0 ]; then
  echo "Error: 'pca-html' returned non-zero status!"
  exit 1
fi

#########################################
### NORMALISED FILTERED SANS OUTLIERS ###
#########################################

if [ -d "$COUNTDIR_SANS" ]; then
  inputfile="$COUNTDIR_SANS/gene_count_normalised_filtered_log2_CPM+1.txt"
  fbase=`basename $inputfile .txt`
  cd $SUBREAD
  R --slave -f $SUBREAD/scripts/subread_PCA.R --args -input=$inputfile -output="$PCAOUT/$fbase.png" -title="Normalised, gene filtered, sans outliers log2(CPM+1)"
  if [ $? -ne 0 ]; then
    echo "Error: Script '$SUBREAD/scripts/subread_PCA.R' exited with non-zero status!"
    cd $OLDPWD
    exit 1
  fi
  cd $OLDPWD

  PCANAME="norm_sans_outliers_log2_CPM+1"
  PCATSV="$PCAOUT/${fbase}_sample_data.tsv"
  if [ ! -f "$PCATSV" ]; then
    echo "Error: Required PCA data output '$PCATSV' not found!"
    exit 1
  fi
  ## make header correct for pca_reports module
  sed -i -r -e '1 s|^|SampleID|' "$PCATSV"
  SCREETABLE="$PCAOUT/${fbase}_screetable_prcomp_variance.tsv"
  outdir="$PCAOUT"
  mkdir -p "$outdir"
  METATSV="$PCAOUT/metadata_$PCANAME.tsv"
  ## make PCA metadata TSV by using clustering metadata from normalisation
  ## (Note: Using same inputs as "normalised" [no samples removed])
  sed -r -e '1s|^.*|SampleID\tSampleType|' "$PROCDIR/post_qc/normalized_filtered_sans_outliers/samples.txt" > "$METATSV.tmp"
  sed -r -e '1s|^|\t|' "$PROCDIR/post_qc/normalized_filtered_sans_outliers/clusters.txt" | cut -f 2- | pr -Jtm "$METATSV.tmp" - > "$METATSV"
  rm -f "$METATSV.tmp"

  ## This is the final one, add finalisation flag
  s4m pca_reports::pca-html -p "$PCATSV" -s "$SCREETABLE" -m "$METATSV" -o "$outdir" -n "$PCANAME" -f
  if [ $? -ne 0 ]; then
    echo "Error: 'pca-html' returned non-zero status!"
    exit 1
  fi
fi

