#!/bin/sh
## Generate ISCANDAR reports and all prerequisite inputs prior

DSID="XXXX"
DSDIR=/path/to/data/datasets/$DSID
MINICONDA_HOME=~/.s4m/moduledata/miniconda/miniconda2

## Pull Iscandar repo first since we don't have Internet access on nodes
mkdir -p $DSDIR/source/custom
cd $DSDIR/source/custom
ISCANDAR_OLDPWD=$OLDPWD

if [ ! -d iscandar ]; then
  echo "ISCANDAR source not present, fetching now.."
  git clone https://github.com/jarny/iscandar.git
  ## Create environment "iscandar" - python 3.6
  ## Will fail gracefully if environment already exists
  stderr=`cd iscandar && $MINICONDA_HOME/bin/conda env create -f environment_py36.yml 2>&1`
  ret=$?
  if [ $ret -ne 0 ]; then
    echo "$stderr" | grep "already exists" > /dev/null
    ## Exit on any error that is not "already exists" 
    if [ $? -ne 0 ]; then
      echo "Error: Failed to setup ISCANDAR environment!"
      exit 1
    fi
  fi

else
  ## update repo
  echo; echo "Updating Iscandar from Git.."
  (cd iscandar && git checkout master && git pull)
  ## latest commit
  echo; echo "Git HEAD (master):"
  (cd iscandar && git log -1)
fi

cd $ISCANDAR_OLDPWD

qsub ISCANDAR_pre_reports.pbs
exit $?
