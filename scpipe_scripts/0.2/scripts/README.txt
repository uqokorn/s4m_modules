
0. Set up required Conda environment

	s4m install scpipe_scripts/0.1

1. Copy all top-level PBS, shell and scripts subdirectories to Stemformatics dataset

2. Replace all "XXXX" in PBS and shell scripts with actual DSID e.g.

	sed -i -r -e 's|XXXX|9999|g' *pbs *sh

3. Edit the dataset "processed" output path in "scrnaseq_subread_scpipe_HUMAN.pipeline"
   (OR "scrnaseq_subread_scpipe_MOUSE.pipeline" for mouse)

4. Replace GFF, GTF and other genome references with actual files/paths/references

	sed -i -r -e 's|%GENOME%|hg38.91|g' *pbs *sh

5. Symlink the correct Mouse or Human pipeline files (NOTE: Human = v91, Mouse = v91)
   i.e.
	ln -s scrnaseq_subread_scpipe_HUMAN.pipeline scrnaseq_subread_scpipe.pipeline     OR
	ln -s scrnaseq_subread_scpipe_MOUSE.pipeline scrnaseq_subread_scpipe.pipeline

6. Inspect and submit the scripts in order. Shell scripts are intended to be run on headnode
   (NOTE: Do not submit "ISCANDAR_pre_reports.pbs" as it is auto-submitted by another script)


Required input metadata files in source/raw/:
=============================================

sc_barcodes.csv:
	This is the *global* superset mapping of all cells to barcodes in the experiment.
	Header: "cell_id,barcode"
	Rows: Unique Cell ID and corresponding barcode

sc_barcodes_XXX.csv:
	As above, but subset of cells and barcodes for ONE LANE-MERGED FASTQ LIBRARY (e.g. if multiple plates with reused barcodes are present in experiment)
	Create as many of these as needed (i.e. one per input FASTQ set). NOTE: Replace "XXX" with a short ID that ties to a given FASTQ pair.

targets.txt:
	One row PER FASTQ pair. Must have fields: Sample ID <TAB> BarcodeCSV <TAB> ReadFile1 <TAB> ReadFile2
	The "BarcodeCSV" field receives the name of the *subset* barcode CSV that corresponds to it (see above).

targets_cells.txt:
	This targets provides per-cell phenotypes.
	The Cell IDs should match those in "sc_barcodes.csv"
	Fields: SampleID <TAB> SampleType

targets_cells.txt.multiqc:
	As above, but where phenotype info is encoded into SampleID for MultiQC plotting

