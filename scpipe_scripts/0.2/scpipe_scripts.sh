#!/bin/sh
# Module: scpipe_scripts
# Author: O.Korn

# Include S4M functions (mandatory unless you're doing a "Hello world" module
# and don't need any API functions)
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

### Your code here ###
main () {

  :

  ## Import other module functions etc  
  #s4m_import othermodule/script.sh

  ## Route command(s) from module.ini to custom shell functions defined in this
  ## shell script or included by this shell script.
  ##
  ## Route order does not matter - either one will execute or none will.
  ##
  ## This script will auto-exit immediately after a route is executed because
  ## you can only run a single command at a time.
  ##
  ## This is the recommended way to map commands to shell wrapper functions
  ## to do your work.
  ##
  #s4m_route mycommand my_function
  #s4m_route anothercommand another_function

  ## .. OR remove the above lines and implement your own logic flow
  ## based on the command name in variable  $S4M_MOD_CMD

  ## Options (flags) specified in module.ini are captured and available to you
  ## by accessing variables named like  $<module_name>_<longflagname>
  ##
  ## You can also access your full shell arguments in variable  $S4M_MOD_OPTS
  ## to do as you please, if you prefer.
  ##
}

### START ###
main

