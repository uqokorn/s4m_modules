##=============================================================================
## Script:	read_size_percentiles_from_mapstats.R	
##
## Description:	Output binned library size and mapped read count %
##
## Author:	O.Korn
##
## Created:	2018-02-09
##=============================================================================

source("./scripts/inc/util.R")

Usage <- function() {
   writeLines("Usage:  R -f <script_name.R> --args -input=<mapstats.txt> -output=<read_size_percentiles.txt>\n\n")
   writeLines("  (NOTE: 'mapstats.txt' as output by s4m::samtools module)\n")
}

## Expecting input data frame (CPM)
inputfile <- ARGV.get("input",T)
outputfile <- ARGV.get("output",T)

output_path <- dirname(outputfile)
fbase <- sub(".txt", "", basename(outputfile))

## Load mapstats
dat <- read.table(as.character(inputfile), sep="\t", header=T, row.names=1, na.strings="", quote="", check.names=F, as.is=T)

if (! "Total Reads" %in% colnames(dat)) {
  stop("Input mapstats missing column 'Total Reads'!")
}
if (! "Mapped Reads" %in% colnames(dat)) {
  stop("Input mapstats missing column 'Mapped Reads'!")
}

percentiles_total <- round(quantile(dat[,"Total Reads"], c(0,0.05,0.1,0.25,0.5,0.75,0.9,0.95)))
pct_ind <- lapply(dat[,"Total Reads"], function(x) { max(which(x >= percentiles_total)) })
pct_labels <- lapply(pct_ind, function(x) { names(percentiles_total)[x] })
pct_tot_table <- as.matrix(pct_labels)

percentiles_mapped <- round(quantile(dat[,"Mapped Reads"], c(0,0.05,0.1,0.25,0.5,0.75,0.9,0.95)))
pct_ind <- lapply(dat[,"Mapped Reads"], function(x) { max(which(x >= percentiles_mapped)) })
pct_labels <- lapply(pct_ind, function(x) { names(percentiles_mapped)[x] })

pct_table <- cbind(pct_tot_table, pct_labels)
row.names(pct_table) <- row.names(dat)
colnames(pct_table) <- c("total_libsize_percentile","mapped_libsize_percentile")

write.table(pct_table, file=outputfile, sep="\t", quote=F)

## Example output (2 column table, sample IDs in column 1):
## 	total_libsize_percentile	mapped_libsize_percentile
## A10	50%	50%
## A11	50%	50%
## A12	25%	10%
## A13	5%	0%
## A14	5%	0%
## A15	25%	25%
## ...

