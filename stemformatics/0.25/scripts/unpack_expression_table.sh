#!/bin/sh
##===========================================================================
## Script: unpack_expression_table.sh
## Author: Othmar Korn
##
## "Unwind" an expression table to long format.
##===========================================================================
inputfile="$1"
outputfile="$2"
tmpdir="$3"

if [ -z "$tmpdir" ]; then
  tmpdir=`dirname "$outputfile"`
fi
if [ -z "$inputfile" ]; then
  echo "$0: Error: Must provide name of input file containing expression table!" 1>&2; echo 1>&2
  exit 1
fi
if [ -z "$outputfile" ]; then
  echo "$0: Error: Must provide name of output unpacked expression file!" 1>&2; echo 1>&2
  exit 1
fi

inputfile_org="$inputfile"
REPLACED="false"
## If input table contains blank values, temporarily replace them with "%%" for the Perl
## script to work as intended
grep -P '\t\t' "$inputfile" > /dev/null 2>&1
if [ $? -eq 0 ]; then
  REPLACED="true"
  fbase=`basename "$inputfile"`
  cp "$inputfile" "$tmpdir/$fbase.$$"
  sed -i -e ':a; s/\t\t/\t%%\t/g; ta;' -e 's/\t$/\t%%/' "$tmpdir/$fbase.$$"
  inputfile="$tmpdir/$fbase.$$"
fi

SCRIPTDIR=`dirname $0`
## The Perl script does the guts of the work
$SCRIPTDIR/inc/unpack_expression_table.pl "$inputfile" > "$outputfile" 2> "$outputfile.err"

## Replace %% with blanks
if [ $REPLACED = "true" ]; then
  rm -f "$tmpdir/$fbase.$$"
  sed -i 's/%%//g' "$outputfile"
  inputfile="$inputfile_org"
fi
## sort command needs to use our custom temp directory otherwise uses /tmp or $TMPDIR from environment
sort -T "$tmpdir" "$outputfile" > "$outputfile.sorted"
mv "$outputfile.sorted" "$outputfile"
if [ "$?" -ne 0 ]; then
  echo "$0: Error: Failed to unpack expression matrix, exiting.." 1>&2; echo 1>&2
  exit 1
fi

## Check that file dimensions look right
## Expecting number of rows = input expression table nCols * (nRows - 1)


echo "Row count validation ($outputfile):"

table_rows=`wc -l $inputfile | awk '{print $1}'`
table_cols=`head -1 $inputfile | sed -r -e 's/^\s+//g' | awk -F'\t' '{print NF}'`

## DEBUG
#echo "table_rows=[$table_rows]"
#echo "table_cols=[$table_cols]"; echo
## END

expected_rows=`expr \( $table_rows - 1 \) \* $table_cols`
output_rows=`wc -l $outputfile | awk '{print $1}'`

## We had some skipped probes, recalculate expected unpacked row count
if [ -s "$outputfile.err" ]; then
  numskipped=`grep "Skipped feature" "$outputfile.err" | wc -l`
  new_expected_rows=`expr \( $table_rows - 1 - $numskipped \) \* $table_cols`
fi

if [ $output_rows -ne $expected_rows ]; then
  if [ ! -z "$new_expected_rows" ]; then
    if [ $output_rows -ne $new_expected_rows ]; then
      echo "[ FAIL ]"
      echo "$0: Error: Expected $new_expected_rows rows in output '$outputfile' but got $output_rows!" 1>&2
      echo "There were $numskipped skipped features due to bad field vs. sample count, but even so, we still don't get what we expected." 1>&2; echo 1>&2
      exit 1
    fi
  else
    echo "[ FAIL ]"
    echo "$0: Error: Expected $expected_rows rows in output '$outputfile' but got $output_rows!" 1>&2; echo 1>&2
    exit 1
  fi
fi

echo "[ PASS ]"
if [ ! -z "$numskipped" ]; then
  echo "(With $numskipped bad features - see: '$outputfile.err' for more details.)"
  sleep 3
fi
