#!/bin/sh
## This script makes a best guess of read header format from input FASTQ pair (R1, R2 files)
fastq1="$1"
fastq2="$2"
## If non null, debug output is enabled
do_debug="$3"

if [ ! -f "$fastq1" ]; then
  echo "$0: Error: Failed to find R1 FASTQ file '$fastq1'!" 1>&2
  exit 1
fi
if [ ! -f "$fastq2" ]; then
  echo "$0: Error: Failed to find R2 FASTQ file '$fastq2'!" 1>&2
  exit 1
fi

fq1_read1=`head -1 "$fastq1" | grep -P "^\@"`
ret1=$?
fq2_read1=`head -1 "$fastq2" | grep -P "^\@"`
ret2=$?
if [ $ret1 -ne 0 -o $ret2 -ne 0 ]; then
  echo "Error: Malformed read ID/s in R1 and/or R2 input (lines not starting with '@')!" 1>&2
  exit 1
fi

## ============================================================================
##                    PAIRED END READ ID FORMAT TESTS
## ============================================================================

## [1] Read IDs the same
## First read pair has identical read name - return blank string
if [ "$fq1_read1" = "$fq2_read1" ]; then
  if [ ! -z "$do_debug" ]; then
    echo "DEBUG: fq1_read1 == fq2_read2" 1>&2
  fi
  echo ""
  exit
fi

## [2] Most common format with changing string ".:N:0"
## Test for ".:N:0" changing regex e.g. R1 == "^@.* 1:N:0"  and R2 == "^@.* 2:N:0"
echo "$fq1_read1" | grep -P " 1\:N\:0" > /dev/null 2>&1
ret1=$?
echo "$fq2_read1" | grep -P " 2\:N\:0" > /dev/null 2>&1
ret2=$?
if [ $ret1 -eq 0 -a $ret2 -eq 0 ]; then
  echo ".:N:0"
  exit
fi

## [3] Least common format with changing string "/."
## Test for "/." changing regex e.g. R1 == "^@.*/1"  and R2 == "^@.*/2"
echo "$fq1_read1" | grep -P "\/1\s*" > /dev/null 2>&1
ret1=$?
echo "$fq2_read1" | grep -P "\/2\s*" > /dev/null 2>&1
ret2=$?
if [ $ret1 -eq 0 -a $ret2 -eq 0 ]; then
  echo "\/."
  exit
fi

## [*] Null result - unknown format
## No output, return non-zero to indicate that no regex guess is returned
if [ ! -z "$do_debug" ]; then
  echo "DEBUG: No paired end changing regex detected, exiting" 1>&2
fi
exit 1
