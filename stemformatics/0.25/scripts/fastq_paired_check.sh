#!/bin/sh
fastq1="$1"
fastq2="$2"
## The regex pattern which includes the changing char/s. The rest of the string must be identical.
## e.g. regex pattern ".:N:0" matches on paired IDs containing "1:N:0" and "2:N:0"
## Leave blank if there are NO changing elements in paired IDs.
pattern="$3"

echo "
LHS=[$fastq1]
RHS=[$fastq2]
pattern=[$pattern]
"

paste "$fastq1" "$fastq2" | awk -F '\t' -v pattern="$pattern" -v strict=true 'BEGIN {
  LINECOUNT=0
}
## We process the first and every 4 lines thereafter where these correspond to read IDs
NR%4==1 {
  ## Only check that read IDs start with "@" in strict mode
  ## (otherwise adds about 30% processing time to run time)
  if (strict == "true") {
    if ($0 !~ /^@[A-Z]+/) {
      print "Error: Malformed FASTQ? Line " NR " not a valid read ID?" | "cat 1>&2"
      exit 1
    }
  }
  LINECOUNT++
  ## No string defines the read pair - both lines therefore must be IDENTICAL
  if (pattern == "") {
    if ($1 != $2) {
      print "Error: Read IDs do not match on line " NR "!" | "cat 1>&2"
      print "LHS: ["$1"]"
      print "RHS: ["$2"]"
      exit 1
    }
  ## Only the "%" part/s of pattern should change i.e. typically "1" vs "2"
  } else {
    split($1,array1,pattern)
    split($2,array2,pattern)
    if ( array1[1]array1[2] == array2[1]array2[2] ) {
      next
    } else {
      print "WARNING: Paired read IDs NOT matching LHS or RHS of input pattern marked by ***:\n" array1[1] "***" array1[2] " == " array2[1] "***" array2[2] " on line " NR "!" | "cat 1>&2"
      exit 1
    }
  }
}
END {
  print "Processed " LINECOUNT " read ID pairs (check reported read count vs expected in case of any issues)"
}'
