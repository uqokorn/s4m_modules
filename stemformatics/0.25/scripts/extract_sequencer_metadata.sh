#!/bin/sh
## Script: extract_sequencer_metadata.sh
## Author: O.Korn
##
## Desc:   Extract metadata from FASTQ listed in targets (ReadFile1 only)
##
## Return: SampleID\tMachineID\tRunID\t... (STDOUT)
##
## All errors printed to STDERR (and exit status > 0 in that case)
##
fastqdir="$1"
if [ ! -d "$fastqdir" ]; then
  echo "$0: Error: FASTQ directory '$fastqdir' not found!" 1>&2
  exit 1
fi
targets="$2"
if [ ! -f "$targets" ]; then
  echo "$0: Error: Targets file '$targets' not found!" 1>&2
  exit 1
fi

rf1_ind=`head -1 "$targets" | tr "\t" "\n" | grep -n ReadFile1 | cut -d':' -f 1`
echo "$rf1_ind" | grep -P "^[0-9]+\s*$" > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "$0: Error: Missing 'ReadFile1' column in input targets!" 1>&2
  exit 1
fi

/bin/echo -e "SampleID\tMachineID\tRunID\tFlowCellID"
tail -n+2 "$targets" | while read line
do
  sid=`echo "$line" | cut -f 1`
  fq1=`echo "$line" | cut -f $rf1_ind`
  if [ ! -f "$fastqdir/$fq1" ]; then
    /bin/echo -e "$sid\tUnknown\tUnknown\tUnknown"
    echo "$0: WARNING: FASTQ file '$fastqdir/$fq1' not found, cannot extract library sequencer details for QC" 1>&2
    continue
  fi

  ## If this looks like it's from SRA, there is no metadata to harvest
  read_header=`head -1 "$fastqdir/$fq1"`
  echo "$read_header" | grep -P "^@SRR[0-9]+.*length\=" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    /bin/echo -e "$sid\tUnknown\tUnknown\tUnknown"
    echo "$0: WARNING: FASTQ file '$fastqdir/$fq1' looks like it's from SRA; no machine/run/flowcell info will be available" 1>&2
    continue
  fi

  ## Parse Illumina read headers

  machine_id=`head -1 "$fastqdir/$fq1" | cut -d':' -f 1 | tr -d '@' | tr " " "_"  2>/dev/null`
  if [ $? -ne 0 ]; then
    machine_id="Unknown"
  else 
    echo "$machine_id" | grep "SRR[0-9\.]+" > /dev/null
    if [ $? -eq 0 ]; then
      ## Strip the "SRRx" part if it's there
      machine_id=`echo $machine_id | sed -E 's|[-_ ]*SRR[0-9\.]+[-_ ]*||'`
    fi
  fi
  run_id=`head -1 "$fastqdir/$fq1" | cut -d':' -f 2 2>/dev/null`
  if [ $? -ne 0 ]; then
    run_id="Unknown"
  fi
  flowcell_id=`head -1 "$fastqdir/$fq1" | cut -d':' -f 3 2>/dev/null`
  if [ $? -ne 0 ]; then
    flowcell_id="Unknown"
  fi
  /bin/echo -e "$sid\t$machine_id\t$run_id\t$flowcell_id"
done

