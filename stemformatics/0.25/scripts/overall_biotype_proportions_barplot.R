##=============================================================================
## Script:	overall_biotype_proportions_barplot.R
##
## Description:	Output N horizontal barplots per observation (rows) from input
##		biotype CPM stat table.
##
##		Example input format (two rows only):
##
##
##		protein_coding	processed_transcript	rRNA	pseudogene	...
##		6.822	3.594	17.410	0.001	...
##
##
## Author:	O.Korn
##
## Created:	2016-03-24
## Modified:	2017-02-27
##=============================================================================

source("scripts/inc/util.R")

Usage <- function() {
   print("Usage:  R -f <script_name.R> --args -input=<dataframe.txt> -output=<output.txt> [-islog2=TRUE/FALSE] [-units=<text>] [-xmax=0-100]")
   print("")
}


### START ###

## Expecting input data frame (CPM)
inputfile <- ARGV.get("input",T)
outputfile <- ARGV.get("output",T)
plottitle <- ARGV.get("title")
if (is.na(plottitle)) {
  plottitle <- "Distribution (%)"
} else {
  ## Replace underscores in title with spaces (if any)
  plottitle <- gsub("_", " ", plottitle, fixed=T)
}
units <- ARGV.get("units")
if (is.na(units)) {
  units <- ""
}
xmax <- ARGV.get("xmax")
if (is.na(xmax)) {
  ## Default X max is 100%
  xmax <- 100
} else {
  xmax <- as.numeric(xmax)
}


islog2 <- FALSE
if (! is.na(ARGV.get("islog2"))) {
  islog2 <- as.logical(ARGV.get("islog2"))
}
output_path <- dirname(outputfile)
fbase <- sub(".txt", "", basename(outputfile))


## Load count table
count_table <- read.table(as.character(inputfile), sep="\t", header=T, row.names=1, na.strings="", quote="", check.names=F, as.is=T)
if (islog2) {
  ## replace any NA values with -Inf
  ## will result in un-log2 value of zero
  count_table[is.na(count_table)] <- -Inf
} else {
  ## replace NA with zero
  count_table[is.na(count_table)] <- 0
}

## need to un-log if input is log2
if (islog2) {
  count_data <- 2^ as.matrix(count_table)
} else {
  count_data <- as.matrix(count_table)
}

## Print input stats for confirmation
print("Got input data:")
count_data

## disable scientific notation for large numbers
options(scipen=999)

summary_table <- matrix(nrow=0, ncol=length(colnames(count_data)))

for (rowID in row.names(count_data)) {
  ## Bar plot of % distributions
  bitmap(paste(output_path,"/",fbase,"_",rowID,"_barplot.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
  colors <- rainbow(length(colnames(count_data))+1)
  ## make sure legend doesn't get plotted over the top of the bars by extending right margin,
  ## also ensure enough space for vertical library names at the bottom
  ##   xpd=TRUE   - clipped to the figure region leaving plot region for custom legend (see below)
  par(mar = c(7.5, 12, 4.1, 7.5), xpd=TRUE)
  ## calculate percentages
  percentages <- count_data[rowID,] / sum(count_data[rowID,]) * 100
  summary_table <- rbind(summary_table, percentages)
  print("Calculated percentages:")
  print(percentages)
  if (units != "") {
    xlab <- paste("proportion of ",units," (%)",sep="")
  } else {
    xlab <- "proportion (%)"
  }
  bxpos <- barplot(percentages, main=paste(plottitle,"- ",rowID), horiz=TRUE, xlim=c(0,xmax), xlab=xlab, las=1, names.arg=names(count_data), col=colors)
  dev.off()
}

## output percentages
row.names(summary_table) <- row.names(count_data)
names(summary_table) <- names(count_data)
write.table(summary_table, file=outputfile, sep="\t", quote=F)

