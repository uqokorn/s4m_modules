##=============================================================================
## Script:	mapped_read_stats.R
##
## Description:	Output total counts and proportions of mapped counts by
##		sample pheno
##
## Author:	O.Korn
##
## Created:	2014-04-23
## Modified:	2017-02-27
##=============================================================================

source("./scripts/inc/util.R")

Usage <- function() {
   print("Usage:  R -f <script_name.R> --args -input=<dataframe.txt> -output=<output.txt> -phenofile=<sample_pheno.txt> [-islog2=TRUE/FALSE] [-units=<text>]")
   print("")
}

## Expecting input data frame (CPM)
inputfile <- ARGV.get("input",T)
outputfile <- ARGV.get("output",T)
## Targets file
phenofile <- ARGV.get("phenofile",T)
plottitle <- ARGV.get("title")
if (is.na(plottitle)) {
  plottitle <- "Read counts distribution (%)"
} else {
  ## replace any underscores with spaces in title
  plottitle <- gsub("_", " ", plottitle, fixed=T)
}
units <- ARGV.get("units")
if (is.na(units)) {
  units <- ""
}

islog2 <- FALSE
if (! is.na(ARGV.get("islog2"))) {
  islog2 <- as.logical(ARGV.get("islog2"))
}
output_path <- dirname(outputfile)
fbase <- sub(".txt", "", basename(outputfile))


## Load count table
count_table <- read.table(as.character(inputfile), sep="\t", header=T, row.names=1, na.strings="", quote="", check.names=F, as.is=T)
## need to un-log if input is log2
if (islog2) {
  count_data <- 2^ as.matrix(count_table)
} else {
  count_data <- as.matrix(count_table)
}
sampleNames <- colnames(count_table)

## Load sample phenotype file
## Sample names always in column 1 (SampleID)
if (! file.exists(phenofile)) {
  print(paste("Error: Input targets file (phenofile) '",phenofile,"' not found!"))
  q(status=1)
}
targets <- read.table(phenofile, sep="\t", header=T, stringsAsFactors=FALSE, colClasses="character")

if (identical(sort(as.vector(targets[,1])), sort(sampleNames)) != TRUE) {
  print(paste("Error: Samples in targets file '",phenofile,"' do not match samples in count data (check quantity and IDs)!",sep=""))
  print("Samples from phenoMap: ")
  print(sort(as.vector(targets[,1])))
  print("Samples from input data: ")
  print(sort(as.vector(sampleNames)))
  q(status=1)
}

if ("SampleType" %in% colnames(targets)) {
  phenocol <- "SampleType"
} else {
  phenocol <- 2
}

## Must be ordered as per sampleNames(data)
sampleGroupsOrdered <- as.vector(unlist(lapply(sampleNames, function(x) { 
  subset <- targets[targets[,1] == x,]
  subset[,phenocol] }))
)
sampleGroupVector <- as.numeric(as.factor(sampleGroupsOrdered))
groupNames <- unique(sampleGroupsOrdered)

grandTotalReads <- sum(count_data, na.rm=T)

summary_table <- matrix(nrow=0, ncol=2)

for (g in groupNames) {
  sampleIndexes <- which(sampleGroupsOrdered == g)
  numreps <- sum(sampleGroupsOrdered == g)

  print("Group name:")
  print(g)
  print("Sample indexes:")
  print(sampleIndexes)
  print("Number of reps:")
  print(numreps)

  count_subset <- count_data[,sampleIndexes]
  ## Input CPM is per sample, so we have to divide by number of reps for this sample group as well
  subset_tot <- sum(count_subset, na.rm=T) / numreps

  summary_table <- rbind(summary_table, c(subset_tot, as.numeric(subset_tot) / grandTotalReads * 100 * numreps))
}
colnames(summary_table) <- c("#reads all reps for condition", "% total mapped")
row.names(summary_table) <- groupNames

## disable scientific notation for large numbers
options(scipen=999)

print("Grand total reads:")
print(grandTotalReads)
print("Summary table:")
print(summary_table)

write.table(summary_table, file=outputfile, sep="\t", quote=F)

colors <- topo.colors(length(groupNames)*2)

## Bar plot of % distributions
bitmap(paste(output_path,"/",fbase,"_barplot.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
## make sure legend doesn't get plotted over the top of the bars by extending right margin,
## also ensure enough space for vertical library names at the bottom
##   xpd=TRUE   - clipped to the figure region leaving plot region for custom legend (see below)
par(mar = c(15, 4.1, 4.1, 7.5), xpd=TRUE)

## Render the barplot
##   las=3      - makes x axis labels vertical
##   cex.names  - font size for x labels as a percentage of default
bxpos <- barplot(summary_table[,2], main=plottitle, names.arg=groupNames, ylim=c(0,100), xlab="", col=tail(colors,-1), las=3, cex.names=1)

text(bxpos, 0.05, paste(floor(summary_table[,1]), units), cex=1, pos=3)

dev.off()

