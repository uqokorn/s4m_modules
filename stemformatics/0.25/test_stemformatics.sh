#!/bin/sh
## test_stemformatics.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./inc/stemformatics_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir -p "$S4M_TMP/$$"

  ## Load pairedend functions being tested
  test -f ./inc/stemformatics_pairedend_funcs.sh && . ./inc/stemformatics_pairedend_funcs.sh

  FASTQ_FILES="./etc/test"; export FASTQ_FILES
  FASTQ_TEST_FILES=$S4M_TMP/$$/test; export FASTQ_TEST_FILES
}

## Run after all tests finished
oneTimeTearDown () {
  ## Clean up test files and data
  rm -rf "$S4M_TMP/$$"
}

## Run between tests (uncomment if needed)
setUp () {
  ## Refresh test data
  rm -rf $S4M_TMP/$$/*
  cp -R $FASTQ_FILES $S4M_TMP/$$/
}



## TESTS ##

## Test that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  assertEquals "One or more binary dependencies are missing" 0 $?
}


## Test: paired-end
##
testInputFilesAvailable () {
  TARGET_LIST="paired-end-Targets.txt paired-end-bad-index-R1.fastq paired-end-bad-index-R2.fastq \
  paired-end-bad-order-and-index-R1.fastq paired-end-bad-order-and-index-R2.fastq \
  paired-end-bad-order-only-R1.fastq paired-end-bad-order-only-R2.fastq \
  paired-end-good-R1.fastq paired-end-good-R2.fastq targets.regex.txt \
  Targets-paired-end-bad-index.txt \
  Targets-paired-end-bad-order-and-index.txt \
  Targets-paired-end-bad-order-only.txt \
  Targets-paired-end-good.txt"

  for t in $TARGET_LIST
  do
    test -f "$FASTQ_TEST_FILES/$t"
    assertEquals "Missing file: $t" 0 $? 
  done
}

## Test: paired-end: Mismatching R1 and R2 read index
##
testTargetProcessBadIndex () {
  TARGET_LIST="Targets-paired-end-bad-index.txt"
  patternfile="$FASTQ_TEST_FILES/targets.regex.txt"

  for t in $TARGET_LIST
  do
    targets="$FASTQ_TEST_FILES/$t"
    s4m_exec  stemformatics::paired-end -f "$FASTQ_TEST_FILES"  -t "$targets" --patternfile "$patternfile"  -N 2 1>/dev/null  2>&1
    assertEquals "Testing target file: $t" 1 $?
  done

}

## Test: paired-end: Mismatching R1 and R2 read order only
##
testTargetProcessBadOrder () {
  TARGET_LIST="Targets-paired-end-bad-order-only.txt"
  patternfile="$FASTQ_TEST_FILES/targets.regex.txt"

  for t in $TARGET_LIST
  do
    targets="$FASTQ_TEST_FILES/$t"
    s4m_exec  stemformatics::paired-end -f "$FASTQ_TEST_FILES"  -t "$targets" --patternfile "$patternfile"  -N 2  1>/dev/null  2>&1
    assertEquals "Testing target file: $t" 0 $?
  done

}

## Test: paired-end: Mismatching R1 and R2 index AND read order
##
testTargetProcessBadOrderAndIndex () {
  TARGET_LIST="Targets-paired-end-bad-order-and-index.txt"
  patternfile="$FASTQ_TEST_FILES/targets.regex.txt"

  for t in $TARGET_LIST
  do
    targets="$FASTQ_TEST_FILES/$t"
    s4m_exec  stemformatics::paired-end -f "$FASTQ_TEST_FILES"  -t "$targets" --patternfile "$patternfile"  -N 2  1>/dev/null  2>&1
    assertEquals "Testing target file: $t" 1 $?
  done
}

## Test: paired-end: Positive test case
##
testTargetProcessGoodFile () {
  TARGET_LIST="Targets-paired-end-good.txt"
  patternfile="$FASTQ_TEST_FILES/targets.regex.txt"

  for t in $TARGET_LIST
  do
    targets="$FASTQ_TEST_FILES/$t"
    s4m_exec  stemformatics::paired-end -f "$FASTQ_TEST_FILES"  -t "$targets" --patternfile "$patternfile"  -N 2  1>/dev/null  2>&1
    assertEquals "Testing target file: $t" 0 $?
  done
}

## Test: paired-end: Manual invocation of paired end index (regex) detection
##
## Test to see that we can correctly detect the changing R1 and R2 string in read headers
## e.g. typically we see R1 headers with "1:N:0" and R2 with "2:N:0".
##
testPairedEndRegexDetectionExplicit () {
  detected_regex=`./scripts/detect_fastq_paired_regex.sh "$FASTQ_TEST_FILES/paired-end-good-R1.fastq" "$FASTQ_TEST_FILES/paired-end-good-R2.fastq"`
  assertEquals ".:N:0" "$detected_regex"
}

## Test: paired-end: Integrated paired end index detection (i.e when pattern not supplied to paired-end command)
##
## As previous, but testing that auto-detection is working by not supplying pattern
##
testPairedEndRegexDetectionIntegration () {
  TARGET_LIST="Targets-paired-end-good.txt"
  patternfile="$FASTQ_TEST_FILES/targets.regex.txt"

  for t in $TARGET_LIST
  do
    targets="$FASTQ_TEST_FILES/$t"
    s4m_exec  stemformatics::paired-end -f "$FASTQ_TEST_FILES"  -t "$targets"  -N 2  1>/dev/null  2>&1
    assertEquals "Testing target file: $t" 0 $?
  done
}


## TODO: More tests..

