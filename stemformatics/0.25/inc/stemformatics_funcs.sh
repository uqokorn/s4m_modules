
## Path to pipeline template(s), relative to module directory
## TODO: Get rid of these relative paths. Store path information in module database instead.
RNASEQ_PIPELINE_TEMPLATE="./etc/pipelines/rnaseq_subread_Voom_TMM.pipeline"
SMALL_RNASEQ_PIPELINE_TEMPLATE="./etc/pipelines/small_rnaseq.pipeline"
FASTQ_ALIGN_TEMPLATE="./etc/pipelines/align_fastq.pipeline"
CHIPSEQ_PIPELINE_TEMPLATE="./etc/pipelines/chipseq_macs2.pipeline"
FASTQC_ONLY_PIPELINE_TEMPLATE="./etc/pipelines/fastqc_only.pipeline"
ERCCQC_ONLY_PIPELINE_TEMPLATE="./etc/pipelines/erccqc_only.pipeline"
ERCC_ALL_PIPELINE_TEMPLATE="./etc/pipelines/ercc_all.pipeline"

## Path to count table to use in finalisation, relative to dataset directory
## TODO: Remove this path. Store in module database instead.
FINAL_COUNT_TABLE="gene_count_frags_TMM_RPKM_log2_plus1.txt"

## TODO: Remove this. Store NCPU config in module database instead.
PIPELINE_DEFAULT_NCPU=4
## Memory in GB
## TODO: Remove this. Store Memory config in module database instead.
PIPELINE_DEFAULT_MEMORY=10

### HELPERS ###

validate_rnaseq_pipeline_template () {
  vrpt_template="$1"
  if [ -z "$vrpt_template" ]; then
    vrpt_template=RNASEQ_PIPELINE_TEMPLATE
  fi
  s4m_debug "validate_rnaseq_pipeline_template(): Got target template name = [$vrpt_template]"
  vrpt_template_path=`eval echo \\$$vrpt_template`

  if [ ! -s "$vrpt_template_path" ]; then
    s4m_error "RNASeq pipeline template file '$vrpt_template_path' empty or not found!"
    return 1
  fi
}


validate_chipseq_pipeline_template () {
  if [ ! -s "$CHIPSEQ_PIPELINE_TEMPLATE" ]; then
    s4m_error "ChIP-seq pipeline template file '$CHIPSEQ_PIPELINE_TEMPLATE' empty or not found!"
    return 1
  fi
}


##
## Get parsed / var replaced version of template (taking multiple %VAR%=<value> args
## for token replacement).
##
get_instance_template () {
  template="$1"
  shift

  sed_cmd="cat $template | sed -r "
  sed_opts=""

  while [ ! -z "$1" ];
  do
    kv="$1"
    key=`echo "$kv" | cut -d'=' -f 1`
    val=`echo "$kv" | cut -d'=' -f 2`
    sed_opts="$sed_opts -e \"s|$key|$val|g\""
    shift
  done

  ## DEBUG
  #echo "[eval $sed_cmd $sed_opts]"

  eval "$sed_cmd $sed_opts"
}


##
## Perform stand-alone alignment of FASTQ files
##
stemformatics_align_fastq () {
  fastqdir="$1"
  outputdir="$2"
  genome="$3"
  targets="$4"
  ncpu="$5"
  memory="$6"
  minfrag="$7"
  maxfrag="$8"
  user_R="$9"
  if [ -z "$ncpu" ]; then
    ncpu=$PIPELINE_DEFAULT_NCPU
  fi
  if [ -z "$memory" ]; then
    memory=$PIPELINE_DEFAULT_MEMORY
  fi
  ## Default to global R in the absence of an override
  if [ -z "$user_R" ]; then
    user_R=R
  else
    if [ ! -x "$user_R" ]; then
      s4m_error "R override binary '$user_R' is not executable or not a valid path!"
      return 1
    fi
  fi

  if [ ! -z "$genome" ]; then
    ## Fetch configured genomes list to check user value against
    genomes=`s4m_module_db_get "genomes"`
    if [ $? -ne 0 -o -z "$genomes" ]; then
      s4m_error "No genomes have been configured, please run the setup wizard (see module help)"
      return 1
    fi
    ## check user supplied genome against list
    if s4m_in_array "$genome" "$genomes"; then
      ## fetch subread index location (directory)
      subread_dir=`s4m_module_db_get "subread_$genome"`
      if [ $? -ne 0 ]; then
        s4m_error "Subread index directory not configured for genone '$genome', please run the setup wizard (see module help)"
        return 1
      fi

    else
      s4m_error "Genome '$genome' is not configured! Specify one from the following list, or run the setup wizard (see module help):
      $genomes
"
    fi

  else
    s4m_error "Must supply target genome for RNA-seq processing!"
    return 1
  fi

  template_basename=`basename "$FASTQ_ALIGN_TEMPLATE"`

  bamdir="$outputdir/aligned"
  mkdir -p "$bamdir"

  instance_template=`get_instance_template "$FASTQ_ALIGN_TEMPLATE" \
    %FASTQ_DIR%="$fastqdir" %OUTPUT_DIR%="$outputdir" %TARGETS_FILE%="$targets" \
    %GENOME_INDEX%="$subread_dir" %NCPU%=$ncpu %MEMORY%=$memory \
    %MINFRAGLEN%="$minfrag" %MAXFRAGLEN%="$maxfrag" \
    %R_BIN%="$user_R"`

  ## Write instance pipeline file
  echo "$instance_template" > "$outputdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$outputdir/$template_basename"
}


##
## Execute RNASeq pipeline on a Stemformatics dataset
##
## NOTE: Does "small" RNAseq too - most of the work happens in the target
##       pipeline file so this function is re-used.
##
run_rnaseq_pipeline_on_dataset () {
  dsdir="$1"
  genome="$2"
  ncpu="$3"
  memory="$4"
  minfrag="$5"
  maxfrag="$6"
  phredoffset="$7"
  doreplace="$8"
  user_R="$9"
  dryrun="${10}"
  ## New in v0.11: Override template target. Default is "RNASEQ_PIPELINE_TEMPLATE"
  ## (See top of this script)
  pipeline_template_name="${11}"

  ## Load fastqc which is used in pipeline
  if ! miniconda_activate; then
    return 1
  fi

  if [ -z "$ncpu" ]; then
    ncpu=$PIPELINE_DEFAULT_NCPU
  fi
  if [ -z "$memory" ]; then
    memory=$PIPELINE_DEFAULT_MEMORY
  fi

  ## Default to global R in the absence of an override
  if [ -z "$user_R" ]; then
    user_R=R
  else
    if [ ! -x "$user_R" ]; then
      s4m_error "R override binary '$user_R' is not executable or not a valid path!"
      return 1
    fi
  fi
  if [ -z "$pipeline_template_name" ]; then
    pipeline_template_name=RNASEQ_PIPELINE_TEMPLATE
  fi

  if ! validate_rnaseq_pipeline_template "$pipeline_template_name"; then
    return 1
  fi 

  timestamp=`date +"%Y%m%d-%H%M%S"`


  if [ ! -z "$genome" ]; then
    ## Fetch configured genomes list to check user value against
    genomes=`s4m_module_db_get "genomes"`
    if [ $? -ne 0 -o -z "$genomes" ]; then
      s4m_error "No genomes have been configured, please run the setup wizard (see module help)"
      return 1
    fi
    ## check user supplied genome against list
    if s4m_in_array "$genome" "$genomes"; then
      ## fetch GTF location
      gtf_file=`s4m_module_db_get "gtf_$genome"`
      if [ $? -ne 0 ]; then
        s4m_error "GTF file not configured for genone '$genome', please run the setup wizard (see module help)"
        return 1
      fi
      ## fetch subread index location (directory)
      subread_dir=`s4m_module_db_get "subread_$genome"`
      if [ $? -ne 0 ]; then
        s4m_error "Subread index directory not configured for genone '$genome', please run the setup wizard (see module help)"
        return 1
      fi

    else
      s4m_error "Genome '$genome' is not configured! Specify one from the following list, or run the setup wizard (see module help):
      $genomes
"
    fi

  else
    s4m_error "Must supply target genome for RNA-seq processing!"
    return 1
  fi

  procdir_genome="processed.$genome"
  ## Create new timestamped processed output dir, or overwrite existing?
  if [ "$doreplace" = "true" ]; then
    procdir="processed"
  else
    procdir="$procdir_genome.$timestamp"
  fi

  template_path=`eval echo \\$$pipeline_template_name`
  s4m_debug "Just before template instantiation, got template path = [$template_path]"

  instance_template=`get_instance_template "$template_path" %DATASET_DIR%="$dsdir" \
    %GENOME_INDEX%="$subread_dir" %GTF_FILE%="$gtf_file" %PROCESSED_OUTDIR%="$procdir" \
    %PHRED_OFFSET%="$phredoffset" %MINFRAGLEN%="$minfrag" %MAXFRAGLEN%="$maxfrag" \
    %NCPU%="$ncpu" %MEMORY%="$memory" %R_BIN%="$user_R"`

  template_basename=`basename "$template_path"`

  if [ "$dryrun" = "true" ]; then
    echo "DRY RUN mode Enabled.."
    echo 
    echo "COMMAND=[$S4M_SCRIPT_NAME pipeline::pipeline -f $dsdir/source/$procdir/$template_basename]"
    echo "PIPELINE FILE=[
$instance_template
]"
  else
    ## Overwrite 'processed'
    if [ "$doreplace" = "true" ]; then
      ## First time - create source/processed directory
      mkdir -p "$dsdir/source/$procdir"

    ## Default behaviour, create new timestamped 'processed' output directory
    else
      mkdir -p "$dsdir/source/$procdir"
      if [ -L "$dsdir/source/processed" ]; then
        unlink "$dsdir/source/processed"
      fi
      ## Create 'processed' symlink for new timestamped, unless already exists as a *directory*
      ## in which case, back up the old output
      if [ -d "$dsdir/source/processed" ]; then
        mv "$dsdir/source/processed" "$dsdir/source/processed.backup.pre-$timestamp"
      fi
      (cd "$dsdir/source" && ln -s "$procdir" ./processed)
      ## Also create genome-only referenced processed symlink
      (cd "$dsdir/source" && if [ -L ./"$procdir_genome" ]; then unlink ./"$procdir_genome"; fi && ln -s "$procdir" ./"$procdir_genome")
    fi

    ## Write instance pipeline file
    echo "$instance_template" > "$dsdir/source/$procdir/$template_basename"

    s4m_exec pipeline::pipeline -f "$dsdir/source/$procdir/$template_basename"
    return $?
  fi
}


##
## Execute ChIP-seq pipeline on a Stemformatics dataset
##
run_chipseq_pipeline_on_dataset () {
  dsdir="$stemformatics_datasetdir"
  genome="$stemformatics_genome"
  genomesize="$stemformatics_genomesize"
  peaktype="$stemformatics_peaktype"
  macsformat="$stemformatics_macsformat"
  macsqvalue="$stemformatics_macsqvalue"
  mismatches="$stemformatics_mismatches"
  ncpu="$stemformatics_ncpu"
  continuemode="$stemformatics_continue"
  dryrun="$stemformatics_dryrun"
  user_R="$stemformatics_R"
  user_MACS2="$stemformatics_MACS2"

  if [ -z "$ncpu" ]; then
    ncpu=$PIPELINE_DEFAULT_NCPU
  fi
  ## Default to global R in the absence of an override
  if [ -z "$user_R" ]; then
    user_R=R
  else
    if [ ! -x "$user_R" ]; then
      s4m_error "R override binary '$user_R' is not executable or not a valid path!"
      return 1
    fi
  fi
  ## Default to global macs2 in absence of override
  if [ -z "$user_MACS2" ]; then
    user_MACS2=macs2
  else
    if [ ! -x "$user_MACS2" ]; then
      s4m_error "MACS2 override binary '$user_MACS2' is not executable or not a valid path!"
      return 1
    fi
  fi

  if ! validate_chipseq_pipeline_template; then
    return $?
  fi

  if [ ! -z "$genome" ]; then
    ## Fetch configured genomes list to check user value against
    genomes=`s4m_module_db_get "genomes"`
    if [ $? -ne 0 -o -z "$genomes" ]; then
      s4m_error "No genomes have been configured, please run the setup wizard (see module help)"
      return 1
    fi
    ## check user supplied genome against list
    if s4m_in_array "$genome" "$genomes"; then
      ## fetch GTF location
      gtf_file=`s4m_module_db_get "gtf_$genome"`
      if [ $? -ne 0 ]; then
        s4m_error "GTF file not configured for genone '$genome', please run the setup wizard (see module help)"
        return 1
      fi
      ## fetch subread index location (directory)
      subread_dir=`s4m_module_db_get "subread_$genome"`
      if [ $? -ne 0 ]; then
        s4m_error "Subread index directory not configured for genone '$genome', please run the setup wizard (see module help)"
        return 1
      fi

    else
      s4m_error "Genome '$genome' is not configured! Specify one from the following list, or run the setup wizard (see module help):
      $genomes
"
    fi

  else
    s4m_error "Must supply target genome for ChIP-seq processing!"
    return 1
  fi

  timestamp=`date +"%Y%m%d-%H%M%S"`

  procdir_genome="processed.$genome"
  ## Create new timestamped processed output dir, or overwrite existing?
  if [ "$continuemode" = "true" ]; then
    procdir="processed"
  else
    procdir="$procdir_genome.$timestamp"
  fi

  ## set some defaults
  if [ -z "$peaktype" ]; then
    peaktype="regular"
  fi
  if [ -z "$macsformat" ]; then
    macsformat="AUTO"
  fi
  if [ -z "$macsqvalue" ]; then
    macsqvalue="0.01"
  fi
  if [ -z "$mismatches" ]; then
    mismatches=2
  fi

  instance_template=`get_instance_template "$CHIPSEQ_PIPELINE_TEMPLATE" %DATASET_DIR%="$dsdir" \
    %GENOME%="$genome" %GENOME_INDEX%="$subread_dir" %GTF_FILE%="$gtf_file" %GENOME_SIZE%="$genomesize" \
    %PROCESSED_OUTDIR%="$procdir" %PEAK_TYPE%="$peaktype" %MACS_INPUT_FORMAT%="$macsformat" \
    %MACS_QVALUE%="$macsqvalue" %MISMATCHES%="$mismatches" %NCPU%="$ncpu" \
    %R_BIN%="$user_R" %MACS2_BIN%="$user_MACS2"`

  template_basename=`basename "$CHIPSEQ_PIPELINE_TEMPLATE"`

  if [ "$dryrun" = "true" ]; then
    echo "DRY RUN mode Enabled.."
    echo 
    echo "COMMAND=[$S4M_SCRIPT_NAME pipeline::pipeline -f $dsdir/source/$procdir/$template_basename]"
    echo "PIPELINE FILE=[
$instance_template
]"
  else
    replacemode="false"
    if [ ! -z "$continuemode" ]; then
      replacemode="true"
    fi
    ## Overwrite 'processed'
    if [ "$replacemode" = "true" ]; then
      ## First time - create source/processed directory
      mkdir -p "$dsdir/source/$procdir"
    ## Default behaviour, create new timestamped 'processed' output directory
    else
      mkdir -p "$dsdir/source/$procdir"
      if [ -L "$dsdir/source/processed" ]; then
        unlink "$dsdir/source/processed"
      fi
      ## Create 'processed' symlink for new timestamped, unless already exists as a *directory*
      ## in which case, back up the old output
      if [ -d "$dsdir/source/processed" ]; then
        mv "$dsdir/source/processed" "$dsdir/source/processed.backup.pre-$timestamp"
      fi
      (cd "$dsdir/source" && ln -s "$procdir" ./processed)
      ## Also create genome-only referenced processed symlink
      (cd "$dsdir/source" && if [ -L ./"$procdir_genome" ]; then unlink ./"$procdir_genome"; fi && ln -s "$procdir" ./"$procdir_genome")
    fi

    ## Write instance pipeline file
    echo "$instance_template" > "$dsdir/source/$procdir/$template_basename"

    s4m_exec pipeline::pipeline -f "$dsdir/source/$procdir/$template_basename"
    return $?
  fi
}



##
## Finalise a post-RNAseq pipeline Stemformatics dataset.
##
## By default, will prepare Gene count tables for loading.
##
## OK: Not sure yet how best to specify override for transcript table loading
## because anything passed in has to make sense from a user-specified s4m args
## point of view?
##
## NOTE: Assumes dataset directory has been prevalidated.
##
finalise_s4m_rnaseq_dataset () {
  dsdir="$1"
  ## Optional override - finalise from "processed.$genome" symlink
  ## vs. "processed"
  genome="$2"
  ## Name of target (final) count table to finalise for loading
  ## Default if not specified: $FINAL_COUNT_TABLE
  target_count_table="$3"

  normdir="./source/normalized"

  if [ ! -z "$genome" ]; then
    s4m_log "Attempting to finalise from '$genome' outputs.."
    procdir="$dsdir/source/processed.$genome"
  else
    procdir="$dsdir/source/processed"
  fi

  if [ ! -d "$procdir" -a ! -L "$procdir" ]; then
    s4m_error "No '$procdir' - has this RNASeq dataset been processed?"
    return 1
  fi

  if [ -z "$target_count_table" ]; then
    target_count_table="$FINAL_COUNT_TABLE"
  fi

  if [ ! -s "$procdir/normalized/$target_count_table" ]; then
    s4m_error "No '$procdir/normalized/$target_count_table' - has this RNASeq dataset been processed?"
    return 1
  fi

  if [ ! -d "$dsdir/$normdir" ]; then
    mkdir -p "$dsdir/$normdir"
  else
    ## move existing symlinks out of the way (add .bak extension, overwrite any previous)
    if [ -L "$dsdir/$normdir/normalized_expression.txt" ]; then
      mv "$dsdir/$normdir/normalized_expression.txt" "$dsdir/$normdir/normalized_expression.txt.bak"
    fi
  fi

  cd "$dsdir/$normdir"

  procdirname=`basename "$procdir"`

  ## Preferentially use batch-removed outputs in finalisation, if available
  checkbatchdirs="removebatch
combat"
  for d in $checkbatchdirs
  do
    batchdir=../"$procdirname/$d"
    if [ -d "$batchdir" ]; then
      s4m_log "Found batch-removed outputs (dir '$batchdir'), will attempt to finalise using this data.."
      ## ComBat output file
      batchexpression="post_combat_expression.txt"
      if [ ! -f "$batchdir/$batchexpression" ]; then
       ## Limma (removeBatchEffect) output file
         batchexpression="post_batch_removed_expression.txt"
      else
        s4m_error "Batch-removed output data directory exists but found no expression data file!
Either remove '$batchdir', or re-run batch removal before trying again.
"
        return 1
      fi
      s4m_log "Symlinking from '$batchdir/$batchexpression'.."
      ## Symlink <dsdir>/source/normalized/ files from batch removed expression
      ln -s "$batchdir/$batchexpression" ./normalized_expression.txt
      break
    fi
  done

  ## No batch-removed expression available, use regular
  if [ ! -L ./normalized_expression.txt ]; then
    ## Symlink <dsdir>/source/normalized/ files from RNASeq outputs
    ## NOTE: Uses a relative link otherwise symlink will have full system path to 'FINAL_COUNT_TABLE'
    ## which makes the dataset non-portable once removed from the system where it was processed
    s4m_log "Symlinking from '$procdirname/normalized/$target_count_table'.."
    ln -s ../"$procdirname/normalized/$target_count_table" ./normalized_expression.txt
  fi

  ## Unwind normalized_expression.txt, add "detection" TRUE/FALSE for "normalized_expression_detection.txt"

  s4m_log "Unpacking normalized counts table for Stemformatics loading.."
  capture=`$SHELL "$S4M_THIS_MODULE"/scripts/unpack_expression_table.sh ./normalized_expression.txt ./normalized_expression_unpacked.txt 2>&1`
  ret=$?
  s4m_log "$capture"
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  sed -r -e 's|\t$|\t\tFALSE|g' -e 's|([0-9]+)$|\1\tTRUE|g' ./normalized_expression_unpacked.txt > ./normalized_expression_detection.txt
  return $?
}


##
## Create "normalized_expression.txt" and "normalized_expression_detection.txt" files required
## for ChIP-seq dataset finalisation
##
## TODO: Shell calls were updated in T#2690 but ChIP-seq pipeline requires regression test
##
chipseq_create_normalized_files () {
  ccnf_targets="$1"
  ccnf_peakcalls="$2"
  ccnf_normdir="$3"

  ## find *narrowPeak or *broadPeak
  peakfiles=`ls -1 "$ccnf_peakcalls"/*Peak`

  ## get control sample IDs
  controlcol=`head -1 "$ccnf_targets" | tr "\t" "\n" | grep -n "ControlID" | cut -d':' -f 1`
  controls=`cut -f $controlcol "$ccnf_targets"`

  ## to hold incrementally build sample ID header for merged table
  merged_samples_header=""

  ## output probeID -> fold change for chip library
  for pf in $peakfiles
  do
    pfbase=`basename "$pf"`
    probevalues="$ccnf_normdir/${pfbase}.expression"
    sampleid=`echo "$pfbase" | sed -r -e 's/^(.*)_peaks\.(narrow|broad)Peak$/\1/'`

    ## if sampleid is a control, ignore otherwise parse
    echo "$controls" | grep -P "^$sampleid$" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      s4m_log "Sample '$sampleid' is a ChIP control library, ignoring"
    else
      ## add sampleid to header
      merged_samples_header="${merged_samples_header}\t$sampleid"

      s4m_log "Extracting peak IDs and fold changes for significant peaks in library '$sampleid'"
      awk -F'\t' '{
        subpeak=substr($4, length($4), 1)
        ## MACS may have called a subpeak within the same chr loc, assigning "a", "b" etc to
        ## peak IDs. We take the letter (if any), make it upper case and appened with underscore
        ## to the chr location
        if (subpeak ~ /[a-z]/) {
          sc=toupper(subpeak)
          print "chr"$1":"$2"-"$3"_"sc "\t" $7
        } else {
          print "chr"$1":"$2"-"$3 "\t" $7
        }
      }' "$pf" > "$probevalues"
    fi
  done

  echo; s4m_log "Merging per-library ChIP peak expressions (fold changes) into one table:"
  ## merge all probe values into one table
  $SHELL ./scripts/merge_tables.sh '' --noheader --tmpdir "$ccnf_normdir" "$ccnf_normdir/"*expression > "$ccnf_normdir"/normalized_expression.txt.nonlog2.datonly
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_error "Failed to merge ChIP library peak expression values"
    return $ret
  fi
  ## prepend header
  /bin/echo -e "$merged_samples_header" | cat - "$ccnf_normdir"/normalized_expression.txt.nonlog2.datonly > \
    "$ccnf_normdir"/normalized_expression.txt.nonlog2

  s4m_log "  done"

  ## log2 transform peak fold change expression table
  if s4m_import "Rutil/Rutil.sh"; then
    echo; s4m_log "Log2 transforming fold change table (writing 'normalized_expression.txt'):"
    call_R "$stemformatics_R" "./scripts/log2_table.R" "--slave --args -input=$ccnf_normdir/normalized_expression.txt.nonlog2 -output=$ccnf_normdir/normalized_expression.txt"
    ret=$?
    if [ $ret -eq 0 ]; then
      s4m_log "  done"
    else
      return $ret
    fi
  else
    return 1
  fi

  cd $ccnf_normdir

  echo; s4m_log "Unpacking table for Stemformatics loading (writing 'normalized_expression_detection.txt')"
  capture=`$SHELL "$S4M_THIS_MODULE"/scripts/unpack_expression_table.sh ./normalized_expression.txt ./normalized_expression_unpacked.txt 2>&1`
  ret=$?
  s4m_log "$capture"
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  sed -r -e 's|\t$|\t\tFALSE|g' -e 's|([0-9\.\-]+)$|\1\tTRUE|g' ./normalized_expression_unpacked.txt > ./normalized_expression_detection.txt
  ret=$?
  s4m_log "  done"
  return $ret
}


##
## Create mapping file from HOMER peak annotation outputs
##
chipseq_create_mapping_file () {
  ccmf_targets="$1"
  ccmf_annotated="$2"
  ccmf_mapdir="$3"
  ## Ignoring these, for now. Could possibly use if we decide to have this function
  ## create a full feature mapping table for Stemformatics loading.
  #ccmf_chiptype="$4"
  #ccmf_dbid="$5"

  ## find *annotated.txt
  annofiles=`ls -1 "$ccmf_annotated"/*annotated.txt`

  ## get control sample IDs
  controlcol=`head -1 "$ccmf_targets" | tr "\t" "\n" | grep -n "ControlID" | cut -d':' -f 1`
  controls=`cut -f $controlcol "$ccmf_targets"`

  mapfile="$ccmf_mapdir/peak2enst.map"
  rm -f "$mapfile"

  for af in $annofiles
  do
    afbase=`basename "$af"`
    sampleid=`echo "$afbase" | sed -r -e 's/^(.*)_peaks.*annotated\.txt$/\1/'`

    ## if sampleid is a control, ignore otherwise parse
    echo "$controls" | grep -P "^$sampleid$" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      s4m_log "Sample '$sampleid' is a ChIP control library, ignoring"
    else
      s4m_log "Appending ENST annotation for peaks in library '$sampleid' to map file"
      awk -F'\t' '{
        ## Only process peak if annotated to ENST
        if ($8 ~ /ENST[0-9]+/) {
          if (match($8, /ENST[0-9]+/)) {
            enst = substr($8, RSTART, RLENGTH)

            subpeak=substr($1, length($1), 1)
            ## MACS may have called a subpeak within the same chr loc, assigning "a", "b" etc to
            ## peak IDs. We take the letter (if any), make it upper case and appened with underscore
            ## to the chr location
            if (subpeak ~ /[a-z]/) {
              sc=toupper(subpeak)
              print "chr"$2":"$3"-"$4"_"sc "\t" enst
            } else {
              print "chr"$2":"$3"-"$4 "\t" enst
            }
          }
        }
      }' "$af" >> "$mapfile"
    fi
  done

  ## there may (?) be multi-mappings, if so, create a unique version as well
  sort "$mapfile" | uniq > "${mapfile}.uniq"
  fbase=`basename "${mapfile}.uniq"`
  s4m_log; s4m_log "Wrote map file '$fbase'"

  if [ ! -s "${mapfile}.uniq" ]; then
    s4m_error "Map file is empty; indicates peak annotation failure (check input annotated peak data files)!"
    return 1
  fi
}


##
## Finalise a post-ChIP-seq pipeline Stemformatics dataset.
##
## By default, will prepare peak enrichment fold change tables for loading.
##
## NOTE: Assumes dataset directory has been prevalidated.
##
finalise_s4m_chipseq_dataset () {
  dsdir="$1"
  ## Optional override - finalise from "processed.$genome" symlink
  ## vs. "processed"
  genome="$2"

  normdir="$dsdir/source/normalized"

  if [ ! -z "$genome" ]; then
    s4m_log "Attempting to finalise from '$genome' outputs.."
    procdir="$dsdir/source/processed.$genome"
  else
    procdir="$dsdir/source/processed"
  fi

  if [ ! -d "$procdir" -a ! -L "$procdir" ]; then
    s4m_error "No '$procdir' - has this ChIP-seq dataset been processed?"
    return 1
  fi

  ## write "normalized_expression.txt" and "normalized_expression_detection.txt"
  mkdir -p "$normdir"
  chipseq_create_normalized_files "$dsdir/source/raw/targets.txt" "$procdir/peakcalls" "$normdir"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## Was going to write full map file for stemformatics loading but it requires a fair
  ## bit of extra work e.g. specifying Ensembl versions and keeping paths to ENST <-> ENSG
  ## map files in ~/.s4m/moduledata/stemformatics/database.ini
  ##
  ## If we want to pursue this functionality, the following lines might be helpful.
  ##
  ## See: chipseq_create_mapping_file()
  ##
  #chiptype=`grep s4m_chip_type "$dsdir/METASTORE" | cut -d'=' -f 2 2> /dev/null`
  #if [ $? -ne 0 ]; then
  #  s4m_error "Cannot create mapping file; failed to determine chip type from '$dsdir/METASTORE'"
  #  return 1
  #fi
  #dbid=`grep db_id "$dsdir/METASTORE" | cut -d'=' -f 2 2> /dev/null`
  #if [ $? -ne 0 ]; then
  #  s4m_error "Cannot create mapping file; failed to determine db_id from '$dsdir/METASTORE'"
  #  return 1
  #fi

  ## write mapping file/s for stemformatics loading
  mapdir="$procdir/mappings"
  mkdir -p "$mapdir"
  chipseq_create_mapping_file "$dsdir/source/raw/targets.txt" "$procdir/annotatedpeaks" "$mapdir"
  return $?
}


##
## Validate the format of a Stemformatics RNASeq dataset on disk
##
prevalidate_s4m_rnaseq_dataset () {
  dsdir="$1"
  if [ ! -d "$dsdir" ]; then
    s4m_error "Target dataset directory '$dsdir' not found!"
    return 1
  fi

  if [ ! -d "$dsdir/source/raw" ]; then
    s4m_error "Target dataset directory '$dsdir' does not look like an s4m dataset (no ./source/raw)"
    return 1
  fi

  if [ ! -w "$dsdir/source" ]; then
    s4m_error "Target dataset location '$dsdir/source' is not writeable!"
    return 1
  fi

  ## NOTE: Support for 'SRA' files not yet implemented!
  raw_extensions="(\.fastq|\.fq|\.sra)"
  cd "$dsdir/source/raw"
  ls -1 | grep -i -P "$raw_extensions" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    cd $OLDPWD
    s4m_warn "Dataset raw path '$dsdir/source/raw' does not contain FASTQ (*.fastq, *.fq) or SRA files (*.sra)!"
    #return 1
  fi
  cd $OLDPWD

  if [ ! -s "$dsdir/source/raw/targets.txt" ]; then
    s4m_error "Targets file '$dsdir/source/raw/targets.txt' is missing!

Example targets file (TAB-delimited) describing single or paired end (optional) read design:

`cat "$S4M_THIS_MODULE"/etc/examples/readTargets.txt`

Example location: $S4M_THIS_MODULE/etc/examples/readTargets.txt
"
    return 1
  fi
}


##
## Validate the format of a Stemformatics ChIP-seq dataset on disk
## *** NOTE: Currently identical to the RNAseq validator ***
##
prevalidate_s4m_chipseq_dataset () {
  prevalidate_s4m_rnaseq_dataset "$1"
  return $?
}


##
## Simple check for existing RNAseq outputs
##
rnaseq_dataset_has_outputs () {
  rdho_dsdir="$1"

  if [ -d "$rdho_dsdir/source/processed" -o -L "$rdho_dsdir/source/processed" ]; then
    echo "true"
  else
    echo "false"
  fi
}


##
## Simple check for existing ChIP-seq outputs
##
chipseq_dataset_has_outputs () {
  cdho_dsdir="$1"

  if [ -d "$cdho_dsdir/source/processed" -o -L "$cdho_dsdir/source/processed" ]; then
    echo "true"
  else
    echo "false"
  fi
}


##
## Compile RNAseq QC plots into PDF files - gene and transcript versions (RNASeq)
##
stemformatics_compile_qc_pdf_rnaseq () {
  scqpr_procdir="$1"
  scqpr_postqcout="$scqpr_procdir/qc"
  scqpr_normout="$scqpr_procdir/normalized"

  s4m_log "Compiling gene counts QC PDF.."
  density_non_norm_extra=`ls "$scqp_postqcout"/density_gene_expression_non_normalized_*png 2>/dev/null | grep -v _single.png | tr "\n" " "`
  ## Leaving here for reference in case it's needed
  #density_RPKM_extra=`ls "$scqp_postqcout"/density_gene_expression_RPKM_log2_*png 2>/dev/null | grep -v _single.png | tr "\n" " "`
  density_TMM_RPKM_extra=`ls "$scqp_postqcout"/density_gene_expression_TMM_RPKM_log2_*png 2>/dev/null | grep -v _single.png | tr "\n" " "`
  pca_non_norm_extra=`ls "$scqp_postqcout"/PCA_gene_expression_non_normalized_*_pc*png 2>/dev/null | tr "\n" " "`
  ## Leaving here for reference in case it's needed
  #pca_RPKM_extra=`ls "$scqp_postqcout"/PCA_gene_expression_RPKM_log2_*_pc*png 2>/dev/null | tr "\n" " "`
  pca_TMM_RPKM_extra=`ls "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM_log2_*_pc*png 2>/dev/null | tr "\n" " "`

  mean_variance_if_exists=""
  if [ -f "$scqp_normout"/gene_count_frags_voom_mean_variance.png ]; then
    mean_variance_if_exists="$scqp_normout"/gene_count_frags_voom_mean_variance.png
  fi
  ## Full repertoire, for reference
  #convert "$scqp_postqcout"/density_gene_expression_non_normalized_single.png "$scqp_postqcout"/density_gene_expression_non_normalized.png $density_non_norm_extra "$scqp_postqcout"/boxplot_gene_expression_non_normalized.png "$scqp_postqcout"/PCA_gene_expression_non_normalized_pc*png $pca_non_norm_extra "$scqp_postqcout"/PCA_gene_expression_non_normalized_screeplot.png "$scqp_postqcout"/HC_gene_expression_non_normalized.png "$scqp_postqcout"/density_gene_expression_RPKM_log2_single.png "$scqp_postqcout"/density_gene_expression_RPKM_log2.png $density_RPKM_extra "$scqp_postqcout"/boxplot_gene_expression_RPKM_log2.png "$scqp_postqcout"/PCA_gene_expression_RPKM_log2_pc*png $pca_RPKM_extra "$scqp_postqcout"/PCA_gene_expression_RPKM_log2_screeplot.png "$scqp_postqcout"/HC_gene_expression_RPKM_log2.png "$mean_variance_if_exists" "$scqp_postqcout"/density_gene_expression_TMM_RPKM_log2_single.png "$scqp_postqcout"/density_gene_expression_TMM_RPKM_log2.png $density_TMM_RPKM_extra "$scqp_postqcout"/boxplot_gene_expression_TMM_RPKM_log2.png "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM_log2_pc*png $pca_TMM_RPKM_extra "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM_log2_screeplot.png "$scqp_postqcout"/HC_gene_expression_TMM_RPKM_log2.png   "$scqp_postqcout"/QC_gene_expression.PDF
  convert "$scqp_postqcout"/density_gene_expression_non_normalized_single.png "$scqp_postqcout"/density_gene_expression_non_normalized.png $density_non_norm_extra "$scqp_postqcout"/boxplot_gene_expression_non_normalized.png "$scqp_postqcout"/PCA_gene_expression_non_normalized_pc*png $pca_non_norm_extra "$scqp_postqcout"/PCA_gene_expression_non_normalized_screeplot.png "$scqp_postqcout"/HC_gene_expression_non_normalized.png "$mean_variance_if_exists" "$scqp_postqcout"/density_gene_expression_TMM_RPKM_log2_single.png "$scqp_postqcout"/density_gene_expression_TMM_RPKM_log2.png $density_TMM_RPKM_extra "$scqp_postqcout"/boxplot_gene_expression_TMM_RPKM_log2.png "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM_log2_pc*png $pca_TMM_RPKM_extra "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM_log2_screeplot.png "$scqp_postqcout"/HC_gene_expression_TMM_RPKM_log2.png   "$scqp_postqcout"/QC_gene_expression.PDF
  if [ $? -ne 0 ]; then
    s4m_error "Failed creating gene QC PDF"
    return 1
  fi

  s4m_log "Compiling gene counts QC PDF (log2+1 count variants).."
  density_TMM_RPKM_plus1_extra=`ls "$scqp_postqcout"/density_gene_expression_TMM_RPKM+1_log2_*png 2>/dev/null | grep -v _single.png | tr "\n" " "`
  pca_TMM_RPKM_plus1_extra=`ls "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM+1_log2_*_pc*png 2>/dev/null | tr "\n" " "`
  convert "$scqp_postqcout"/density_gene_expression_TMM_RPKM+1_log2_single.png "$scqp_postqcout"/density_gene_expression_TMM_RPKM+1_log2.png $density_TMM_RPKM_plus1_extra "$scqp_postqcout"/boxplot_gene_expression_TMM_RPKM+1_log2.png "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM+1_log2_pc*png $pca_TMM_RPKM_plus1_extra "$scqp_postqcout"/PCA_gene_expression_TMM_RPKM+1_log2_screeplot.png "$scqp_postqcout"/HC_gene_expression_TMM_RPKM+1_log2.png   "$scqp_postqcout"/QC_gene_expression_plus1.PDF
  if [ $? -ne 0 ]; then
    s4m_error "Failed creating gene QC PDF (log2+1 count variants)"
    return 1
  fi


  ## T#2916: Skipping transcript QC since we don't use them
  if false; then
  s4m_log "Compiling transcript counts QC PDF.."
  density_non_norm_extra=`ls "$scqp_postqcout"/density_transcript_expression_non_normalized_*png | grep -v _single.png | tr "\n" " "`
  density_RPKM_extra=`ls "$scqp_postqcout"/density_transcript_expression_RPKM_log2_*png | grep -v _single.png | tr "\n" " "`
  density_TMM_RPKM_extra=`ls "$scqp_postqcout"/density_transcript_expression_TMM_RPKM_log2_*png | grep -v _single.png | tr "\n" " "`
  pca_non_norm_extra=`ls "$scqp_postqcout"/PCA_transcript_expression_non_normalized_*_pc*png 2>/dev/null | tr "\n" " "`
  pca_RPKM_extra=`ls "$scqp_postqcout"/PCA_transcript_expression_RPKM_log2_*_pc*png 2>/dev/null | tr "\n" " "`
  pca_TMM_RPKM_extra=`ls "$scqp_postqcout"/PCA_transcript_expression_TMM_RPKM_log2_*_pc*png 2>/dev/null | tr "\n" " "`

  mean_variance_if_exists=""
  if [ -f "$scqp_normout"/transcript_count_frags_voom_mean_variance.png ]; then
    mean_variance_if_exists="$scqp_normout"/transcript_count_frags_voom_mean_variance.png
  fi
  convert "$scqp_postqcout"/density_transcript_expression_non_normalized_single.png "$scqp_postqcout"/density_transcript_expression_non_normalized.png $density_non_norm_extra "$scqp_postqcout"/boxplot_transcript_expression_non_normalized.png "$scqp_postqcout"/PCA_transcript_expression_non_normalized_pc*png $pca_non_norm_extra "$scqp_postqcout"/PCA_transcript_expression_non_normalized_screeplot.png "$scqp_postqcout"/HC_transcript_expression_non_normalized.png "$scqp_postqcout"/density_transcript_expression_RPKM_log2_single.png "$scqp_postqcout"/density_transcript_expression_RPKM_log2.png $density_RPKM_extra "$scqp_postqcout"/boxplot_transcript_expression_RPKM_log2.png "$scqp_postqcout"/PCA_transcript_expression_RPKM_log2_pc*png $pca_RPKM_extra "$scqp_postqcout"/PCA_transcript_expression_RPKM_log2_screeplot.png "$scqp_postqcout"/HC_transcript_expression_RPKM_log2.png "$mean_variance_if_exists" "$scqp_postqcout"/density_transcript_expression_TMM_RPKM_log2_single.png "$scqp_postqcout"/density_transcript_expression_TMM_RPKM_log2.png $density_TMM_RPKM_extra "$scqp_postqcout"/boxplot_transcript_expression_TMM_RPKM_log2.png "$scqp_postqcout"/PCA_transcript_expression_TMM_RPKM_log2_pc*png $pca_TMM_RPKM_extra "$scqp_postqcout"/PCA_transcript_expression_TMM_RPKM_log2_screeplot.png "$scqp_postqcout"/HC_transcript_expression_TMM_RPKM_log2.png   "$scqp_postqcout"/QC_transcript_expression.PDF
  if [ $? -ne 0 ]; then
    s4m_error "Failed creating transcript QC PDF"
    return 1
  fi
  fi
}


##
## Compile RNAseq QC plots into PDF files - gene version only (small RNAseq)
##
stemformatics_compile_qc_pdf_smallrnaseq () {
  scqps_procdir="$1"
  scqps_postqcout="$scqps_procdir/qc"
  scqps_normout="$scqps_procdir/normalized"

  ## Compile QC plots into PDF files
  s4m_log "Compiling gene counts QC PDF.."
  density_non_norm_extra=`ls "$scqps_postqcout"/density_gene_expression_non_normalized_*png 2>/dev/null | grep -v _single.png | tr "\n" " "`
  density_RPKM_extra=`ls "$scqps_postqcout"/density_gene_expression_RPKM_log2_*png 2>/dev/null | grep -v _single.png | tr "\n" " "`
  pca_non_norm_extra=`ls "$scqps_postqcout"/PCA_gene_expression_non_normalized_*_pc*png 2>/dev/null | tr "\n" " "`
  pca_RPKM_extra=`ls "$scqps_postqcout"/PCA_gene_expression_RPKM_log2_*_pc*png 2>/dev/null | tr "\n" " "`

  convert "$scqps_postqcout"/density_gene_expression_non_normalized_single.png "$scqps_postqcout"/density_gene_expression_non_normalized_single.png $density_non_norm_extra "$scqps_postqcout"/boxplot_gene_expression_non_normalized.png "$scqps_postqcout"/PCA_gene_expression_non_normalized_pc*png $pca_non_norm_extra "$scqps_postqcout"/PCA_gene_expression_non_normalized_screeplot.png "$scqps_postqcout"/HC_gene_expression_non_normalized.png   "$scqps_postqcout"/density_gene_expression_RPKM_log2_single.png "$scqps_postqcout"/density_gene_expression_RPKM_log2.png $density_RPKM_extra "$scqps_postqcout"/boxplot_gene_expression_RPKM_log2.png "$scqps_postqcout"/PCA_gene_expression_RPKM_log2_pc*.png $pca_RPKM_extra "$scqps_postqcout"/PCA_gene_expression_RPKM_log2_screeplot.png "$scqps_postqcout"/HC_gene_expression_RPKM_log2.png   "$scqps_postqcout"/QC_gene_expression.PDF
  if [ $? -ne 0 ]; then
    s4m_error "Failed creating gene QC PDF"
    return 1
  fi
}


##
## Compile RNAseq QC plots into PDF files - gene and transcript versions
##
stemformatics_compile_qc_pdf () {
  scqp_procdir="$1"
  scqp_postqcout="$scqp_procdir/qc"
  scqp_normout="$scqp_procdir/normalized"

  ## Is this small RNAseq or regular? Different sets of QC plots are compiled, depending
  ls -1 "$scqp_procdir"/*pipeline | grep small_rnaseq > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    stemformatics_compile_qc_pdf_smallrnaseq "$scqp_procdir"
    return $?
  fi

  stemformatics_compile_qc_pdf_rnaseq "$scqp_procdir"
  return $?
}


##
## Create RNA composition plots (Ensembl biotypes) i.e. assess proportion of
## mapped reads / counts falling into various biotype categories.
## Useful when looking for rRNA contamination etc. Works with at least human,
## mouse, and rat.
##
## TODO: Refactor (shorten and reduce code duplication)
##
do_rnaqc () {
  drqc_input="$1"
  drqc_outdir="$2"
  drqc_gtf="$3"
  drqc_targets="$4"

  ## https://www.gencodegenes.org/gencode_biotypes.html
  biotypes="protein_coding
processed_transcript
pseudogene
non_coding
known_ncrna
3prime_overlapping_ncrna
Mt_rRNA
Mt_tRNA
miRNA
rRNA
scRNA
snRNA
snoRNA
sRNA
miscRNA
lincRNA"

  biotypedir="$drqc_outdir/gene_biotype"

  if [ ! -d "$biotypedir" ]; then
    mkdir -p "$biotypedir"
    s4m_log "Building Ensembl ID reference lists by biotype (this can take a while).."
    for b in $biotypes
    do
      bfixed=`echo "$b" | tr "_" "-"`
      grep -F "gene_biotype \"$b\"" "$drqc_gtf" | sed -r -e 's|^.*\sgene_id \"(\w+)\";.*$|\1|' | sort | uniq \
> "$drqc_outdir/gene_biotype/ensembl_${bfixed}.tsv"
      ## If TSV zero size, that biotype was not found in GTF - remove empty file so we
      ## don't pick it up in the subsetting operation below
      if [ ! -s "$biotypedir/ensembl_${bfixed}.tsv" ]; then
        rm -f "$biotypedir/ensembl_${bfixed}.tsv"
      fi
    done
    s4m_log "  done"
  else
    s4m_log "Gene biotype reference lists already exist, skipping.."
  fi

  ## Subset input expression table by biotype so we can calculate CPMs
  ## of just that biotype per library
  head -1 "$drqc_input" > "$drqc_outdir/header.tmp"
  fbase=`basename "$drqc_input" .txt`

  s4m_log "Subsetting input expression by biotype.."
  for biotypegenes in `ls $biotypedir/ensembl_*tsv`
  do
    biotype=`echo "$biotypegenes" | awk -F'_' '{print $NF}' | sed -r -e 's/\.tsv//'`
    grep -F -f "$biotypegenes" "$drqc_input" > "$drqc_outdir/${fbase}_${biotype}_only.txt.tmp"
    s4m_log "Writing biotype gene count subset: $drqc_outdir/${fbase}_${biotype}_only.txt"
    cat "$drqc_outdir/header.tmp" "$drqc_outdir/${fbase}_${biotype}_only.txt.tmp" > "$drqc_outdir/${fbase}_${biotype}_only.txt"
    rm "$drqc_outdir/${fbase}_${biotype}_only.txt.tmp"
  done
  rm "$drqc_outdir/header.tmp"
  s4m_log "  done"


  ## ================================================================
  ## 	RNA composition barplots (% CPMs by phenotype)
  ## ================================================================

  s4m_log "Plotting RNA composition (% CPMs across phenotypes).."
  exprsubsets=`ls "$drqc_outdir"/${fbase}_*_only.txt`
  s4m_debug "Got exprsubsets=[$exprsubsets]" 

  for subset in $exprsubsets
  do
    subsetname=`basename $subset`
    s4m_debug "got expr subset: [$subsetname]"
    subfbase=`basename "$subsetname" .txt`
    s4m_debug "got subfbase: [$subfbase]"
    biotype=`echo "$subsetname" | sed -r -e 's|^.*_([A-Za-z0-9\-]+)_only.*$|\1|'`
    s4m_debug "got biotype: [$biotype]"
    s4m_debug "Input file = [$drqc_outdir/$subsetname]"

    ## Plot barplot of RNA composition (by phenotype)
    call_R "$stemformatics_R" "$S4M_THIS_MODULE/scripts/mapped_read_stats.R" "-input='$drqc_outdir/$subsetname' -output='$drqc_outdir/${subfbase}_stats.txt' -phenofile='$drqc_targets' -islog2=T -title='Read_dist_of_mapped_${biotype}_annotated_counts_CPM_(%)' -units='CPM'"
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret 
    fi
  done


  ## ================================================================
  ## 		ALL RNA horizontal barplot
  ## ================================================================

  s4m_log "Plotting RNA composition ALL (% CPMs within each phenotype).."

  newheader=""
  rm -rf "$drqc_outdir/.rowids"
  for b in $biotypes
  do
    ## replace underscore with dash in biotype name to correctly find stats files
    bfixed=`echo "$b" | tr "_" "-"`
    statsfile=`find $drqc_outdir -name "*_${bfixed}_only_stats.txt"`
    ## No stats file, GTF annotation doesn't include this biotype
    if [ -z "$statsfile" ]; then
      continue
    fi
    newheader="$newheader\t$b"
    nmatch=`echo "$statsfile" | wc -l`
    if [ $nmatch -gt 1 ]; then
      s4m_fatal_error "Internal: Found more than one stats file in '$drqc_outdir' for biotype '$bfixed'!"
    elif [ $nmatch -eq 0 ]; then
      s4m_fatal_error "Internal: Failed to find biotype '$bfixed' stats file in '$drqc_outdir'!"
    fi
    tail -n+2 $statsfile | cut -f 2 > "$drqc_outdir/.$b.column"
    if [ ! -f "$drqc_outdir/.rowids" ]; then
      tail -n+2 $statsfile | cut -f 1 > "$drqc_outdir/.rowids"
      pr -Jtm "$drqc_outdir/.rowids" "$drqc_outdir/.$b.column" > "$drqc_outdir/.tmp"
    else
      pr -Jtm "$drqc_outdir/.tmp" "$drqc_outdir/.$b.column" > "$drqc_outdir/.tmp2"
      mv "$drqc_outdir/.tmp2" "$drqc_outdir/.tmp"
    fi
  done
  /bin/echo -e "$newheader" | cat - "$drqc_outdir/.tmp" > "$drqc_outdir/ALL_biotype_CPM_table.txt"
  rm -f "$drqc_outdir"/.*column "$drqc_outdir"/.rowids "$drqc_outdir/.tmp"

  ## Plot horizontal barplot of non-coding RNA composition (by phenotype)
  call_R "$stemformatics_R" "$S4M_THIS_MODULE/scripts/overall_biotype_proportions_barplot.R" "-input='$drqc_outdir/ALL_biotype_CPM_table.txt' -output='$drqc_outdir/ALL_biotype_proportions.txt' -units='CPM' -title='Mapped_read_targets_ALL' -xmax=75"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret 
  fi


  ## ================================================================
  ## 		Non-coding RNA horizontal barplot
  ## ================================================================

  s4m_log "Plotting RNA composition NON-CODING (% CPMs within each phenotype).."

  biotypesNonCoding=`echo "$biotypes" | grep -v protein_coding`
  newheader=""
  rm -rf "$drqc_outdir/.rowids"
  for b in $biotypesNonCoding
  do
    ## replace underscore with dash in biotype name to correctly find stats files
    bfixed=`echo "$b" | tr "_" "-"`
    statsfile=`find $drqc_outdir -name "*_${bfixed}_only_stats.txt"` 
    ## No stats file, GTF annotation doesn't include this biotype
    if [ -z "$statsfile" ]; then
      continue
    fi
    newheader="$newheader\t$b"
    nmatch=`echo "$statsfile" | wc -l`
    if [ $nmatch -gt 1 ]; then
      s4m_fatal_error "Internal: Found more than one stats file in '$drqc_outdir' for biotype '$bfixed'!"
    elif [ $nmatch -eq 0 ]; then
      s4m_fatal_error "Internal: Failed to find biotype '$bfixed' stats file in '$drqc_outdir'!"
    fi
    tail -n+2 $statsfile | cut -f 2 > "$drqc_outdir/.$b.column"
    if [ ! -f "$drqc_outdir/.rowids" ]; then
      tail -n+2 $statsfile | cut -f 1 > "$drqc_outdir/.rowids"
      pr -Jtm "$drqc_outdir/.rowids" "$drqc_outdir/.$b.column" > "$drqc_outdir/.tmp"
    else
      pr -Jtm "$drqc_outdir/.tmp" "$drqc_outdir/.$b.column" > "$drqc_outdir/.tmp2"
      mv "$drqc_outdir/.tmp2" "$drqc_outdir/.tmp"
    fi
  done
  /bin/echo -e "$newheader" | cat - "$drqc_outdir/.tmp" > "$drqc_outdir/noncoding_biotype_CPM_table.txt"
  rm -f "$drqc_outdir"/.*column "$drqc_outdir"/.rowids "$drqc_outdir/.tmp"

  ## Plot horizontal barplot of non-coding RNA composition (by phenotype)
  call_R "$stemformatics_R" "$S4M_THIS_MODULE/scripts/overall_biotype_proportions_barplot.R" "-input='$drqc_outdir/noncoding_biotype_CPM_table.txt' -output='$drqc_outdir/all_non_coding_biotype_proportions.txt' -units='CPM' -title='Mapped_read_targets_(non-coding_ONLY)' -xmax=75"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret 
  fi


  ## ================================================================
  ## 		Compile PDF
  ## ================================================================

  s4m_log "Compiling RNA QC plots into PDF files.."

  biotype_plots=`cd "$drqc_outdir" && ls -1 *only_stats_barplot.png`
  count=0
  page=1
  montage_plots=""
  rm -rf "$drqc_outdir/"page*png

  for b in $biotype_plots
  do
    count=`expr $count + 1`
    if [ $count -lt 4 ]; then
      montage_plots="$montage_plots $b"
    elif [ $count -eq 4 ]; then
      montage_plots="$montage_plots $b"
      (cd "$drqc_outdir" && montage -geometry 600x $montage_plots page$page.png)
      if [ $? -ne 0 ]; then
        s4m_fatal_error "Failed 'montage' command (ImageMagick) for RNA QC PDF compilation!"
      fi
      page=`expr $page + 1`
      montage_plots=""
      count=0
    fi
  done

  cd "$drqc_outdir"
  ## Add overall proportion plots
  convert page*.png ALL*png all_non_coding*png  read_distributions_by_gene_biotype.PDF
  if [ $? -ne 0 ]; then
    s4m_error "Failed 'convert' command (ImageMagick) for RNA QC PDF compilation!"
    return 1
  fi
  ## as above, but without the individual bar plots
  convert ALL*png all_non_coding*png  read_proportions_by_gene_biotype.PDF
  if [ $? -ne 0 ]; then
    s4m_error "Failed 'convert' command (ImageMagick) for RNA QC PDF compilation!"
    return 1
  fi
  rm -rf page*png
  cd $OLDPWD

  s4m_log "  done"

  return 0
}


##
## Generate multi-mapping QC (assess multi-mapping frequency)
##
do_bam_multimapqc () {
  dbm_inputdir="$1"
  dbm_outdir="$2"
  dbm_ncpu="$3"
  if [ -z "$dbm_ncpu" ]; then
    dbm_ncpu=1
  fi

  bamfiles=`ls -1 "$dbm_inputdir/"*.bam`

  s4m_debug "Got bamfiles=[$bamfiles]"
  sleep 3

  ## Generate per-BAM stats in parallel. Default = 1 CPU
  echo "$bamfiles" | xargs --verbose -n 1 -P $dbm_ncpu -I %  $SHELL $S4M_THIS_MODULE/scripts/bam_multimap_stats.sh % "$dbm_outdir/"
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_error "Failed when generating multi-map stats for BAM files"
    return 1
  fi

  bamfilestats="$dbm_outdir/"*.multimap_stats.txt

  s4m_log "Plotting BAM multi-map stats.."
  for bs in $bamfilestats
  do
    fbase=`basename "$bs" .multimap_stats.txt`
    ## Trim string to 35 chars for formatting purposes
    shortfbase=`echo "$fbase" | sed -r -e 's|^(.{35}).*$|\1|'`
    call_R "$stemformatics_R" $S4M_THIS_MODULE/scripts/multimap_stats_barplot.R  "-input='$bs' -output='$dbm_outdir/${fbase}.multimap_frequency.png' -title='${shortfbase}_-_Multi-mapping_read_distribution'"
    if [ $? -ne 0 ]; then
      s4m_error "Failed when plotting BAM multi-map stats for input '$bs'"
      return 1
    fi
  done
  s4m_log "  done"

  ## Compile PDF
  s4m_log "Compiling QC PDF.."
  (cd "$dbm_outdir" && convert *multimap_frequency.png  multimap_QC_BAM.PDF)
  if [ $? -ne 0 ]; then
    s4m_error "Failed compiling multi-map QC PDF"
    return 1
  fi
  s4m_log "  done"
  return $?
}

