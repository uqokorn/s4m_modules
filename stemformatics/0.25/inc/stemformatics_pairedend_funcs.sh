#!/bin/sh

## Is this a paired end dataset?
##
s4m_paired_end_ispaired () {
  head -1 "$stemformatics_targets" | tr "\t" "\n" | grep -n ReadFile2 > /dev/null 2>&1
  return $?
}


s4m_paired_end_get_fastq_list ()
{
  rf1_ind=`head -1 "$stemformatics_targets" | tr "\t" "\n" | grep -n ReadFile1 | cut -d':' -f 1`
  rf2_ind=`head -1 "$stemformatics_targets" | tr "\t" "\n" | grep -n ReadFile2 | cut -d':' -f 1`

  ## print all R1 and R2 FASTQ file names
  tail -n+2 "$stemformatics_targets" | cut -f $rf1_ind
  ret=$?
  tail -n+2 "$stemformatics_targets" | cut -f $rf2_ind
  [ $? -ne 0 -o $ret -ne 0 ] && return 1 || return 0
}


## Check FASTQ symlink status
##
s4m_paired_end_check_readfile_links ()
{
  fastq_list="$1"
  for fq in $fastq_list
  do
    s4m_debug "Checking '$fq' symlink status.."
    [ -L "$fq" ] && [ -d "original" ] && { s4m_warn "File $fq is a symlink and the directory 'sorted' exists"; return 1 ; }
    [ -L "$fq" ] && [ -d "$fq" ] && { s4m_warn "File $fq is a symlink to a directory"; return 1 ; }
    [ -L "$fq" ] &&  { s4m_warn "$fq is a symlink"; return 1 ; }
    s4m_debug "[OKAY]"
  done
  return 0
}


## Check if a FASTQ pair are in the same read order
##
s4m_paired_end_check_order_paired_ends ()
{
  check_script="$S4M_THIS_MODULE/scripts/fastq_paired_check.sh"
  detect_script="$S4M_THIS_MODULE/scripts/detect_fastq_paired_regex.sh"

  ## Determine ReadFile1 and ReadFile2 col indices from targets
  rf1_ind=`head -1 "$stemformatics_targets" | tr "\t" "\n" | grep -n ReadFile1 | cut -d':' -f 1`
  rf2_ind=`head -1 "$stemformatics_targets" | tr "\t" "\n" | grep -n ReadFile2 | cut -d':' -f 1`
  s4m_debug "  ReadFile1 is column $rf1_ind of targets file"
  s4m_debug "  ReadFile2 is column $rf2_ind of targets file"

  if [ -z "$stemformatics_pattern" ]; then
    s4m_warn "No paired end regex provided, will try to guess.."
    ## If index pattern regex not given, try to guess based on FASTQ data
    test_rf1=`tail -n+2 "$stemformatics_targets" | head -1 | cut -f $rf1_ind`
    test_rf2=`tail -n+2 "$stemformatics_targets" | head -1 | cut -f $rf2_ind`
    s4m_debug "  Test LHS file = '$test_rf1'"
    s4m_debug "  Test RHS file = '$test_rf2'"
    ## Detect paired end index pattern from first FASTQ R1/R2 pair in targets
    detected_regex=`$SHELL $detect_script "$stemformatics_fastqdir/$test_rf1" "$stemformatics_fastqdir/$test_rf2"`
    if [ $? -ne 0 ]; then
      return 1
    fi
    stemformatics_pattern="$detected_regex"
    s4m_log "Using guessed paired end regex pattern = [$stemformatics_pattern]"
  fi

  ## The following awk script creates the command line for paired end checking (per FASTQ pair)
  ## from targets file, and then 'xargs' executes them 
  cat "$stemformatics_targets" | awk -F'\t' \
      -v FQpath="$stemformatics_fastqdir" \
      -v checkscript="$check_script" \
      -v pattern="$stemformatics_pattern" \
      -v rf1=$rf1_ind \
      -v rf2=$rf2_ind \
      'NR>1 {print checkscript" "FQpath"/"$rf1" "FQpath"/"$rf2 " \""pattern"\""}' \
  | xargs -n 1 -I % -P $stemformatics_ncpu  $SHELL -c '%'  
  err=$?
  [ $err -gt 2 ] && return 1  || return 0 
}


## Sort a FASTQ read pair
##
s4m_paired_end_sort_paired_readfiles ()
{
  ## NOTE: Input list must be newline separated
  fastq_list="$1"
  mkdir -p "$stemformatics_fastqdir/original"  2>/dev/null
  mkdir -p "$stemformatics_fastqdir/sorted"  2>/dev/null

  cd "$stemformatics_fastqdir"
  s4m_debug "CWD: `pwd`"

  ret_move=0
  for fq in $fastq_list
  do
    if [ -f "$fq" -a ! -L "$fq" ]; then
      s4m_debug "moving '$fq' to '$stemformatics_fastqdir/original/'"
      ## don't overwrite existing files
      mv -n "$fq" ./original/
      if [ $? -ne 0 ]; then
        s4m_error "An error occurred while trying to move file '$fq' to '$stemformatics_fastqdir/original/'"
        return 1
      fi
      if [ ! -f ./original/"$fq" ]; then
        ret_move=1
      fi
    fi
  done

  if [ $ret_move -ne 0 ]; then
    s4m_error "One or more FASTQ files were not moved correctly, or do not exist in required path '$stemformatics_fastqdir/original/', aborting"
    return 1
  fi

  cd "$stemformatics_fastqdir/original/"
  s4m_debug "CWD: `pwd`"

  sort_tmpdir=""
  if [ ! -z "$stemformatics_tmpdir" -a -d "$stemformatics_tmpdir" -a -w "$stemformatics_tmpdir" ]; then
    sort_tmpdir="-T $stemformatics_tmpdir"
    echo; s4m_log "Passing TMPDIR override path '$stemformatics_tmpdir' to sort command.."
  else
    echo; s4m_warn "No TMPDIR override, sort operation on large files *may* fail. See docs for '$S4M_MODULE' for usage"
  fi

  s4m_log "Sorting LH and RH side files from $stemformatics_targets with files under 'original'"
  echo "$fastq_list" | xargs -n 1 -P $stemformatics_ncpu -I {} $SHELL -c 'cat {} | paste - - - - | sort -k1,1 -t " " '"$sort_tmpdir"' | tr "\t" "\n" > ../sorted/{}'

  cd "$stemformatics_fastqdir"

  s4m_log "Symlinking sorted FASTQ files.." 
  ret=0
  for fq in $fastq_list
  do
    if [ -L "$fq" ] ; then
      s4m_log "Cant link file ./sorted/$fq to ./$fq as link $fq exists" 
    else   
      s4m_debug "Now linking file ./sorted/$fq to ./$fq" 
      ln -s "./sorted/$fq" "./$fq"
      if [ $? -ne 0 ]; then
        ret=1
      fi
    fi
  done
  return $ret
}


## Dump paired-end flag vars
##
s4m_paired_end_print_debug ()
{
  echo "==============================================================="
  echo "S4M_MOD_OPTS : $S4M_MOD_OPTS" | sed 's/ --/\n\t --/g'
  echo "===============================================================
stemformatics_ncpu            = $stemformatics_ncpu
stemformatics_fastqdir        = $stemformatics_fastqdir
stemformatics_targets         = $stemformatics_targets
stemformatics_pattern         = $stemformatics_pattern
stemformatics_patternfile     = $stemformatics_patternfile
stemformatics_sort            = $stemformatics_sort
stemformatics_tmpdir          = $stemformatics_tmpdir
===============================================================
"

}

