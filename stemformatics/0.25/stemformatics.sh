#/bin/sh
##==========================================================================
## Module: s4m
## Author: Othmar Korn
##
## Stemformatics specific functions and pipelines
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##==========================================================================

. inc/setup.sh
. inc/stemformatics_funcs.sh
. inc/stemformatics_pairedend_funcs.sh


##
## Handler for "chipseq-all" command
##
chipseq_handler () {
  datasetdir="$stemformatics_datasetdir"
  finalise="$stemformatics_finalise"
  validateonly="$stemformatics_validate"
  dryrun="$stemformatics_dryrun"
  rerunmode="$stemformatics_rerun"
  continuemode="$stemformatics_continue"

  replacemode="false"
  if [ ! -z "$continuemode" ]; then
    replacemode="true"
  fi
  ## Finalise only?
  if [ ! -z "$finalise" ]; then
    finalise_s4m_chipseq_dataset "$datasetdir" "$genome"
    return $?
  fi

  if ! prevalidate_s4m_chipseq_dataset "$datasetdir"; then
    s4m_error "Failed ChIP-seq dataset pre-validation, exiting.."
    return 1
  fi
  if [ "$validateonly" = "true" ]; then
    s4m_log "Validation complete, exiting"
    return
  fi

  ## Run pipeline
  existing=`chipseq_dataset_has_outputs "$datasetdir"`
  if [ "$existing" = "true" ]; then
    s4m_log "Detected previous outputs for dataset.."
  else
    s4m_log "No previous outputs detected for dataset.."
  fi
  if [ "$existing" = "true" -a -z "$rerunmode" -a -z "$continuemode" ]; then
    s4m_error "Please specify relevant flag to continue with existing, or start a new run."
    return 1
  else
    if [ $replacemode = "true" ]; then
      s4m_log "Replacing / continuing existing run.."
    else
      s4m_log "Starting a new run.."
    fi

    run_chipseq_pipeline_on_dataset
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$dryrun" != "true" ]; then
        finalise_s4m_chipseq_dataset "$datasetdir" "$genome"
        return $?
      fi
    fi
  fi
}


##
## Handler for "small-rnaseq-all" command
##
## NOTE: Largely a copy of "rnaseq_handler()" - it seemed better to do this for clarity
##       (separation of concerns), rather than confusing the default rnaseq_handler() 
##       with stuff that's specific to the small RNAseq pipeline. Can revisit if needed.
##
small_rnaseq_handler () { 
  datasetdir="$stemformatics_datasetdir"
  genome="$stemformatics_genome"
  finalise="$stemformatics_finalise"
  ## NOTE: Default count table target if not given is "gene_count_frags_CPM_log2.txt"
  counttable="$stemformatics_counttable"
  validateonly="$stemformatics_validate"
  rerunmode="$stemformatics_rerun"
  continuemode="$stemformatics_continue"
  phredoffset="$stemformatics_phredoffset"

  ## Small-RNASeq pipeline uses different finalised count table to standard RNAseq pipeline
  if [ -z "$counttable" ]; then
    counttable="gene_count_frags_CPM_log2.txt"
  fi
  replacemode="false"
  if [ ! -z "$continuemode" ]; then
    replacemode="true"
  fi
  ## Finalise only?
  if [ ! -z "$finalise" ]; then
    finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
    return $?
  fi

  if ! prevalidate_s4m_rnaseq_dataset "$datasetdir"; then
    s4m_error "Failed small-RNAseq dataset pre-validation, exiting.."
    return 1
  fi
  if [ "$validateonly" = "true" ]; then
    s4m_log "Validation complete, exiting"
    return
  fi

  ## Run pipeline
  existing=`rnaseq_dataset_has_outputs "$datasetdir"`
  if [ "$existing" = "true" ]; then
    s4m_log "Detected previous outputs for dataset.."
  else
    s4m_log "No previous outputs detected for dataset.."
  fi
  if [ "$existing" = "true" -a -z "$rerunmode" -a -z "$continuemode" ]; then
    s4m_error "Please specify relevant flag to continue with existing, or start a new run."
    return 1
  else
    if [ $replacemode = "true" ]; then
      s4m_log "Replacing / continuing existing run.."
    else
      s4m_log "Starting a new run.."
    fi

    ## Just like standard RNAseq pipeline except we override the pipeline template for small-RNAseq
    run_rnaseq_pipeline_on_dataset "$datasetdir" "$genome" "$ncpu" "$phredoffset" "$replacemode" "$s4m_R" "$dryrun" "SMALL_RNASEQ_PIPELINE_TEMPLATE"
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$dryrun" != "true" ]; then
        finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
        return $?
      fi
    fi
  fi
}


##
## Handler for "rnaseq-all" command
##
rnaseq_handler () {
  datasetdir="$stemformatics_datasetdir"
  genome="$stemformatics_genome"
  finalise="$stemformatics_finalise"
  ## NOTE: Default count table target name in $FINAL_COUNT_TABLE (inc/stemformatics_funcs.sh)
  counttable="$stemformatics_counttable"
  validateonly="$stemformatics_validate"
  rerunmode="$stemformatics_rerun"
  continuemode="$stemformatics_continue"
  phredoffset="$stemformatics_phredoffset"
  memory="$stemformatics_memory"
  minfrag="$stemformatics_minfrag"
  maxfrag="$stemformatics_maxfrag"

  replacemode="false"
  if [ ! -z "$continuemode" ]; then
    replacemode="true"
  fi
  ## Finalise only?
  if [ ! -z "$finalise" ]; then
    finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
    return $?
  fi

  if ! prevalidate_s4m_rnaseq_dataset "$datasetdir"; then
    s4m_error "Failed RNAseq dataset pre-validation, exiting.."
    return 1
  fi
  if [ "$validateonly" = "true" ]; then
    s4m_log "Validation complete, exiting"
    return
  fi

  ## Run pipeline
  existing=`rnaseq_dataset_has_outputs "$datasetdir"`
  if [ "$existing" = "true" ]; then
    s4m_log "Detected previous outputs for dataset.."
  else
    s4m_log "No previous outputs detected for dataset.."
  fi
  if [ "$existing" = "true" -a -z "$rerunmode" -a -z "$continuemode" ]; then
    s4m_error "Please specify relevant flag to continue with existing, or start a new run."
    return 1
  else
    if [ $replacemode = "true" ]; then
      s4m_log "Replacing / continuing existing run.."
    else
      s4m_log "Starting a new run.."
    fi

    run_rnaseq_pipeline_on_dataset "$datasetdir" "$genome" "$ncpu" "$memory" "$minfrag" "$maxfrag" "$phredoffset" "$replacemode" "$s4m_R" "$dryrun"
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$dryrun" != "true" ]; then
        finalise_s4m_rnaseq_dataset "$datasetdir" "$genome" "$counttable"
        return $?
      fi
    fi
  fi
}


##
## Handler for "align-reads" command
##
alignreads_handler () {
  inputdir="$stemformatics_inputdir"
  outputdir="$stemformatics_outputdir"
  genome="$stemformatics_genome"
  targets="$stemformatics_targets"
  memory="$stemformatics_memory"
  minfrag="$stemformatics_minfrag"
  maxfrag="$stemformatics_maxfrag"

  stemformatics_align_fastq "$inputdir" "$outputdir" "$genome" "$targets" "$ncpu" "$memory" "$minfrag" "$maxfrag" "$s4m_R"
  return $?
}


##
## Handler for "qcpdf" command
##
qcpdf_handler () {
  procdir="$stemformatics_procdir"

  stemformatics_compile_qc_pdf "$procdir"
  return $?
}


##
## Handler for "fastqc" command
##
fastqc_handler () {
  dsdir="$stemformatics_datasetdir"
  ncpu="$stemformatics_ncpu"

  ## Load fastqc
  if ! miniconda_activate; then
    return 1
  fi

  ## Default NCPU=2 if not specified
  if [ -z "$ncpu" ]; then
    ncpu=2
  fi

  timestamp=`date +"%Y%m%d-%H%M%S"`
  procdir="processed.fastqc_only.$timestamp"
  outputdir="$dsdir/source/$procdir"
  mkdir -p "$outputdir"

  instance_template=`get_instance_template "$FASTQC_ONLY_PIPELINE_TEMPLATE" \
    %DATASET_DIR%="$dsdir" %PROCESSED_OUTDIR%="$procdir" %NCPU%=$ncpu`

  ## Write instance pipeline file
  template_basename=`basename "$FASTQC_ONLY_PIPELINE_TEMPLATE"`
  echo "$instance_template" > "$outputdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$outputdir/$template_basename"
  return $?
}


##
## Handler for "erccqc" command
##
## TODO: Update for Conda activation if/when needed
##
erccqc_handler () {
  procdir="$stemformatics_procdir"
  ncpu="$stemformatics_ncpu"

  ## Default NCPU=2 if not specified
  if [ -z "$ncpu" ]; then
    ncpu=2
  fi

  ## Handle R overrides
  user_R=R
  if [ ! -z "$s4m_R" ]; then
    user_R="$s4m_R"
  fi

  if [ ! -d "$procdir" ]; then
    s4m_error "Processed data path '$procdir' does not exist! Please check path is valid."
    return 1
  fi
  if [ ! -w "$procdir" ]; then
    s4m_error "Cannot write to processed data path '$procdir'! Please check permissions."
    return 1
  fi

  instance_template=`get_instance_template "$ERCCQC_ONLY_PIPELINE_TEMPLATE" \
    %PROCESSED_DIR%="$procdir" %NCPU%=$ncpu  %R_BIN%="$user_R"`

  ## Write instance pipeline file
  template_basename=`basename "$ERCCQC_ONLY_PIPELINE_TEMPLATE"`
  echo "$instance_template" > "$procdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$procdir/$template_basename"
  return $?
}


##
## Handler for "ercc-all" command
##
## TODO: Update for Conda activation if/when needed
##
ercc_all_handler () {
  procdir="$stemformatics_procdir"
  ncpu="$stemformatics_ncpu"

  ## Default NCPU=2 if not specified
  if [ -z "$ncpu" ]; then
    ncpu=2
  fi

  ## Handle R overrides
  user_R=R
  if [ ! -z "$s4m_R" ]; then
    user_R="$s4m_R"
  fi

  if [ ! -d "$procdir" ]; then
    s4m_error "Processed data path '$procdir' does not exist! Please check path is valid."
    return 1
  fi
  if [ ! -w "$procdir" ]; then
    s4m_error "Cannot write to processed data path '$procdir'! Please check permissions."
    return 1
  fi

  instance_template=`get_instance_template "$ERCC_ALL_PIPELINE_TEMPLATE" \
    %PROCESSED_DIR%="$procdir" %NCPU%=$ncpu  %R_BIN%="$user_R"`

  ## Write instance pipeline file
  template_basename=`basename "$ERCC_ALL_PIPELINE_TEMPLATE"`
  echo "$instance_template" > "$procdir/$template_basename"

  s4m_exec pipeline::pipeline -f "$procdir/$template_basename"
  return $?
}


##
## Handler for "rnaqc" command
##
rnaqc_handler () {
  inputfile="$stemformatics_input"
  outdir="$stemformatics_outputdir"
  gtf="$stemformatics_gtf"
  targets="$stemformatics_targets"

  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  ## Load stemformatics R environment via Conda
  if ! miniconda_activate; then
    return 1
  fi

  mkdir -p "$outdir"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create output path '$outdir'!"
    return 1
  fi

  do_rnaqc "$inputfile" "$outdir" "$gtf" "$targets"
  return $?
}


##
## Handler for "multimapqc" command
##
multimapqc_handler () {
  inputdir="$stemformatics_inputdir"
  outdir="$stemformatics_outputdir"
  ncpu="$stemformatics_ncpu"

  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  ## Load stemformatics R environment via Conda
  if ! miniconda_activate; then
    return 1
  fi

  mkdir -p "$outdir"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create output path '$outdir'!"
    return 1
  fi

  do_bam_multimapqc "$inputdir" "$outdir" "$ncpu"
  return $?
}


##
## Handler for "fastq paired end check and sort" commands
##
paired-end_handler () {

  ## Setup defaults and vars ##
  stemformatics_pattern=""
  if [ -f "$stemformatics_patternfile" ]; then
    stemformatics_pattern=`head -n 1 $stemformatics_patternfile`
  fi
  if [ -z "$stemformatics_sort" ] ; then stemformatics_sort="auto" ; fi
  if [ -z "$stemformatics_ncpu" ] ; then stemformatics_ncpu="2" ; fi
  start_cwd=`pwd`  

  if s4m_isdebug; then
    s4m_paired_end_print_debug 
  fi

  ## Exit early if this isn't a paired end dataset
  if ! s4m_paired_end_ispaired; then
    echo; s4m_log "This is *not* a paired-end RNAseq experiment, skipping paired-end check/sort operation"
    return 0
  fi

  ## cd DSDIR/source/raw for Stemformatics datasets
  cd "$stemformatics_fastqdir"

  already_sorted="false"
  if [ -d "sorted" -a -d "original" ]; then
    already_sorted="true"
  fi
  if [ "$already_sorted" = "true" -a "$stemformatics_sort" = "auto" ]; then
    ## Exit early if we're not enforcing
    s4m_log "Looks like sorted FASTQ already exist - nothing to do, exiting now"
    return 0
  elif [ "$already_sorted" = "true" -a "$stemformatics_sort" = "true" ]; then
    s4m_log "Sorted FASTQ already exist, but sort override was given, will attempt to re-sort"
  fi

  fastq_file_list=`s4m_paired_end_get_fastq_list`
  if [ $? -ne 0 ]; then
    s4m_error "An issue occurred while attempting to get input FASTQ files from targets"
    return 1
  fi

  echo; s4m_log "Running paired end read order check.."
  s4m_paired_end_check_order_paired_ends
  ret_sorted=$?

  if [ $ret_sorted -ne 0 ] ; then s4m_log "FASTQ pairs not sorted"; else s4m_log "FASTQ pairs look like they dont need sorting"; fi 

  ## Exit now if sort is "false" i.e. we just wanted to see if reads are sorted
  if [ "$stemformatics_sort" = "false" ]; then
    s4m_log "Asked not to do any sorting, returning now"
    return 0
  fi

  ## Remove sorted FASTQ symlinks if enforcing (i.e. sort=true) before attempting to re-sort original FASTQ
  if [ "$stemformatics_sort" = "true" -a "$already_sorted" = "true" ]; then
    for fq in $fastq_file_list
    do
      [ -L "$fq" ] && s4m_log "Unlinking sorted FASTQ '$fq'" && unlink "$fq"
    done
  elif [ "$stemformatics_sort" = "auto" ]; then
    ## FASTQ aren't sorted already, but there are some symlinks involved with input and we don't really
    ## want to proceed in case we mess something up, so it's better to return an error
    s4m_paired_end_check_readfile_links "$fastq_file_list"
    if [ $? -ne 0 ]; then
      s4m_error "Input FASTQ symlinked or there was a related issue, too risky to continue - please check and re-run"
      return 1
    fi
  fi

  echo; s4m_log "Sorting FASTQ pairs.."
  s4m_paired_end_sort_paired_readfiles "$fastq_file_list"
  echo; s4m_log "Running post-sort read order check.."
  s4m_paired_end_check_order_paired_ends
  if [ $? -ne 0 ] ; then
    s4m_error "Paired end check function exited with non-zero status, exiting now"
    return 1
  fi
  echo; s4m_log "FASTQ pairs are sorted"

  cd $start_cwd
  return 0
}


## Handler for "rnaseq-qc" command
##
rnaseq_qc_handler () {

  datasetdir="$stemformatics_datasetdir"

  if ! prevalidate_s4m_rnaseq_dataset "$datasetdir"; then
    return 1
  fi
  fastqdir="$datasetdir/source/raw"
  procdir="$datasetdir/source/processed"
  if [ ! -z "$stemformatics_procdir" ]; then
    procdir="$datasetdir/source/$stemformatics_procdir"
  fi
  targets="$datasetdir/source/raw/targets.txt"
  if [ ! -z "$stemformatics_targets" ]; then
    targets="$stemformatics_targets"
  fi
  qc_targets_dir="$procdir/qc/targets_qc"
  mkdir -p "$qc_targets_dir"

  ## We will create this below
  targets_qc="$qc_targets_dir/targets.FINAL.txt"

  if [ -z "$stemformatics_nomachine" ]; then
    echo; s4m_log "Gathering extra QC metadata for sample clustering analyses.."
    $SHELL -c "$S4M_THIS_MODULE/scripts/extract_sequencer_metadata.sh \"$fastqdir\" \"$targets\"" > "$qc_targets_dir/targets.sequencer.txt"
    ret=$?
    if [ $ret -ne 0 ]; then
      ## NOTE: We continue even if this fails; in the worst case, we revert to only using the user targets.txt
      s4m_warn "Failed to extract FASTQ library QC metadata!"
    fi

    ## Load "call_R()" function
    s4m_import "Rutil/Rutil.sh"

    ## Load stemformatics R environment using Conda
    if ! miniconda_activate; then
      return 1
    fi

    bamdir="$procdir/aligned"
    call_R "$s4m_R" "$S4M_THIS_MODULE/scripts/read_size_percentiles_from_mapstats.R" "--slave --args -input=$bamdir/mapstats.txt -output=$qc_targets_dir/targets.readSizePercentiles.txt"
    ret=$?
    if [ $ret -ne 0 ]; then
      ## NOTE: We continue even if this fails; in the worst case, we revert to only using the user targets.txt
      s4m_warn "Failed to generate library size and mapped read percentile metadata!"
    else
      ## fix targets header
      sed -i -r -e '1 s|^.*$|SampleID\tTotalReadsPercentile\tMappedReadsPercentile|' "$qc_targets_dir/targets.readSizePercentiles.txt"
    fi
  
    merge_targets="$qc_targets_dir/targets.sequencer.txt $qc_targets_dir/targets.readSizePercentiles.txt"


    s4m_log "Creating final QC targets '$qc_targets_dir/targets.FINAL.txt'.."
    ## Merge 'N' generated targets with user targets to produce final QC targets.
    ## The merge operation ensures sample ID order integrity by sorting.
    $SHELL -c "$S4M_THIS_MODULE/scripts/merge_tables.sh \"\t\" --tmpdir "$qc_targets_dir" \"$targets\" $merge_targets" > "$targets_qc"
    ret=$?
    if [ $ret -ne 0 ]; then
      ## NOTE: We continue even if this fails; in the worst case, we revert to only using the user targets.txt
      s4m_warn "Failed to create merged QC targets, will revert to input targets instead!"
      cp "$targets" "$targets_qc"
    else
      ## clean up temp files created by "merge_tables.sh" script
      rm -f "$qc_targets_dir/"*header "$qc_targets_dir/"*join
    fi

  ## No machine metadata, use normal targets as "final"
  else
    s4m_log "Skipping machine-generated QC gathering (at user request).."
    cp "$targets" "$targets_qc"
  fi

  ## post-summarisation QC
  ## PCA, boxplots and HC plots (before and after)
  countout="$procdir/counts"
  normout="$procdir/normalized"
  postqcout="$procdir/qc"

  ## Pairs of colon-separated count file path ==> QC name (used to build QC plot titles)

  ## T#2916: Removing transcript QC since we don't ever look at these
#  qc_input_counts_trans="$countout/transcript_count_frags.txt:transcript_expression_non_normalized
#$countout/transcript_count_frags_CPM_log2.txt:transcript_expression_CPM_log2
#$countout/transcript_count_frags_RPKM_log2.txt:transcript_expression_RPKM_log2
#$normout/transcript_count_frags_TMM_RPKM_log2.txt:transcript_expression_TMM_RPKM_log2"

## Do QC for the full set of transforms - we don't use them all, but leaving
## this here for reference
#  qc_input_counts_gene="$countout/gene_count_frags.txt:gene_expression_non_normalized
#$countout/gene_count_frags_CPM_log2.txt:gene_expression_CPM_log2
#$countout/gene_count_frags_RPKM_log2.txt:gene_expression_RPKM_log2
#$normout/gene_count_frags_TMM_RPKM_log2.txt:gene_expression_TMM_RPKM_log2
#$normout/gene_count_frags_TMM_RPKM_log2_median.txt:gene_expression_TMM_RPKM_log2_median
#$normout/gene_count_frags_TMM_RPKM_log2_plus1.txt:gene_expression_TMM_RPKM+1_log2
#$normout/gene_count_frags_TMM_RPKM_log2_median_plus1.txt:gene_expression_TMM_RPKM+1_log2_median"

  qc_input_counts_gene="$countout/gene_count_frags.txt:gene_expression_non_normalized
$normout/gene_count_frags_CPM_log2.txt:gene_expression_CPM_log2
$normout/gene_count_frags_TMM_RPKM_log2.txt:gene_expression_TMM_RPKM_log2
$normout/gene_count_frags_TMM_RPKM_log2_plus1.txt:gene_expression_TMM_RPKM+1_log2"

  ### DEBUG OVERRIDE ###
#  qc_input_counts_gene="$normout/gene_count_frags_TMM_RPKM_log2_median.txt:gene_expression_TMM_RPKM_log2_median
#$normout/gene_count_frags_TMM_RPKM_log2_median_plus1.txt:gene_expression_TMM_RPKM+1_log2_median"
  ### END ###
  qc_input_counts="$qc_input_counts_gene"

  for qcpair in $qc_input_counts
  do
    qc_data=`echo "$qcpair" | cut -d':' -f 1`
    qc_title=`echo "$qcpair" | cut -d':' -f 2`
    if [ -f "$qc_data" ]; then
      s4m_exec subread::qc -i "$qc_data" -o "$postqcout" -t "$targets_qc" -n "$qc_title"
      ret=$?
      if [ $ret -ne 0 ]; then
        return 1
      fi
    else
      s4m_warn "Skipping QC '$qc_title' because data file '$qc_data' not found"
    fi
  done

  ## Collate all post-QC plots into PDFs (transcript and gene plots compiled separately)
  s4m_exec stemformatics::qcpdf -p "$procdir"
  return $?
}


main () {
  ncpu="$stemformatics_ncpu"
  dryrun="$stemformatics_dryrun"

  ## NOTE: This *should* not be required any more as all downstream functions load their own R
  ##   environments using Conda (via miniconda module).
  ## TODO: Further testing/updates or we might keep this override for debugging purposes??
  s4m_R="$stemformatics_R"

  s4m_import "miniconda/lib.sh"

  ## RNAseq suites
  s4m_route "rnaseq-all" "rnaseq_handler"
  s4m_route "small-rnaseq-all" "small_rnaseq_handler"
  ## Align reads only
  s4m_route "align-reads" "alignreads_handler"
  ## ChIP-seq
  s4m_route "chipseq-all" "chipseq_handler"
  ## Regenerate QC report PDF only
  s4m_route "qcpdf" "qcpdf_handler"
  ## Run only FASTQC over sequence libraries
  s4m_route "fastqc" "fastqc_handler"
  ## Run only ERCC QC
  s4m_route "erccqc" "erccqc_handler"
  ## Run full ERCC pipeline
  s4m_route "ercc-all" "ercc_all_handler"
  ## RNA composition QC
  s4m_route "rnaqc" "rnaqc_handler"
  ## Multimap read QC (on BAM files)
  s4m_route "multimapqc" "multimapqc_handler"
  ## Paired end check and sort fastq files)
  s4m_route "paired-end" "paired-end_handler"
  ## Do RNA-seq QC
  s4m_route "rnaseq-qc" "rnaseq_qc_handler"
}

### START ###
main

