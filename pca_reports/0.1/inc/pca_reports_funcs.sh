#!/bin/sh
## ============================================================================
## Script:      pca_reports_funcs.sh
## Author:      Gabriel Brunet
## Description: Generate PCA HTML reports - useful funcs
## Created:     2018.07.13
## Updated:     2018.07.24
## ============================================================================

check_screetable_file () {
  # fn = file_name.
  fn="$1"
  ## Is there any argument to this script ? 
  if [ -z "$fn" ]; then
    s4m_error "Screetable file is absent. Must be supply as argument to this script." 
    exit 1
  fi
  ## Check the contents of the file
  sed -i '1cprcomp	eigenvalue'  "$fn"  1>/dev/null
  if [ $? -ne 0 ]; then
    s4m_error "Screetable file : wrong format or wrong file."
    exit 1
  fi
  sed -n '2p' "$fn" | grep "^PC1" 1>/dev/null
  if [ $? -ne 0 ]; then
    s4m_error "Screetable file must contains at least 2 PC lines."
    exit 1   
  fi
  sed -n '3p' "$fn" | grep "^PC2" 1>/dev/null
  if [ $? -ne 0 ]; then
    s4m_error "Screetable file must contains at leat 2 PC lines."
    exit 1
  fi  
}

check_pca_file () {
  # fn = file_name. 
  fn="$1"  
  ## Is there any argument to this script ? 
  if [ -z "$fn" ]; then
    s4m_error "PCA file is absent. Must be supply as argument to this script." 
    exit 1
  fi 
  ## Check the contents of the file
  sed -n '1p' "$fn" | grep "^SampleID" 1>/dev/null 
  if [ $? -ne 0 ]; then
    s4m_error "PCA file : wrong format or wrong file"
    exit 1 
  fi 
  sed -n '2p' "$fn" 1>/dev/null
  if [ $? -ne 0 ]; then 
    s4m_error "PCA file must contains at least 2 lines."
    exit 1
  fi
  sed -n '3p' "$fn" 1>/dev/null
  if [ $? -ne 0 ]; then 
    s4m_error "PCA file must contains at least 2 lines."
    exit 1
  fi 
}

check_metadata_file () {
  # fn = file_name. 
  fn="$1"
  ## Is there any argument to this script ? 
  if [ -z "$fn" ]; then
    s4m_error "Metadata file is absent. Must be supply as argument to this script." 
    exit 1
  fi
  ## Check the contents of the file
  sed -n '1p' "$fn" | grep "^SampleID" 1>/dev/null
  if [ $? -ne 0 ]; then
    s4m_error "Metadata file : wrong format or wrong file."
    exit 1
  fi
  ## Check the numbers of lines : need at least 3 lines (headline + 2 for samples)
  sed -n '3p' "$fn" 1>/dev/null
  if [ $? -ne 0 ]; then 
    s4m_error "Metadata must contain at least 3 lines."
    exit 1
  fi
}

update_pca_html () {
  ## PCA_filename: .txt file which contain $FINALDIR: cf ../pca_reports.sh
  pca_fn="$1/pca_names.txt"
  ## PCA_html: .html file which contain all <options> line for each rep
  pca_html="$1/pca.html"
  ## Last Directory: Path to $LDIR: cf ../pca_reports.sh
  LDIR="$2"
  ## Number of files
  nbfi=`wc -l "${pca_fn}" | awk '{ print $1 }'`
  ## Adapt selected status on HTML code for each repository
  for line in `seq 1 ${nbfi}`; do
    ## save file name
    fn="$(awk NR==$line ${pca_fn})"
    ## copy pca.html from tmp directory
    cp "${pca_html}" "${LDIR}/${fn}_pca/pca.html"
    ## add selected status for current
    sed -i "/option value=${fn}\>/c\<option value=${fn} selected=\"selected\">${fn} PCA Plots\<\/option\>" "${LDIR}/${fn}_pca/pca.html"
  done

  ## remove tmp pca.html directory
  rm -r "${PCADIR}"  
  ## Add required JavaScript and CSS resources
  if [ ! -d "${SDIR}/js" ]; then
    cp -R "scripts/js" "${SDIR}"
  fi
  if [ ! -d "${SDIR}/css" ]; then
    cp -R "scripts/css/" "${SDIR}"
  fi    
}

archive_tar_gz () {
  ## Path to $OUTDIR
  dir="$1"
  ## Name of the file which needs to be gzip'd
  namedir="$2"
  ## Creating gzip'd archive
  cd "${dir}/PCA_reports"
  ## remove old archive if already exist
  if [ -f "${namedir}.tar.gz" ]; then
    rm "${namedir}.tar.gz"
  fi
  ## Gzip'd archive
  tar -zcf "${namedir}.tar.gz" "$namedir"
  if [ $? -eq 0 ]; then
    s4m_log "Gzip'd archive created"
    return 0
  else
    s4m_error "Might be no space in the .tar file or the output directory is not writeable. Please try again."
    return 1
  fi
}

saving () {
  ## Path to $PCADIR: cf ../pca_reports.sh 
  dir="$1"
  ## File name: pca_reports_name: cd ../pca_reports.sh
  fn="${2}"

  ## Saving file name into $PCADIR
  grep "^$pca_reports_name\$" "${dir}/pca_names.txt" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "${fn}" >> "${dir}/pca_names.txt"
    ## Add a new <option> line with current name
    sed -i "/<select id=/a\<option value=${fn}\>${fn} PCA Plots\<\/option\>" "$dir/pca.html"
    s4m_log "$pca_reports_name data for offline pca reports: loaded."
    return 0
  else
    s4m_error "$pca_reports_name data already loaded."
    return 1
  fi
} 
