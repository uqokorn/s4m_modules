#!bin/bash
## ==================================================================
## Name:    test_pca_reports.sh: For "unittest" module execution 
## Author:  Gabriel Brunet
## Created: 2018.07.20
## Updated: 2018.07.24
## ==================================================================

ts=`date +"%Y%m%d"`

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./pca_reports.sh

  ## Set up temp data / files location and populate dummy files
  mkdir -p "$S4M_TMP/$$"

  ## Load pca_reports_funcs being tested
  test -f scripts/pca_reports_funcs.sh

  PCA_FILES="./etc/tests"; export PCA_FILES

  ## Copy test data
  cp -R $PCA_FILES $S4M_TMP/$$/

  PCA_TEST_FILES=$S4M_TMP/$$/tests ; export PCA_TEST_FILES
}

## Run after all tests finished
oneTimeTearDown () {
  ## remove test files and data
  rm -rf "$S4M_TMP/$$"
}

tearDown () {
  rm "${PCA_TEST_FILES}/PCA_reports/PCA_reports_${ts}.tar.gz"
}

## Run between tests (uncomment if needed)
#setUp () {
#}

### TESTS ###

test_directory1 () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f 
  cd  $PCA_TEST_FILES
  ls -1 | grep "PCA_reports"
  assertEquals 0 $?
}

test_flag_n () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f
  cd  $PCA_TEST_FILES/PCA_reports/PCA_reports_${ts}/pca_reports
  ls -1 | grep RPKM_log2
  assertEquals 0 $?
}

test_flag_paths () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f  
  cd $PCA_TEST_FILES//PCA_reports/PCA_reports_${ts}/pca_reports/RPKM_log2_pca
  ls -1 | grep .tsv
  assertEquals 0 $?
}

test_resources_js () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f
  cd $PCA_TEST_FILES/PCA_reports/PCA_reports_${ts}/js 
  ls -1 | grep .js 
  assertEquals 0 $?
}  

test_resources_css () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f 
  cd $PCA_TEST_FILES/PCA_reports/PCA_reports_${ts}/css   
  ls -1 | grep .css 
  assertEquals 0 $?
}

test_change_html () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f
  cd $PCA_TEST_FILES/PCA_reports/PCA_reports_${ts}/pca_reports/RPKM_log2_pca
  diff $PCA_TEST_FILES/pca.html pca.html 
  assertFalse 1 $?
}  

test_html () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f 
  cd $PCA_TEST_FILES/PCA_reports/PCA_reports_${ts}/pca_reports/RPKM_log2_pca
  grep "<option value=RPKM_log2 selected=\"selected\">RPKM_log2 PCA Plots</option>" "pca.html" 
  assertEquals 0 $?
}

test_moving_name () {
  s4m_exec pca_reports::pca-html -m "$PCA_TEST_FILES/metadata.FINAL.txt" -p "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_sample_data.tsv" -s "$PCA_TEST_FILES/PCA_gene_expression_RPKM_log2_screetable_prcomp_variance.tsv" -o "$PCA_TEST_FILES" -n "RPKM_log2" -f
  cd $PCA_TEST_FILES/PCA_reports/PCA_reports_${ts}/pca_reports/RPKM_log2_pca
  ls -1 | grep PCA_gene_expr 
  assertFalse $?
}
