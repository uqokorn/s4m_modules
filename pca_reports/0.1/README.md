
PCA_reports
===========

## Overview 

The `PCA_reports` module provides the ability to generate offline pca reports for viewing.

## What is a PCA reports ?

Basically, the pca reports will provide you PCA plots for data analysis throught your browser by clicking on *pca.html* file in final directories. A zipped version will be provide for sending and sharing your data.

## General command
``` 
    pca-html -m <path_metadata_file> -p <path_pca_file> -s <path_screetable_file> -n <name_of_the_final_directory > -o <path_output_directory> 
```

Note that if you have more than one sample set to run, you will need to rerun the pca_reports commands for every sample set you have.
Use the "--finalise" flag for the last sample set of data which make the function able to compile the archive (.tar version) and adapt the HTML code for each sample sets.

## Pca_reports file format specification
		
			 "metadata.tsv" 	Must contains SampleID as first word on the first line.
						Must contains at least 3 lines (head + 2 sample's lines).
				
				example : 	SampleID	SampleType	MacrophageType	...		
						A1	colon	CD206pos	...
						A2	colon	CD206neg	...
						...

			"pca.tsv" 		Must contains SampleID as first word on the first line.
						Must contains at least two others columns on the first line (PC1 and PC2).
	
				example :	SampleID	PC1	PC2	...
						A1	33.4	35.9	...  
						A2	22.3	21.4	...
						...

			"screetable.tsv" 	Must contains prcomp as first word on the first line and two columns.
						Must contains at least two other lines (PC1 and PC2).

				example : 	prcomp	eigenvalue
						PC1	2163.643456347
						PC2	2345.523443545
						...


NOTE: Each field must be separate by TAB and your files will be renamed in the final folder. Every path must be a path to a file.

IMPORTANT: Don't use any spaces in names

## Decompress .tar file

To decompress the .tar file follow those steps : 

NOTE: Works only on Linux and Mac OS systems. If you are using Windows, you will need to use a third party application to uncompress.

```
    tar -zxvf <PCA_reports_*date*.tar.gz> 
```

or you can split commands :

```
    gunzip <PCA_reports_*date*.tar.gz> 
    tar -xvf <PCA_reports_*date*.tar>
```
