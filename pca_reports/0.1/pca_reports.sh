#!bin/sh
## ============================================================================
## Module:       pca_reports
## Author:       Gabriel Brunet
## Synopsis:     Generating PCA HTML reports.
## Created:      2018-07-12
## Last update : 2018-07-25
## ============================================================================

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done 

## Import pca_reports functions
. inc/pca_reports_funcs.sh
## ============================================================================

pca_handler () {

  ts=`date +"%Y%m%d"`
  PCAR="PCA_reports"
  NAMEDIR="PCA_reports_${ts}"

  ## DSID/source/processed.xx/qc/ : Output DIR
  OUTDIR="$pca_reports_out"
  ## ...qc/pca_data_${ts} : PCA data DIR
  PCADIR="${OUTDIR}/pca_data_${ts}"
  ## ...qc/PCA_reports/PCA_report_date/ : Second DIR
  SDIR="${OUTDIR}/${PCAR}/${NAMEDIR}"
  ## ...qc/PCA_reports/PCA_report_date/pca_reports : Last DIR
  LDIR="${OUTDIR}/${PCAR}/${NAMEDIR}/pca_reports"
  ## .../qc/PCA_reports/PCA_reports_date/pca_reports/< -n : RPKM-TMM-CPM > : Final DIR
  FINALDIR="${LDIR}/${pca_reports_name}_pca"

  metadata="$pca_reports_metadata"
  pca="$pca_reports_pca"
  screetable="$pca_reports_screetable"
  
  ## Check if already finalised to avoid overwriting
  if [ -f ${SDIR}.tar.gz ]; then
    s4m_error "This PCA_reports is already finalised: please provide an other output path to continue"
    exit 1
  fi

  ## Check file format
  check_metadata_file "$metadata"
  check_pca_file "$pca"
  check_screetable_file "$screetable"

  ## Copy input files and create outputs directory
  mkdir -p "${OUTPUT}/${PCAR}" 2>/dev/null
  mkdir -p "${PCADIR}"  
  mkdir -p "${SDIR}"  
  mkdir -p "${LDIR}" 
  mkdir -p "${FINALDIR}"

  if [ ! -f $LDIR/metadata.txt_pca ]; then
    cp "$metadata" "$LDIR/metadata.txt_pca"
  fi
  cp "$metadata" "$FINALDIR/metadata.tsv"
  cp "$pca" "$FINALDIR/pca.tsv"
  cp "$screetable" "$FINALDIR/screetable.tsv"
  if [ ! -f "${PCADIR}/pca.html" ]; then
    cp scripts/pca.html "${PCADIR}/"
  fi

  ## Creating HTML report 
  if [ -z "$pca_reports_finalise" ]; then
    s4m_log "Compiling report ..."
    saving "$PCADIR" "$pca_reports_name"
    if [ $? -ne 0 ]; then
      s4m_error "Saving failed. Please refer you to README.md"
      exit 1
    fi
  else
    s4m_log "Finalising report ..."
    saving "$PCADIR" "$pca_reports_name"
    if [ $? -ne 0 ]; then
      s4m_error "Saving failed. Please refer you to README.md"
      exit 1
    fi
    ## Copy pca.html in all $LDIR
    ## Adapt selected status on HTML code for each repository 
    update_pca_html "$PCADIR" "$LDIR"
    ## Creating zipped report
    archive_tar_gz "${OUTDIR}" "$NAMEDIR"
    if [ $? -ne 0 ]; then
      s4m_error "Generating archive failed. Please refer you to README.md"
      return 1
    fi
  fi 
}

main () {
  s4m_route "pca-html" "pca_handler"
}

### START ###
main

