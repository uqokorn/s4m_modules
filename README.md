
S4M Modules
===========

## Overview

This repository houses modules (self-contained software bundles) for use with the [s4m](https://bitbucket.org/uqokorn/s4m_base) shell framework used in the [Stemformatics](http://www.stemformatics.org) project.

Note that only modules with "stemformatics" in the name are _specific_ to the Stemformatics project - most modules are general purpose and are designed to be useful to a wide audience (albeit currently only the bioinformatics domain has contributed modules).

In the near future it is expected that s4m modules will be hosted on a web-accessible mirror where each module will have its own Git repository, and the s4m suite will know how to install modules from there. But for now (Aug 2019), given the limited number of available modules, all are housed in this central Git repository and requires manual clone and installation.


## Installation

1) Simply check out the codebase to your preferred install location:

`git clone git@bitbucket.org:uqokorn/s4m_modules.git`


2) Install (clone) [s4m](https://bitbucket.org/uqokorn/s4m_base) if you have not done so already.

3) Tell _s4m_ where to find your modules library:

    s4m -s

Alternatively, ensure that the path to your modules library is configured in default _s4m_ configuration file `$HOME/.s4m/config`:

For example:

    S4M_SCRIPT_HOME=/path/to/s4m_base
    S4M_MODULE_HOME=/path/to/s4m_modules

You can create multiple configuration files e.g. to separate production and development environments.

For example a dev configuration `$HOME/.s4m/config_dev` might contain:

    S4M_SCRIPT_HOME=/path/to/s4m_base_dev
    S4M_MODULE_HOME=/path/to/s4m_modules_dev

A module could then be invoked using the development environment with:

    s4m -C ~/.s4m/config_dev  <module::command> <flags>


## Using modules

To find out how to use a given module, access the module help information:

    s4m help <module_name>

This will provide details of mandatory and optional flags to correctly invoke the target module. If multiple versions of a module are found, documentation for the latest version is displayed.

For non-interactive help (i.e. to dump all help to STDOUT without paging), run instead:

    s4m helpslave <module_name>


Some modules require **installation** - if a module is not installed it will cause a runtime error. In a future update to _s4m_, the module installation process will become a simple command e.g. `s4m install <module_name/version>` which will download and configure the module automatically.

However currently (Aug 2019) module installation is still a manual (one time) process.

Installable modules contain a script called `install.sh` which can contain any set of instructions that the module author requires to check/set up dependencies for their module.

One recommended method of module setup is via [conda](https://conda.io/docs). The s4m module `miniconda` allows a module author to specify a Conda environment file to resolve their depencies from packages hosted on [Anaconda](https://anaconda.org) for example from the [Bioconda](https://bioconda.github.io) or [Conda-forge](https://conda-forge.org) streams.


## Module installation

For modules requiring installation, the user must run:

    s4m install <module_name/version>

For example:

    s4m install multiqc/0.3

Which runs the setup routine for s4m module `multiqc` with version `0.3`.


## Help

If you have any questions, issues or suggestions please join our [Stemformatics Gitter chat](https://gitter.im/stemformatics/Lobby).

Please send email enquiries to [Othmar](mailto:othmar@stemformatics.org).


## License and contributing

The `s4m_modules` project is Open Source and licensed under the [Apache License](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)), Version 2.0 (see `LICENSE.txt`). Feel free to fork the repository for your own needs. We would welcome contributions of any bug fixes and improvements - please contact us (see above) about contributing.

_Please note:_ Third party module authors may provide their software under other licenses; in those cases, please be aware of any impacts this may have on any re-distribution of the Software with third party modules whose licenses may be incompatible with the Apache License, Version 2.0

