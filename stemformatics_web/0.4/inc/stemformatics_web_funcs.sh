
## NOTE: Option "StrictHostKeyChecking=no" disables interactive "are you sure?" prompt
##	for host not being in user's known_hosts file
S4MWEB_SSH_OPTS="-o ConnectTimeout=10 -o StrictHostKeyChecking=no"

## Default PostgreSQL port on Stemformatics web servers
S4MWEB_PG_PORT=5432

## Path to Redis socket on Stemformatics web server
S4MWEB_SOCKET_PATH="/data/redis/redis.sock"

## Path to directory containing "s4m.sh" tool on a Stemformatics web server.
## Normally this env var should already be in the user's ennvironment
if [ -z "$S4M_HOME" ]; then
  export S4M_HOME="/data/repo/git-working/stemformatics_tools/dataset_scripts/s4m"
fi

## Top-level path on WWW servers for reports (DSID subfolders underneath)
## TODO:Re-test Glimma upload due to v0.4 path change
S4MWEB_REPORTS_DIR="/var/www/pylons-data/prod/reports"

## Path holding single cell files on a Stemformatics web server
S4MWEB_SINGLECELL_PATH="/var/www/pylons-data/prod/singleCellFiles"

## Path holding PCA files on a Stemformatics web server
S4MWEB_PCA_PATH="/var/www/pylons-data/prod/PCAFiles"



## Validate report type string
##
s4mweb_validate_report_type () {
  report_type="$1"
  case "$report_type" in
    Glimma|glimma)
      return 0
      ;;
    ISCANDAR|iscandar)
      return 0
      ;;
    PCA|pca)
      return 0
      ;;
    *)
      s4m_error "Report type '$report_type' is unknown!"
      return 1
      ;;
  esac
}


## Validate Stemformatics dataset ID format
##
s4mweb_validate_dsid_format () {
  dsid="$1"
  ## At least 4 digits, but leaving scope for future expansion..
  echo "$dsid" | grep -P "^[0-9]{4}[0-9]*$" > /dev/null 2>&1
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_error "Invalid Stemformatics dataset ID '$dsid'! Expecting 4 (or more) digits."
  fi
  return $ret
}


## Upload various report types to Stemformatics for a target dataset
## Calls specific report type function.
##
## NOTE: Assumes inputs have been format-validated
##
## PORTABILITY: Use of $FUNCNAME in debug output is not available in POSIX
##
s4mweb_do_upload_reports () {
  folder="$1"
  report_type="$2"
  dsid="$3"
  host="$4"

  report_type_lc=`echo "$report_type" | tr "[:upper:]" "[:lower:]"`
  s4m_debug "$FUNCNAME(): Got sdur_type_lc=[$report_type_lc]"

  s4mweb_do_upload_reports_${report_type_lc}  "$folder" "$dsid" "$host"
  return $?
}


## Check SSH access to target as current user
##
s4mweb_check_ssh_access_to_target () {
  ssh_target="$1"
  ssh_debug="$2"
  if [ ! -z "$ssh_debug" ]; then
    S4M_DEBUG=TRUE
  fi

  s4m_debug "Checking access to '$ssh_target' using SSH as user `whoami`.."
  ## Get machine kernel string (for nor particular reason
  ssh_uname=`ssh $S4MWEB_SSH_OPTS "$ssh_target" uname -a 2>/dev/null`
  if [ $? -ne 0 ]; then
    ## Get error message
    ssh_output=`ssh -v $S4MWEB_SSH_OPTS "$ssh_target" uname -a 2>&1`
    s4m_error "Failed to connect to '$ssh_target' via SSH as user `whoami`!

    SSH output:

    $ssh_output
"
    return 1
  fi
  s4m_debug "$ssh_uname
[ OK ]"
  return 0
}


## Transfer a Glimma report directory to target
##
s4mweb_push_glimma_report () {
  folder="$1"
  dsid="$2"
  host="$3"
  
  glimma_path="$S4MWEB_REPORTS_DIR/$dsid/glimma"
  glimma_mkdir=`ssh "$host"  mkdir -p "$glimma_path" 2>&1`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create Glimma report path '$host:$glimma_path':

    $glimma_mkdir
"
    return 1
  fi

  s4m_log "Transferring input Glimma report:
    '$folder' -->
    '$host:$glimma_path/'"
  push_scp=`scp -r "$folder"  "$host:$glimma_path/" 2>&1`
  if [ $? -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to upload Glimma report!

    $push_scp
"
    return 1
  else
    s4m_log "[ OK ]"
  fi

  return 0
}


## Check for any obvious issues with input data
##
s4mweb_validate_iscandar_data () {
  folder="$1"

  ## Minimum set of require data files
  required_files="pca.txt
samples.txt
sampleGroupItems.txt"

  ## At least one of these must exist
  viz_files="tsne.txt
umap.txt"

  ## Optional files with dependencies (must exist if supplied)
  depend_files="clusters.txt:clusterItems.txt"


  for rf in $required_files
  do
    if [ ! -f "$folder/$rf" ]; then
      s4m_error "Required input file '$folder/$rf' not found!"
      return 1
    fi
  done

  found=0
  for vf in $viz_files
  do
    if [ -f "$folder/$vf" ]; then
      found=1
      break
    fi
  done
  if [ $found -eq 0 ]; then
    s4m_error "Failed to find single cell clustering visualisation data file - either 'tsne.txt' or 'umap.txt' must be supplied!"
    return 1
  fi

  for df in $depend_files
  do
    lhs=`echo "$df" | cut -d':' -f 1`
    rhs=`echo "$df" | cut -d':' -f 2`
    if [ -f "$folder/$lhs" ]; then
      if [ ! -f "$folder/$rhs" ]; then
        s4m_error "File '$folder/$rhs' not found (must exist if file '$folder/$lhs' exists!"
        return 1
      fi
    fi
  done

  return 0
}


## Transfer an ISCANDAR report directory to target
##
s4mweb_push_iscandar_data () {
  folder="$1"
  dsid="$2"
  host="$3"
  
  iscandar_path="$S4MWEB_SINGLECELL_PATH/$dsid"
  iscandar_mkdir=`ssh "$host"  mkdir -p "$iscandar_path" 2>&1`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create ISCANDAR data path '$host:$iscandar_path':

    $iscandar_mkdir
"
    return 1
  fi

  s4m_log "Adding required sub-folders.."
  ssh "$host" mkdir -p "$iscandar_path/clusters" "$iscandar_path/geneSets" "$iscandar_path/sampleGroups"
  if [ $? -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to add required sub-directories in remote path!"
    return 1
  else
    s4m_log "[ OK ]"
  fi

  s4m_log "Transferring input ISCANDAR data:
    '$folder' -->
    '$host:$iscandar_path/'"
  push_scp=`scp -r "$folder"/*txt  "$host:$iscandar_path/" 2>&1`
  if [ $? -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to upload ISCANDAR data!

    $push_scp
"
    return 1
  else
    s4m_log "[ OK ]"
  fi

  ## Make a "TSV" copy of every .txt file
  s4m_log "Configuring / reformatting data files.."
  ssh "$host" "cd $iscandar_path && for f in *txt; do fbase=\`basename \$f .txt\`; rm -f \${fbase}.tsv; cp \${fbase}.txt \${fbase}.tsv; done"
  if [ $? -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to set up files in remote location!"
    return 1
  fi
  ## Update metadata as needed based on supplied clustering data
  ssh "$host" "[ -f $iscandar_path/tsne.tsv ]"
  has_tsne=$?
  ssh "$host" "[ -f $iscandar_path/umap.tsv ]"
  has_umap=$?
  ## If no metadata.txt was supplied, we need to remove any target metadata.tsv first
  ## so we don't keep appending to it on next re-upload and subsequently
  if [ ! -f "$folder/metadata.txt" ]; then
    ssh "$host" "rm -f $iscandar_path/metadata.tsv"
  fi
  if [ $has_tsne -eq 0 -a $has_umap -eq 0 ]; then
    s4m_log "  Found both t-SNE and UMAP clustering data.."
    ssh "$host" "/bin/echo -e \"clustering_graphs\tpca,tsne,umap\" >> $iscandar_path/metadata.tsv"
    ret=$?
  elif [ $has_tsne -eq 0 ]; then
    s4m_log "  Found only t-SNE clustering data.."
    ssh "$host" "/bin/echo -e \"clustering_graphs\tpca,tsne\" >> $iscandar_path/metadata.tsv"
    ret=$?
  elif [ $has_umap -eq 0 ]; then
    s4m_log "  Found only UMAP clustering data.."
    ssh "$host" "/bin/echo -e \"clustering_graphs\tpca,umap\" >> $iscandar_path/metadata.tsv"
    ret=$?
  fi
  if [ $ret -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to modify file 'metadata.tsv' in remote location!"
    return 1
  fi
  ## Fix file headers
  ssh "$host" "sed -i -r -e '1s|^.*$|sample_id\tx\ty|' $iscandar_path/pca.tsv"
  if [ $? -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to fix header in 'pca.tsv' in remote location!"
    return 1
  fi
  if [ $has_tsne -eq 0 ]; then
    ssh "$host" "sed -i -r -e '1s|^.*$|sample_id\tx\ty|' $iscandar_path/tsne.tsv"
    ret=$?
  fi
  if [ $has_umap -eq 0 ]; then
    ssh "$host" "sed -i -r -e '1s|^.*$|sample_id\tx\ty|' $iscandar_path/umap.tsv"
    ret=$?
  fi
  if [ $ret -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to fix header in 'tsne.tsv' and/or 'umap.tsv' in remote location!"
    return 1
  fi
  s4m_log "[ OK ]"

  ## Make everything group writeable (play nice with other dataset uploaders)
  ## Server-side needs watchdog crontab for this while Stemformatics is still creating
  ##   JSON cache files without group-write perms for "portaladmin" user.
  ## Commenting this out for now.
  ##ssh "$host" chmod -R g+w "$iscandar_path"

  s4m_log "Attempt to clear JSON cache files.."
  ssh "$host"  find $iscandar_path/ -name \"*.json\" -exec "rm -f {} \\;"
  if [ $? -ne 0 ]; then
    s4m_warn "Single cell viz data *** MAY NOT BE REFRESHED *** if we could not delete JSON cache files!
"
  else
    s4m_log "[ OK ]"
  fi

  return 0
}


## Execute an SQL statement (or block)
##
s4mweb_do_psql () {
  host="$1"
  sql="$2"
  ## Set non-null to suppress any STDOUT that isn't the query result
  valueonly="$3"

  if [ -z "$valueonly" ]; then
    s4m_debug "Calling [psql_wrapper.sh $host $host $S4MWEB_PG_PORT \"$sql\"]"
  fi
  psql_out=`./scripts/psql_wrapper.sh $host $host $S4MWEB_PG_PORT "$sql" 2>&1`
  ret=$?
  echo "$psql_out" | grep ERROR > /dev/null 2>&1
  if [ $? -eq 0 -o $ret -ne 0 ]; then
    s4m_error "[ FAIL ] Failed SQL query!

    psql output:

$psql_out
"
    return 1
  else
    if [ -z "$valueonly" ]; then
      s4m_log "[ OK ]"
    else
      /bin/echo -n "$psql_out"
    fi
  fi
  return 0
}


## Calls a script on Stemformatics web server (stemformatics_tools repo) to load
## the "expression.tsv" into Redis
##
s4mweb_load_iscandar_redis () {
  host="$1"
  dsid="$2"

  redis_script="$S4M_HOME/../../redis/s4m/iscandar/initialise.sh"
  redis_script_args="$S4MWEB_SOCKET_PATH $S4MWEB_SINGLECELL_PATH $dsid"

  s4m_log "Submitting background Redis loading process on '$host'.."
  s4m_log "  (Output log: $host:$S4MWEB_SINGLECELL_PATH/../log/s4mweb_load_iscandar_$dsid.log)"
  ssh "$host" "mkdir -p $S4MWEB_SINGLECELL_PATH/../log"


  remote_debug=""
  if s4m_isdebug; then
    remote_debug="S4M_DEBUG=TRUE; export S4M_DEBUG;"
  fi
  s4m_debug "Calling: [ssh $host  nohup '$SHELL' -c \"$remote_debug $redis_script $redis_script_args > $S4MWEB_SINGLECELL_PATH/../log/s4mweb_load_iscandar_$dsid.log 2>&1 &\"]"
  ssh "$host"  nohup '$SHELL' -c "$remote_debug $redis_script $redis_script_args > $S4MWEB_SINGLECELL_PATH/../log/s4mweb_load_iscandar_$dsid.log 2>&1 &"
  if [ $? -ne 0 ]; then
    s4m_error "Failed early in SSH call to ISCANDAR Redis loading!"
    return 1
  fi

  s4m_log "[ OK ]"
  return 0
}


## New upload routine as of Stemformatics v7.1 - we no longer upload HTML files
## and data, we upload the underlying data TSVs instead
##
s4mweb_do_upload_reports_iscandar () {
  ## The folder containing "expression.txt", "clusters.txt", "genesets.txt"
  input_folder="$1"
  dsid="$2"
  host="$3"

  s4m_log "Validating ISCANDAR input data.."
  if ! s4mweb_validate_iscandar_data "$input_folder"; then
    s4m_error "Failed input validation"
    return 1
  fi
  s4m_log "[ OK ]"

  ## Uses 'scp' to transfer ISCANDAR report to defined reports location on server
  if ! s4mweb_push_iscandar_data "$input_folder" "$dsid" "$host"; then
    return 1
  fi

  ## If asked to reload SQL, remove existing report metadata first
  if [ "$stemformatics_web_reload" = "true" ]; then
    s4m_log "Removing existing ISCANDAR report metadata (reload requested) .."
    sql_delete="delete from dataset_metadata where ds_id=$dsid and ds_name in ('ShowIscandarLinksOnDatasetSummaryPage','use_dataset_specific_iscandar_colours')";
    if ! s4mweb_do_psql "$host" "$sql_delete"; then
      return 1
    fi
  fi
  ## Determine sample group name from input data (required for URI)
  default_sample_group=`head -1 "$input_folder/samples.txt" | cut -f 2`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to determine default sample group name for visualisation from '$input_folder/samples.txt!'"
    return 1
  fi
  sql_insert="insert into dataset_metadata (ds_id, ds_name, ds_value) values ($dsid, 'ShowIscandarLinksOnDatasetSummaryPage', '{\"name\":\"Data Visualization\",\"url\":\"&plotType=pca&plotBy=sample_group&sample_group=$default_sample_group\"}');"

  ## If custom colours supplied in any *Items.txt files, set override
  ## Update: Commenting this behaviour out for now - we almost never want the custom colours,
  ## so the default behaviour should be to never override.
#  head -1 "$input_folder/"*Items.txt | grep colour > /dev/null 2>&1
#  if [ $? -eq 0 ]; then
#    s4m_log "Custom colours provided, will apply override in dataset metadata.."
#    sql_insert="$sql_insert
#insert into dataset_metadata (ds_id, ds_name, ds_value) values ($dsid, 'use_dataset_specific_iscandar_colours', 'true');"
#  fi

  ## If no topDifferentiallyExpressedGenes, set to 'GAPDH' (Ensembl ID) otherwise ISCANDAR will not work
  topDiffGenes=`s4mweb_do_psql "$host" "copy (select ds_value from dataset_metadata where ds_id=$dsid and ds_name='topDifferentiallyExpressedGenes') to stdout" TRUE 2>/dev/null`
  ## Figure out whether human or mouse so we can grab the right gene
  species=`s4mweb_do_psql "$host" "copy (select ds_value from dataset_metadata where ds_id=$dsid and ds_name='Organism') to stdout" TRUE 2>/dev/null`

  s4m_debug "Got species=[$species] (Organism from dataset_metadata)"

  gapdh=""
  if [ ! -z "$species" ]; then
    if [ "$species" = "Homo sapiens" ]; then
      gapdh="GAPDH"
    elif [ "$species" = "Mus musculus" ]; then
      gapdh="Gapdh"
    else
      s4m_log "  Failed to determine species for dataset $dsid (for default gene selection in ISCANDAR)"
    fi
  fi
  if [ -z "$topDiffGenes" -o "$topDiffGenes" = "NULL" -a ! -z "$gapdh" ]; then
    sql_insert="$sql_insert
update dataset_metadata set ds_value=(select gene_id from genome_annotations where db_id=(select db_id from datasets where id=$dsid) and associated_gene_name='$gapdh') where ds_id=$dsid and ds_name='topDifferentiallyExpressedGenes';"
  else
    s4m_log "  Skipping update of topDifferentiallyExpressedGenes"
  fi

  s4m_log "Applying ISCANDAR SQL to Stemformatics database.."
  if ! s4mweb_do_psql "$host" "$sql_insert"; then
    return 1
  fi

  ## Execute Redis expression loading (*** for custom genes only ***) if expression.txt supplied
  if [ -f "$input_folder/expression.txt" ]; then
    s4m_log "Found 'expression.txt' - loading ISCANDAR expression values into Redis"
    s4m_log "  NOTE: This can take 30 mins or more!"
    ## Calls a script installed on the web server to do Redis loading (background process,
    ## so this call returns immediately)
    if ! s4mweb_load_iscandar_redis "$host" "$dsid"; then
      return 1
    fi
    s4m_log "Done adding ISCANDAR to Stemformatics server '$host'"
  fi

  return 0
}


## Upload Glimma reports
##
## NOTE: Assumes we've already checked SSH access to host using
## 	"s4mweb_check_ssh_access_to_target()"
##
s4mweb_do_upload_reports_glimma () {
  input_folder="$1"
  dsid="$2"
  host="$3"

  glimma_html=`s4mweb_check_find_glimma_HTML_files "$input_folder"`
  if [ $? -ne 0 ]; then
    return 1
  fi

  s4m_debug "Found Glimma HTML files:
$glimma_html
"
  ## Get unique parent directory paths (dirname) of HTML files
  ## so we can use to build further paths e.g. to JS files
  ## to extract DEG comparison metadata
  glimma_dirs=""
  for ghtml in $glimma_html
  do
    gd=`dirname "$ghtml"`
    if [ -z "$glimma_dirs" ]; then
      glimma_dirs="$gd"
    else
      glimma_dirs="$glimma_dirs
$gd"
    fi
  done

  glimma_dirs=`echo "$glimma_dirs" | tr " " "\n" | sort -u`
  s4m_log "Found Glimma directories (containing HTML reports):
[$glimma_dirs]"
  sleep 3

  ## Transfer files and add SQL to Stemformatics database
  ## to set up Glimma reports in Stemformatics front-end
  for glimmadir in `echo "$glimma_dirs"`
  do
    if [ -z "$glimmadir" ]; then
      continue
    fi
    if [ ! -f "$input_folder/$glimmadir/js/volcano.js" ]; then
      s4m_error "Failed to find Glimma asset '$input_folder/$glimmadir/js/volcano.js' (required for metadata extraction)"
      return 1
    fi
    ## Extract comparison name from Glimma report itself (to use in frontend
    ## reports) since we can't trust that Glimma directory names will be fit
    ## for purpose.
    glimma_comparison=`grep -P "\"title\"\:\"Volcano plot\: [A-Za-z0-9]+" "$input_folder/$glimmadir/js/volcano.js" | 
sed -r -e 's|^.*Volcano plot\: ([^\"]+)\".*$|\1|' -e 's|\-| vs |' 2>/dev/null`

    ## Uses 'scp' to transfer glimma report to defined reports location on server
    if ! s4mweb_push_glimma_report "$input_folder/$glimmadir" "$dsid" "$host"; then
      return 1
    fi

    ## Insert/update dataset_metadata report SQL ..

    meta_volcano="{\"name\":\"DEG $glimma_comparison Volcano Plot\",\"url\":\"/glimma/$glimmadir/volcano.html\"}"
    meta_MD="{\"name\":\"DEG $glimma_comparison MD Plot\",\"url\":\"/glimma/$glimmadir/MD-Plot.html\"}"
    ## Build SQL queries for report listings on Stemformatics web front-end

    ## If asked to reload SQL, remove existing Glimma report metadata first
    if [ "$stemformatics_web_reload" = "true" ]; then
      s4m_log "Removing existing Glimma report metadata (reload requested) .."
      sql_delete="delete from dataset_metadata where ds_id=$dsid and ds_name='ShowGlimmaLinksOnDatasetSummaryPage' and (ds_value like '%Volcano%' or ds_value like '%MD Plot%') and ds_value like '% $glimma_comparison %'";
      if ! s4mweb_do_psql "$host" "$sql_delete"; then
        return 1
      fi
    fi

    sql_insert="insert into dataset_metadata (ds_id, ds_name, ds_value) values ($dsid,'ShowGlimmaLinksOnDatasetSummaryPage','$meta_volcano'); insert into dataset_metadata (ds_id, ds_name, ds_value) values ($dsid,'ShowGlimmaLinksOnDatasetSummaryPage','$meta_MD');"
    s4m_log "Inserting report SQL into Stemformatics database for comparison '$glimma_comparison' .."
    if ! s4mweb_do_psql "$host" "$sql_insert"; then
      return 1
    fi
  done

  s4m_log "Done adding Glimma reports to Stemformatics server '$host'"
  return 0
}


## Return paths to Glimma Volcano and MD-plot HTML reports
##
s4mweb_check_find_glimma_HTML_files () {
  check_folder="$1"
  glimma_html=`cd "$check_folder" && find -name '*.html'`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to find any Glimma HTML reports under path '$check_folder'"
    return 1
  fi
  echo "$glimma_html"
}


## Upload PCA data from a folder i.e. input folder is the one that has directory
## structure like:
##
##|-- CPM_log2_pca
##|   |-- metadata.tsv
##|   |-- pca.html
##|   |-- pca.tsv
##|   `-- screetable.tsv
##
## In the above example, the PCA will show up in the front-end with label "CPM_log2"
##
## The above files are output of "pca_reports" module.
##
s4mweb_do_upload_reports_pca () {
  input_folder="$1"
  dsid="$2"
  host="$3"

  if [ ! -f $input_folder/pca.tsv -o ! -f $input_folder/metadata.tsv -o ! -f $input_folder/screetable.tsv ]; then
    s4m_error "Input path '$input_folder' does not contain one or more required files, which are:

pca.tsv
metadata.tsv
screetable.tsv
"
    return 1
  fi

  ## Upload files
  if ! s4mweb_push_pca_data "$input_folder" "$dsid" "$host"; then
    return 1
  fi

  ## Add metadata entry to enable on dataset summary page
  pca_name=`basename "$input_folder" | tr "_" " " | sed -r -e 's| *pca$||g'`
  ## If asked to reload SQL, remove existing PCA report metadata first
  if [ "$stemformatics_web_reload" = "true" ]; then
    s4m_log "Removing existing PCA report metadata (reload requested) .."
    sql_delete="delete from dataset_metadata where ds_id=$dsid and ds_name='ShowPCALinksOnDatasetSummaryPage' and ds_value like '%name\":\"%$pca_name%'";
    if ! s4mweb_do_psql "$host" "$sql_delete"; then
      return 1
    fi
  fi
  ## Insert new record
  pca_name_no_spaces=`echo "$pca_name" | tr " " "_"`
  meta_pca="{\"name\":\"$pca_name\", \"url\":\"/datasets/pca?ds_id=$dsid&pca_type=$pca_name_no_spaces\"}"
  sql_insert="insert into dataset_metadata (ds_id, ds_name, ds_value) values ($dsid,'ShowPCALinksOnDatasetSummaryPage','$meta_pca');"
  s4m_log "Inserting PCA SQL into Stemformatics database for '$pca_name' .."
  if ! s4mweb_do_psql "$host" "$sql_insert"; then
    return 1
  fi

  s4m_log "Done adding PCA data to Stemformatics server '$host'"
  return 0
}  


## Upload PCA data files (TSV) to correct location on Stemformatics web server
##
s4mweb_push_pca_data () {
  folder="$1"
  dsid="$2"
  host="$3"

  pca_path="$S4MWEB_PCA_PATH/$dsid"
  fbase=`basename "$folder" | sed -r -e 's|[ _]+pca$||'`
  pca_mkdir=`ssh "$host"  mkdir -p "$pca_path/$fbase" 2>&1`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to create PCA data path '$host:$pca_path/$fbase':

    $pca_mkdir
"
    return 1
  fi

  s4m_log "Transferring input PCA folder '$fbase':
    '$folder' -->
    '$host:$pca_path/'"
  push_scp=`scp -r "$folder"/*tsv  "$host:$pca_path/$fbase/" 2>&1`
  if [ $? -ne 0 ]; then
    s4m_error "[ FAIL ] Failed to upload PCA data!

    $push_scp
"
    return 1
  else
    s4m_log "[ OK ]"
  fi
  return 0
}

