#!/bin/sh
## test_stemformatics_web: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./inc/stemformatics_web_funcs.sh

  S4MWEB_TEST_DATADIR="./etc/tests"
  export S4MWEB_TEST_DATADIR


  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests (uncomment if needed)
#setUp () {
#}


## TESTS ##

## Test that required binary dependencies exist.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  msg="Binary dependencies not satisfied!"
  assertEquals "$msg" 0 $?
}

## Some env vars that should be set on a Stemformatics pipeline server
testEnvironment () {
  ## Do a loop so we can test multiple vars if needed
  vars="S4M_HOME
"
  ret=0
  for v in $vars
  do
    eval test ! -z \$$v
    if [ $? -ne 0 ]; then
      ret=1
    fi
  done
  msg="One or more expected environment variables not set!"
  assertEquals "$msg" 0 $ret
}

testRequiredPaths () {
  test -d $S4M_HOME/../../../s4m_passwords
  msg="The required 's4m_passwords' Git repository not found!"
  assertEquals 0 $?
}

testValidReportTypesGood () {
  report_types="Glimma
glimma
ISCANDAR
iscandar
PCA
pca"
  msg="Failed to validate valid report types!"
  for rt in $report_types
  do
    s4mweb_validate_report_type "$rt" 2> /dev/null
    assertEquals "$msg" 0 $ret
  done
}

testValidReportTypesBad () {
  report_types="foo
bar"
  msg="Failed to recognise invalid report types!"
  for rt in $report_types
  do
    s4mweb_validate_report_type "$rt" 2> /dev/null
    assertEquals "$msg" 1 $?
  done
}

testValidDatasetIDsGood () {
  dsids="1234
9999
12345"
  msg="Failed to validate good dataset ID formats!"
  for dsid in $dsids
  do
    s4mweb_validate_dsid_format "$dsid" 2> /dev/null
    assertEquals "$msg" 0 $?
  done
}

testValidDatasetIDsBad () {
  dsids="123
123A
ABCD
"
  msg="Failed to recognise invalid dataset ID formats!"
  for dsid in $dsids
  do
    s4mweb_validate_dsid_format "$dsid" 2> /dev/null
    assertEquals "$msg" 1 $?
  done
}

testIscandarDataFilesGood () {
  test_inputdir="$S4MWEB_TEST_DATADIR/validInputs"

  msg="Failed to validate good ISCANDAR inputs!"
  s4mweb_validate_iscandar_data "$test_inputdir" 2>/dev/null
  assertEquals "$msg" 0 $?
}

testIscandarDataFilesBadMissingVizData () {
  test_inputdir="$S4MWEB_TEST_DATADIR/invalidInputsMissingVizData"

  msg="Failed to recognise invalid ISCANDAR inputs (missing clustering viz data)!"
  s4mweb_validate_iscandar_data "$test_inputdir" 2>/dev/null
  assertEquals "$msg" 1 $?
}

testIscandarDataFilesBadMissingSampleGroupItems () {
  test_inputdir="$S4MWEB_TEST_DATADIR/invalidInputsMissingSampleGroupItems"

  msg="Failed to recognise invalid ISCANDAR inputs (missing sampleGroupItems)!"
  s4mweb_validate_iscandar_data "$test_inputdir" 2>/dev/null
  assertEquals "$msg" 1 $?
}

## TODO.. more
