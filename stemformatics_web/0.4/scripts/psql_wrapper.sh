#!/bin/sh
## Wrapper for psql client connection to Stemformatics PostgreSQL DB instances.
## NOTE: Adapted version of original script from 'stemformatics_tools' Git repo.
##
## Author: O.Korn
HOST="$1"
HOSTALIAS="$2"
PORT="$3"
SQL_OR_CMD="$4"


## Get parsed / var replaced version of input file (taking multiple %VAR%=<value> args
## for token replacement).
ReplaceTokens () {
  template="$1"
  shift

  sed_cmd="cat $template | sed -r "
  sed_opts=""

  while [ ! -z "$1" ];
  do
    ## Escape any ampersands in value (twice because it runs through 'eval')
    ## Note: It is not expected that there are any backslashes in values.
    kv=`echo "$1" | sed -e 's|&|\\\\\&|g'`
    key=`echo "$kv" | cut -d'=' -f 1`
    ## Need to escape some special chars in replace string
    val=`echo "$kv" | cut -d'=' -f 2 | tr -d "'"`
    sed_opts="$sed_opts -e 's|$key|$val|g'"
    shift
  done

  ## DEBUG
  #echo "[eval $sed_cmd $sed_opts]"

  eval "$sed_cmd $sed_opts"
}

## Replace tokens in target file from key/val dictionary (.ini file)
function ReplaceTokensInFileFromINI {
  replacetarget="$1"
  keyvalini="$2"

  replace_spec=""
  ## iterate over key/val pairs to build replacement spec
  while read line
  do
    echo "$line" | grep -P "^#" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      continue
    fi
    ## if "a = b" then trim spaces around the equal sign
    line=`echo "$line" | sed -r -e 's|\s+\=\s+|\=|'`
    ## extract key and value
    srckey=`echo "$line" | cut -d'=' -f 1`
    srcval=`echo "$line" | cut -d'=' -f 2-`
    replace_spec="$replace_spec %$srckey%='$srcval'"
  done < "$keyvalini"

  #echo "DEBUG: ReplaceTokens $replacetarget $replace_spec > $replacetarget.new" 1>&2

  ReplaceTokens "$replacetarget" $replace_spec > "$replacetarget.new"
  mv "$replacetarget.new" "$replacetarget"
}



## User's pgpass file for this host
PGPASSFILE="$HOME/.pgpass_${HOSTALIAS}_$USER"
## Must export for 'psql' tool to use
export PGPASSFILE
if [ -z "$S4M_PROJECT_LABEL" ]; then
  export S4M_PROJECT_LABEL="s4m"
fi
## s4m_passwords repo path
s4m_passwords="$S4M_HOME/../../../${S4M_PROJECT_LABEL}_passwords"


## No .pgpass for target host, create it and populate with DBUSER and DBPASSWORD
## from passwords repo
if [ ! -d "$s4m_passwords" ]; then
  echo "Error: Failed to determine path to 's4m_passwords' repository needed for .pgpass setup
(for psql_wrapper.sh host instance script usage)!
" 1>&2;
  exit 1

else
  if [ ! -f "$s4m_passwords/passwords.ini" ]; then
    echo "Error: Could not find master password .ini file in location '$s4m_passwords/passwords.ini'"
    exit 1
  fi

  /bin/echo -n "$HOST:5432:portal_prod:%DBUSER%:%DBPASSWORD%" > "$PGPASSFILE"

  ReplaceTokensInFileFromINI "$PGPASSFILE" "$s4m_passwords/passwords.ini"
  ret=$?
  if [ $ret -ne 0 ]; then
    echo "Error: Failed to set up '$PGPASSFILE' - cannot load DB settings"
    exit $ret
  fi
fi


## .pgpass file must be chmod 600 or psql won't load the pgpass file
chmod 600 "$PGPASSFILE"

DBUSER=`cut -d':' -f 4 "$PGPASSFILE"`


## Load a table from file
if [ "$SQL_OR_CMD" = "load" ]; then
  table="$5"
  sqlfile="$6"
  nullflag="$7"
  nullcmd="NULL AS ''"
  if [ ! -z "$nullflag" ]; then
    if [ "$nullflag" = "-nonull" ]; then
      nullcmd=""
    fi
  fi
  if [ -f "$sqlfile" ]; then
    cat "$sqlfile" | psql -h $HOST -p $PORT -U $DBUSER portal_prod -c "COPY $table FROM STDIN $nullcmd;"
  else
    echo "Error: SQL data file '$sqlfile' not found!"; echo
  fi

## Execute arbitrary SQL as string or from file
else
  if [ -f "$SQL_OR_CMD" ]; then
    cat "$SQL_OR_CMD" | psql -h $HOST -p $PORT -U $DBUSER portal_prod
  else
    echo "$SQL_OR_CMD" | psql -h $HOST -p $PORT -U $DBUSER portal_prod
  fi
fi
