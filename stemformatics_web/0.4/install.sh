#!/bin/sh
## NOTE: This module assumes running in a standard Stemformatics pipeline environment
## and is targeted at dataset processors for the www.stemformatics.org project
##
## *** NOTE: This script performs rudimentary checking of required operating environment /
## software and no "installation" per-se is required ***
##

## Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## Check for "s4m.sh" tool
if [ -z "$S4M_HOME" ]; then
  s4m_error "No 'S4M_HOME' (s4m.sh path) in environment!"
  exit 1
fi

## We need the s4m_passwords Git repo as well
if [ ! -d $S4M_HOME/../../../s4m_passwords ]; then
  s4m_error "Could not find 's4m_passwords' repo in path '$S4M_HOME/../../../s4m_passwords'!"
  exit 1
fi
