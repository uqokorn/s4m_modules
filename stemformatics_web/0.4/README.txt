
This module expects to be run on a Stemformatics "pipeline" server.

A server with this config has an environment that (as of Nov 2017) was
setup by script/s in the "pipeline_deploy" Git repository of the
Stemformatics project.

It would be difficult if not impossible to run on an non pipeline server
without a lot of work to bring required elements into this S4M module.

-Othmar
