#!/bin/sh
# Module: stemformatics_web
# Author: O.Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

. ./inc/stemformatics_web_funcs.sh


## Handler for "upload-reports" command
s4mweb_upload_reports_handler () {
  if [ ! -d "$stemformatics_web_folder" ]; then
    s4m_error "Input report folder '$stemformatics_web_folder' not found!"
    return 1
  fi
  if ! s4mweb_validate_report_type "$stemformatics_web_type"; then
    return 1
  fi
  if ! s4mweb_validate_dsid_format "$stemformatics_web_dsid"; then
    return 1
  fi

  if ! s4mweb_check_ssh_access_to_target "$stemformatics_web_host"; then
    return 1
  fi
 
  ## Upload files and insert required SQL 
  s4mweb_do_upload_reports "$stemformatics_web_folder" "$stemformatics_web_type" "$stemformatics_web_dsid" "$stemformatics_web_host"
  return $?
}


## Handler for "ssh-test" command
s4mweb_ssh_test_handler () {
  ## Implicit "TRUE" debug flag to enable output messages for the
  ## "ssh-test" command (not used normally)
  s4mweb_check_ssh_access_to_target "$stemformatics_web_host" TRUE
  return $?
}



main () {

  ## Import other module functions etc  
  #s4m_import othermodule/script.sh

  s4m_route upload-reports	s4mweb_upload_reports_handler
  s4m_route ssh-test		s4mweb_ssh_test_handler

}

### START ###
main

