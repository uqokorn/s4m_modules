## Temporary buffer for target commands to pipeline
S4M_PIPELINE_CMDS=""; export S4M_PIPELINE_CMDS


## Execute line by line, test for non-zero status and break if error.
## Report on how many steps were completed vs. total.
##
## NOTE: Capture of STDOUT/STDERR not implemented (to fix, use indirection instead of piping
##       of input commands)
## NOTE: Line number corresponds to post-range and exclude filter application (if any)
##
## Unit Test/s:
##	- testLoopsAndConditionalParsing
##	- testBadIfStatementSyntax
##	- testBadLoopSyntax
##      - testMultiLineCommands
##
run_commands () {
  #stdout="$1"
  #stderr="$2"

  lintmode="$pipeline_lint"
  linecount=0
  in_loop=0
  in_if=0
  ## track nesting of if-clauses
  if_counter=0
  start_loop_parse=0
  start_if_parse=0
  loop_cmd=""
  if_cmd=""

  ## Read lines (don't parse backslash escapes [read -r])
  echo "$S4M_PIPELINE_CMDS" |
  while read -r line
  do
    linecount=`expr $linecount + 1`
    ## skip comment line or blank line
    echo "$line" | grep -P "^ *#" > /dev/null 2>&1
    if [ $? -eq 0 -o -z "$line" ]; then
      continue
    fi
    ## remove trailing/leading whitespace
    line=`echo "$line" | sed -r -e 's|^\s+||' -e 's|\s+$||'`
    ## double up on existing backslash escapes before passing into eval
    s4m_debug "Input line before backslash replacement = [$line]" 3
    line=`echo "$line" | sed -r -e 's|\\\\|\\\\\\\\|g'`

    ## In loop but no "do" keyword so far, check we have it in this line
    if [ $in_loop -eq 1 -a $start_loop_parse -eq 0 ]; then
      echo "$line" | grep -P "^ *do\s*" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        ## add missing "do" and continue
        loop_cmd="$loop_cmd ; do"
        start_loop_parse=1
        continue
      else
        s4m_error "Pipeline encountered malformed syntax in 'for' or 'while' statement near line $linecount:

  cmd>  $loop_cmd

"
        if [ "$lintmode" = "true" ]; then
          s4m_error "*** PIPELINE SYNTAX [ FAIL ] ***"
        fi
        return 1
      fi

    ## start appending within-loop lines (comma-separated) up to a "done" keyword
    elif [ $in_loop -eq 1 -a $start_loop_parse -eq 1 ]; then
      ## Make sure we don't append a ";" after "do", "then", "else" keyword, or if line already ends with ";"
      ## (Ensure that we can deal with "if..then..else" statements spanning multiple lines within our loops
      echo "$loop_cmd" | tail | grep -P "(\bdo *|\bthen *|\belse *|\; *)$" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        loop_cmd="$loop_cmd $line"
      else
        loop_cmd="$loop_cmd ; $line"
      fi
      echo "$line" | grep -P "\bdone\b" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        in_loop=0
        start_loop_parse=0
        if [ "$lintmode" != "true" ]; then
          ## Run completed loop command
          s4m_debug "eval CMD=[$loop_cmd]" 2
          ## NOTE: If command fails, the shell exits and we can't (?) intercept and print a better error message,
          ##  so we're not bothering to check "$?" after the eval call.
          ## PORTABILITY: Is "set -e" a valid Bourne expression??
          ##  Seems to be in POSIX (XSI): http://pubs.opengroup.org/onlinepubs/009696799/utilities/set.html
          set -e
          eval "$loop_cmd"
          s4m_debug "eval return value = [$?]" 2
          set +e
        fi
      fi
      continue

    ## Not in a loop, not in an if-statement
    elif [ $in_loop -eq 0 -a $in_if -eq 0 ]; then 

      ## Process a "while" or "for" statement (comma-separate all lines)
      echo "$line" | grep -P "^ *(while|for)\s" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        ## need this so we can run the loop when finished parsing
        loop_cmd="$line"
        in_loop=1
        ## is the "do" keyword already provided?
        echo "$line" | grep -P "^ *(while|for).*do\s*$" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
          start_loop_parse=1
        fi
        continue
      fi

      ## Process "if" statements that are outside of loops
      echo "$line" | grep -P "^ *if\s+" > /dev/null 2>&1 
      if [ $? -eq 0 ]; then
        ## need this so we can run the loop when finished parsing
        if_cmd="$line"
        ## Do we have a "one liner"? If yes, we can execute the entire statement.
        if echo "$if_cmd" | grep -P "fi\s*$" > /dev/null 2>&1; then
          if [ "$lintmode" != "true" ]; then
            ## Run completed if-statement
            s4m_debug "eval CMD=[$if_cmd]" 2
            set -e
            eval "$if_cmd"
            s4m_debug "eval return value = [$?]" 2
            set +e
          fi
          continue
        fi
        ## .. otherwise, we probably have a multi-line "if" statement
        in_if=1
        if_counter=`expr $if_counter + 1`
        ## is the "then" keyword already provided?
        echo "$line" | grep -P "^ *if\s+.*then\s*$" > /dev/null 2>&1
        if [ $? -eq 0 ]; then
          start_if_parse=1
        fi
        continue
      fi

    ## In if-statement but no "then" keyword so far, check we have it in this line
    elif [ $in_if -eq 1 -a $start_if_parse -eq 0 ]; then
      echo "$line" | grep -P "^ *then\s*" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        ## add missing "then" and continue
        if_cmd="$if_cmd ; then"
        start_if_parse=1
        continue
      else
        s4m_error "Pipeline encountered malformed syntax in 'if' statement near line $linecount:

  cmd>  $if_cmd

"
        if [ "$lintmode" = "true" ]; then
          s4m_error "*** PIPELINE SYNTAX [ FAIL ] ***"
        fi
        return 1
      fi

    ## Start appending within-if-statement lines (comma-separated) until we're done parsing "if" block
    elif [ $in_if -eq 1 -a $start_if_parse -eq 1 ]; then
      ## Make sure we don't append a ";" after "then", "else" keyword, or if line already ends with ";"
      echo "$if_cmd" | tail | grep -P "(\bthen *|\belse *|\; *)$" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        if_cmd="$if_cmd $line"
      else
        if_cmd="$if_cmd ; $line"
      fi
      echo "$line" | grep -P "\bif\b" > /dev/null 2>&1
      ## While building top-level "if" block, we found another inner one
      if [ $? -eq 0 ]; then
        if_counter=`expr $if_counter + 1`
      fi
      ## An if-clause is ending
      echo "$line" | grep -P "\bfi\b" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        if_counter=`expr $if_counter - 1`
      fi
      ## When counter reaches zero, we now have a full top-level if-block to run
      if [ $if_counter -eq 0 ]; then
        in_if=0
        start_if_parse=0
        if [ "$lintmode" != "true" ]; then
          ## Run completed if-statement
          s4m_debug "eval CMD=[$if_cmd]" 2
          set -e
          eval "$if_cmd"
          s4m_debug "eval return value = [$?]" 2
          set +e
        fi
      fi
      continue
    fi 

    ## *** If we made it here, we're a normal non-loop, non-if command ***

    ## Is command an S4M command?
    ## Currently don't need (hopefully) to treat different to a non-S4M command,
    ## but if we ever do, can test for it using something like the below:

    #cmd=`echo "$line" | sed 's|^ +||' | cut -d' ' -f 1`
    #echo "$cmd" | grep -P "^ *($S4M_SCRIPT_NAME|s4m_exec)" > /dev/null 2>&1
    #if [ $? -eq 0 ]; then
    ## other shell command
    #else
    #fi

    ## Run command with STDOUT or STDERR redirects if necessary
    #if [ ! -z "$stdout" -a ! -z "$stderr" ]; then
    #  eval "$line"  > "$stdout" 2> "$stderr"
    #elif [ ! -z "$stdout" ]; then
    #  eval "$line"  > "$stdout"
    #elif [ ! -z "$stderr" ]; then
    #  eval "$line"  2> "$stderr"
    #else

    s4m_debug "eval CMD=[$line]" 2

    if [ "$lintmode" != "true" ]; then
      eval "$line"
      ret=$?
      s4m_debug "eval return value = [$ret]" 2
      ## If an error occurred, exit and report line number of command which failed
      if [ $ret -ne 0 ]; then
        s4m_error "Pipeline command on line $linecount (post-filtering) exited with non-zero status:

  cmd>  $line
"
        return 1
      fi
    fi
  done
}


## Pre-parser to check that the input pipeline is compliant
##
## Unit Test/s:
##	testValidationShouldFailOnNestedLoop
##	testValidationShouldFailOnLineContinuation
##
validate_loaded_command_file () {
  if [ -z "$S4M_PIPELINE_CMDS" ]; then
    s4m_error "No pipeline commands loaded, cannot run validation!"
    return 1
  fi

  ### NESTED LOOP VALIDATION ###

  nested_loop=0
  ## replace all spaces with newlines, retain only (while|for|do|done) so we can check the order
  ## in which the keywords appear
  loop_keywords=`echo "$S4M_PIPELINE_CMDS" | grep -Pv "^#" | sed -r -e 's|^\s+||g' | grep -e "^\(while\|for\)\s\+" -e "^do\s*$" -e "^while\s\+.*\;\s\+do\s*$" -e "^\s*done\s\+" | sed -r -e 's|\s|\n|g' | tr -d ';\"\|\`' | grep -P "^(while|for|do|done)$" | tr "\n" "\t"`
  echo "$loop_keywords" | grep -P "(while|for)\s+do\s+(while|for)" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    nested_loop=1
  fi

  if [ $nested_loop -eq 1 ]; then
    s4m_error "Sorry, nested loops in pipeline commands are disallowed!"
    if [ "$pipeline_lint" = "true" ]; then
      s4m_error "*** PIPELINE SYNTAX [ FAIL ] ***"
    fi
    return 1
  fi

  ### LINE CONTINUATIONS DISALLOWED ###

  ## i.e. a pipeline line cannot end with backslash
  echo "$S4M_PIPELINE_CMDS" | grep -P '\\$' > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    s4m_error "Sorry, line continuations (line wrapping with backslash escapes) are disallowed!"
    if [ "$pipeline_lint" = "true" ]; then
      s4m_error "*** PIPELINE SYNTAX [ FAIL ] ***"
    fi
    return 1
  fi
}


## Write post-parsed pipeline command file
##
save_command_file () {
  outfilepath="$1"

  if [ -z "$S4M_PIPELINE_CMDS" ]; then
    s4m_error "Can't save post-parsed pipeline file, no pipeline commands loaded!"
    return 1
  fi
  if [ -z "$outfilepath" ]; then
    s4m_error "No file path given to write post-parsed pipeline file!"
    return 1
  fi
  if [ -d "$outfilepath" ]; then
    s4m_error "The given post-parsed output file path must be a valid file path!"
    return 1
  fi
  if [ ! -d `dirname "$outfilepath"` ]; then
    s4m_error "The directory path for post-parsed output file does not exist!"
    return 1
  fi

  echo "$S4M_PIPELINE_CMDS" > "$outfilepath"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to write post-parsed pipeline file!"
    return 1
  else
    s4m_log "Saved post-parsed pipeline file to [$outfilepath]"
  fi
}


## Get parsed / var replaced version of template (taking multiple %VAR%=<value> args
## for token replacement).
##
replace_keyvals_stdin () {

  sed_cmd="cat | sed -r "
  sed_opts=""

  while [ ! -z "$1" ];
  do
    kv="$1"
    key=`echo "$kv" | cut -d'=' -f 1`
    val=`echo "$kv" | cut -d'=' -f 2`
    sed_opts="$sed_opts -e \"s|$key|$val|g\""
    shift
  done

  eval "$sed_cmd $sed_opts"
}


## Parse command lines from STDIN.
## Saves as a temp file and runs default "from file" load routine.
##
load_command_file_stdin () {
  start="$1"
  end="$2"
  exclude_regex="$3"
  keepheader="$4"

  lcfs_ts=`date +"$S4M_TS_FORMAT"`
  ## dump STDIN to temp file
  pipeline_tmp="$S4M_TMP/pipeline_stdin.$$.$lcfs_ts"
  cat > "$pipeline_tmp"

  load_command_file_default "$pipeline_tmp" "$start" "$end" "$exclude_regex" "$keepheader"
  return $?
}


## Print header to STDOUT
## Returns: Numerical index of last header line
##
extract_command_file_header () {
  cmdfile="$1"
  first_module_cmd_index=`cat "$cmdfile" | grep -n -P "^$S4M_SCRIPT_NAME" | cut -d':' -f 1 | head -1  2> /dev/null`
  if [ $? -ne 0 -o -z "$first_module_cmd_index" ]; then
    s4m_warn "No module commands in target pipeline and 'keep header' flag given, retaining ALL input lines"
    cat "$cmdfile"
    last_cmdfile_line_index=`cat "$cmdfile" | wc -l`
    return $last_cmdfile_line_index
  else
    max_header_index=`expr $first_module_cmd_index - 1`
    sed -n "1,$max_header_index p" "$cmdfile"
    return $max_header_index
  fi
}


## Parse a command file from file and return contents, or an error status.
## Assumes "start" is less than or equal to "end" (if both supplied)
##
load_command_file_default () {
  cmdfile="$1"
  start="$2"
  end="$3"
  exclude_regex="$4"
  keepheader="$5"

  if [ -z "$start" ]; then
    start=1
  fi
  ## We'll be passing this to 'sed' so set to EOF now.
  if [ -z "$end" -o "$end" = "-" ]; then
    end='$'
  fi

  ## The subset of command file we'll keep after all filtering
  cmdfile_keep_lines=""

  if [ "$keepheader" = "true" ]; then
    cmdfile_keep_lines=`extract_command_file_header "$cmdfile"`
    last_header_line=$?
    cmdfile_keep_lines="$cmdfile_keep_lines
"

    ## If keeping the header then our start line is at least
    ## the very next non-header line.
    if [ $start -le $last_header_line ]; then
      #s4m_debug "load_command_file_default(): start $start <= $last_header_line : setting to $last_header_line + 1"
      start=`expr $last_header_line + 1`
    fi
  fi

  ## Append command file body, and regex filter body if needed
  cmdfile_body=`sed -n "$start,$end p" "$cmdfile"`
  if [ -z "$exclude_regex" ]; then
    cmdfile_keep_lines="${cmdfile_keep_lines}${cmdfile_body}"
  else
    s4m_debug "Applying regex filter.."
    cmdfile_body_filtered=`echo "$cmdfile_body" | grep -v -P "$exclude_regex"`
    cmdfile_keep_lines="${cmdfile_keep_lines}${cmdfile_body_filtered}"
  fi

  ## T#2513:
  ## Remove superfluous spaces between newline disabling slashes for
  ## natural multi-line input (if we need to print out the parsed line
  ## it looks much better without the extra spaces a user might have added)

  cmdfile_keep_lines=`echo "$cmdfile_keep_lines" | sed -r -e 's| +\\\\$| \\\|g'`

  ## T#2513:
  ## Remove any indented comments (they belong to a multi-line block) -
  ## otherwise any following commands would be treated as belonging to the
  ## comment line (causing early exit or non-execution of command lines)

  cmdfile_keep_lines=`echo "$cmdfile_keep_lines" | sed -r -e '/^ +\#+.*$/d'`
  
  if [ -z "$cmdfile_keep_lines" ]; then
    s4m_error "Post-parsed command file '$cmdfile' is empty, nothing to do!"
    return 1
  fi

  /bin/echo -n "$cmdfile_keep_lines"
}


## Check filtering options are sane (pre-validation)
## Some things can only be checked while attempting to parse the command file.
## e.g. see 'load_command_file_stdin()' and 'load_command_file_default()'
##
validate_filter_options () {
  start="$1"
  end="$2"
  exclude_regex="$3"

  echo "$start" | grep -P "^[0-9]+$" > /dev/null 2>&1
  if [ $? -ne 0 -o "$start" = "0" ]; then
    s4m_error "If supplied, pipeline starting line position must be a number >= 1"
    return 1
  fi

  if [ ! -z "$end" ]; then
    echo "$end" | grep -P "^[0-9]+$" > /dev/null 2>&1
    if [ $? -ne 0 -o "$end" = "0" ]; then
      s4m_error "If supplied, pipeline end line position must be a number >= 1 (default EOF)"
      return 1
    fi
    s4m_log "Got pipeline range filter lines $start:$end"
  fi

  if [ ! -z "$exclude_regex" ]; then
    s4m_log "Will filter out commands matching exclude pattern /$exclude_regex/ .."
  fi
}


## Load command file from file or STDIN.
##
## NOTE: Can't use "-" as STDIN file proxy due to the way S4M does its
##   argument processing, S4M core script would need an update.
##
## Unit Test/s:
##	- various (used in all cases of loading test pipelines)
##
## PORTABILITY:  grep -P
##
load_command_file () {
  cmdfile="$1"
  start="$2"
  end="$3"
  exclude_regex="$4"
  keep_header="$5"
  token_replacements="$6"

  s4m_log "Loading command file [$cmdfile].."

  if [ "$end" = "-" ]; then
    end=""
  fi
  validate_filter_options "$start" "$end" "$exclude_regex"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  cmdlines=""

  ## Process STDIN
  if [ $cmdfile = "STDIN" ]; then
    cmdlines=`load_command_file_stdin "$cmdfile" "$start" "$end" "$exclude_regex" "$keep_header"`
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi

  ## Not STDIN, and given name is not a file
  elif [ ! -f "$cmdfile" ]; then
    s4m_error "Command file '$cmdfile' not found! Check input path to pipeline file."
    return 1

  ## .. it's a file, load it
  else
    cmdlines=`load_command_file_default "$cmdfile" "$start" "$end" "$exclude_regex" "$keep_header"`
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi
  fi

  ## Replace tokens now if we need to
  echo "$token_replacements" | grep -P "[^\s]+\=[^\s]+" > /dev/null 2>&1
  if [ $? -eq 0 -a ! -z "$token_replacements" ]; then
    cmdlinesreplaced=`echo "$cmdlines" | replace_keyvals_stdin $token_replacements`
    S4M_PIPELINE_CMDS="$cmdlinesreplaced"
  else
    S4M_PIPELINE_CMDS="$cmdlines"
  fi
  export S4M_PIPELINE_CMDS

  s4m_debug "Got command file contents = [$S4M_PIPELINE_CMDS]
"

}


## Extract "valid" modules from pipeline file i.e. those modules that exist.
## NOTE: Does not check for module installability e.g. existence of module/install.sh
##
## Return list can include just "<modulename>" or "<modulename>/<vers>" format
## depending on how they were specified in the pipeline file
##
## PORTABILITY: grep -P usage
##
extract_modules_from_pipeline () {
  pipelinefile="$1"

  mod_vers=`grep -v -P "^(\s*$S4M_SCRIPT_NAME\s+start\b|${S4M_SCRIPT_NAME}_exec\s+start\b)" "$pipeline_file" | grep -P "\s*($S4M_SCRIPT_NAME|${S4M_SCRIPT_NAME}_exec)\s+[A-Za-z0-9_]+[\/0-9\.]*::" | cut -d' ' -f 2 | cut -d':' -f 1 | grep -P "\w" | sort -u`

  s4m_debug "Possible module list parsed from '$pipelinefile':
[$mod_vers]" 3

  mod_vers_parsed=""
  ## Now remove the ones that aren't actually modules
  for m in $mod_vers
  do
    ## Skip core (or pipeline) module that can never be installed
    ## TODO: This needs to be implemented as an s4m module func!!
    ##   And determine installability in terms of install.sh existence, or a new flag in module.ini
    ##   that might say whether a module can be installed?
    stopmods="_core
_selftest
pipeline"
    if s4m_is_module_version_spec "$m"; then
      mname=`s4m_extract_module "$m"`
    else
      mname="$m"
    fi
    if echo "$stopmods" | grep -P "^$mname$" > /dev/null 2>&1; then
      s4m_debug "  Module '$m' is a core module, skipping" 2
      continue
    fi
    if s4m_check_module_exists "$m" > /dev/null 2>&1; then
      mod_vers_parsed="$mod_vers_parsed
$m"
    else
      s4m_debug "  Module '$m' is not valid, skipping" 2
    fi
  done
  mod_vers_valid=`echo "$mod_vers_parsed" | grep -P "\w"`

  s4m_debug "Post-validated module list from '$pipelinefile':
[$mod_vers_valid]" 2

  echo "$mod_vers_valid"
}

