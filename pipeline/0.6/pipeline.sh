#/bin/sh
##==========================================================================
## Module: pipeline
## Author: Othmar Korn
##
## Allows a user to provide a list of S4M commands (and their args) to be
## executed in sequential fashion. Pipeline may be a text file or passed
## via STDIN.
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

## Include subread utility functions
. ./inc/pipeline_funcs.sh


## Handler for "pipeline" command
##
pipeline_handler () {
  if [ "$pipeline_lint" = "true" ]; then
    s4m_log "Lint-mode (syntax checking) ENABLED"
  fi
  load_command_file "$commandfile_in" "$startline" "$endline" "$exclude_cmd_regex" "$keep_header" "$token_replacements"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  validate_loaded_command_file
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  if [ ! -z "$commandfile_out" ]; then
    save_command_file "$commandfile_out"
    if [ $? -ne 0 ]; then
      ## Not a fatal error, don't exit
      s4m_warn "Skipped writing post-parsed pipeline file."
    fi
  fi
  if [ ! -z "$dryrun" -o "$dryrun" = "true" ]; then
    s4m_log "Dry-run mode ENABLED, no commands will be executed."
  else
    ## NOTE: Capture of STDOUT broken for nested s4m output - disabling for now.
    #run_commands "$stdoutlog" "$stderrlog"
    run_commands
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    else
      if [ "$pipeline_lint" = "true" ]; then
        s4m_log "*** PIPELINE SYNTAX [ OK ] ***"
      fi
    fi
  fi
}


## Handler for "installdeps" command
##
## PORTABILITY: grep -P usage
##
installdeps_handler () {
  if [ ! -f "$pipeline_file" ]; then
    s4m_error "Pipeline file '$pipeline_file' not found!"
    return 1
  fi

  extracted_modules=`extract_modules_from_pipeline "$pipeline_file"`

  if [ -z "$extracted_modules" ]; then
    s4m_warn "No installable modules found in '$pipeline_file', doing nothing.."
    return 0
  fi
  ## TODO: Remove possible duplicates if same module given with/without version numbers
 
  s4m_log "Attempting module installation now..
(NOTE: Any modules that do not require installation will be skipped)
"

  ret=0
  failed_modules=""
  for m in $extracted_modules
  do
    if ! s4m_is_module_version_spec "$m"; then
      ## Resolve module by specific version or newest if not specified
      modpath=`s4m_check_module_exists "$m" 2>/dev/null`
      if [ $? -ne 0 ]; then
        ret=1
        failed_modules="$failed_modules
$m"
        continue
      fi
      maxvers=`s4m_get_max_module_version "$modpath"`
      ## Replace modulename with module/version
      m="$m/$maxvers"
    fi

    ## Dry run mode - just print the install command
    if [ ! -z "$pipeline_dryrun" ]; then
      echo "s4m s4m::install $m -s"
    else
      ## '-s' to skip non-installable module
      s4m_exec s4m::install $m -s
      if [ $? -ne 0 ]; then
        ret=1
        failed_modules="$failed_modules
$m"
      fi
    fi
  done

  if [ $ret -ne 0 ]; then
    s4m_error "One or more modules failed installation, they are:
$failed_modules
"
  fi
  return $ret
}



main () {

  ## Exit immediately if command was "start"
  if [ "$S4M_MOD_CMD" = "start" ]; then
    return 0
  fi

  commandfile_in="$pipeline_commandfile"
  ## File to write post-parsed pipeline file. If not given, it is not written.
  commandfile_out="$pipeline_outfile"
  
  ## NOTE: Capture of STDOUT broken for nested s4m output - disabling for now.
  #stdoutlog="$pipeline_stdout"
  #stderrlog="$pipeline_stderr"

  dryrun="$pipeline_dryrun"

  ## Start and End filter
  startline="$pipeline_startat"
  if [ $? -ne 0 -o -z "$startline" ]; then
    startline=1
  fi
  endline="$pipeline_endat"
  if [ $? -ne 0 -o -z "$endline" ]; then
    endline="-"
  fi

  ## Regex exclude filter
  exclude_cmd_regex="$pipeline_exclude"
  if [ "$exclude_cmd_regex" = "true" ]; then
    exclude_cmd_regex=""
  fi

  ## Keep all before module commands? (Overrides regex exclusion)
  keep_header="$pipeline_keepheader"

  token_replacements="$pipeline_replacevars"

  ## Execute pipeline
  s4m_route "pipeline" "pipeline_handler"

  ## Install pipeline dependencies
  s4m_route "installdeps" "installdeps_handler"
}

### START ###
main

