
pipeline
===========

## Overview

The `pipeline` module provides the ability to run simple linear pipelines (i.e. end-to-end), from a file or otherwise, in a controlled fashion.

## What is a pipeline file?

By convention, a text file with extension `.pipeline`. A pipeline file is written like a Shell script and is interpreted by the `pipeline` module.

## Why would I use this?

Some benefits to using pipelines instead of your own custom shell script/s include:

1. Errors logged as S4M timestamped errors and guaranteed non-zero status interception
2. Error report gives line number in pipeline file that failed
3. Dry-run mode that shows the token/variable replaced pipeline file instead of running it (useful for debugging)
4. Run or re-run a pipeline in full or in part by specifying optional start and end lines to execute
5. (Not yet implemented) Automatically pre-parses pipeline file to check for any missing S4M module/versions before execution

## Pipeline file format specification

Basically the same as a Shell script (without the "shebang" line e.g. `#! /bin/sh`). See below for some rules and common pitfalls associated with pipeline syntax parsing.

Comment lines starting with `#` are skipped.

### Pipeline header

It is common to declare variables, check input/output paths etc. prior to pipeline execution. A developer may use the top section of a pipeline file for this purpose. There is no special syntax associated with this section, but only **one important caveat**: the "header" block is defined as *any* line that precedes the *first instance* of an `s4m` command. If your pipeline file doesn't use any `s4m` commands, you might use instead a special dummy command created for this purpose: `s4m pipeline::start` which does nothing but returns immediately. *NOTE:* This does *not* force the pipeline to start at that position, use the `-s` flag of the `pipeline` module to give a start line.

The concept of the header is important because a user can choose to *keep* the header *and* execute the pipeline file starting from any arbitrary non-header line - in this way, any dependencies are preserved prior to execution from the user's starting point of choice.

### Syntax rules and limitations

Due to the way that the pipeline file is parsed and lines executed, there are some things that differ from normal Shell script execution.

**Main tips**:

1. Non-control statements should be given as a single-line command i.e. do not wrap over lines (as of `pipeline v0.5`, line continuations i.e. backslash escaping of EOLs are not supported)
2. Do not place loops within an *if*-statement - see below (section *Conditional statements*)

#### Control flow statements

** Conditional statements **

Multi-line *if*-statements in pipeline file are parsed correctly both within and outside of loops. However it is currently not supported to nest a *loop* (e.g. `for` or `while` statement) within an *if*-statement (however, a future parser update will most likely remove this restriction)!

An example of valid *if*-statement syntax:

```
	if <condition>; then
		...
	else
		...
	fi
```

or

```
	if <condition>
	then
		...
	else
		...
	fi
```

It is supported to nest *if*-statements.


**Loops**

The pipeline parser re-writes `for` and `while` loops so that the inner commands are chained together on a single line. This is required because the pipeline parser cannot (at least for now) interpret pipeline code "on the run" but only supply *complete* commands to the Shell. What this means for pipeline file development is that loops should be kept as short and clean as possible. The parser expects `for` and `while` loops to be written in the usual fashion like the following examples:

```
	for x in $y; do
		...
	done
```

or

```
	for x in $y
	do
		...
	done
```

or

```
	while <condition>; do
		...
	done
```

or

```
	while <condition>
	do
		...
	done
```

For file parsing line-by-line, use indirection (do not use the form: `cat <file> | for x in $y; do` as this creates a sub-shell):

```
	while read line
	do
		...
	done <  </path/to/input.txt>
```

If your pipeline is executed under *Bash*, your indirected text could of course be read from an in-memory stream.

#### Lint mode (syntax checking)

There is a "lint" mode which can be run by supplying the `-L or --lint` flags. Some basic validation is performed regardless, but only the "lint" mode provides a comprehensive pre-validation of your input pipeline commands.


## Token replacement (pipeline templating)

It is possible to replace tokens in pipeline files at runtime. Use the `-v` flag to specify one or more token key/val pairs for replacement prior to execution. Token variable names should not contain regex or other special characters.

For example:

```
s4m pipeline::pipeline -f <template.pipeline> -v %FOO%=bar -v %BAR%=baz ...
```
(NOTE: `%FOO%` and `%BAR%` exist somewhere with the file template.pipeline - the token format here is arbitrary, you could use `__FOO__` or `#FOO#` or any other scheme that you prefer as long as characters used are not going to interfer with regex-based substitution)

When specified together with `-o` it's also a handy way of writing out the post-parsed pipeline commands for reference or debugging purposes:

```
s4m pipeline::pipeline -f template.pipeline  -v %FOO%=bar -v %BAR%=baz -o post_parsed.pipeline
```

## Help

If you have any questions, issues or suggestions please join our [Stemformatics Gitter chat](https://gitter.im/stemformatics/Lobby).

Please send email enquiries to [Othmar](mailto:othmar@stemformatics.org).

