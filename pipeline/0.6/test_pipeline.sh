#!/bin/sh
## test_pipeline.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  PIPELINE_TEST_DATADIR="./etc/tests"
  export PIPELINE_TEST_DATADIR
 
  ## Load functions being tested
  . ./inc/pipeline_funcs.sh

  ## Set up temp data / files location and populate dummy files
  PIPELINE_TEST_TMP="$S4M_TMP/$$"
  export PIPELINE_TEST_TMP
  mkdir "$PIPELINE_TEST_TMP"
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$PIPELINE_TEST_TMP"
}

## Run between tests (NA)
#setUp () {
#}


### TESTS ###

testPipelineFileLoad () {
  ## Load from line 1 to end
  load_command_file "$PIPELINE_TEST_DATADIR"/test_error.pipeline 1 -  > /dev/null 2>&1
  assertTrue "$?"
  assertFalse "[ -z $S4M_PIPELINE_CMDS ]"
}

testPipelineFileLoadError () {
  ## Load from line 1 to end
  load_command_file "$PIPELINE_TEST_DATADIR"/test.does_not_exist 1 -  2> "$PIPELINE_TEST_TMP/err.log" > /dev/null
  grep -i "not found" "$PIPELINE_TEST_TMP/err.log"  > /dev/null 2>&1
  assertTrue "$?"
}

## Assumes something ran if there is an output log. Probably not the best test.
testPipelineRun () {
  ## Load from line 1 to end
  load_command_file "$PIPELINE_TEST_DATADIR"/test_error.pipeline 1 -  > /dev/null 2>&1
  run_commands > "$PIPELINE_TEST_TMP/out.log" 2>&1
  assertTrue "[ -s $PIPELINE_TEST_TMP/out.log ]"
}

testPipelineCaptureSTDERR () {
  run_commands 2> "$PIPELINE_TEST_TMP/err.log" > /dev/null
  assertTrue "[ -s $PIPELINE_TEST_TMP/err.log ]"
}

## *** NOTE: If $PIPELINE_TEST_DATADIR/test_error.pipeline lines added or removed before line 9, this will obviously need updating! ***
testPipelineErrorLineReporting () {
  exitLineNumber=`grep -P "exited.*non-zero" "$PIPELINE_TEST_TMP/err.log" | sed -r -e 's|^.*on line ([0-9]+).*$|\1|'`
  assertEquals 9 $exitLineNumber
}

testLoopsAndConditionalParsing () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_loops_conditionals.pipeline 1 - > /dev/null 2>&1
  run_commands > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  ret=$?
  if [ $ret -ne 0 ]; then
    cat "$PIPELINE_TEST_TMP/err.log"
  fi
  assertEquals 0 $ret
}

## Use a bad command name within a loop to test that errors are thrown properly
testErrorWithinLoop () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_error_within_loop.pipeline 1 - > /dev/null 2>&1
  run_commands > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  assertEquals 1 $?
  grep "command not found" "$PIPELINE_TEST_TMP/err.log" > /dev/null
  assertEquals 0 $?
}

testBadLoopSyntax () {
  ## Lines 1 to 16, just the "for" loop
  load_command_file "$PIPELINE_TEST_DATADIR"/test_malformed_loops.pipeline 1 16 > /dev/null 2>&1
  run_commands > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  assertEquals 1 $?

  ## Lines 17 to end, just the "for" loop
  load_command_file "$PIPELINE_TEST_DATADIR"/test_malformed_loops.pipeline 17 - > /dev/null 2>&1
  run_commands > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  assertEquals 1 $?
}

testValidationShouldFailOnNestedLoop () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_bad_nested_loops.pipeline 1 - > /dev/null 2>&1
  validate_loaded_command_file > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  assertEquals 1 $?
}

testBadIfStatementSyntax () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_malformed_conditional.pipeline 1 - > /dev/null 2>&1
  run_commands > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  assertEquals 1 $?
}

testTokenReplacement () {
  exclude_regex_arg=""
  keep_header_arg="true"
  token_replacements_arg="%FOO%=fooval %BAR%=bazbaz"
  load_command_file "$PIPELINE_TEST_DATADIR"/test_token_replacement.pipeline 1 - "$exclude_regex_arg" "$keep_header_arg" "$token_replacements_arg" > /dev/null 2>&1
  echo "$S4M_PIPELINE_CMDS" > "$PIPELINE_TEST_TMP/out.log" 2>&1
  grep -P "foo=fooval" "$PIPELINE_TEST_TMP/out.log" > /dev/null 2>&1
  assertEquals 0 $?
  grep -P "bar=bazbaz" "$PIPELINE_TEST_TMP/out.log" > /dev/null 2>&1
  assertEquals 0 $?
}

testBackslashEscapeParsing () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_backslash_escape_parsing.pipeline 1 - > /dev/null 2>&1
  run_commands > "$PIPELINE_TEST_TMP/out.log" 2> "$PIPELINE_TEST_TMP/err.log"
  grep -P "\[abc\|def\|ghi\|123" "$PIPELINE_TEST_TMP/out.log" > /dev/null 2>&1
  assertEquals 0 $?
}

testValidationShouldFailOnLineContinuation () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_bad_line_continuations.pipeline 1 - > /dev/null 2>&1
  validate_loaded_command_file > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  assertEquals 1 $?
}

testNestedConditionalParsing () {
  load_command_file "$PIPELINE_TEST_DATADIR"/test_nested_conditionals.pipeline 1 - > /dev/null 2>&1
  run_commands > /dev/null 2> "$PIPELINE_TEST_TMP/err.log"
  ret=$?
  if [ $ret -ne 0 ]; then
    cat "$PIPELINE_TEST_TMP/err.log"
  fi
  assertEquals 0 $ret
}

testNonInstallableModulesParsedFromPipelineCorrectly () {
  installable_mods=`extract_modules_from_pipeline "$PIPELINE_TEST_DATADIR"/test_non_installable_module_parsing.pipeline 2>/dev/null`
  test -z "$installable_mods"
  assertEquals 0 $?
}
