#!/bin/sh
## Path to output script
target="$1"

which wget > /dev/null 2>&1
if [ $? -eq 0 ]; then
  MINICONDA_WGET=`which wget`
fi
which curl > /dev/null 2>&1
if [ $? -eq 0 ]; then
  MINICONDA_CURL=`which curl`
fi

if [ -z "$MINICONDA_WGET" -a -z "$MINICONDA_CURL" ]; then
  echo "Error: Require either 'wget' or 'curl' to download Miniconda2 bootstrap script!
" 1>&2
  exit 1
fi

echo "Fetching latest Miniconda2 bootstrap script (saving to: '$target')"
if [ ! -z "$MINICONDA_WGET" ]; then
  $MINICONDA_WGET https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh -O "$target"
  exit $?
else
  $MINICONDA_CURL -o "$target" https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
  exit $?
fi
