#/bin/sh
# Module: miniconda
# Author: O.Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

## Include functions and variables
. ./inc/miniconda_funcs.sh


## Use miniconda to set up a target module.
##
miniconda_setup_handler () {

  ## NOTE: Input environment file path should already be a *full* path
  if [ ! -f "$miniconda_environment" ]; then
    s4m_error "Environment file '$miniconda_environment' not found!"
    return 1
  fi

  miniconda_validate_environment_yaml "$miniconda_environment"
  if [ $? -ne 0 ]; then
    s4m_error "Bad environment file, cannot proceed with Miniconda-based setup"
    return 1
  fi

  ## PORTABILITY:
  ## Abs path to environment location will not be reproducible across machines but the
  ## stuff installed *within* the environment should be consistent (from a software
  ## stack point of view - we know we can't literally move or copy Conda envs easily)

  msh_env_name=`miniconda_get_env_name`
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_error "Failed to determine environment name, cannot proceed with Miniconda-based setup"
    return $ret
  fi
  s4m_debug "Using conda env name: $msh_env_name"

  msh_miniconda_home="$MINICONDA_M2_HOME"
  ## Miniconda home override path given, use that instead
  if [ ! -z "$miniconda_miniconda_path" ]; then
    msh_miniconda_home="$miniconda_miniconda_path"
    if [ ! -f "$msh_miniconda_home/bin/conda" ]; then
      s4m_error "Miniconda home path override supplied ('$msh_miniconda_home'), but doesn't look like a valid installation!"
      return 1
    fi
  fi

  ## NOTE: This path may break some time if a major Miniconda update changes
  ## "envs" subdirectory path!
  msh_env_prefix="$msh_miniconda_home/envs/$msh_env_name"
  msh_tmp_env="$S4M_TMP/.env_${msh_env_name}.$$.yml"
  touch "$msh_tmp_env" 2>/dev/null
  if [ $? -ne 0 ]; then
    ## Need to write somewhere else, try home directory
    msh_tmp_env="$HOME/.env_${msh_env_name}.$$.yml"
  fi
  cat "$miniconda_environment" | sed -r -e 's|^name\:.*$|name: '$msh_env_name'|g' \
    -e "s|prefix\:.*|prefix: \!\!python\/unicode '$msh_env_prefix'|" \
    > "$msh_tmp_env"

  s4m_debug "Using parsed environment.yml:"
  s4m_debug "[`cat $msh_tmp_env`]"

  miniconda_install_environment "$msh_tmp_env" "$msh_env_name" "$msh_miniconda_home"
  ret=$?
  rm -f "$msh_tmp_env"
  return $ret
}


## Handler for 'load_environment'
##
## !!! NOTE: - returns a command string that must be evaluated by the caller (in order to
##     source into the caller's environment
## !!!
##
miniconda_load_env_handler () {

  if [ -z "$miniconda_environment" ]; then
    s4m_log "Conda environment file not give, seeing if we can find it.."
    env_file=`miniconda_get_default_env_file_path "$miniconda_module"`
    ret=$?
    if [ $ret -eq 0 -a -f "$env_file" ]; then
      s4m_log "Found and using default environment file '$env_file'"
      miniconda_environment="$env_file"
    else
      s4m_error "No conda environment file given and we could not find a default one, cannot proceed to load environment"
      return $ret
    fi
  else
    s4m_log "Got environment file override: '$miniconda_environment'"
  fi
  ## Determine env name from env.yml and then activate it
  env_GUID=`miniconda_guid_from_environment_file "$miniconda_environment"`
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_error "Failed to determine environment GUID (needed to find env name), cannot proceed to load environment"
    return $ret
  fi
  env_name=`miniconda_env_name_from_module_guid "$miniconda_module" "$env_GUID"`

  ## print the command that the caller needs to 'eval'
  echo ". $MINICONDA_M2_HOME/bin/activate $env_name"
}


main () {

  s4m_route setup            miniconda_setup_handler
  ## NOTE: The caller must 'eval' or otherwise execute the returned command
  s4m_route load_environment miniconda_load_env_handler 

}

### START ###
main

