#!/bin/sh
## test_miniconda.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./inc/miniconda_funcs.sh

  MINICONDA_TEST_DIR="./etc/test"
  export MINICONDA_TEST_DIR

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"

  ## Set Miniconda home override if given
  miniconda_path=`unittest_get_arg "miniconda_path"`
  if [ ! -z "$miniconda_path" ]; then
    export MINICONDA_M2_HOME="$miniconda_path"
  fi
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  :
}



## TESTS ##

testMiniconda2Installed () {
  "$MINICONDA_M2_HOME/bin/conda" --version > /dev/null 2>&1
   assertEquals 0 $?
}

testGetAvailableDiskSpace () {
  availMB=`df --block-size=1MB "$MINICONDA_M2_HOME" | tail -n+2 | awk '{print $4}' 2>/dev/null`
  assertEquals 0 $?
  ## A number was extracted correctly from "df" for available space in MB
  echo "$availMB" | grep -P "^[0-9]+$" > /dev/null 2>&1
  assertEquals 0 $?
}

## Test that well formed environment.yml files are correctly validated
testEnvironmentYAMLValidationPass () {
  miniconda_validate_environment_yaml "$MINICONDA_TEST_DIR/environment_good1.yml"
  assertEquals 0 $?
}

## Test that ill-formed environment.yml files are correctly invalidated
testEnvironmentYAMLValidationFail () {
  miniconda_validate_environment_yaml "$MINICONDA_TEST_DIR/environment_bad1.yml"
  assertEquals 1 $?
  miniconda_validate_environment_yaml "$MINICONDA_TEST_DIR/environment_bad2.yml"
  assertEquals 1 $?
  miniconda_validate_environment_yaml "$MINICONDA_TEST_DIR/environment_bad3.yml"
  assertEquals 1 $?
  miniconda_validate_environment_yaml "$MINICONDA_TEST_DIR/environment_bad4.yml"
  assertEquals 1 $?
}

