#!/bin/sh
## Modules requiring miniconda functions should invoke within:
##   "s4m_import miniconda/lib.sh"

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done


## Attempt to load environment
##
miniconda_activate () {
  ## Get '$MINICONDA_M2_HOME' path so we know how to invoke conda
  s4m_import miniconda:/inc/miniconda_funcs.sh

  env_file=`miniconda_get_default_env_file_path "${S4M_MODULE}/${S4M_MOD_VERS}"`
  #s4m_debug "miniconda_activate(): Got env file [$env_file]"
  env_GUID=`miniconda_guid_from_environment_file "$env_file"`
  env_name=`miniconda_env_name_from_module_guid "${S4M_MODULE}/${S4M_MOD_VERS}" "$env_GUID"`
  #s4m_debug "miniconda_activate(): Got env name = $env_name"
  env_path="$MINICONDA_M2_HOME/envs/$env_name"

  if [ ! -d "$env_path" ]; then
    s4m_debug "
  MINICONDA_M2_HOME=[$MINICONDA_M2_HOME]
  Conda env name=[$env_name]"
    s4m_log "Failed to find Conda environment '$env_name', attempting installation now.."

    if ! s4m_has_internet_connection; then
      s4m_pop_error
      s4m_error "Please re-install module '$S4M_MODULE' manually"
      return 1
    fi
    ## Install the module
    s4m_exec install "${S4M_MODULE}/${S4M_MOD_VERS}"
    ret=$?
    if [ $ret -ne 0 ]; then
      s4m_error "Could not load the environment because module installation failed"
      return $ret
    fi
  fi

  s4m_log "Activating Conda environment '$env_name'.."
  . $MINICONDA_M2_HOME/bin/activate "$env_name"
  return $?
}

