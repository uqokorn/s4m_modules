#Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## miniconda installation path (miniconda2 at present time)
## We want this to be fixed per machine/user
if [ -z "$MINICONDA_M2_HOME" ]; then
  MINICONDA_M2_HOME=$HOME/.s4m/moduledata/miniconda/miniconda2
  export MINICONDA_M2_HOME
fi

## Default python version to use for new miniconda environments if
## not overriden by caller. Used in cases where no other base software
## is specified or required.
##
## NOTE: Not currently used
##
MINICONDA_PYTHON_VERS=2.7
export MINICONDA_PYTHON_VERS


### METHODS ###

## Calculate and return a GUID hash from environment file
##
miniconda_guid_from_environment_file () {
  env_file="$1"
  if [ ! -f "$env_file" ]; then
    workdir=`pwd`
    s4m_error "Environment file '$env_file' not found! CWD=[$workdir]"
    return 1
  fi
  cat "$env_file" | tr -d [:cntrl:] | md5sum | cut -d' ' -f 1
}


## Return environment name from module name and GUID
## NOTE: We clean up the module name in case was given as 'module/version' spec
##
miniconda_env_name_from_module_guid () {
  module_name=`s4m_remove_module_version "$1"`
  module_GUID="$2"
  
  echo "${module_name}_${module_GUID}"
}


## Get default module environment file for a given input module/version spec
##
miniconda_get_default_env_file_path () {
  target=`s4m_extract_module "$1"`
  ## If environment file wasn't given, try to see if it's there with standard name
  found_files=`find "$S4M_MODULE_HOME/$1" -name "${target}_env.yml" 2>/dev/null`
  if [ $? -eq 0 ]; then
    ret_file=`echo "$found_files" | head -1`
    echo "$ret_file"
    test -f "$ret_file"
    return
  fi
  return 1
}


## Return a suitable environment name for new environment.
## The name is based on the contents of the conda environment file.
##
## NOTE: Requires 'miniconda_environment' global var
##
## TODO: Complete & Test API calls
##
miniconda_get_env_name () {

  if [ -z "$miniconda_environment" ]; then
    s4m_error "Cannot build conda environment name without environment file!"
    return 1
  fi

  ## Use MD5 hash of environment file contents (sans control chars) as a GUID
  if [ ! -z "$miniconda_module" ]; then
    target=`s4m_extract_module "$miniconda_module"`
    env_GUID=`miniconda_guid_from_environment_file "$miniconda_environment"`
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi

  ## Called via API
  else
    ## TODO: These global vars need to be set in s4m!
    target=$S4M_FROM_MODULE
    ## TODO: Need to find abs path to environment file and then get its MD5 hash
    ## as above
    env_GUID=$S4M_FROM_MODULE_VERS
    s4m_fatal_error "miniconda_get_env_name(): API calls not yet implemented!"
  fi

  echo "${target}_${env_GUID}"
  return
}


## Look for or bootstrap a working 'conda' command.
##
## (In order to run "conda env create -f <environment.yml>" we need conda).
##
## We do this by checking for and installing a Miniconda2 environment for
## *this* module in a known location.
##
## !!! NOTE: Assumes Linux x86_64 !!!
##
miniconda_check_install_bootstrap () {
  mcib_miniconda_home="$1"
  if [ -z "$mcib_miniconda_home" ]; then
    mcib_miniconda_home="$MINICONDA_M2_HOME"
  fi

  ret=0
  if [ ! -x "$mcib_miniconda_home/bin/conda" ]; then
    s4m_log "Could not find available conda binary, bootstrapping new Miniconda installation.."
    if [ ! -d "$S4M_MODULE_DB/tmp" ]; then
      mkdir -p "$S4M_MODULE_DB/tmp"
      if [ $? -ne 0 ]; then
        s4m_error "Failed to create temp location for Miniconda install script download ('$S4M_MODULE_DB/tmp')"
        return 1
      fi
    fi

    ## Remove possibility of global Python interfering with install process
    unset PYTHONPATH
    `s4m_get_shell` ./scripts/fetch_bootstrap_miniconda.sh "$S4M_MODULE_DB/tmp/Miniconda2-Linux-x86_64.sh" 
    ## Silent (non-interactive) installation to target directory
    `s4m_get_shell` "$S4M_MODULE_DB/tmp/Miniconda2-Linux-x86_64.sh"  -b -p "$mcib_miniconda_home"

    ret=$?
  fi
  if [ $ret -ne 0 ]; then
    s4m_error "Miniconda bootstrap script returned non-zero status!"
    return 1
  fi

  s4m_log "Using conda:  $mcib_miniconda_home/bin/conda"
  return 0
}


## Check for free space in Miniconda2 env path (give warning if < 1GB free)
##
miniconda_check_available_disk () {
  mcad_miniconda_home="$1"
  if [ -z "$mcad_miniconda_home" ]; then
    mcad_miniconda_home="$MINICONDA_M2_HOME"
  fi

  availMB=`df --block-size=1MB "$mcad_miniconda_home" | tail -n+2 | awk '{print $4}' 2>/dev/null`
  if [ $? -ne 0 ]; then
    if [ $availMB -lt 1024 ]; then
      s4m_warn "*** Less than 1GB free space for Miniconda; run 'conda clean -p' or symlink '$mcad_miniconda_home' to a partition with more space ***"
      sleep 3
      ## Not an error per-se, but caller can know we gave a warning with non-zero status
      return 1
    fi
  fi
}


## Install target module Conda environment
##
miniconda_install_environment () {
  ## path to environment.yml
  mie_yml_path="$1"
  mie_env_name="$2"
  mie_miniconda_home_override="$3"

  mie_miniconda_home="$MINICONDA_M2_HOME"
  if [ ! -z "$mie_miniconda_home_override" ]; then
    mie_miniconda_home="$mie_miniconda_home_override"
  fi
  s4m_log "Miniconda home path for module installation:  $mie_miniconda_home"

  if [ ! -f "$mie_yml_path" ]; then
    s4m_error "Target environment file path '$mie_yml_path' does not exist!"
    return 1
  fi

  ## We need a "conda" binary. Look for an environment under $MINICONDA_M2_HOME
  ## which should be installed when running *this* module for the first time
  miniconda_check_install_bootstrap "$mie_miniconda_home"
  if [ $? -ne 0 ]; then
    s4m_error "Failed to boostrap a Miniconda installation (for required 'conda' binary)!"
    return 1
  fi

  ## Search for existing environment - don't run "conda env create" if target exists
  mie_found_env=`"$mie_miniconda_home/bin/conda" env list | grep -P "^$mie_env_name\s" 2> /dev/null`
  if [ $? -eq 0 ]; then
    s4m_log "Found environment:
$mie_found_env"
    ## No "force" reinstall flag given, abort
    if [ -z "$miniconda_force" ]; then
      s4m_log "(nothing to do)"
      return 0
    ## Remove existing environment first
    else
      s4m_log "Removing existing environment before re-installation.."
      mie_found_env_name=`echo "$mie_found_env" | cut -d' ' -f 1`
      "$mie_miniconda_home/bin/conda" env remove --name "$mie_found_env_name"
      if [ $? -ne 0 ]; then
        s4m_warn "Possible failure to remove environment; this may impact on environment re-installation attempt"
      fi
    fi
  fi

  ## Install into target environment
  ## Check available disk space before installing target module; give a warning if less than 1GB remains
  if ! miniconda_check_available_disk "$mie_miniconda_home"; then
    s4m_log "NOTE: Insufficient disk space may cause Conda environment installation failure"
  fi

  ## Make sure conda is up-to-date (for latest channel info etc)
  s4m_log "Making sure our conda binary is up-to-date.."
  "$mie_miniconda_home/bin/conda" update -y conda
  if [ $? -ne 0 ]; then
    s4m_warn "There was an issue during Conda binary update attempt; this may impact on environment installation"
  fi

  s4m_log "Installing target environment from YAML.."
  ## Create environnment from target YAML
  "$mie_miniconda_home/bin/conda" env create -f "$mie_yml_path"
  ret=$?
  if [ $ret -eq 0 ]; then
    s4m_log "Successfully installed Conda environment:  $mie_env_name"
  fi
  return $ret
}


## Test that environment.yml file looks legitimate
##
miniconda_validate_environment_yaml () {
  mve_env_path="$1"

  if [ ! -f "$mve_env_path" ]; then
    return 1
  fi

  grep -F "name:" "$mve_env_path"  > /dev/null 2>&1
  mve_hasname=$?
  grep -F "prefix:" "$mve_env_path" > /dev/null 2>&1
  mve_hasprefix=$?
  mve_lines=`wc -l "$mve_env_path" | cut -d' ' -f 1`

  if [ $mve_hasname -eq 0 -a $mve_hasprefix -eq 0 -a $mve_lines -ge 4 ]; then
    return 0
  fi

  ## did not pass required filters
  return 1
}

