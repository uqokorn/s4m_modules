#/bin/sh
##===========================================================================
## Module: unittest
## Author: Othmar Korn
##
## A wrapper for "shUnit2" https://code.google.com/p/shunit2/
## shUnit2 (C) Kate Ward, 2008 and is licensed under the LGPL.
##
## See: ./include/shunit2*
##===========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##===========================================================================

## Version of shUnit2 bundled with this module
SHUNIT2_LIB="shunit2-2.1.6"

## A unit test script client can call this to return value for
## an argument that was passed using the "--args" argument
unittest_get_arg () {
  uga_target="$1"
  shift

  #echo "DEBUG: unittest_get_arg(): UNNITEST_TARGET_ARGS=[$UNITTEST_TARGET_ARGS]" 1>&2

  ## Locally replace default array with user args so we can process
  set -- "$UNITTEST_TARGET_ARGS"

  while [ ! -z "$1" ];
  do
    ## remove leading dashes, if any
    ## NOTE: In current s4m v0.5 (2016-08-23), the arg will never have
    ##  leading dashes because s4m would have interpreted it as a module
    ##  command arg for 'unittest' module
    arg=`echo "$1" | sed -r -e 's|^\-+||'`
    key=`echo "$arg" | cut -d'=' -f 1`
    val=`echo "$arg" | cut -d'=' -f 2`

    if [ "$key" = "$uga_target" ]; then
      echo "$val"
      return
    fi
    shift
  done
}


## Find scripts named "test_*.sh" in target module path
##
find_module_tests () {
  targetmodpath="$1"
  testscripts=`cd $targetmodpath && ls -1 test_*.sh 2> /dev/null` 
  echo $testscripts
}


## Run shUnit2 on each test shell script found in target module path
##
run_shunit2_over_test_scripts () {
  targetmodpath="$1"
  targetargs="$2"
  shift; shift
  testscripts=$@

  ## Make available target args to test script
  if [ ! -z "$targetargs" ]; then
    UNITTEST_TARGET_ARGS="$targetargs"
    export UNITTEST_TARGET_ARGS
  fi

  ## change into target module path as any test scripts there are written
  ## relative to that module's path
  UNITTEST_OLD_PWD=`pwd`
  export UNITTEST_OLD_PWD
  cd "$targetmodpath"

  for testfile in $testscripts
  do
    s4m_log "Running test script [$testfile].."
    
    . "$S4M_MODULE_PATH/$S4M_MODULE/$S4M_MOD_VERS/include/$SHUNIT2_LIB/src/shunit2" "$testfile"

    s4m_log "  done"
  done

  ## change back to this module path
  cd "$UNITTEST_OLD_PWD"
}


## Handler for "unittest" command`
##
unittest_handler () {
  ## Can be <module/version> spec
  targetmodule="$unittest_module"
  targetargs="$unittest_args"
  s4m_debug "unittest_args=[$unittest_args]"

  ## NOTE: If user input module name matches a relative path (due to having
  ## been run in modules subdirectory of s4m, for example), then the module
  ## name will have been replaced by a full absolute path - to undo this, we
  ## take the module/version spec or just the last token if no version given
  echo "$targetmodule" | grep -P "\w+\/[0-9]+\.[0-9]+\/*" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    numtokens=`echo "$targetmodule" | sed 's|\/$||' | tr "/" " " | wc -w`
    first=`expr $numtokens - 1`
    last=$numtokens
    ## Result is the last two folders of path e.g. "<module>/<version>"
    targetmodule=`echo "$targetmodule" | cut -d'/' -f $first,$last | tr " " "/"`
  ## Not a module/version spec, so the last token should just be the module name
  else
    targetmodule=`basename "$targetmodule"`
  fi
  ## Use the given module version
  if s4m_is_module_version_spec "$targetmodule"; then
    modulename=`s4m_extract_module "$targetmodule"`
    modulevers=`s4m_extract_module_version "$targetmodule"`
  ## Module version not given, use the highest module version found
  else
    modulename="$targetmodule"
    modulevers=`s4m_get_max_module_version "$S4M_MODULE_PATH/$targetmodule"`
  fi

  s4m_log "Checking for unit tests in module '$modulename/$modulevers'.."

  if [ ! -d "$S4M_MODULE_PATH/$modulename/$modulevers" ]; then
    s4m_fatal_error "Target module/version '$modulename/$modulevers' not found in modules path '$S4M_MODULE_PATH'!"
  fi
  ## Get test_*.sh files in default (latest) module version
  testscripts=`find_module_tests "$S4M_MODULE_PATH/$modulename/$modulevers"`
  ret=$?
  if [ $ret -ne 0 ]; then
    exit $ret
  fi

  ## Test scripts can use this variable to find out which module/version they belong to
  UNITTEST_TARGET_MODULE="$modulename"
  export UNITTEST_TARGET_MODULE
  UNITTEST_TARGET_MOD_VERS="$modulevers"
  export UNITTEST_TARGET_MOD_VERS

  if [ -z "$testscripts" ]; then
    s4m_fatal_error "Failed to find any unit test scripts in target module '$modulename/$modulevers' directory!"
  else
    s4m_debug "Found test scripts: [$testscripts]"
  fi
  run_shunit2_over_test_scripts "$S4M_MODULE_PATH/$modulename/$modulevers" "$targetargs" $testscripts
  return $?
}


main () {
  s4m_route "unittest" "unittest_handler"
}

### START ###
main

