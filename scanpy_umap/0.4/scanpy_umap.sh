#!/bin/sh
# Module: scanpy_umap
# Author: O.Korn

for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================


## Pass UMAP params to inner python script
umap_handler () {

  if [ ! -d "$scanpy_umap_outdir" ]; then
    mkdir -p "$scanpy_umap_outdir"
  fi

  optional_args=""
  if [ ! -z "$scanpy_umap_columns" ]; then
    optional_args="$optional_args --columns $scanpy_umap_columns"
  fi
  if [ ! -z "$scanpy_umap_is10x" -a "$scanpy_umap_is10x" == "true" ]; then
    optional_args="$optional_args --is10x"
  fi
  if [ ! -z "$scanpy_umap_islogged" -a "$scanpy_umap_islogged" == "false" ]; then
    :
  else
    optional_args="$optional_args --isLogged"
  fi
  if [ ! -z "$scanpy_umap_cluster" -a "$scanpy_umap_cluster" == "true" ]; then
    optional_args="$optional_args --cluster"
    ## If doing louvain clustering, check for optional output sample table arg
    if [ ! -z "$scanpy_umap_targetsOut" ]; then
      optional_args="$optional_args --sampleOut $scanpy_umap_targetsOut"
    fi
  fi
  if [ ! -z "$scanpy_umap_tag" ]; then
    optional_args="$optional_args --tag $scanpy_umap_tag"
  fi

  s4m_debug "Calling [python scripts/process_singlecell.py $scanpy_umap_input $scanpy_umap_outdir --sample $scanpy_umap_targets $optional_args]"

  python scripts/process_singlecell.py "$scanpy_umap_input" "$scanpy_umap_outdir" --sample "$scanpy_umap_targets" $optional_args > "$scanpy_umap_outdir/sessionInfo.txt"
  ret=$?
  if [ $ret -eq 0 ]; then
    ## Tweak output targets if it was created (add missing SampleID header)
    if [ ! -z "$scanpy_umap_targetsOut" ]; then
      sed -i -r -e '1s|^|SampleID|' "$scanpy_umap_targetsOut"
    fi
    s4m_log "All done"
  fi
  return $ret
}


main () {
  ## Import miniconda (for automatic dependency management)
  s4m_import miniconda/lib.sh

  ## Load the Python env for scanpy
  if ! miniconda_activate; then
    return 1
  fi

  s4m_route umap umap_handler
}

### START ###
main

