"""Python script to process single cell RNA-Seq dataset in stemformatics so that sample relationships can be visualised.
This script is designed to run from command line, under a conda environment with required packages installed.
Its basic functionality is to takes expression matrix file as input and outputs dimensionality reduced coordinates and corresponding plots
as files.

Usage example:
> conda activate s4msinglecell
> python process_singlecell.py ../datasets/7302/gene_count_normalised_filtered.txt output_7302 -s ../datasets/7302/targets_cells.txt > output_7302/sessionInfo.txt
This will create files in output_7302/ (which should already exist).

The input file could be .h5ad file:
> python process_singlecell.py data/E-MTAB-7303/E-MTAB-7303.h5ad data/E-MTAB-7303/output/ -c "Sample Characteristic[disease]" > data/E-MTAB-7303/output/sessionInfo.txt
In this example, we also specify that we want plots to be coloured by a particular column of the sample table. 
If this is left out, as many plots as there are columns in the sample table will be produced.

Author: Jarny Choi
Contributors: Othmar Korn
"""

import os, platform, sys
import argparse
import anndata, pandas, numpy, scanpy
import matplotlib
import matplotlib.pylab as plt  

# scanpy's plotting function calls plot() from matplotlib, but it throws an error on Mac OS (mojave).
# See https://github.com/matplotlib/matplotlib/issues/13414. Using the following line fixes this.
matplotlib.use("Agg")

def readfile(filepath, is10x=False, sampleTable=None):
    """Read file at filepath using scanpy and return AnnData instance.
    Set is10x True if the file is 10x formatted hdf5 file.
    sampleTable should be a pandas DataFrame object, and this will be added to the obs attribute of the AnnData instance returned.
    """
    if is10x:   # use scanpy's dedicated read function
        adata = scanpy.read_10x_h5(filepath)
    elif "h5" in os.path.splitext(filepath)[1]:  # h5 format, so use scanpy.read function
        adata = scanpy.read(filepath)
    elif "mtx" in os.path.splitext(filepath)[1]: # Matrix Market format
        if sampleTable is None: # Sample table required as MTX format can't hold obs (column) names
            raise Exception("When expression input is Matrix Market (MTX) format, sample table must be supplied!")
        adata = scanpy.AnnData(anndata.read_mtx(filepath).transpose())
        # set obs (column) names from sample table
        adata.obs_names = sampleTable.index
    else:  # assume tsv - use pandas to read file first because scanpy needs samples as rows, genes as columns, then assign it to scanpy
        adata = scanpy.AnnData(pandas.read_csv(filepath, "\t", index_col=0).transpose())

    if sampleTable is not None:   # align index to be same as adata.obs and transfer columns to adata.obs
        df = sampleTable.reindex(adata.obs.index)
        for col in df.columns:
            adata.obs[col] = df[col]

    return adata

def umap(adata, isLogged=False):
    """Perform dimensionality reduction on the data and return adata object, which will contain pca and umap coordinates.
    """
    if not isLogged:
        scanpy.pp.log1p(adata)
    scanpy.pp.pca(adata)
    scanpy.pp.neighbors(adata)
    scanpy.tl.umap(adata)
    return adata

def aggregatedClustering(adata):
    """For our atlases, we're interested in projecting single cell data. To do this, we aggregate values on louvain clusters
    """
    df = pandas.DataFrame(index=adata.var_names)
    for cluster in adata.obs["louvain"].unique():
        subset = adata[adata.obs["louvain"] == cluster, :]
        # If the cluster if very large, we'll just subsample 1000 points
        df["louvain%s" % cluster] = scanpy.pp.subsample(subset, n_obs=min([1000,subset.n_obs]), copy=True).X.sum(axis=0).T
    return df

def main():
    # Set up arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("expression", help="path to expression file")
    parser.add_argument("output", help="path to output directory")
    parser.add_argument("-x", "--is10x", help="set if expression file is 10x-Genomics-formatted hdf5 file", action="store_true")
    parser.add_argument("-s", "--sample", help="path to sample table file or targets file (tab separated)")
    parser.add_argument("-so", "--sampleOut", help="optional name of sample table to write (within output path) containing louvain cluster assignments")
    parser.add_argument("-c", "--columns", help="sample column to colour by (use quote, and comma separate)")
    parser.add_argument("-l", "--isLogged", help="set if expression values have been logged already", action="store_true")
    parser.add_argument("-cl", "--cluster", help="set if you want to apply louvain clustering", action="store_true")
    parser.add_argument("-t", "--tag", help="optional tag to display as part of all plot titles")
    args = parser.parse_args()

    # Check for mandatory fields: expression file and output directory
    if not os.path.exists(args.expression):
        raise Exception("Expression file '" + args.expression + "' does not exist.")
    elif not os.path.exists(args.output):
        raise Exception("Output directory '" + args.output + "' does not exist.")

    # Check for optional fields
    sampleTable = None
    if args.sample:
        if os.path.exists(args.sample):
            sampleTable = pandas.read_csv(args.sample, sep="\t", index_col=0)
        else:
            raise Exception("Sample table file '" + args.sample + "' does not exist.")
        
    # Read file and return AnnData instance
    adata = readfile(args.expression, is10x=args.is10x, sampleTable=sampleTable)
    
    #print(adata.X.max().max())
    #print(adata.X.todense()[:3,:4])
  
    # Print expression input log status
    # NOTE: Print to STDERR as we expect that ONLY session info is printed to STDOUT 
    if args.isLogged:
        sys.stderr.write("Treating input as already logged\n")
    else:
        sys.stderr.write("Treating input as NOT logged\n")
 
    # perform pca and umap and save coords; note that by default, scanpy will run pca on 50 dimensions, so just take first two to save
    adata = umap(adata, isLogged=args.isLogged)
    pandas.DataFrame(adata.obsm["X_pca"][:,:2], index=adata.obs.index, columns=["x","y"]).to_csv(os.path.join(args.output, "pca.tsv"), sep="\t")
    pandas.DataFrame(adata.obsm["X_umap"], index=adata.obs.index, columns=["x","y"]).to_csv(os.path.join(args.output, "umap.tsv"), sep="\t")

    if args.cluster:
        scanpy.tl.louvain(adata)  # this will create "louvain" column in adata.obs data frame
        aggregatedClustering(adata).to_csv(os.path.join(args.output, "aggregated.louvain.tsv"), sep="\t")
        # Write sample table with addition of "louvain" column
        if args.sampleOut:
            sys.stderr.write("Writing sample table with added louvain cluster assignments\n")
            adata.obs.to_csv(os.path.join(args.output,args.sampleOut), sep="\t")

    # Plot pca and umap and save figures; Use all columns of adata.obs to colour the plots, unless user specified particular columns.
    # Note that even if sample table file was not specified initially, it could exist within adata if .h5 file was used to read that in.
    fileIds = ["", ""]  # used to create a unique id field within each file name for the plots
    if len(adata.obs.columns)>0:
        if args.columns is None:
            fileIds = adata.obs.columns.tolist()
        else:
            columns = args.columns.replace("'","").replace('"',"").split(",")
            columns = [item for item in columns if item in adata.obs.columns]
            ## Make sure we don't forget to include cluster information if it's there
            if "louvain" in adata.obs.columns:
                columns.append("louvain")
            if len(columns)>0:
                fileIds = columns

    # Name of transform to display in plot title
    tag = args.tag
    if tag is None:
        tag = "log" if args.isLogged else "scanpy log1p"

    for fileId in fileIds:
        ax = scanpy.pl.pca(adata, color=None if fileId=="" else fileId, show=False, title=fileId + " (" + tag + " PCA)")
        ax.figure.savefig(os.path.join(args.output, "pca.%s.png" % fileId), bbox_inches="tight")

        ax = scanpy.pl.umap(adata, color=None if fileId=="" else fileId, show=False, title=fileId + " (" + tag + " UMAP)")
        ax.figure.savefig(os.path.join(args.output, "umap.%s.png" % fileId), bbox_inches="tight")

    # print versions of relevant packages used
    print("Python " + platform.sys.version)
    sessionInfo = scanpy.logging.print_versions()

if __name__ == "__main__":
    main()

"""
Notes made while writing this script.
    # If adata.var should be the sample table with unique row ids
    #adata.var.index.name = "gene_symbol"
    #adata.var.reset_index(inplace=True)
    #adata.var.set_index("gene_ids", inplace=True)
    
In this code, adata.var = adata.var.reset_index() did not work.

"""
