#/bin/sh
##==========================================================================
## Module: samtools
## Author: Othmar Korn
##
## Wrapper for popular 'samtools' SAM/BAM utility
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##==========================================================================

## Default mapping stats file output name
SAMTOOLS_MAPSTATS_FILE="mapstats.txt"


## Output table of mapped vs unmapped read counts and a stacked barplot of
## mapping percentages across samples
##
generate_mapping_stats () {
  gms_samdir="$1"
  gms_outdir="$2"
  gms_ymax="$3"
  gms_stacked="$4"
  gms_title="$5"

  gms_extraflags=""
  if [ ! -z "$gms_ymax" ]; then
    gms_extraflags="-ymax=$gms_ymax"
  fi
  if [ ! -z "$gms_stacked" ]; then
    gms_extraflags="$gms_extraflags -stacked=$gms_stacked"
  fi
  if [ ! -z "$gms_title" ]; then
    ## replace spaces with underscores for safe input to R script
    gms_title_underscores=`echo "$gms_title" | sed -r -e 's| |_|g'`
    gms_extraflags="$gms_extraflags -title=$gms_title_underscores"
  fi

  if [ ! -d "$gms_samdir" ]; then
    s4m_fatal_error "Input SAM/BAM directory '$gms_samdir' does not exist!"
  fi

  ## Generate table of #s reads mapped vs total
  samOrBamFiles=`ls -1 "$gms_samdir"/*.[BbSs][Aa][Mm]`
  if [ -z "$samOrBamFiles" ]; then
    s4m_fatal_error "No SAM or BAM files found in directory '$gms_samdir'!"
  fi

  s4m_debug "Found SAM or BAM files:
$samOrBamFiles"

  gms_map_data="$gms_outdir/$SAMTOOLS_MAPSTATS_FILE"
  ## Output data header 
  /bin/echo -e "Sample\tTotal Reads\tMapped Reads\tMapped %" > "$gms_map_data"

  s4m_debug "Using samtools:"
  s4m_debug `which samtools`

  ## Generate per mapped BAM/SAM stats and append to stats file
  for f in $samOrBamFiles
  do
    ext=`echo "$f" | sed -r -e 's|^.*\.([A-Za-z]+)$|\1|'`
    fbase=`basename $f .$ext`
    s4m_log "Calculating mapped read stats for sample '$fbase'"
    totalreads=`samtools view -c "$f"`
    mappedreads=`samtools view -c -F 4 "$f"`
    mappedpct=`echo "$mappedreads / $totalreads * 100" | bc -l`
    ## Sample ID, total reads, mapped reads, mapped %
    /bin/echo -e "$fbase\t$totalreads\t$mappedreads\t$mappedpct" >> "$gms_map_data"
    s4m_log "  done"
  done

  s4m_log "Plotting read library mapped vs unmapped"
  ## Plot barplot of mapped vs unmapped reads % across samples
  call_R "$samtools_R" "$S4M_THIS_MODULE/scripts/plot_map_stats.R" "-input='$gms_map_data' -outdir='$gms_outdir' $gms_extraflags"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  s4m_log "  done"
}


## Sort and index BAM files
samtools_sortbams () {
  ss_inputdir="$1"
  ss_ncpu="$2"

  if [ ! -d "$ss_inputdir" ]; then
    s4m_error "BAM files input directory '$ss_inputdir' does not exist!"
    return 1
  fi

  cd "$ss_inputdir"

  ## Only sort BAMs if we don't have as many *.bam.bai as *.bam files
  ss_bams=`ls -1 *.bam 2> /dev/null`
  ss_bais=`ls -1 *.bam.bai 2> /dev/null`
  ss_num_bams=`echo "$ss_bams" | wc -l`
  ss_num_bais=`echo "$ss_bais" | wc -l`

  if [ -z "$ss_bais" -o $ss_num_bais -lt $ss_num_bams ]; then
    s4m_log "Renaming existing BAMs to '*.bam.unsorted'"
    for b in *.bam
    do
      fbase=`basename "$b" .bam`
      mv "$b" "$fbase.bam.unsorted"
    done
    s4m_log "done"

    s4m_debug "Using samtools:"
    s4m_debug `which samtools`

    s4m_log "Sorting BAMs.."
    for b in *.bam.unsorted
    do
      fbase=`basename "$b" .bam.unsorted`
      if [ ! -z "$ss_ncpu" ]; then
        s4m_log "  Sorting '$b' using $ss_ncpu threads -> '$fbase.bam'"
        samtools sort -f -@ $ss_ncpu "$b" "$fbase.bam"
        ret=$?
        if [ $ret -ne 0 ]; then
          s4m_error"  Failed to created sorted BAM '$fbase.bam'"
          cd $OLDPWD
          return $ret
        fi
      else
        s4m_log "  Sorting '$b' using (default) single thread -> '$fbase.bam'"
        samtools sort -f "$b" "$fbase.bam"
        ret=$?
        if [ $ret -ne 0 ]; then
          s4m_error "  Failed to created sorted BAM '$fbase.bam'"
          cd $OLDPWD
          return $ret
        fi
      fi
      s4m_log "  Indexing sorted BAM '$fbase.bam'"
      samtools index "$fbase.bam"
      ret=$?
      if [ $ret -ne 0 ]; then
        s4m_error "  Failed to index sorted BAM '$fbase.bam'"
        cd $OLDPWD
        return $ret
      fi
    done
    s4m_log "done"

  else
    s4m_log "Skip BAM sorting, it appears that BAM files are already sorted and indexed?"
    s4m_log "Tip: Remove *.bam.bai files to force re-sort and re-index"
  fi

  cd $OLDPWD
}


## Handler for "mapstats" command
mapstats_handler () {
  samdir="$samtools_samdir"
  outdir="$samtools_outdir"
  ymax="$samtools_ymax"
  stacked="$samtools_stacked"
  title="$samtools_title"

  ## Activate R environment
  if ! miniconda_activate; then
    return 1
  fi

  generate_mapping_stats "$samdir" "$outdir" "$ymax" "$stacked" "$title"
  return $?
}


## Handler for "sortbams" command
sortbams_handler () {
  samdir="$samtools_samdir"
  ncpu="$samtools_ncpu"

  samtools_sortbams "$samdir" "$ncpu"
  return $?
}


main () {
  s4m_import "Rutil/Rutil.sh"
  s4m_import "miniconda/lib.sh"

  s4m_route "mapstats" "mapstats_handler"
  s4m_route "sortbams" "sortbams_handler"
}

### START ###
main

