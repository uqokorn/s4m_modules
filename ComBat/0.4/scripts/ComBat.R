## ==========================================================================
## File:        ComBat.R
##
## Description: Remove batch effects using R/sva ComBat.
##
##              Will fail if input contains infinite or NA values as ComBat
##              will remove them from the data.
##
##              *** NOTE: Intended for R 2.15 with "affycoretools" package. ***
##
##              Script can be updated to work with R v3.x quite easily though.
##
## Author:      O.Korn
## Modified:    2016-09-19
## ==========================================================================

source("scripts/inc/util.R")

# Path to raw/normalized expression table
input_file <- ARGV.get("inputfile", T)
# Path to output directory
output_path <- ARGV.get("outdir", T)
# Phenotype (sample_pheno.txt)
pheno_file <- ARGV.get("phenofile", T)

debug <- ARGV.get("debug")
if (! is.na(debug)) {
  debug <- as.logical(ARGV.get("debug"))
} else {
  debug <- FALSE
}

if(nchar(input_file) == 0) {
   print("Error: Path to input expression data table must be provided!\n\n")
   q(status=1)
}
if(nchar(output_path) == 0) {
   print("Error: Output directory must be provided!\n\n")
   q(status=1)
}

## and for "plotPCA()" function:
require('affycoretools')
## ComBat
require('sva')


###############################################################################
#
# Load and check sample info vs. input data table
#
###############################################################################

## Load phenotype file (must have "batch" column somewhere)
## The second column must contain the class factor, header "class"
phenoMap <- read.table(pheno_file, sep="\t", header=T, colClasses="character", as.is=T, check.names=F, row.names=1)

## Load input expression table
dat <- read.table(input_file, sep="\t", header=T, check.names=F, as.is=T, quote="", row.names=1)
sampleNamesData <- colnames(dat)

## Replace all -Inf with NA (no matter the source)
dat[ dat == -Inf ] <- NA

## Find how many rows contain NA values (if any) and if so, exit
ind <- apply(dat, 1, function(x) { any(is.na(x)) })
rowsWithNA <- length(which(ind == TRUE))
if (rowsWithNA > 0) {
  writeLines(paste("\nError: There are ", rowsWithNA, " rows containing NA values - ComBat will remove these.",sep=""))
  writeLines("Consider using limma's 'removeBatchEffect()' if lossless output is important.\n")
  q(status=1)
}

## Check sample ID match between data and pheno file
if (identical(sort(row.names(phenoMap)), sort(sampleNamesData)) != TRUE) {
  print(paste("Error: Samples in phenotype file '",pheno_file,"' do not match samples in expression data (check quantity and IDs)!",sep=""))
  q(status=1)
}
## Must be ordered as per sampleNames(data)
## NOTE: Assumes column names "class" and "batch" are used
sampleGroupsOrdered <- unlist(lapply(sampleNamesData, function(x) { phenoMap[x,"class"] }))
sampleGroupsOrderedBatch <- unlist(lapply(sampleNamesData, function(x) { phenoMap[x,"batch"] }))
sampleGroupVector <- as.numeric(as.factor(sampleGroupsOrdered))
sampleGroupVectorBatch <- as.numeric(as.factor(sampleGroupsOrderedBatch))
groupNames <- unique(sampleGroupsOrdered)
groupNamesBatch <- unique(sampleGroupsOrderedBatch)

## Set sample names for normal PCAs
sampleRepIDs <- c()
processed <- list()
for (s in sampleGroupsOrdered) {
  if (s %in% names(processed)) {
    processed[[s]] <- processed[[s]] + 1
  } else {
    processed[[s]] <- 1
  }
  sampleRepIDs <- c(sampleRepIDs, paste(s, processed[[s]], sep=" "))
}
sampleNamesPCA <- sampleRepIDs

## Set sample names for batch PCAs
sampleRepIDs <- c()
processed <- list()
for (s in sampleGroupsOrderedBatch) {
  s <- paste("batch_",s,sep="")
  if (s %in% names(processed)) {
    processed[[s]] <- processed[[s]] + 1
  } else {
    processed[[s]] <- 1
  }
  sampleRepIDs <- c(sampleRepIDs, paste(s, processed[[s]], sep=" "))
}
sampleNamesPCABatch <- sampleRepIDs


### DEBUG ###
if (debug) {
  print("Sample group vector:")
  print(sampleGroupVector)
  print("Sample group names:")
  print(groupNames)
  print("Sample names for PCA:")
  print(sampleNamesPCA)
  Sys.sleep(5)
### END DEBUG ###
}

## PCA options are set here
legendX <- 0
legendY <- 0
## Disable legend by default because the legend plotting algorithm really sucks
## and legend either gets placed really poorly or not at all (requiring script
## halt while waiting for user input for x,y coordinates)
doLegend <- FALSE
doSquarePCA <- FALSE



###############################################################################
#
# QC metrics and plots
#
###############################################################################


bitmap(paste(output_path,"/qc_density_plot_pre_combat.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
# shows distrubtion of probe intensities
plot(density(as.matrix(dat), na.rm=TRUE), main="Expression density pre-ComBat")
dev.off()

## Pre-norm PCA and "screeplot"
bitmap(paste(output_path,"/pre_combat_pca_pc12.png",sep=""),type="png16m",width=1200, height=1200, units="px")
#par(ps=9)
plotPCA(as.matrix(dat),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,2),doLegend,"Sample PCA Plot - Pre-ComBat PC1/2",FALSE)
dev.off()
bitmap(paste(output_path,"/pre_combat_pca_pc13.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(dat),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,3),doLegend,"Sample PCA Plot - Pre-ComBat PC1/3",FALSE)
dev.off()
bitmap(paste(output_path,"/pre_combat_pca_pc23.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(dat),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(2,3),doLegend,"Sample PCA Plot - Pre-ComBat PC2/3",FALSE)
dev.off()
bitmap(paste(output_path,"/pre_combat_screeplot.png",sep=""),type="png16m")
plotPCA(as.matrix(dat),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,TRUE,FALSE,NULL,NULL,c(1,2),doLegend,"Sample PCA screeplot - Pre-ComBat",FALSE)
dev.off()

## Pre-norm PCA and "screeplot" - BATCH
bitmap(paste(output_path,"/pre_combat_pca_pc12_batch.png",sep=""),type="png16m",width=1200, height=1200, units="px")
#par(ps=9)
plotPCA(as.matrix(dat),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,2),doLegend,"Sample PCA Plot - Pre-ComBat BATCH PC1/2",FALSE)
dev.off()
bitmap(paste(output_path,"/pre_combat_pca_pc13_batch.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(dat),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,3),doLegend,"Sample PCA Plot - Pre-ComBat BATCH PC1/3",FALSE)
dev.off()
bitmap(paste(output_path,"/pre_combat_pca_pc23_batch.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(dat),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(2,3),doLegend,"Sample PCA Plot - Pre-ComBat BATCH PC2/3",FALSE)
dev.off()
bitmap(paste(output_path,"/pre_combat_screeplot_batch.png",sep=""),type="png16m")
plotPCA(as.matrix(dat),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,TRUE,FALSE,NULL,NULL,c(1,2),doLegend,"Sample PCA screeplot - Pre-ComBat BATCH",FALSE)
dev.off()


###############################################################################
#
# Batch variation removal / reduction
#
###############################################################################


mod <- model.matrix(~as.factor(class), data=phenoMap)
bitmap(paste(output_path,"/ComBat_prior_plots.png",sep=""),type="png16m",width=1200, height=1200, units="px")
combatted <- ComBat(dat=dat, batch=phenoMap$batch, mod=mod, par.prior=TRUE, prior.plots=TRUE)
dev.off()


write.table(combatted, file=paste(output_path,"/post_combat_expression.txt",sep=""), sep="\t", row.names=TRUE, col.names=NA, quote=FALSE, na="")

# Post-ComBat density plot
bitmap(paste(output_path,"/qc_density_plot_post_combat.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
plot(density(as.matrix(combatted), na.rm=TRUE), main="Post-ComBat expression density")
dev.off()

## Post-ComBat PCA and "screeplot"
bitmap(paste(output_path,"/post_combat_pca_pc12.png",sep=""),type="png16m",width=1200, height=1200, units="px")
#par(ps=9)
plotPCA(as.matrix(combatted),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,2),doLegend,"Sample PCA Plot - Post-ComBat PC1/2",FALSE)
dev.off()
bitmap(paste(output_path,"/post_combat_pca_pc13.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(combatted),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,3),doLegend,"Sample PCA Plot - Post-ComBat PC1/3",FALSE)
dev.off()
bitmap(paste(output_path,"/post_combat_pca_pc23.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(combatted),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(2,3),doLegend,"Sample PCA Plot - Post-ComBat PC2/3",FALSE)
dev.off()
bitmap(paste(output_path,"/post_combat_screeplot.png",sep=""),type="png16m")
plotPCA(as.matrix(combatted),sampleGroupVector,groupNames,sampleNamesPCA,legendX,legendY,TRUE,FALSE,NULL,NULL,c(1,2),doLegend,"Sample PCA screeplot - Post-ComBat",FALSE)
dev.off()

## Post-ComBat PCA and "screeplot"
bitmap(paste(output_path,"/post_combat_pca_pc12_batch.png",sep=""),type="png16m",width=1200, height=1200, units="px")
#par(ps=9)
plotPCA(as.matrix(combatted),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,2),doLegend,"Sample PCA Plot - Post-ComBat BATCH PC1/2",FALSE)
dev.off()
bitmap(paste(output_path,"/post_combat_pca_pc13_batch.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(combatted),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(1,3),doLegend,"Sample PCA Plot - Post-ComBat BATCH PC1/3",FALSE)
dev.off()
bitmap(paste(output_path,"/post_combat_pca_pc23_batch.png",sep=""),type="png16m",width=1200, height=1200, units="px")
plotPCA(as.matrix(combatted),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,FALSE,doSquarePCA,NULL,NULL,c(2,3),doLegend,"Sample PCA Plot - Post-ComBat BATCH PC2/3",FALSE)
dev.off()
bitmap(paste(output_path,"/post_combat_screeplot_batch.png",sep=""),type="png16m")
plotPCA(as.matrix(combatted),sampleGroupVectorBatch,groupNamesBatch,sampleNamesPCABatch,legendX,legendY,TRUE,FALSE,NULL,NULL,c(1,2),doLegend,"Sample PCA screeplot - Post-ComBat BATCH",FALSE)
dev.off()

## Write sessionInfo() for R and library version info
writeLines(capture.output(sessionInfo()), paste(output_path,"/sessionInfo_ComBat.txt",sep=""))
