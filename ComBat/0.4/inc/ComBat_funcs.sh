
## Compile output PNG to PDF (ComBat)
##
compile_combat_qc_pdf () {
  ccqp_outdir="$1"
  cd "$ccqp_outdir" && convert qc_density_plot_pre_combat.png pre_combat_pca_pc12.png pre_combat_pca_pc12_batch.png pre_combat_pca_pc13.png pre_combat_pca_pc13_batch.png pre_combat_pca_pc23.png pre_combat_pca_pc23_batch.png pre_combat_screeplot.png ComBat_prior_plots.png \
qc_density_plot_post_combat.png post_combat_pca_pc12.png post_combat_pca_pc12_batch.png post_combat_pca_pc13.png post_combat_pca_pc13_batch.png post_combat_pca_pc23.png post_combat_pca_pc23_batch.png post_combat_screeplot.png \
QC_ComBat.PDF
  cd $OLDPWD
  return $?
}


## Compile output PNG to PDF (limma removeBatchEffect)
##
compile_removeBatch_qc_pdf () {
  crbqp_outdir="$1"
  cd "$crbqp_outdir" && convert qc_density_plot_pre_batch_removed.png pre_batch_removed_pca_pc12.png pre_batch_removed_sampleids_pca_pc12.png pre_batch_removed_pca_pc12_batch*.png pre_batch_removed_pca_pc13.png pre_batch_removed_sampleids_pca_pc13.png pre_batch_removed_pca_pc13_batch*.png pre_batch_removed_pca_pc23.png pre_batch_removed_sampleids_pca_pc23.png pre_batch_removed_pca_pc23_batch*.png pre_batch_removed_screeplot.png \
qc_density_plot_post_batch_removed.png post_batch_removed_pca_pc12.png post_batch_removed_sampleids_pca_pc12.png post_batch_removed_pca_pc12_batch*.png post_batch_removed_pca_pc13.png post_batch_removed_sampleids_pca_pc13.png post_batch_removed_pca_pc13_batch*.png post_batch_removed_pca_pc23.png post_batch_removed_sampleids_pca_pc23.png post_batch_removed_pca_pc23_batch*.png post_batch_removed_screeplot.png \
QC_BatchRemoval.PDF
  cd $OLDPWD
  return $?
}


## ComBat batch removal
##
do_combat () {
  inputfile="$1"
  phenofile="$2"
  outdir="$3"

  call_R "$ComBat_R" "$S4M_THIS_MODULE/scripts/ComBat.R"  "-inputfile='$inputfile' -phenofile='$phenofile' -outdir='$outdir'"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  s4m_log "Compiling ComBat QC plots PDF.."
  compile_combat_qc_pdf "$outdir"

  s4m_log "Done"
  return $?
}


## Limma removeBatchEffect()
##
do_removebatch () {
  inputfile="$1"
  phenofile="$2"
  outdir="$3"

  extraflags=""
  if s4m_isdebug; then
    extraflags="-debug=TRUE"
  fi
  call_R "$ComBat_R" "$S4M_THIS_MODULE/scripts/limma_removeBatchEffect.R"  "-inputfile='$inputfile' -phenofile='$phenofile' -outdir='$outdir' $extraflags"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  s4m_log "Compiling batch corrected QC plots PDF.."
  compile_removeBatch_qc_pdf "$outdir"

  s4m_log "Done"
  return $?
}
