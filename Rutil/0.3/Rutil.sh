#/bin/sh
##===========================================================================
## Module: Rutil
## Author: Othmar Korn
##
## Utility module - library module (not executable)
##===========================================================================

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done


## Global default system R path
RUTIL_DEFAULT_R="R"


## Check libraries install status and versions if supplied.
##
## Unit Test/s: TODO
##
check_R_libraries () {
  ## Path to R installation to test
  crd_R_BIN="$1"

  ## List of packages with or without version numbers.
  ##
  ## Examples:
  ## crd_package_list="tools>=2.0.5
  ##                   util==3.3.3"
  ##
  ## crd_package_list="tools
  ##                   util"
  ##
  crd_package_list="$2"

  for p in $crd_package_list
  do
    ## PORTABILITY - grep
    echo "$p" | grep -P "(\>\=|\=\=|\<\=)" > /dev/null
    if [ $? -eq 0 ]; then
      crd_operator=`echo "$p" | sed -r -e 's|^.*([\<\>\=]{2}).*$|\1|'`
      crd_package=`echo "$p" | tr "$crd_operator" ' ' | cut -d' ' -f 1`
      crd_version=`echo "$p" | tr "$crd_operator" ' ' | sed -r -e 's| +| |' | cut -d' ' -f 2`
      if s4m_isdebug; then
        echo "   DEBUG: Checking package=[$crd_package], operator=[$crd_operator], version=[$crd_version]" 1>&2
      fi
      crd_check_versions=TRUE
    fi

    ## Simple check for library
    ## Tested in R 2.x and 3.x
    $crd_R_BIN --slave -e "if (! library('$crd_package', logical.return=T)) { q(status=1) }" 2> /dev/null
    if [ $? -ne 0 ]; then
      ret=1
      echo "   Error: Library '$crd_package' not installed!" 1>&2
      return $ret
    else
      echo "   Library '$crd_package' is installed." 1>&2
    fi

    ## Check package version
    ## Tested in R 2.x and 3.x
    if [ -z "$crd_check_versions" -o "$crd_check_versions" == "TRUE" ]; then
      ## Yes.. we can use >= or <= in string comparisons in R e.g. if ("1.0.1" >= "1.0") ...
      ## NOTE: There's a chance this is broken for some version strings. Not tested exhaustively.
      $crd_R_BIN --slave -e "if (! packageVersion('$crd_package') $crd_operator '$crd_version') { q(status=1); }" 2> /dev/null
      if [ $? -ne 0 ]; then
        echo "   Error: Library failed version check $crd_operator $crd_version" 1>&2
        return 1
      else
        echo "   Library '$crd_package' is $crd_operator $crd_version" 1>&2
      fi
    fi
  done
}


## Process 'R' binary override if not using default 'R'
##
## NOTE: Deprecated but remains here until we're sure this API function is no longer used.
##
## TODO: Deprecate usage in favour of callers using "call_R()" (below)
##
## Unit Test/s: TODO
##
check_set_alternate_R () {
  ## the variable name containing R path
  Rpath_varname="$1"
  ## the variable name which will hold our final R path
  export_R_varname="$2"

  ## set default to 'R'
  eval $export_R_varname="R"

  Rpath=`eval echo "\\$$Rpath_varname"`

  ## Process 'R' override if needed
  if [ ! -z "$Rpath" ]; then
    ## simple check of R operation
    rvers=`$Rpath --version 2> /dev/null`
    if [ $? -eq 0 ]; then
      rvers=`echo "$rvers" | head -1`
      s4m_log "Using alternate R binary: $Rpath ($rvers)"
      eval "$export_R_varname=$Rpath; export $export_R_varname"
    else
      s4m_error "Bad R binary '$Rpath'!"
      return 1
    fi
  fi
}


## Validate given R path
##
## Unit Test/s: TODO
##
validate_R_path () {
  vr_path="$1"

  if [ ! -z "$vr_path" ]; then
    ## simple check of R operation
    rvers=`$vr_path --version 2> /dev/null`
    if [ $? -eq 0 ]; then
      rvers=`echo "$rvers" | head -1`
      s4m_log "Using R binary: $vr_path ($rvers)"
    else
      s4m_error "Bad R binary path '$vr_path'!"
      return 1
    fi
  else
    s4m_error "Failed to validate R path; no path or binary name was given"
    return 1
  fi
}


## Run R using the given user R binary path
##
## Unit Test/s: TODO
##
call_R () {
  cr_path="$1"
  cr_target="$2"
  ## force all args in one string, there might be many so we don't want to use '$@'
  cr_args="$3"

  ## Set default to global R if not given
  if [ -z "$cr_path" ]; then
    s4m_log "No R override given, setting default '$RUTIL_DEFAULT_R'"
    cr_path="$RUTIL_DEFAULT_R"
  fi

  if validate_R_path "$cr_path"; then
    s4m_debug "Invoking R script '$cr_target' with args=[$cr_args]"
    ## Enclose any space-separated values in "[-]key=val" assignments with extra double quotes
    ## NOTE: This doesn't work - shell strips the quotes and issue persists -
    ##   for now, the caller should make sure to not require spaces in input strings
    #cr_args=`echo "$cr_args" | sed -r -e "s|([A-Za-z_\-]+\='[^']+[ ]+.*')|\"\1\"|"`
    #s4m_error "DEBUG: cr_args=[$cr_args]"
    $cr_path --slave -f "$cr_target" --args $cr_args
    return $?
  else
    return 1
  fi
}

