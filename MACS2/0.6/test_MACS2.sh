#!/bin/sh
## test_MACS2.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested
  . ./inc/targets_file_utils.sh
  . ./inc/MACS2_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests (uncomment if needed)
#setUp () {
#}


testdatadir=./etc/testdata

## TESTS ##

test_macs2_environment () {
  s4m_exec MACS2::version > /dev/null 2>&1
  assertEquals 0 $?
}

test_fileutil_validate_targets_file_good () {
  fileutil_validate_targets_file "$testdatadir/targets_good.txt"
  assertEquals 0 $?

  fileutil_validate_targets_file "$testdatadir/targets_good_1.txt"
  assertEquals 0 $?
}

test_fileutil_validate_targets_file_bad () {
  ## Rows with different numbers of fields
  fileutil_validate_targets_file "$testdatadir/targets_bad_1.txt" 2> /dev/null
  assertEquals 1 $?

  ## Missing mandatory 'ReadFile1' column
  fileutil_validate_targets_file "$testdatadir/targets_bad_2.txt" 2> /dev/null
  assertEquals 1 $?
}

test_fileutil_get_targets_attr_map () {
  attrmap_get=`fileutil_get_targets_attr_map "$testdatadir/targets_good.txt"`
  attrmap_expect="SampleID:1
SampleType:2
ReadFile1:3
ControlID:4"
  assertEquals "$attrmap_get" "$attrmap_expect"

  ## should fail on bad targets file
  fileutil_get_targets_attr_map "targets_file_does_not_exist.txt" 2> /dev/null
  assertEquals 1 $?
}

test_fileutil_extract_target_token () {
  foo=`fileutil_extract_target_token "foo	bar	baz" 1`
  assertEquals "$foo" "foo"

  bar=`fileutil_extract_target_token "foo	bar	baz" 2`
  assertEquals "$bar" "bar"

  baz=`fileutil_extract_target_token "foo	bar	baz" 3`
  assertEquals "$baz" "baz"
}

test_fileutil_extract_token_by_name () {
  targetsline=`sed -n '2p' "$testdatadir/targets_good.txt"`

  attrmap=`fileutil_get_targets_attr_map "$testdatadir/targets_good.txt"`
  sampleid_get=`fileutil_extract_token_by_name "$targetsline" "$attrmap" "SampleID"`
  sampleid_expect="GSM_XXX"
  assertEquals "$sampleid_get" "$sampleid_expect"

  readfile1_get=`fileutil_extract_token_by_name "$targetsline" "$attrmap" "ReadFile1"`
  readfile1_expect="GSMXXX.fastq"
  assertEquals "$readfile1_get" "$readfile1_expect"

  ## bad attribute name
  fileutil_extract_token_by_name "$targetsline" "$attrmap" "DoesNotExist" 2> /dev/null
  assertEquals 1 $?

  ## null args
  fileutil_extract_token_by_name "$targetsline" "" "" 2> /dev/null
  assertEquals 1 $?
}

test_fileutil_get_targets_rows () {
  targetsrows=`fileutil_get_targets_rows "$testdatadir/targets_good.txt"`
  nrows=`echo "$targetsrows" | wc -l`
  assertEquals 5 "$nrows"

  ## bad targets file
  fileutil_get_targets_rows "$testdatadir/targets_file_does_not_exist.txt" 2> /dev/null
  assertEquals 1 $?
}

test_fileutil_iterate_targets () {

  my_simple_targets_iterator () {
    echo "$1"
  }

  my_targets_iterator () {
    targetsrow="$1"
    attrmap="$2"
    ## print SampleID
    fileutil_extract_token_by_name "$targetsrow" "$attrmap" SampleID
  }

  ## Simple example which prints each row of targets file
  iter_return=`fileutil_iterate_targets "$testdatadir/targets_good.txt" my_simple_targets_iterator`
  iter_return_nrows=`echo "$iter_return" | wc -l`
  assertEquals 5 $iter_return_nrows

  ## More advanced example which tests attribute map query and token extraction
  iter_return=`fileutil_iterate_targets "$testdatadir/targets_good.txt" my_targets_iterator`
  iter_expect=`tail -n +2 "$testdatadir/targets_good.txt" | cut -f 1`
  assertEquals "$iter_return" "$iter_expect"

  ## bad targets file
  fileutil_iterate_targets "$testdatadir/targets_file_does_not_exist.txt" my_simple_targets_iterator > /dev/null 2>&1
  assertEquals 1 $?
}


test_find_files_by_basename () {
  touch "$S4M_TMP/$$/foo.txt" "$S4M_TMP/$$/foo.bar" "$S4M_TMP/$$/baz.txt"

  ret=`macs2_find_files_by_basename "foo" "$S4M_TMP/$$" "txt"`
  assertEquals 0 $?
  assertEquals "$S4M_TMP/$$/foo.txt" "$ret"

  ret=`macs2_find_files_by_basename "foo" "$S4M_TMP/$$" "txt bar"`
  assertEquals 0 $?
  numret=`echo "$ret" | wc -l`
  #echo "DEBUG: ret=[$ret]"
  assertEquals 2 $numret

  ret=`macs2_find_files_by_basename "file_does_not_exist" "$S4M_TMP/$$" ""`
  assertEquals 1 $?

  ret=`macs2_find_files_by_basename "baz" "$S4M_TMP/$$"`
  assertEquals 0 $?
  assertEquals "$S4M_TMP/$$/baz.txt" "$ret"

  ret=`macs2_find_files_by_basename "baz" "$S4M_TMP/$$" ""`
  assertEquals 0 $?
  assertEquals "$S4M_TMP/$$/baz.txt" "$ret"
}


test_write_chipqc_samples_file () {
  MACS2_targets="$testdatadir/targets_good.txt"
  MACS2_inputdir="$testdatadir/bams"
  MACS2_outputdir="$S4M_TMP/$$/peakcalls"
  mkdir -p "$MACS2_outputdir"

  macs2_write_chipqc_samples_file "$MACS2_outputdir/chipqc_samples.txt"
  assertEquals 0 $?
  echo "Wrote $MACS2_outputdir/chipqc_samples.txt:"; echo
  cat "$MACS2_outputdir/chipqc_samples.txt"

  ## Again, but for a targets file that doesn't have ControlID - it should be
  ## written in output however

  MACS2_targets="$testdatadir/targets_good_1.txt"
  macs2_write_chipqc_samples_file "$MACS2_outputdir/chipqc_samples_1.txt"
  assertEquals 0 $?
  ## Output must contain 'ControlID' column
  head -1 "$MACS2_outputdir/chipqc_samples_1.txt" | grep ControlID > /dev/null 2>&1
  assertEquals 0 $?
  echo "Wrote $MACS2_outputdir/chipqc_samples_1.txt:"; echo
  cat "$MACS2_outputdir/chipqc_samples_1.txt"
}

