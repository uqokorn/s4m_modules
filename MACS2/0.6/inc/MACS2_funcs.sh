## MACS2 binary name
MACS2_BIN=macs2

## Helper - get file(s) from a directory by its base name and a selection
## of allowed file extensions
##
## NOTE: Can return multiple matches.
##
## Unit Test:
##   test_MACS2.sh:test_find_files_by_basename()
##
macs2_find_files_by_basename () {
  mffbb_basename="$1"
  mffbb_find_dir="$2"
  ## Allowed file extensions for match - if empty, ignore file extensions
  mffbb_search_exts="$3"
  ## If not empty, assume true (resolve found file to full abs path)
  mffbb_resolve_path="$4"

  if [ ! -z "$mffbb_search_exts" ]; then
    mffbb_ret=""
    for ext in $mffbb_search_exts
    do
      mffbb_found=`find "$mffbb_find_dir" -maxdepth 1 -type f -name "${mffbb_basename}.$ext" -print`
      if [ $? -eq 0 -a ! -z "$mffbb_found" ]; then
        if [ ! -z "$mffbb_resolve_path" ]; then
          mffbb_found=`readlink -f "$mffbb_found"`
        fi
        if [ -z "$mffbb_ret" ]; then
          mffbb_ret="$mffbb_found"
        else
          mffbb_ret="$mffbb_ret
$mffbb_found"
        fi
      fi
    done

    if [ ! -z "$mffbb_ret" ]; then
      echo "$mffbb_ret"
      return
    fi

  else
    mffbb_found=`find "$mffbb_find_dir" -maxdepth 1 -type f -name "${mffbb_basename}.*" -print`
    if [ $? -eq 0 -a ! -z "$mffbb_found" ]; then
      if [ ! -z "$mffbb_resolve_path" ]; then
        mffbb_found=`readlink -f "$mffbb_found"`
      fi
      echo "$mffbb_found"
      return
    fi
  fi

  return 1
}


## Run MACS2 over a given ChIP-seq library (with or without matched control)
##
## Args:
##   target: Row from targets file
##   attr_pos_map: Targets "<column>:<pos>" multi-line string (one line per attribute)
##
macs2_run_target () {
  m2r_targetline="$1"
  m2r_attr_pos_map="$2"

  m2r_sampleid=`fileutil_extract_token_by_name "$m2r_targetline" "$m2r_attr_pos_map" SampleID`
  m2r_readfile1=`fileutil_extract_token_by_name "$m2r_targetline" "$m2r_attr_pos_map" ReadFile1`
  ## ignore missing = true (ControlID might not be present when not treat vs control design)
  m2r_controlid=`fileutil_extract_token_by_name "$m2r_targetline" "$m2r_attr_pos_map" ControlID true`

  m2r_ext_whitelist="bam bed"
  ## If multiple hits, take the first one
  m2r_treatfile=`macs2_find_files_by_basename "$m2r_sampleid" "$MACS2_inputdir" "$m2r_ext_whitelist" | head -1`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to find input treatment file for sample '$m2r_sampleid' (allowed extensions: $m2r_ext_whitelist)!"
  else
    s4m_log "Using treat file:  $m2r_treatfile"
  fi
  if [ ! -z "$m2r_controlid" ]; then
    m2r_controlfile=`macs2_find_files_by_basename "$m2r_controlid" "$MACS2_inputdir" "$m2r_ext_whitelist"`
  fi

  ## Base args regardless of peak type
  ## --verbose 3 (or higher) will give details peak stats per chromosome etc.
  m2r_args="callpeak  --treatment $m2r_treatfile --outdir $MACS2_outputdir -n $m2r_sampleid --verbose 3"

  if [ ! -z "$m2r_controlfile" ]; then
    if [ ! -f "$m2r_controlfile" ]; then
      s4m_error "Control library '$m2r_controlfile' not found for sample '$m2r_sampleid'!"
      return 1
    else
      s4m_log "Using control file:  $m2r_controlfile"
    fi
    m2r_args="$m2r_args -c $m2r_controlfile"
  fi

  ## Default "regular" peaks
  ## NOTE: --call-summits is *ONLY* currently applied for this peak type (not when "broad")
  if [ -z "$MACS2_peaktype" ]; then
    MACS2_peaktype="regular"
  fi

  ## TODO: T#2642: Test
  ## Set pval/qval parameters for broad or regular peaks
  ## Use EITHER pvalue or qvalue. P-val takes precedence.
  m2r_sigparam=""
  if [ ! -z "$MACS2_pvalue" ]; then
    s4m_log "Using supplied p-value override = $MACS2_pvalue"
    m2r_pval="$MACS2_pvalue"
    m2r_sigparam="--pvalue $m2r_pval"
  elif [ ! -z "$MACS2_qvalue" ]; then
    s4m_log "Using supplied q-value override = $MACS2_qvalue"
    m2r_qval="$MACS2_qvalue"
    m2r_sigparam="--qvalue $m2r_qval"
  fi
  ## No user-input p-value or q-value, set default
  if [ -z "$m2r_sigparam" ]; then
    ## MACS2 docs recommend 0.05 qvalue for broad peaks, 0.01 otherwise
    if [ "$MACS2_peaktype" = "broad" ]; then
      s4m_log "Setting q-value = 0.05 for 'broad' peaks"
      m2r_sigparam="--qvalue 0.05"
    else 
      s4m_log "Setting q-value = 0.01 for 'regular' (narrow) peaks"
      m2r_sigparam="--qvalue 0.01"
    fi
  fi
  ## Bring all the args together for "broad" or "regular" peaks
  if [ "$MACS2_peaktype" = "broad" ]; then
    ## MACS2 '--broad-cutoff' default is 0.1 but specify it for verbosity
    m2r_args="$m2r_args --broad --broad-cutoff 0.1 $m2r_sigparam"
  else
    m2r_args="$m2r_args $m2r_sigparam --call-summits"
    ## Additionally generate bedGraph "signal" track - see:
    ## https://github.com/taoliu/MACS/wiki/Build-Signal-Track
    if [ ! -z "$MACS2_signaltrack" ]; then
      s4m_log "Generating 'signal track' (bedGraph) output.."
      m2r_args="$m2r_args -B --SPMR"
    fi
  fi
  ## Override --keep-dup setting if given. Default "auto" (binomial model) to work
  ## out how many to keep.
  ## Can also be "all" or an integer for number of duplicates to keep at a location.
  if [ ! -z "$MACS2_keepdup" ]; then
    m2r_args="$m2r_args --keep-dup $MACS2_keepdup"
  fi
  ## Overrides to force MACS2 to use fixed shift/extension length instead of modelling these.
  ## If input is PE (BAMPE or BEDPE) and want MACS2 to use actual observed fragments,
  ## *do not* specify "--nomodel", "--extsize" or "--shift".
  ## NOTE: If format is BAMPE or BEDPE, MACS2 will force --shift  to be '0'
  if [ ! -z "$MACS2_nomodel" ]; then
    m2r_args="$m2r_args --nomodel"
    if [ ! -z "$MACS2_extsize" ]; then
      m2r_args="$m2r_args --extsize $MACS2_extsize" 
    fi
    if [ ! -z "$MACS2_shift" ]; then
      m2r_args="$m2r_args --shift $MACS2_shift" 
    fi
  fi
  ## MACS2 can't determine BAMPE format automatically, so if input format is given (i.e. not "AUTO"),
  ## make sure MACS2 knows about it
  if [ ! -z "$MACS2_format" ]; then
    m2r_args="$m2r_args --format $MACS2_format"
  fi
  ## Set tempdir if given
  if [ ! -z "$MACS2_tempdir" ]; then
    m2r_args="$m2r_args --tempdir $MACS2_tempdir"
  fi

  ## Run MACS2 with appropriate args
  s4m_debug "Calling MACS2: [$MACS2_BIN $m2r_args]"; sleep 2
  $MACS2_BIN $m2r_args
  return $?

  ## TODO: If $MACS2_signaltrack given, need to run "macs2 bdgcmp .."
  ## See ENCODE example:
  ## https://github.com/kundajelab/atac_dnase_pipelines/blob/master/modules/callpeak_macs2.bds
}


## Write a single row of the ChIPQC sample info table (one chip library / control row).
## Used by 'macs2_write_chipqc_samples_file()'
##
## Unit Test:
##   test_MACS2.sh:test_write_chipqc_samples_file()
##
macs2_write_chipqc_sample_row () {
  m2w_targetline="$1"
  m2w_attr_pos_map="$2"

  m2w_sampleid=`fileutil_extract_token_by_name "$m2w_targetline" "$m2w_attr_pos_map" SampleID`
  m2w_sampletype=`fileutil_extract_token_by_name "$m2w_targetline" "$m2w_attr_pos_map" SampleType`
  m2w_readfile1=`fileutil_extract_token_by_name "$m2w_targetline" "$m2w_attr_pos_map" ReadFile1`
  ## ControlID could be blank if no controls used anywhere
  m2w_controlid=`fileutil_extract_token_by_name "$m2w_targetline" "$m2w_attr_pos_map" ControlID "true"`
  ## Input targets.txt may not require 'Replicate' field (e.g. if not using R/ChIPQC downstream)
  m2w_replicate=`fileutil_extract_token_by_name "$m2w_targetline" "$m2w_attr_pos_map" Replicate "true"`
  m2w_already_warned="false"

  ## If 'Replicate' isn't there, give value "1" for non-controls, and "c1" for controls
  if [ -z "$m2w_replicate" -a "$m2w_already_warned" = "false" ]; then
    warn="ChIPQC sample table writer: Auto-assigning replicate value '1' to each ChIP library and
  'c1' to each control. If not correct, edit file:
  '$MACS2_CHIPQC_SAMPLES'
  before running ChIPQC downstream."
    s4m_warn "$warn"
    m2w_already_warned="true"
    sleep 2
    if [ -z "$m2w_controlid" ]; then
      m2w_replicate="c1"
    else
      m2w_replicate="1"
    fi
  fi

  m2w_ext_whitelist="bam bed"
  ## If multiple hits, take the first one
  m2w_treatfile=`macs2_find_files_by_basename "$m2w_sampleid" "$MACS2_inputdir" "$m2w_ext_whitelist" "true" | head -1`
  if [ $? -ne 0 ]; then
    s4m_error "Failed to find input treatment file for sample '$m2w_sampleid' (allowed extensions: $m2w_ext_whitelist)!"
  fi
  m2w_controlfile=""
  ## Only look for control library if a control is used
  if [ ! -z "$m2w_controlid" ]; then
    m2w_controlfile=`macs2_find_files_by_basename "$m2w_controlid" "$MACS2_inputdir" "$m2w_ext_whitelist" "true"`
  fi

  m2w_peakfile_ext="narrowPeak"
  if [ ! -z "$MACS2_peaktype" ]; then
    if [ "$MACS2_peaktype" = "broad" ]; then
      m2w_peakfile_ext="broadPeak"
    fi
  fi
  m2w_peakfile="$MACS2_outputdir/${m2w_sampleid}_peaks.$m2w_peakfile_ext"

  ## SampleID <TAB> Factor <TAB> Replicate <TAB> bamReads <TAB> ControlID <TAB> bamControl <TAB> Peaks
  /bin/echo -e "$m2w_sampleid\t$m2w_sampletype\t$m2w_replicate\t$m2w_treatfile\t$m2w_controlid\t$m2w_controlfile\t$m2w_peakfile" >> "$MACS2_CHIPQC_SAMPLES"
}


## Write samples input file for R/ChIPQC - saves having to create it manually
## if wanting to use ChIPQC on MACS2 outputs.
##
## Unit Test:
##   test_MACS2.sh:test_write_chipqc_samples_file()
##
macs2_write_chipqc_samples_file () {
  MACS2_CHIPQC_SAMPLES="$1"
  rm -f "$MACS2_CHIPQC_SAMPLES"

  ## Write minimum header
  /bin/echo -e "SampleID\tFactor\tReplicate\tbamReads\tControlID\tbamControl\tPeaks" > "$MACS2_CHIPQC_SAMPLES"

  fileutil_iterate_targets "$MACS2_targets" "macs2_write_chipqc_sample_row"
}

