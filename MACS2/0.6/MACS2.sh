#/bin/sh
##===========================================================================
## Module:   MACS2
## Author:   Othmar Korn
##
## Wrapper for MACS2 peak caller
##===========================================================================

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

. ./inc/targets_file_utils.sh
. ./inc/MACS2_funcs.sh


macs2_do_peak_calls () {
  ## Run MACS2 over each library (iterate over targets rows)
  fileutil_iterate_targets "$MACS2_targets" "macs2_run_target"
  ret=$?

  if [ $ret -eq 0 ]; then
    s4m_log "MACS2 completed successfully over all targets"
  fi

  ## Write a suitable sample table input file for ChIPQC now, otherwise it's
  ## a real pain to automate later.
  ## If users don't care about ChIPQC they can just ignore this output file!
  ##
  ## TODO: Maybe turn this into an optional flag, although it runs fast anyway.
  ##
  if [ $ret -eq 0 ]; then
    s4m_log "Writing sample input table for R/ChIPQC (ignore this if not needed)"
    fbase=`basename "$MACS2_targets" .txt`
    macs2_write_chipqc_samples_file "$MACS2_outputdir/${fbase}_chipqc.txt"
    ret=$?
  fi
  return $ret
}


## Run the peak model image plotting .R scripts created by MACS2 for each
## ChIP library
macs2_plot_peak_models () {

  cd "$MACS2_outputdir"
  modelfiles=`ls *model.r 2>/dev/null` 

  if [ ! -z "$modelfiles" ]; then
    s4m_log "Plotting per-library peak models and cross-correlation (fragment length prediction)"
    for model_r in $modelfiles
    do
      call_R "$MACS2_R" "$model_r"
      ret=$?
      if [ $ret -ne 0 ]; then
        cd $OLDPWD
        return $ret
      fi
    done
  else
    s4m_log "No peak model R files found, skip plotting.."
  fi

  cd $OLDPWD
}


## Validate ChIPQC specific fields in input targets file
macs2_validate_targets_file_chipqc () {
  mvtf_targets="$1"
  head -1 "$mvtf_targets" | grep "\bReplicate\b" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    warn="No 'Replicate' field in input targets file - this could prevent
  successful operation of R/ChIPQC over MACS2 results. If outputting ChIPQC-compatible sample
  table after peak calling, will attempt to add 'Replicate' values automatically."
    s4m_warn "$warn"
  fi
}


## Call peaks using MACS2
macs2_callpeaks_handler () {

  ## Check that we can run macs2
  if ! miniconda_activate; then
    return 1
  fi

  ## Validate minimum targets fields (does not check for ChIP-specific fields)
  fileutil_validate_targets_file "$MACS2_targets"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## Check ChIPQC-specific targets fields
  macs2_validate_targets_file_chipqc "$MACS2_targets"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  macs2_do_peak_calls
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
 
  macs2_plot_peak_models
  return $?
}


macs2_version_handler () {
  if ! miniconda_activate; then
    return 1
  fi

  $MACS2_BIN --version
  return $?
}


main () {
  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"
  ## Load module conda environment
  s4m_import "miniconda/lib.sh"

  ## Wrapper for "callpeak" command - handles "regular" and "broad" peak calls
  ## via "peaktype" option
  s4m_route callpeaks macs2_callpeaks_handler

  s4m_route version   macs2_version_handler

}

### START ###
main

