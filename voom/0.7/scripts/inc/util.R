## Script: 	util.R
## Description:	Simple arguments processing API and other utility functions
## Author: 	O.Korn

## ===========================================================================
##                             ARGS PROCESSING
## ===========================================================================
## Captures args invoked like:
##   R -f command_args_processing.R --args -input="input.txt" -output="output.txt"

## Only capture the part given on the shell after the "--args" bit
BH_args <- commandArgs(trailingOnly=TRUE)

## Split into key/vals
BH_argskv <- strsplit(BH_args, "=")

## Create a dictionary lookup of arguments
## e.g. structure like:
##
## argsLookup [
##     input : input.txt
##     output : output.txt
##     ...
## ]
##
ARGV <- list()
for (kv in BH_argskv) {
  ## Strip the leading "-" from arg name while we're here, and
  ## any single or double quotes wrapping the value
  ARGV[gsub("^-", "", kv[1])] <- gsub("^[\"\']+|[\"\']+$", "", kv[2])
}

ARGV.get <- function (arg, required=FALSE) {
  if (! arg %in% names(ARGV)) {
    if (required) {
      stop(paste("No such argument '",arg,"'!",sep=""))
    }
    warning(paste("No such argument '",arg,"'!",sep=""))
    NA
  } else {
    ARGV[[arg]]
  }
}



## ===========================================================================
##                             FILE FORMATS
## ===========================================================================

is.gct.file <- function (filePath) {
  f <- file(filePath, "r")
  firstLine <- readLines(f, n=1)
  close(f)
  if (length(firstLine) == 1) {
    firstLine == "#1.2"
  } else {
    FALSE
  }
}


is.cls.file <- function (filePath) {
  f <- file(filePath, "r")
  lines <- readLines(f, n=3)
  close(f)
  ## CLS file has at least 3 lines
  if (length(lines) == 3) {
    ## Second line starts with '#', number of samples annotated (first line) matches number of elements in 3rd line
    substr(lines[2], 1, 1) == "#" && length(unlist(strsplit(lines[3], " "))) == as.integer(unlist(strsplit(lines[1], "\t", perl=TRUE))[1])
  } else {
    FALSE
  }
}


## From: GenePattern module v1.0.2.
## A backup function if we can't find or load the GenePattern module
if (! exists("read.gct")) {
  read.gct <- function (file)  {
    if (is.character(file)) 
        if (file == "") 
            file <- stdin()
        else {
            file <- file(file, "r")
            on.exit(close(file))
        }
    if (!inherits(file, "connection")) 
        stop("argument `file' must be a character string or connection")
    version <- readLines(file, n = 1)
    dimensions <- scan(file, what = list("integer", "integer"), 
        nmax = 1, quiet = TRUE)
    rows <- dimensions[[1]]
    columns <- dimensions[[2]]
    column.names <- read.table(file, header = FALSE, nrows = 1, 
        sep = "\t", fill = FALSE)
    column.names <- column.names[3:length(column.names)]
    if (length(column.names) != columns) {
        stop(paste("Number of sample names", length(column.names), 
            "not equal to the number of columns", columns, "."))
    }
    colClasses <- c(rep(c("character"), 2), rep(c("double"), 
        columns))
    x <- read.table(file, header = FALSE, quote = "", row.names = NULL, 
        comment.char = "", sep = "\t", colClasses = colClasses, 
        fill = FALSE)
    row.descriptions <- as.character(x[, 2])
    data <- as.matrix(x[seq(from = 3, to = dim(x)[2], by = 1)])
    column.names <- column.names[!is.na(column.names)]
    colnames(data) <- column.names
    row.names(data) <- x[, 1]
    return(list(row.descriptions = row.descriptions, data = data))
  }
}


## From: GenePattern module v1.0.2.
## A backup function if we can't find or load the GenePattern module
if (! exists("read.cls")) {
  read.cls <- function (file)  {
    if (is.character(file)) 
        if (file == "") 
            file <- stdin()
        else {
            file <- file(file, "r")
            on.exit(close(file))
        }
    if (!inherits(file, "connection")) 
        stop("argument `file' must be a character string or connection")
    line1 <- scan(file, nlines = 1, what = "character", quiet = TRUE)
    numberOfDataPoints <- as.integer(line1[1])
    numberOfClasses <- as.integer(line1[2])
    line2 <- scan(file, nlines = 1, what = "character", quiet = TRUE)
    classNames <- NULL
    if (line2[1] == "#") {
        classNames <- as.vector(line2[2:length(line2)])
        line3 <- scan(file, what = "character", nlines = 1, quiet = TRUE)
    }
    else {
        line3 <- line2
    }
    if (is.null(classNames)) {
        labels <- as.factor(line3)
        classNames <- levels(labels)
    }
    else {
        labels <- factor(line3, labels = classNames)
    }
    if (numberOfDataPoints != length(labels)) {
        stop("Incorrect number of data points")
    }
    r <- list(labels = labels, names = classNames)
    r
  }
}
