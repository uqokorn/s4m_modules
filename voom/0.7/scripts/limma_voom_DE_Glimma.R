## ==========================================================================
## File:    limma_voom_DE_Glimma.R
##
## Author:  Othmar Korn
##
## Created: March 2015
##
## Description:
##
## Perform limma Voom DE analysis on count data and output Glimma HTML
## reports.
##
## Script expects raw count input.
##
## Can account for a known batch effect by specifying column which contains
## the batch factor in input "targets.txt" file.
##
## Annotation file *must* be supplied for Glimma.
##
## If 'cpmfilterval' flag given, counts will be filtered on given CPM being met
## in at least one factor group (using factors input as "contrasts").
##
## If 'cpmfilterminprop' flag given, require this fraction of samples in a
## group to meet the CPM criteria above. Default = 1 (100%)
##
## By default, a TMM scaling is applied to the data.
##
## By default, a final median scaling is applied to log2 CPM counts.
##
## CHANGELOG
## =========
## May  3, 2019 - Annotation file requirement is now optional
## Feb  6, 2018 - Add donor blocking (duplicateCorrelation) and factorial design support
## Jan 25, 2018 - Check sample ordering targets vs input data
## Oct 13, 2017 - Ensure non-Glimma outputs written to given folder
## Aug 15, 2017 - Add gene desc column to Glimma report
## Aug  4, 2017 - Glimma report sample labelling updates from T#2724
##
## ==========================================================================


## ==========================================================================
##	Globals
## ==========================================================================

## Is input data log2?
## !!! IMPORTANT for fold change calculation !!!
IS_LOG2 <- TRUE
## Pre-filter on min fold change in at least one comparison for a probe?
## NOTE: This boolean is not used - remove ASAP.
DO_FC_FILTER <- FALSE
## Regardless of above TRUE/FALSE, this value is used to set minimum fold
## change in output top tables and Venn tests
FC_FILTER <- 1.5

## Threshold on a minimum CPM value?
DO_CPM_FILTER <- FALSE
## The minimum CPM value
CPM_FILTER <- NA
## If have N > 2 in a sample group, we apply a min fraction of samples which must
## pass the CPM filter (range 0.1 to 1, where 1 denotes 100%).
## Any sample group with N <= 2 will require all samples to pass the CPM filter.
CPM_FILTER_MIN_PROPORTION <- 1


## Perform Voom normalization prior to model fitting?
## NOTE: Requires input raw counts! (not RPKM or scaled otherwise)
DO_VOOM <- FALSE

## If the "batch" input argument is given, this becomes TRUE
## and we factor a batch variable into the linear model
HAS_BATCH <- FALSE

## Annotation file optional (can use generic input eg gene symbols)
## if file not provided/found, raise warning and continue skipping annotations
HAS_ANNOTATIONS <- TRUE

## If experiment compares factors within donors (e.g. donors supply more than
## one sample e.g. treat vs control from each patient) then specify the
## targets column name that contains the donor information.
## Default is no donor blocking when flag not supplied.
HAS_DONOR_BLOCKING <- FALSE

## For designs like "Sex * Tissue"
## See argument "factorialdesign" below. If argument does not exist, this is false.
HAS_FACTORIAL_DESIGN <- FALSE

## Do Benjamini & Hochberg "FDR" multiple hypothesis testing p-val adjustment
## (in addition to raw p-val outputs)
DO_FDR <- TRUE
FDR_ALPHA <- 0.01
## Raw p-val cutoff whenever it's used
## NOTE: Not currently used
RAW_P <- 0.05


## ==========================================================================
##	Includes
## ==========================================================================

## args processing
source("./scripts/inc/util.R")

library(limma)
library(Glimma)
## edgeR: For cpm() only
library(edgeR)


## ==========================================================================
##	Main
## ==========================================================================

## Print warnings as they occur
options(warn=1)

## Expression table or GCT file
input <- ARGV.get("input",required=T)
## As per: http://bioinf.wehi.edu.au/RNAseqCaseStudy/
targetsfile <- ARGV.get("targets",required=T)
outdir <- ARGV.get("outdir",required=T)
folder <- ARGV.get("folder",required=T)
islog2 <- ARGV.get("islog2")
fcfilter <- ARGV.get("fcfilter")
fcfilterval <- ARGV.get("fcfilterval")
cpmfilter <- ARGV.get("cpmfilter")
## NOTE: Voom step can fail if 'cpmfilterval' is not an integer (something about NaN's
## being produced). Haven't looked into it deeply, just make sure this is an integer!
cpmfilterval <- ARGV.get("cpmfilterval")
if (! is.na(cpmfilterval)) {
  cpmfilter <- "TRUE"
}
cpmfilterminprop <- ARGV.get("cpmfilterminprop")
batchfactor <- ARGV.get("batchfactor")
if (! is.na(batchfactor)) {
  HAS_BATCH <- TRUE
  batchfactor <- as.character(batchfactor)
}
## Annotation file optional (can use generic input eg gene symbols)
annotfile <- ARGV.get("annotfile")
if (! file.exists(as.character(annotfile))) {
  HAS_ANNOTATIONS <- FALSE
}

## NOTE: non-null batchfactor2 implies that batchfactor was also provided
batchfactor2 <- ARGV.get("batchfactor2")
if (! is.na(batchfactor2)) {
  batchfactor2 <- as.character(batchfactor2)
}
## Donor blocking factor
donorfactor <- ARGV.get("donorfactor")
if (! is.na(donorfactor)) {
  HAS_DONOR_BLOCKING <- TRUE
  donorfactor <- as.character(donorfactor)
}
## Factorial (combination) comparisons
## e.g. for experiments like investigation of Sex by Tissue such as "Male.Mock-Male.Treat,Female.Treat-Male.Treat,..."
## This argument takes the format:
##
##	<Factor1><asterisk><Factor2>
##
## NOTE: Factor names must correspond to column names in targets file
##
factorial1 <- NULL
factorial2 <- NULL
factorialdesign <- ARGV.get("factorialdesign")
if (! is.na(factorialdesign)) {
  factorialdesign <- as.character(factorialdesign)
  HAS_FACTORIAL_DESIGN <- TRUE
  factornames <- strsplit(c(factorialdesign), "*", fixed=T)
  if (length(factornames) < 1) {
    stop("Factorial design must be given as 'X*Y'")
  }
  factornames <- factornames[[1]]
  writeLines("\nUsing factorial design:")
  print(factornames)
  Sys.sleep(3)
  factorial1 <- factornames[1]
  factorial2 <- factornames[2]
}

dovoom <- ARGV.get("dovoom")
if (! is.na(dovoom)) {
  DO_VOOM <- as.logical(dovoom)
}
fdralpha <- ARGV.get("fdralpha")
## Comma-separated sample type (pheno) pairs (NOTE: newlines not permitted!)
## e.g. Fib-hESC,Fib-iPSC,hESC-iPSC
## Names must match those in targets.txt SampleType column
## NOTE: Individual pheno names must not contain dash '-' character!
contrasts <- ARGV.get("contrasts",required=T)
## If not default SampleType / CellType (second column of targets), then specify column name
contrast_target <- ARGV.get("contrast_target")

contrastPairs <- strsplit(c(as.character(contrasts)), ",", fixed=T)
if (length(contrastPairs) < 1) {
  stop("Require at least one contrast (sample-sample comparison) as input!")
}
contrastPairs <- contrastPairs[[1]]
## Split hyphen separated contrast pairs to get unique pheno values for target factor
uniqContrasts <- unique(unlist(strsplit(contrastPairs, "-", fixed=T)))

### DEBUG ###
writeLines("\ncontrastPairs: ")
contrastPairs
writeLines("uniqContrasts (unique pheno values): ")
uniqContrasts
### END DEBUG ###

if (! is.na(islog2)) {
  IS_LOG2 <- as.logical(islog2)
}
print(paste("Input data log2 == ",as.character(IS_LOG2)))
Sys.sleep(3)

if (! is.na(fcfilter)) {
  DO_FC_FILTER <- as.logical(fcfilter)
}
if (! is.na(fcfilterval)) {
  ## NOTE: This boolean is not used - remove ASAP.
  FC_FILTER <- as.numeric(fcfilterval)
}
if (! is.na(fdralpha)) {
  FDR_ALPHA <- as.numeric(fdralpha)
}
if (! is.na(cpmfilter)) {
  DO_CPM_FILTER <- as.logical(cpmfilter)
}
if (! is.na(cpmfilterval)) {
  CPM_FILTER <- as.numeric(cpmfilterval)
}
if (! is.na(cpmfilterminprop)) {
  CPM_FILTER_MIN_PROPORTION <- as.numeric(cpmfilterminprop)
}

targets <- readTargets(as.character(targetsfile))
targets$SampleID <- as.character(targets$SampleID)
writeLines("\nTargets:")
print(targets)

## Default phenotype target SampleType or CellType (second column) unless overriden
## NOTE: Not used for factorial designs
targetcol <- 2
if (! is.na(contrast_target)) {
  targetcol <- as.character(contrast_target)
}

## Unique, ordered list of phenotypes involved in comparisons
## NOTE: This is set automatically below for "simple" comparisons,
##	and is not applicable (not used) for factorial designs
designLevels <- NULL

if (HAS_FACTORIAL_DESIGN) {
  ## Each factorial phenotype gets "X.Y" from column X and column Y
  ## e.g. "Sex.Tissue" following the factorial example used throughout
  phenotype <- factor(paste(targets[,factorial1], targets[,factorial2], sep="."))
} else {
  phenotype <- targets[,targetcol]
  ## Prepend "X" to any numeric class names, so we match result of "make.names" later on
  phenotype <- gsub("^([0-9]+.*)$", "X\\1", phenotype, fixed=F, perl=T)
  ## If pairwise contrasts given, use them and proceed to ignore other values
  ## from phenotype file that are not used
  designLevels <- unique(targets[,targetcol])
  if (length(uniqContrasts)) {
    designLevels <- uniqContrasts
  }
}

writeLines("\nGot phenotype vector (or factorial combination vector) corresponding to input samples:")
print(phenotype)

## Simple design testing differences across several groups, single factor, with a batch
## component
if (HAS_BATCH) {

  batch <- factor(targets[,batchfactor])
  batch2 <- NULL
  if (! is.na(batchfactor2)) {
    batch2 <- factor(targets[,batchfactor2])
  }
  if (! HAS_FACTORIAL_DESIGN) {
    class <- factor(phenotype, levels=designLevels)
  } else {
    class <- factor(phenotype)
  }

  writeLines("\nGot batch:")
  print(batch)
  if (! is.null(batch2)) {
    writeLines("Got batch 2:")
    print(batch2)
  }
  writeLines("Got class:")
  print(class)

  if (is.null(batch2)) {
    writeLines(paste("\nBatch factor",batchfactor,"added to linear model.."))
    design <- model.matrix(~0 +class+batch)
  } else {
    writeLines(paste("Batch factors",batchfactor,"and",batchfactor2,"added to linear model.."))
    design <- model.matrix(~0 +class+batch+batch2)
  }
  colnames(design)[1:length(levels(class))] <- make.names(levels(class))

## Simple design testing differences across several groups, single factor
## See Limma User's Guide for examples for many experimental models.
} else {

  if (! HAS_FACTORIAL_DESIGN) {
    class <- factor(phenotype, levels=designLevels)
  } else {
    class <- factor(phenotype)
  }

  design <- model.matrix(~0 + class)
  ## NOTE: Any phenotype classes with dash  '-' will be replaced automatically by '.' (period)
  #colnames(design) <- make.names(levels(class))
  colnames(design)[1:length(levels(class))] <- make.names(levels(class))
}

### DEBUG ###
writeLines("\nGot design:")
design
writeLines("Got designLevels: ")
designLevels
writeLines("Got class levels: ")
levels(class)
### END DEBUG ###

input <- as.character(input)
outdir <- as.character(outdir)
folder <- as.character(folder)

dat <- NULL
## Test for GCT file input
if (is.gct.file(input)) {
  writeLines("\nLoading GCT expression file..")
  library("GenePattern")
  gct <- read.gct(input)
  dat <- as.data.frame(gct$data)

## expression table (.txt)
} else {
  writeLines("\nLoading expression table..")
  ## Header must contain one less field than data rows
  dat <- read.table(as.character(input), sep="\t", header=T, check.names=F, as.is=T, row.names=1)
}

## Replace ".bam" in Sample IDs, if applicable
colnames(dat) <- gsub(".bam", "", colnames(dat), ignore.case=T)

### DEBUG ###
writeLines("\nFirst 5 lines of input data:\n")
head(dat, 5)
### END DEBUG ###

## Enforce that order of samples in targets matches column order of data
if (identical(targets[,1], colnames(dat)) != TRUE) {
  writeLines(paste("\nError: Samples in targets file '",targetsfile,"' do not match expression data (check IDs and sample order)!",sep=""), stderr())
  writeLines("\nSample IDs from targets: ")
  print(targets[,1])
  writeLines("\nSample IDs from input data: ")
  print(colnames(dat))
  q(status=1)
}

## Get the base name of the input file to craft output file names based on it
fbase <- basename(input)
fbase <- gsub(".txt", "", fbase)

if (IS_LOG2) {
  log2dat <- dat
} else {
  log2dat <- log2(dat)
  log2dat[ log2dat == -Inf ] <- NA
}

### Raw density plot - all samples ###
bitmap(paste(outdir,"/",folder,"/density_",fbase,"_ALL.png",sep=""),type="png16m",width=1000, height=1000, units="px")
plot(density(as.matrix(log2dat), na.rm=TRUE), main="log2 count density all samples")
dev.off()

### Raw density plot - sample overlay ###
bitmap(paste(outdir,"/",folder,"/density_",fbase,"_overlay.png",sep=""),type="png16m",width=1000, height=1000, units="px")
samples <- colnames(dat)
maxy <- max(density(as.matrix(log2dat), na.rm=TRUE)$y)
plot(density(log2dat[,samples[1]], na.rm=TRUE), main="log2 count density per sample", ylim=c(0, maxy))
for (sample in samples[2:length(samples)]) {
  lines(density(log2dat[,sample], na.rm=TRUE))
}
dev.off()

## Plot raw boxplot
bitmap(paste(outdir,"/",folder,"/boxplot_",fbase,"_raw.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
## Extend bottom margin to 15 lines - left, top and right using defaults
par(mar = c(15, 4, 4, 2) + 0.1)
## Decrease font size if many samples to prevent label overlap
if (length(colnames(dat)) >= 50) {
  par(cex.axis=0.45)
}
## las=2 makes vertical sample labels
boxplot(log2dat, na.rm=TRUE, outline=FALSE, main="log2 raw counts", ylab="count (log2)", las=2, names=colnames(dat))
dev.off()


## Write stats on raw data
write(fivenum(as.matrix(dat), na.rm=T), paste(outdir,"/",folder,"/",fbase,"_fivenum_stats.txt",sep=""))
write(fivenum(as.matrix(log2dat), na.rm=T), paste(outdir,"/",folder,"/",fbase,"_fivenum_stats_log2.txt",sep=""))


## Keep raw data around (before any pre-filtering)
dat.raw <- dat


## Apply min expression filter (keep gene if all reps in any group express at least 1 CPM)
if (DO_CPM_FILTER) {
  datcpm <- cpm(dat)
  ## Save the CPM table for validation if needed
  write.table(datcpm, paste(outdir,"/",folder,"/",fbase,"_data_before_CPM_filter_CPM.txt",sep=""),sep="\t", quote=F)
  writeLines(paste("\nApplying CPM value filter (all samples in any group satisfying value  > ", CPM_FILTER ,")",sep=""))

  isexprRowIDs <- c()

  if (HAS_FACTORIAL_DESIGN) {
    targetFactors <- c(factorial1, factorial2)
    for (tf in targetFactors) {
      factorVars <- unique(targets[,tf])
      for (v in factorVars) {
        sample_indexes <- which(targets[,tf] == v)

        ### DEBUG ###
        print(v)
        print(sample_indexes)
        ### END ###

        ## TRUE/FALSE returned for each data row (keep or filter)
        isexpr <- unlist(apply(datcpm, 1, function(x) {
          if (length(sample_indexes) > 2) {
            ## At least a mininum proportion of all samples in group meet numerical filter criteria
            length(which(x[sample_indexes] >= CPM_FILTER)) >= (length(sample_indexes) * CPM_FILTER_MIN_PROPORTION)
          } else {
            ## All samples in group meet numerical filter criteria
            length(which(x[sample_indexes] >= CPM_FILTER)) == length(sample_indexes)
          }
        }))
        ## new superset of all row IDs which are expressed
        isexprRowIDs <- union(isexprRowIDs, row.names(dat)[which(isexpr)])
      }
    }

  } else {
    for (uc in uniqContrasts) {
      sample_indexes <- which(targets[,targetcol] == uc)

      ### DEBUG ###
      print(uc)
      print(sample_indexes)
      ### END ###

      ## TRUE/FALSE returned for each data row (keep or filter)
      isexpr <- unlist(apply(datcpm, 1, function(x) {
	if (length(sample_indexes) > 2) {
	  ## At least a mininum proportion of all samples in group meet numerical filter criteria
	  length(which(x[sample_indexes] >= CPM_FILTER)) >= (length(sample_indexes) * CPM_FILTER_MIN_PROPORTION)
	} else {
	  ## All samples in group meet numerical filter criteria
	  length(which(x[sample_indexes] >= CPM_FILTER)) == length(sample_indexes)
	}
      }))
      ## new superset of all row IDs which are expressed
      isexprRowIDs <- union(isexprRowIDs, row.names(dat)[which(isexpr)])
    }
  }

  ## Subset on superset of all row IDs which are expressed
  dat <- dat[isexprRowIDs,]
  dat.raw.filtered <- dat.raw[isexprRowIDs,]

  ### DEBUG ###
  writeLines(paste("\n# Data rows before CPM filter: ", nrow(dat.raw), sep=""))
  writeLines(paste("# Data rows AFTER CPM filter: ", nrow(dat), sep=""))
  Sys.sleep(3)
  ### END DEBUG ###

  ## And write out the post-CPM filtered data (both filtered raw counts and CPM, CPM log2)
  write.table(dat.raw.filtered, paste(outdir,"/",folder,"/",fbase,"_data_after_CPM_filter.txt",sep=""),sep="\t", quote=F)
  write.table(cpm(dat), paste(outdir,"/",folder,"/",fbase,"_data_after_CPM_filter_CPM.txt",sep=""),sep="\t", quote=F)
  write.table(log2(cpm(dat)), paste(outdir,"/",folder,"/",fbase,"_data_after_CPM_filter_CPM_log2.txt",sep=""),sep="\t", quote=F)

  ## Plot CPM boxplot (pre-filter)
  bitmap(paste(outdir,"/",folder,"/boxplot_",fbase,"_CPM_log2_prefilter.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
  ## Extend bottom margin to 15 lines - left, top and right using defaults
  par(mar = c(15, 4, 4, 2) + 0.1)
  ## Decrease font size if many samples to prevent label overlap
  if (length(colnames(dat)) >= 50) {
    par(cex.axis=0.45)
  }
  ## las=2 makes vertical sample labels
  boxplot(log2(datcpm), na.rm=TRUE, outline=FALSE, main=paste("log2 CPM pre-filtered (",nrow(dat.raw)," rows)",sep=""), ylab="counts per million (log2)", las=2, names=colnames(dat))
  dev.off()

  ## Plot CPM boxplot (post-filter)
  bitmap(paste(outdir,"/",folder,"/boxplot_",fbase,"_CPM_log2_postfilter.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
  ## Extend bottom margin to 15 lines - left, top and right using defaults
  par(mar = c(15, 4, 4, 2) + 0.1)
  ## Decrease font size if many samples to prevent label overlap
  if (length(colnames(dat)) >= 50) {
    par(cex.axis=0.45)
  }
  ## las=2 makes vertical sample labels
  boxplot(log2(cpm(dat)), na.rm=TRUE, outline=FALSE, main=paste("log2 CPM post-filtered (",nrow(dat)," rows)",sep=""), ylab="counts per million (log2)", las=2, names=colnames(dat))
  dev.off()

  ## Density plot (post-filter)
  bitmap(paste(outdir,"/",folder,"/density_",fbase,"_log2_postfilter_ALL.png",sep=""),type="png16m",width=1000, height=1000, units="px")
  plot(density(log2(as.matrix(dat)), na.rm=TRUE), main="log2 post-filtered density all samples")
  dev.off()
}


## If model matrix excludes samples from data, need to subset data otherwise
## lmFit() will not work
if (nrow(design) < ncol(dat)) {
  dat <- dat[,as.numeric(row.names(design))]
}

## Do 'Voom' (internal library size norm, CPM, log2, median centering and lowess regression weighting)
if (DO_VOOM) {
  writeLines("\nPerforming 'voom' normalization..")
  dat.prevoom <- dat

  ## TMM normalised raw counts, returns DGEList
  dat <- DGEList(counts=dat)
  dat <- calcNormFactors(dat)
  bitmap(paste(outdir,"/",folder,"/",fbase,"_voom_mean_variance.png",sep=""),type="png16m",width=1000, height=1000, units="px")
  ## Voom with median centering on internal voom transformed log2 CPM counts
  ## "voomcenter" is now an EList/DGEList object with log CPM counts in '$E' attribute
  voomcenter <- voom(dat, design, plot=TRUE, normalize.method="scale")
  write.table(voomcenter, paste(outdir,"/",folder,"/",fbase,"_TMM_voom_normalized_CPM_log2_plus_median.txt",sep=""),sep="\t", quote=F)
  dev.off()

  ## Plot CPM boxplot (post-filter)
  bitmap(paste(outdir,"/",folder,"/boxplot_",fbase,"_TMM_voom_normalized_plus_median.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
  ## Extend bottom margin to 15 lines - left, top and right using defaults
  par(mar = c(15, 4, 4, 2) + 0.1)
  ## Decrease font size if many samples to prevent label overlap
  if (length(colnames(voomcenter)) >= 50) {
    par(cex.axis=0.45)
  }
  ## las=2 makes vertical sample labels
  boxplot(as.matrix(voomcenter), outline=FALSE, main=paste("log2 CPM post-TMM-Voom, median centered (",nrow(voomcenter)," rows)",sep=""), ylab="counts per million (log2)", las=2, names=colnames(as.matrix(voomcenter)))
  dev.off()

  voomnocenter <- voom(dat, design, plot=FALSE)
  write.table(voomnocenter, paste(outdir,"/",folder,"/",fbase,"_TMM_voom_normalized_CPM_log2.txt",sep=""),sep="\t", quote=F)
  ## Plot CPM boxplot (post-filter)
  bitmap(paste(outdir,"/",folder,"/boxplot_",fbase,"_TMM_voom_normalized.png",sep=""), type="png16m", height=1200, width = 1200, units = "px")
  ## Extend bottom margin to 15 lines - left, top and right using defaults
  par(mar = c(15, 4, 4, 2) + 0.1)
  ## Decrease font size if many samples to prevent label overlap
  if (length(colnames(voomnocenter)) >= 50) {
    par(cex.axis=0.45)
  }
  ## las=2 makes vertical sample labels
  boxplot(as.matrix(voomnocenter), outline=FALSE, main=paste("log2 CPM post-TMM-Voom (",nrow(voomnocenter)," rows)",sep=""), ylab="counts per million (log2)", las=2, names=colnames(as.matrix(voomnocenter)))
  dev.off()

  dat <- voomcenter

} else {
  dat <- as.matrix(dat)
}

## Apply intra-donor variability to linear model
if (HAS_DONOR_BLOCKING) {
  writeLines("\nApplying intra-donor correlation to linear model..")
  if (! donorfactor %in% colnames(targets)) {
    stop(paste("Donor factor '",donorfactor,"' not found in targets!",sep=""))
  } else {
    writeLines(paste("(Blocking on factor '",donorfactor,"')",sep=""))
  }
  corfit <- duplicateCorrelation(dat, design, ndups=1, block=targets[,donorfactor])
  writeLines("Consensus correlation:")
  print(corfit$consensus)
  writeLines("(A positive value is expected indicating that donor samples are correlated.\n*** NOTE: A negative value here would probably indicate an error in donor assignments! ***)")
  fit <- lmFit(dat, design, block=targets[,donorfactor], correlation=corfit$consensus)

} else {
  fit <- lmFit(dat, design)
}

## If any names start with a number, prepend "X" to match make.names() output on level names
contrastPairsReplaced <- gsub("^([0-9]+.*)$", "X\\1", contrastPairs, fixed=F, perl=T)
contrast.matrix <- makeContrasts(contrasts=gsub("^(.*)-([0-9]+.*)$", "\\1-X\\2", contrastPairsReplaced, fixed=F, perl=T), levels=design)

writeLines("\nGot contrast.matrix: ")
print(contrast.matrix)

writeLines("\nFitting linear model according to contrast matrix..")
fit2 <- contrasts.fit(fit, contrast.matrix)
fit2 <- eBayes(fit2)

maxn <- nrow(dat)
minfc <- FC_FILTER
fdrp <- FDR_ALPHA

## Load annotation - no row names
## Need to disable '#' comment char otherwise breaks because some gene descs / names have '#' symbol in them!
##
## The only mandatory columns from annotation file are:
##	Ensembl_ID	Entrez_ID	Symbol	Description	Chromosome	Start	End	Strand
##
## Optional fields:
##	Aliases
##

## New for Glimma script - annotation file is *required* if gene ids are provided
if (! HAS_ANNOTATIONS) {
  writeLines("Note that this was originally designed to be used with a set of ENSEMBL IDs and their corresponding annotations, so an annotation file should be supplied if gene ids (eg ENSG00001) are used. If you are using another type of identification you can ignore this message.")
  glannot <- as.matrix(setNames(data.frame(matrix(ncol=4, nrow=0)), c('Gene', 'Symbol', 'Ensembl_ID', 'Description')))
} else {
  annot <- read.table(as.character(annotfile), sep="\t", header=T, row.names=1, stringsAsFactors=F, as.is=T, check.names=F, quote="", na.strings="", comment.char="")
  ## NOTE: Ignoring Ensembl_ID because we're using row.names=1
  if (! all(c("Entrez_ID","Symbol","Description","Chromosome","Start","End","Strand") %in% colnames(annot))) {
    stop("Annotation file must have fields: Ensembl_ID,Entrez_ID,Symbol,Description,Chromosome,Start,End,Strand!")
  } else {
    ## Reformat as: Gene (Symbol + Ensembl), Symbol, Ensembl, Description
    glannot <- cbind(paste(annot[,"Symbol"]," (",row.names(annot),")",sep=""),annot[,"Symbol"],row.names(annot),annot[,"Description"])
    colnames(glannot) <- c("Gene","Symbol","Ensembl_ID","Description")
    row.names(glannot) <- row.names(annot)
  
    ## Sort annotation table by data row Ensembl IDs
    glannot <- glannot[row.names(dat),]
  }
}

## For glMDPlot() - volcano plot up/down colouring??
results <- decideTests(fit2, p.value=fdrp, adjust.method="BH")


writeLines("\nRunning pairwise tests for comparisons:")
print(contrastPairs)
cat("\n")

## Output lists of top genes for comparison X (as coef param), and Glimma MD and Volcano plots only
for (c in 1:length(contrastPairs)) {
  cTokens <- strsplit(c(as.character(contrastPairs[c])), "-", fixed=T)
  comp1 <- cTokens[[1]][1]
  comp2 <- cTokens[[1]][2]

  ## Very important - maintain sort order of expression data for Glimma
  tt1 <- topTable(fit2, coef=c, adjust.method="BH", number=maxn, sort.by="none")
  ## Sorted version for CSV output only
  tt1_sorted <- topTable(fit2, coef=c, adjust.method="BH", number=maxn, sort.by="P")
  write.table(tt1_sorted, paste(outdir,"/",folder,"/toptable_",contrastPairs[c],"_BH.txt",sep=""), quote=F, sep="\t", col.names=NA)
  write.csv(tt1_sorted, paste(outdir,"/",folder,"/toptable_",contrastPairs[c],"_BH.csv",sep=""))

  ## Write annotated top table
  if (HAS_ANNOTATIONS) {
    tt1_annot <- cbind(tt1_sorted, annot[row.names(tt1_sorted),])
    #print("DEBUG: colnames(tt1_annot): ")
    #print(colnames(tt1_annot))

    ## Some annotation tables don't have "Aliases"
    ## The only mandatory columns from annotation file are:
    ##	Ensembl_ID	Entrez_ID	Symbol	Description	Chromosome	Start	End	Strand
    if ("Aliases" %in% colnames(annot)) {
      tt1_column_reordered <- tt1_annot[,c("Symbol","Aliases","logFC","AveExpr","t","P.Value","adj.P.Val","B","Entrez_ID","Description","Chromosome","Start","End","Strand")]
    }
  } else {
    tt1_annot <- tt1_sorted
    tt1_column_reordered <- tt1_annot[,c("logFC","AveExpr","t","P.Value","adj.P.Val","B")]
  }
  write.table(tt1_column_reordered, paste(outdir,"/",folder,"/toptable_",contrastPairs[c],"_BH_ANNOTATED.txt",sep=""), quote=F, sep="\t", na="", col.names=NA)
  write.csv(tt1_column_reordered, paste(outdir,"/",folder,"/toptable_",contrastPairs[c],"_BH_ANNOTATED.csv",sep=""), na="")

  ## NOTE: Get sample names from original non-subsetted expression data 'dat.raw' (otherwise we can sometimes
  ##   get index out of bounds issues for indices in phenotype that no longer exist in potentially subsetted 'dat' matrix)
  sample_names <- colnames(dat.raw)[c(which(phenotype==comp1),which(phenotype==comp2))]
  cont_pheno <- phenotype[c(which(phenotype==comp1),which(phenotype==comp2))]
  row.names(targets) <- targets[, 1]
  ## NOTE: Not currently used, but if we wanted sample types instead of Sample IDs as labels, give this as "samples" arg to glMDPlot() below
  sample_pheno <- targets[sample_names, 2]

  writeLines("\nSample labels for Glimma hover-overs:")
  print(sample_names)

  ## Shoe-horn in the 'adj_P_Val' field so we can see it on the front-end in Volcano plot.
  ## NOTE: Glimma can't handle name "adj.P.Val" extraction in Data tables JS front-end - hence the rename
  if (HAS_ANNOTATIONS) {
    glannot_tmp <- cbind(glannot, tt1["adj.P.Val"])
    names(glannot_tmp) <- c(names(glannot_tmp)[1:ncol(glannot)], "adj_P_Val")
    columns <- c("Gene","Symbol","Ensembl_ID","Description","adj_P_Val")
  } else {
    glannot_tmp <- tt1["adj.P.Val"]
    names <- rownames(glannot_tmp)
    rownames(glannot_tmp) <- NULL
    glannot_tmp <- cbind(names, glannot_tmp)
    columns <- c("Gene","adj_P_Val")
    colnames(glannot_tmp) <- columns
  }

  ## NOTE: If we wanted Glimma to calculate CPM from raw data (and not use our normalized log2 values),
  ##	we'd  uncomment the next line.
  #cont_counts <- dat.raw.filtered[,cont_samples]
  cont_counts <- dat[,sample_names]

  writeLines(paste("\nGlimma: Writing MD plot: ", contrastPairs[c], "..", sep=""))

  ## MD (mean-difference) plot
  glMDPlot(tt1, xval="AveExpr", yval="logFC", side.xlab="Group", side.ylab="Log2 CPM", counts=cont_counts, anno=glannot_tmp, samples=sample_names, groups=cont_pheno, status=results[,c], display.columns=columns, side.main="Gene", path=outdir, folder=paste(folder ,"/Glimma_out_",contrastPairs[c],sep=""), main=paste("MD plot:",contrastPairs[c]), transform=F, launch=FALSE)

  writeLines(paste("Glimma: Writing volcano plot: ", contrastPairs[c], "..", sep=""))
  ## Volcano plot
  ## NOTE: Added "adj_P_Val" which might be useful to look at rather than just the "B" stat value
  glMDPlot(tt1, xval="logFC", yval="B", side.xlab="Group", side.ylab="Log2 CPM", counts=cont_counts, anno=glannot_tmp, samples=sample_names, groups=cont_pheno, status=results[,c], display.columns=columns, side.main="Gene", path=outdir, folder=paste(folder,"/Glimma_out_",contrastPairs[c],sep=""), main=paste("Volcano plot:",contrastPairs[c]), html="volcano", transform=F, launch=FALSE)
}

## Save session if we want to inspect objects later
save.image(file=paste(outdir,"/",folder,"/session.RData",sep=""))
## Write sessionInfo() for R and library version info
writeLines(capture.output(sessionInfo()), paste(outdir,"/",folder,"/sessionInfo.txt",sep=""))
