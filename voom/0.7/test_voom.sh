#!/bin/sh
## test_voom: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested

  ## Set up temp data / files location and populate dummy files
  VOOM_TMP="$S4M_TMP/$$"; export VOOM_TMP
  mkdir -p "$VOOM_TMP"

  ## Test inputs location
  VOOM_TESTDIR=./etc/test; export VOOM_TESTDIR

}

## Run after all tests finished
oneTimeTearDown () {
  if [ -z "$VOOM_ERROR" ]; then
    rm -rf "$VOOM_TMP"
  else
    echo "There were errors, see log files in: $VOOM_TMP/" 1>&2
  fi
}

## Run between tests (uncomment if needed)
#setUp () {
#}


## TESTS ##

testVoomEnvironment () {
  ## If this returns non-null, the environment is properly set up
  s4m_exec voom::version 2> /dev/null | grep -P -v "^\[" | sed 's|^|\t|g'
  assertEquals 0 $?
}

## Test that there are no errors in limma/Voom analysis with all good inputs
##
testVoomAnalysisBasic () {
  s4m_exec  voom::voom \
    --input "$VOOM_TESTDIR/expression.txt" \
    --outdir "$VOOM_TMP" \
    --targets "$VOOM_TESTDIR/targets.txt" \
    --contrasts "L-M" \
    --islog2 TRUE \
    --dovoom FALSE \
    --fdralpha 0.05 \
    --contrast_target "class" \
  > "$VOOM_TMP/testVoomAnalysisBasic.log" 2>&1

  ret=$?
  if [ $ret -ne 0 ]; then
    VOOM_ERROR="TRUE"; export VOOM_ERROR
  fi
  assertEquals "Check basic Voom DEG analysis non-zero status" 0 $ret
}

## Test that we error out limma/Voom analysis if sample ordering does not 
## match between targets.txt and expression data
##
testVoomSampleOrderingBad () {
  s4m_exec  voom::voom \
    --input "$VOOM_TESTDIR/expression.txt" \
    --outdir "$VOOM_TMP" \
    --targets "$VOOM_TESTDIR/targets.txt.wrongorder" \
    --contrasts "L-M" \
    --islog2 TRUE \
    --dovoom FALSE \
    --fdralpha 0.05 \
    --contrast_target "class" \
  > "$VOOM_TMP/testVoomSampleOrderingBad.log" 2>&1

  ret=$?
  if [ $ret -ne 1 ]; then
    VOOM_ERROR="TRUE"; export VOOM_ERROR
  fi
  assertEquals "Check Voom DEG analysis error status" 1 $ret
}

## Glimma test, no annotation file (probe-based expression)
testGlimmaAnalysisBasic () {
  s4m_exec  voom::glimma \
     --input "$VOOM_TESTDIR/expression.txt" \
     --outdir "$VOOM_TMP" \
     --targets "$VOOM_TESTDIR/targets.txt" \
     --contrasts "L-M" \
     --islog2 TRUE \
     --dovoom FALSE \
     --fdralpha 0.05 \
     --contrast_target "class" \
  > "$VOOM_TMP/testGlimmaAnalysisBasic.log" 2>&1

  ret=$?
  if [ $ret -ne 0 ]; then
    VOOM_ERROR="TRUE"; export VOOM_ERROR
  fi
  assertEquals "Check basic Glimma DEG analysis non-zero status" 0 $ret
}

## Glimma test, with Ensembl annotation file (Ensembl-based expression)
testGlimmaAnalysisEnsemblAnnotation () {
  s4m_exec  voom::glimma \
     --input "$VOOM_TESTDIR/expression_ensembl.txt" \
     --outdir "$VOOM_TMP" \
     --targets "$VOOM_TESTDIR/targets.txt" \
     --annotfile "$VOOM_TESTDIR/ensmusg_v67_annotation.tsv.subset" \
     --contrasts "L-M" \
     --islog2 TRUE \
     --dovoom FALSE \
     --fdralpha 0.05 \
     --contrast_target "class" \
  > "$VOOM_TMP/testGlimmaAnalysisEnsemblAnnotation.log" 2>&1

  ret=$?
  if [ $ret -ne 0 ]; then
    VOOM_ERROR="TRUE"; export VOOM_ERROR
  fi
  assertEquals "Check Glimma DEG analysis (with Ensembl annotation) non-zero status" 0 $ret
}

