#!/usr/bin/env bash 
# Module: voom
# Author: Steve Englart 

for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

. ./inc/voom_funcs.sh


voom_debug_vars () {

  echo "============================== START DEBUG ================================"

  if [ 1 ]
  then
    echo "voom_outdir           : ${voom_outdir}"
    echo "voom_input            : ${voom_input}"
    echo "voom_targets          : ${voom_targets}"
    echo "voom_annotfile        : ${voom_annotfile}"
    echo "voom_folder           : ${voom_folder}"
    echo "voom_contrasts        : ${voom_contrasts}"
    echo "voom_batchfactor      : ${voom_batchfactor}"
    echo "voom_batchfactor2     : ${voom_batchfactor2}"
    echo "voom_islog2           : ${voom_islog2}"
    echo "voom_fcfilter         : ${voom_fcfilter}"
    echo "voom_fcfilterval      : ${voom_fcfilterval}"
    echo "voom_cpmfilter        : ${voom_cpmfilter}"
    echo "voom_cpmfilterval     : ${voom_cpmfilterval}"
    echo "voom_cpmfilterminprop : ${voom_cpmfilterminprop}"
    echo "voom_factorialdesign  : ${voom_factorialdesign}"
    echo "voom_dovoom           : ${voom_dovoom}"
    echo "voom_donorfactor      : ${voom_donorfactor}"
    echo "voom_fdralpha         : ${voom_fdralpha}"
    echo "voom_contrast_target  : ${voom_contrast_target}"
  fi

  if test 1
  then
     echo  "=============================="
     echo  "S4M_MOD_OPTS:  $S4M_MOD_OPTS "
     echo  "=============================="
     echo  "myargs:        $myargs"
     echo  "=============================="
  fi
  echo "============================== END DEBUG and exit 1 ================================"
  exit 1

}


## Handler for the "glimma" command
voom_de_glimma_handler () {

  ################# example input data #################
  ## test command ##
  #s4m  -C ~/.s4m/config_new  voom::glimma --contrasts "GEN-HHH,GEN-PKCi,PKCi-HHH"  --input inputs/gene_count_frags.txt  --outdir "./outputs" --targets inputs/targets.txt --annotfile inputs/ensg_v86_annotation.tsv

  myargs=""
  ###### test if we have any set varable values then setup defaults etc... ######
  [ -z ${voom_outdir+x} ]           && voom_outdir="${S4M_RUNDIR}/glimma_outputs"
	  ## fix paths for --outdir #
	  if [ $( echo "${voom_outdir}" |egrep "(^\.\/|^\.\.\/|^/)" ) ] ; then 
	       myargs+=" -outdir=\"${voom_outdir}\""                
	  else
	       myargs+=" -outdir=\"${S4M_RUNDIR}/${voom_outdir}\"" 
	       voom_outdir="${S4M_RUNDIR}/${voom_outdir}" 
	  fi
  [ -z ${voom_input+x} ]            || myargs+=" -input=\"${voom_input}\""
  [ -z ${voom_targets+x} ]          || myargs+=" -targets=\"${voom_targets}\""                 
  [ -z ${voom_annotfile+x} ]        || myargs+=" -annotfile=\"${voom_annotfile}\""
  [ -z ${voom_folder+x} ]           && voom_folder="glimma"  
				       myargs+=" -folder=\"${voom_folder}\""  
				       mkdir -p "${voom_outdir}/${voom_folder}"  
  [ -z ${voom_contrasts+x} ]        || myargs+=" -contrasts=\"${voom_contrasts}\""
  [ -z ${voom_batchfactor+x} ]      || myargs+=" -batchfactor=\"${voom_batchfactor}\""
  [ -z ${voom_batchfactor2+x} ]     || myargs+=" -batchfactor2=\"${voom_batchfactor2}\""
  [ -z ${voom_islog2+x} ]           && myargs+=" -islog2=\"FALSE\""                                || myargs+=" -islog2=\"${voom_islog2}\""
  [ -z ${voom_fcfilter+x} ]         || myargs+=" -fcfilter=\"${voom_fcfilter}\""
  [ -z ${voom_fcfilterval+x} ]      || myargs+=" -fcfilterval=\"${voom_fcfilterval}\""
  [ -z ${voom_cpmfilter+x} ]        || myargs+=" -cpmfilter=\"${voom_cpmfilter}\""
  [ -z ${voom_cpmfilterval+x} ]     && myargs+=" -cpmfilterval='1'"                                || myargs+=" -cpmfilterval=\"${voom_cpmfilterval}\""
  [ -z ${voom_cpmfilterminprop+x} ] || myargs+=" -cpmfilterminprop=\"${voom_cpmfilterminprop}\""
  [ -z ${voom_factorialdesign+x} ]  || myargs+=" -factorialdesign=\"${voom_factorialdesign}\""
  [ -z ${voom_dovoom+x} ]           && myargs+=" -dovoom='TRUE'"                                   || myargs+=" -dovoom=\"${voom_dovoom}\""
  [ -z ${voom_donorfactor+x} ]      || myargs+=" -donorfactor=\"${voom_donorfactor}\""
  [ -z ${voom_fdralpha+x} ]         && myargs+=" -fdralpha='0.01'"                                 || myargs+=" -fdralpha=\"${voom_fdralpha}\""
  [ -z ${voom_contrast_target+x} ]  && myargs+=" -contrast_target=\"SampleType\""                  || myargs+=" -contrast_target=\"${voom_contrast_target}\""

  ## uncomment to debug vars (and exit) ##
  #voom_debug_vars

  R --slave -f ./scripts/limma_voom_DE_Glimma.R --args ${myargs} 
  if [ $? -ne 0 ]; then
    s4m_error  "Voom Glimma script exited with non-zero status!"
    return 1
  fi
  s4m_log "Voom Glimma script completed successfully."
  return
}


## Handler for the "voom" command
voom_handler () {

  ## test command ##
  ## s4m -C ~/.s4m/config_new  voom::voom  --input "limma_inputs/gene_count_frags.txt" --targets "limma_inputs/targets.txt" --folder $(pwd) --outdir "./limma_outputs" --islog2 FALSE --cpmfilterval 1 --contrasts "TypeA-TypeD,TypeB-TypeD,TypeC-TypeD" --dovoom TRUE --annotfile "limma_inputs/ensg_v69_annotation.small.subset.tsv"

  myargs=""
  ###### test if we have any set varable values then setup defaults etc... ######
  [ -z ${voom_outdir+x} ]           && voom_outdir="${S4M_RUNDIR}/limma_outputs"
	  ## fix paths for --outdir ##
	  if [ $( echo "${voom_outdir}" |egrep "(^\.\/|^\.\.\/|^/)" ) ] ; then
	       myargs+=" -outdir=\"${voom_outdir}\""
	  else
	       myargs+=" -outdir=\"${S4M_RUNDIR}/${voom_outdir}\""
	       voom_outdir="${S4M_RUNDIR}/${voom_outdir}"
	  fi
	  mkdir -p "${voom_outdir}"
  [ -z ${voom_input+x} ]            || myargs+=" -input=${voom_input}"         
  [ -z ${voom_targets+x} ]          || myargs+=" -targets=${voom_targets}"                   ## note translation to targetsfile fro targets 
  [ -z ${voom_annotfile+x} ]        || myargs+=" -annotfile=${voom_annotfile}" 
  [ -z ${voom_contrasts+x} ]        || myargs+=" -contrasts=${voom_contrasts}" 
  [ -z ${voom_islog2+x} ]           && myargs+=" -islog2=FALSE"                                  || myargs+=" -islog2=${voom_islog2}"
  [ -z ${voom_fcfilter+x} ]         || myargs+=" -fcfilter=${voom_fcfilter}"   
  [ -z ${voom_fcfilterval+x} ]      || myargs+=" -fcfilterval=${voom_fcfilterval}"
  [ -z ${voom_cpmfilter+x} ]        || myargs+=" -cpmfilter=${voom_cpmfilter}" 
  [ -z ${voom_cpmfilterval+x} ]     && myargs+=" -cpmfilterval=1"                                || myargs+=" -cpmfilterval=${voom_cpmfilterval}"
  [ -z ${voom_cpmfilterminprop+x} ] || myargs+=" -cpmfilterminprop=${voom_cpmfilterminprop}" 
  [ -z ${voom_dovoom+x} ]           && myargs+=" -dovoom=TRUE"                                   || myargs+=" -dovoom=${voom_dovoom}"
  [ -z ${voom_fdralpha+x} ]         || myargs+=" -fdralpha=${voom_fdralpha}"   
  [ -z ${voom_contrast_target+x} ]  && myargs+=" -contrast_target=SampleType"                    || myargs+=" -contrast_target=${voom_contrast_target}" 

  ## uncomment to debug vars (and exit) ##
  #voom_debug_vars

  R --slave -f ./scripts/limma_voom_DE.R --args ${myargs}
  if [ $? -ne 0 ]; then
    s4m_error  "Voom script exited with non-zero status!"
    return 1
  fi
  s4m_log "Voom script completed successfully."
  return
}


## Print version information (R, limma and Glimma)
voom_version_handler () {
  voom_print_version
  return $?
}



############### main ###############
main () {
  ## Import miniconda (for automatic dependency management)
  s4m_import miniconda/lib.sh

  ## All commands require environment to be active, so we're doing it here
  if ! miniconda_activate; then
    exit 1
  fi

  s4m_route "voom"    voom_handler
  s4m_route "glimma"  voom_de_glimma_handler
  s4m_route "version" voom_version_handler

## END function ##
}

### START ###
main

