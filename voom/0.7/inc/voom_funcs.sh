
voom_get_R_version () {
  vgr_vers=`R --version | head -1 | cut -d' ' -f 3`
  echo "$vgr_vers"
  ## Test that version string is just numbers and dots
  echo "$vgr_vers" | grep -P "\s*[0-9\.]+\s*" > /dev/null 2>&1
  return $?
}


voom_get_limma_version () {
  vgl_vers=`R -e "as.data.frame(installed.packages()[,c(1,3:4)])" 2>/dev/null | grep "^limma" | awk '{print $3}'`
  echo "$vgl_vers"
  ## Test that version string is just numbers and dots
  echo "$vgl_vers" | grep -P "\s*[0-9\.]+\s*" > /dev/null 2>&1
  return $?
}


voom_get_glimma_version () {
  vgg_vers=`R -e "as.data.frame(installed.packages()[,c(1,3:4)])" 2>/dev/null | grep "^Glimma" | awk '{print $3}'`
  echo "$vgg_vers"
  ## Test that version string is just numbers and dots
  echo "$vgg_vers" | grep -P "\s*[0-9\.]+\s*" > /dev/null 2>&1
  return $?
}


voom_print_version () {
  vpv_rvers=`voom_get_R_version`
  vpv_limma_vers=`voom_get_limma_version`
  vpv_glimma_vers=`voom_get_glimma_version`

  /bin/echo -e "R:\t$vpv_rvers
limma:\t$vpv_limma_vers
Glimma:\t$vpv_glimma_vers
"
}
