
expression.txt:
	Top 500 probes by pearson correlation - group "L" vs group "M" from a public mouse microarray study
	(GSE20402), quantile normalised log2 expression

expression_ensembl.txt:
	As above, but annotated to Ensembl IDs. This table contains 263 genes due to probe/gene multi-mapping
	(the first probe for a given gene was retained).
