#!/bin/sh
##===========================================================================
## Module:   chipqc
## Author:   Othmar Korn
##
## Wrapper for R/ChIPQC package
##===========================================================================

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

## Include chipqc funcs
. ./inc/chipqc_funcs.sh


## Generate ChIPQC HTML report
##
chipqc_report () {
  cr_sampletable="$chipqc_sampletable"
  cr_outdir="$chipqc_outputdir"

  mkdir -p "$cr_outdir"
  ret=$?
  if [ $ret -ne 0 ]; then
    s4m_error "Failed to create ChIPQC output path '$cr_outdir'"
    return $ret
  fi

  chipqc_validate_sampletable "$cr_sampletable"
  ret=$?
  if [ ! $ret ]; then
    return $ret
  fi
  ## Handle user override chromosomes spec
  if [ ! -z "$chipqc_chromosomes" ]; then
    extraflags="-chromosomes=$chipqc_chromosomes"
  fi

  call_R $chipqc_R "$S4M_THIS_MODULE/scripts/chipqc_report.R" "-sampletable='$cr_sampletable' -outputdir='$cr_outdir' $extraflags"
  return $? 
}


## Handler for "qcreport" command
##
chipqc_report_handler () {
  chipqc_report
  return $?
}


main () {
  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  s4m_route qcreport chipqc_report_handler

}

### START ###
main

