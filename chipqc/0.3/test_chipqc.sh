#!/bin/sh
## test_chipqc.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  CHIPQC_TEST_FILES="./etc/test"
  export CHIPQC_TEST_DIR
 
  ## Load functions being tested
  test -f ./inc/chipqc_funcs.sh && . ./inc/chipqc_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  return
}



## TESTS ##

## Test that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  assertEquals 0 $?
}

testInternalUnitTestFilesAvailable () {
  test -f "$CHIPQC_TEST_FILES/samples_minimum.tsv"
  assertTrue $?
  test -f "$CHIPQC_TEST_FILES/samples_control.tsv"
  assertTrue $?
}

testInputSamplesFileValidation () {
  echo "  samples_minimum.tsv"
  chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_minimum.tsv"
  assertTrue $?
  echo "  samples_control.tsv"
  chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_control.tsv"
  assertTrue $?
}

testInputSamplesFileValidationBad () {
  ## input file does not exist, should fail
  echo "  samples_DOES_NOT_EXIST.tsv"
  stderr=`chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_DOES_NOT_EXIST.tsv" 2>&1`
  ret=$?
  echo "$stderr" | grep "not found" > /dev/null 2>&1
  assertTrue $?
  assertFalse $ret

  ## missing column/s, should fail
  echo "  samples_bad1.tsv"
  chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_bad1.tsv" 2> /dev/null
  assertFalse $?
  echo "  samples_bad2.tsv"
  chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_bad2.tsv" 2> /dev/null
  assertFalse $?
  echo "  samples_bad3.tsv"
  chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_bad3.tsv" 2> /dev/null
  assertFalse $?

  ## fails minimum criteria for control metadata (if given)
  echo "  samples_bad4.tsv"
  stderr=`chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_bad4.tsv" 2>&1`
  ret=$?
  echo "$stderr" | grep "must use both" > /dev/null 2>&1
  assertTrue $?
  assertFalse $ret

  ## SampleIDs have dashes in them, should fail
  echo "  samples_bad5.tsv"
  stderr=`chipqc_validate_sampletable "$CHIPQC_TEST_FILES/samples_bad5.tsv" 2>&1`
  ret=$?
  echo "$stderr" | grep "dashes" > /dev/null 2>&1
  assertTrue $?
  assertFalse $ret
}

