#!/bin/sh

## Check that the sample table satisfies requirements for ChIPQC to work
##
## Unit Tests:
##   test_chipqc.sh:testInputSamplesFileValidation()
##   test_chipqc.sh:testInputSamplesFileValidationBad()
##
chipqc_validate_sampletable () {
  cvs_sampletable="$1"
  if [ ! -f "$cvs_sampletable" ]; then
    s4m_error "ChIPQC sample table '$cvs_sampletable' not found!"
    return 1
  fi

  ## Check file format:

  ## Check mandatory headers
  cvs_mandatory_headers="SampleID
Factor
Replicate
bamReads
Peaks"

  ret=0
  for header in $cvs_mandatory_headers
  do
    head -1 "$cvs_sampletable" | grep "\b$header\b" > /dev/null 2>&1
    if [ $? -ne 0 ]; then
      ret=1
      s4m_error "ChIPQC input sample table missing mandatory column '$header'"
    fi
  done
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## Either none or both of 'ControlID' and 'bamControl' must be specified
  ## (if a control is used by a ChIP library, the path to the control BAM
  ## must also be given)
  head -1 "$cvs_sampletable" | grep "\bControlID\b" > /dev/null 2>&1
  hasControlID=$?
  head -1 "$cvs_sampletable" | grep "\bbamControl\b" > /dev/null 2>&1
  hasBamControl=$?
  if [ $hasControlID -eq 0 ]; then
    if [ $hasBamControl -ne 0 ]; then
      s4m_error "ChIPQC input sample table must use both 'ControlID' and 'bamControl' fields (if at all)"
      return 1
    fi
  elif [ $hasBamControl -eq 0 ]; then
    if [ $hasControlID -ne 0 ]; then
      s4m_error "ChIPQC input sample table must use both 'ControlID' and 'bamControl' fields (if at all)"
      return 1
    fi
  fi

  ## Check SampleIDs - ChIPQC may not yet be patched to handle sample names with dashes ("-") in them.
  ## In fact, sample IDs need to be completely syntactically valid (as per R make.names() function).
  ##
  ## From R docs:
  ## "A syntactically valid name consists of letters, numbers and the dot or underline characters and
  ## starts with a letter or the dot not followed by a number"
  ##
  ## Otherwise ChIPQC errors out with:
  ## 	"Error: Faceting variables must have at least one value when plotting with ChIPQC"
  ##
  ## See: https://support.bioconductor.org/p/86483/

  ## Assume 'SampleID' is first column
  cut -f 1 "$cvs_sampletable" | grep -v -P "^([A-Za-z][A-Za-z0-9_\.]+|\.[A-Za-z_][A-Za-z0-9_\.]+)" > /dev/null
  if [ $? -eq 0 ]; then
    s4m_error "Sample IDs in ChIPQC sample table must by syntactically valid for R, i.e
  consists of: letters, number and dot or underline, starting with a letter or dot not followed by a number

  (NOTE: This *may* be fixed in a ChIPQC update at some point, but as of v1.11.1 it doesn't appear to be fixed)"
    return 1
  fi
}

