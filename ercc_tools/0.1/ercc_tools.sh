#/bin/sh
# Module: ercc_tools
# Author: O.Korn

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================

. ./inc/ercc_tools_include.sh


## T#2513: TODO: Move to includes when completed
ercc_do_recovery_qc () {
  edaq_readcounts="$1"
  edaq_molecules="$2"
  edaq_outdir="$3"

  if s4m_isdebug; then
    edaq_extraflags="-debug=TRUE"
  fi
  call_R "$ercc_tools_R" "$S4M_THIS_MODULE/scripts/ercc_reads_vs_molecules.R" "-readcounts='$edaq_readcounts' -molecules='$edaq_molecules' -outdir='$edaq_outdir' $edaq_extraflags"
  return $?
}

## T#2513: TODO: Move to includes when completed
ercc_do_mapstats_qc () {
  edmq_mapstatsfile="$1"
  edmq_outdir="$2"

  if s4m_isdebug; then
    edmq_extraflags="-debug=TRUE"
  fi
  call_R "$ercc_tools_R" "$S4M_THIS_MODULE/scripts/plot_mapstats_qc_boxplot.R" "-mapstatsfile='$edmq_mapstatsfile' -outdir='$edmq_outdir' $edmq_extraflags"
}



recovery_qc_handler () {
  ercc_do_recovery_qc "$ercc_tools_readcounts" "$ercc_tools_molecules" "$ercc_tools_outdir"
  return $?
}

mapstats_qc_handler () {
  ercc_do_mapstats_qc "$ercc_tools_mapstatsfile" "$ercc_tools_outdir"
  return $?
}


### Your code here ###
main () {
  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"

  s4m_route ercc_mapstats mapstats_qc_handler
  s4m_route ercc_recovery recovery_qc_handler

}

### START ###
main

