#!/bin/sh

## If no user NCPUs given, use this many threads in multi-process steps.
SUBREAD_DEFAULT_NTHREADS=2; export SUBREAD_DEFAULT_NTHREADS

## Default max #mismatches allowed in alignment
SUBREAD_DEFAULT_MISMATCHES=5; export SUBREAD_DEFAULT_MISMATCHES

## Poll wait time for batch FASTX quality stats completion checking
SUBREAD_FASTX_WAIT_SECONDS=60; export SUBREAD_WAIT_SECONDS

##
## Test whether we're in dryrun mode
##
is_subread_dryrun () {
  if [ "$SUBREAD_DRYRUN" = "TRUE" ]; then
    echo "true"
    return
  fi
  echo "false"
  return 1
}


##
## Get FASTQ file names from a directory
##
subread_get_fastq_files () {
  sgff_fastqdir="$1"
  ls -1 "$sgff_fastqdir/" | grep -P "\.(fastq|fq)$"
}


##
## Get fastq file names from targets file (strips leading paths)
## 
## NOTE: Assumes targets file is valid
##
subread_get_fastq_names_from_targets () {
  sgft_targets="$1"

  if [ -f "$sgft_targets" ]; then

    retfiles=""
    rfcols=`head -1 "$sgft_targets" | tr "\t" "\n" | grep -n -P "ReadFile[12]" | cut -d':' -f 1`

    for c in $rfcols
    do
      files=`cut -f $c "$sgft_targets" | grep -v ReadFile | grep -P "\w"`
      for f in $files
      do
        retfiles="$retfiles
`basename $f`"
      done
    done

    echo "$retfiles" | grep -P "\w" | sed -r 's| +||g'
    return
  fi
  return 1
}


##
## Get BAM file (base) name for a FASTQ file defined in targets file.
## 
## FASTQ name matches in ReadFile1 or ReadFile2 and returns Sample ID
##
## NOTE: Assumes targets file is valid
##
subread_get_bamname_for_fastq () {
  sgbff_fastq="$1"
  sgbff_targets="$2"

  if [ -f "$sgbff_targets" ]; then
    targetline=`grep -P "\b$sgbff_fastq" "$sgbff_targets" 2> /dev/null`
    if [ $? -eq 0 ]; then
      echo "$targetline" | cut -f 1 | tr -d [:cntrl:]
      return
    fi
  fi
  return 1
}


##
## Check that a targets file looks valid
##
validate_targets_file () {
  vtf_targets="$1"

  ## Exists, non-zero
  if [ ! -s "$vtf_targets" ]; then
    return 1
  fi

  ## Minimum header
  grep -P "^SampleID\t.*SampleType.*\tReadFile1" "$vtf_targets" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    return 1
  fi

  ## At least one sample specified
  nlines=`cat "$vtf_targets" | wc -l`
  if [ $nlines -lt 2 ]; then
    return 1
  fi

  ## Has at least one valid FASTQ path
  cutcol=`head -1 "$vtf_targets" | tr "\t" "\n" | grep -n ReadFile1 | cut -d':' -f 1`
  fqpaths=`cut -f "$cutcol" "$vtf_targets" | grep -P "\w" | wc -l`
  if [ $fqpaths -lt 1 ]; then
    return 1
  fi
}


##
## Get the base file name of a FASTQ file
##
subread_fastq_basename () {
  sfb_fqfile="$1"
  echo "$sfb_fqfile" | grep -P "\.fastq$" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    fbase=`basename "$sfb_fqfile" .fastq`
  else
    fbase=`basename "$sfb_fqfile" .fq`
  fi
  echo "$fbase"
}


##
## Look for base or colour space index data file.
## Returns match if found, or blank string if not found.
##
subread_find_index_file () {
  sfif_indexdir="$1"
  sfif_indexpattern="00.[bc].tab\$"
  matches=`ls "$sfif_indexdir" | grep -P "$sfif_indexpattern" 2> /dev/null`
  if [ $? -eq 0 ]; then
    nmatches=`echo "$matches" | wc -l 2> /dev/null`
    if [ $nmatches -eq 1 ]; then
      echo "$matches" | tr -d [:cntrl:]
      return
    fi
  fi
  echo ""
}


##
## Split a targets file into "sst_user_split" size chunks for coarse-grained
## parallel processing of sample data
##
subread_split_targets () {
  sst_targets="$1"
  sst_outdir="$2"
  sst_user_split="$3"

  if [ -z "$sst_targets" -o ! -f "$sst_targets" ]; then
    s4m_error "Targets file '$sst_targets' missing or invalid!" 1>&2
    return 1
  fi
  if [ $sst_user_split -lt 2 ]; then
    s4m_error "Number of splits must be 2 or more" 1>&2
    return 1
  fi

  ## Create subset targets files (one subset sent to each instance)
  sst_nsamples=`tail -n+2 "$sst_targets" | wc -l`
  sst_chunksizedefault=`expr $sst_nsamples "/" $sst_user_split`
  sst_modchunksize=`expr $sst_chunksizedefault + 1`

  if [ $sst_nsamples -lt $sst_user_split ]; then
    s4m_error "Not enough FASTQ files ($sst_nsamples) to align with given sample split ($sst_user_split) parameters!"
    return 1
  fi

  sst_user_mod=`expr $sst_nsamples "%" $sst_user_split`
  if [ $sst_user_mod -eq 0 ]; then
    s4m_log "Targets perfectly divisible; performing even split .."
    subread_split_targets_subset "$sst_targets" "$sst_outdir" $sst_user_split $sst_chunksizedefault
    if [ $? -ne 0 ]; then
      return 1
    fi
  else
    s4m_log "Targets not perfectly divisible; peforming unbalanced split (this is fine) .."
    subread_split_targets_subset "$sst_targets" "$sst_outdir" $sst_user_split $sst_modchunksize 1 $sst_user_mod
    if [ $? -ne 0 ]; then
      return 1
    fi
    ## Do remainder - how many splits left at default chunksize?
    sst_split_remain=`expr $sst_user_split - $sst_user_mod`
    s4m_debug "Got remaining splits = [$sst_split_remain]"
    if [ $sst_split_remain -gt 0 ]; then
      subread_split_targets_subset "$sst_targets" "$sst_outdir" $sst_user_split $sst_chunksizedefault `expr $sst_user_mod + 1` $sst_user_split
      if [ $? -ne 0 ]; then
        return 1
      fi
    fi
  fi
  return 0
}


##
## Utility function to split a targets file with variable start, end
## and chunk size parameters. To be called by "subread_split_targets()"
##
subread_split_targets_subset () {
  ## Input targets to be split
  ssts_targets="$1"
  ## Output path for target subsets
  ssts_outdir="$2"
  ## Number of target subsets to produce (GLOBAL i.e. user supplied total split value)
  ssts_split=$3
  ## The number of rows to use per split in this invocation only
  ssts_chunksize=$4
  ## Starting subset number (default: 1)
  ssts_split_start=$5
  ## Ending subset number (default: $split_start + 1)
  ssts_split_end=$6

  if [ -z "$ssts_targets" -o ! -f "$ssts_targets" ]; then
    s4m_error "subread_split_targets_subset(): Targets file '$ssts_targets' missing or invalid!" 1>&2
    return 1
  fi
  if [ $ssts_split -lt 2 ]; then
    s4m_error "subread_split_targets_subset(): Number of splits must be 2 or more" 1>&2
    return 1
  fi
  if [ $ssts_chunksize -lt 1 ]; then
    s4m_error "subread_split_targets_subset(): chunk size must be at least 1" 1>&2
    return 1
  fi

  if [ -z "$ssts_split_start" ]; then
    ssts_split_start=1
  fi
  if [ -z "$ssts_split_end" ]; then
    ssts_split_end=`expr $ssts_split_start + $ssts_split - 1`
  fi

  ssts_targets_body="$ssts_outdir/.targets.txt.body"
  tail -n+2 "$ssts_targets" > "$ssts_targets_body"
  ssts_nsamples=`cat "$ssts_targets_body" | wc -l`

  if [ $ssts_nsamples -lt 2 ]; then
    s4m_error "subread_split_targets_subset(): Input targets must contain at least two samples!" 1>&2
    return 1
  fi

  ssts_nstart=1
  ## If not starting with first row of first subset, set starting position accordingly
  if [ $ssts_split_start -gt 1 ]; then
    ssts_chunk=`expr $ssts_nsamples "/" $ssts_split`
    ssts_mod=`expr $ssts_nsamples "%" $ssts_split`
    ssts_modchunk=`expr $ssts_chunk + 1`
    ssts_nstart=`expr $ssts_modchunk "*" $ssts_mod + 1`
  fi
  ssts_nend=`expr $ssts_nstart + $ssts_chunksize - 1`
  for i in `seq $ssts_split_start $ssts_split_end`; do
    s4m_log "Processing split $i with chunksize=$ssts_chunksize .."
    head -1 "$ssts_targets" > "$ssts_outdir/targets_$i.txt"
    s4m_debug "["sed -n "$ssts_nstart,${ssts_nend}p" "$ssts_targets_body" ">>" "$ssts_outdir/targets_$i.txt""]"
    sed -n "$ssts_nstart,${ssts_nend}p" "$ssts_targets_body" >> "$ssts_outdir/targets_$i.txt"
    s4m_debug "[nstart=\`expr $ssts_nend + 1\`]"
    ssts_nstart=`expr $ssts_nend + 1`
    s4m_debug "[nend=\`expr $ssts_nend + $ssts_chunksize\`]"
    ssts_nend=`expr $ssts_nend "+" $ssts_chunksize`
  done
  return 0
}

