## ==========================================================================
## File:	subread_density.R
##
## Description:	Generic sample class overlay density (distribution) plots,
##		and combined (single) plots.
##
##		Will log2() input data if flag "-islog2" not given or value
##		is "FALSE"
##
## Author:	O.Korn
## Created:	2015-04-24
## Modified:	2016-11-08
## ==========================================================================

source("scripts/inc/util.R")

Usage <- function() {
   print("Usage:  R -f <script_name.R> --args -input=<dataframe.txt> -output=<output.png> -phenofile=<sample_pheno.txt> [-islog2=TRUE/FALSE] [-title=Custom title] [-maxy=<value>] [-maxx=<value>] [-minx=<value>] [-showdeciles=<TRUE/FALSE>] [-debug=<TRUE/FALSE>]")
   print("")
   print(" 'phenofile' expects 2 or 3 tab-separated columns: SAMPLE_ID  GROUP  [OPTIONAL_SAMPLE_LABEL]")
}

## Expecting input data frame
inputfile <- ARGV.get("input",required=T)
outputfile <- ARGV.get("output",required=T)
phenofile <- ARGV.get("phenofile",required=T)
showdeciles <- ifelse(is.na(ARGV.get("showdeciles")), FALSE, as.logical(ARGV.get("showdeciles")))
## optionally set the Y axis limit instead of from max(dens$y)
maxy <- ifelse(is.na(ARGV.get("maxy")), NA, as.numeric(ARGV.get("maxy")))
minx <- ifelse(is.na(ARGV.get("minx")), NA, as.numeric(ARGV.get("minx")))
maxx <- ifelse(is.na(ARGV.get("maxx")), NA, as.numeric(ARGV.get("maxx")))
islog2 <- ifelse(is.na(ARGV.get("islog2")), FALSE, as.logical(ARGV.get("islog2")))
title <- ifelse(is.na(ARGV.get("title")), "Density plot log2", ARGV.get("title"))
## Toggle for trace debug of sample / group metadata
debug <- ifelse(is.na(ARGV.get("debug")), FALSE, as.logical(ARGV.get("debug")))

output_path <- dirname(outputfile)
fbase <- sub(".png", "", basename(outputfile))

## Load expression table
expression_table <- read.table(as.character(inputfile), sep="\t", header=T, row.names=1, na.strings="", quote="", check.names=F, as.is=T)
if (! islog2) {
  expression_data <- log2(as.matrix(expression_table))
  expression_data[ expression_data == -Inf ] <- NA
} else {
  expression_data <- as.matrix(expression_table)
}

sampleNames <- colnames(expression_table)
## Strip .bam from sample IDs if present so sample IDs in data match
## those in sample_pheno
if (length(grep(".bam", sampleNames, fixed=T))) {
   sampleNames <- gsub(".bam", "", sampleNames, fixed=T)
}

## Load sample phenotype file
phenoMap <- read.table(phenofile, sep="\t", stringsAsFactors=FALSE, colClasses="character")
if (identical(sort(as.vector(phenoMap$V1)), sort(sampleNames)) != TRUE) {
  print(paste("Error: Samples in phenotype file '",phenofile,"' do not match samples in expression data (check quantity and IDs)!",sep=""))
  print("Samples from phenoMap: ")
  print(sort(as.vector(phenoMap$V1)))
  print("Samples from input data: ")
  print(sort(as.vector(sampleNames)))
  q(status=1)
}
## Must be ordered as per sampleNames(data)
sampleGroupsOrdered <- as.vector(unlist(lapply(sampleNames, function(x) { phenoMap[phenoMap$V1 == x,]$V2 })))
#sampleGroupVector <- as.numeric(as.factor(sampleGroupsOrdered))
sampleGroupVector <- as.numeric(factor(sampleGroupsOrdered, levels=unique(sampleGroupsOrdered)))
groupNames <- unique(sampleGroupsOrdered)

### DEBUG ###
if (debug) {
  print("DEBUG: factor(sampleGroupsOrdered)")
  print(factor(sampleGroupsOrdered, levels=unique(sampleGroupsOrdered)))
  print("DEBUG: sampleGroupsOrdered:")
  print(sampleGroupsOrdered)
  print("DEBUG: sampleGroupVector:")
  print(sampleGroupVector)
  print ("DEBUG: groupNames:")
  print(groupNames)
  print("DEBUG: sampleNames:")
  print(sampleNames)
}
### END ###

## Calculate overall density to get max X
dens <- density(expression_data, na.rm=T)
densMaxYList <- c()
if (is.na(maxy)) {
  ## To get max y, need to look at every sample and get the max of those otherwise
  ## if looking at global density, of course the maximum will be dampened because
  ## it's proportional to the whole - this results in plots being truncated sometimes
  ## at the top of the Y axis.
  densMaxYList <- unlist(lapply(c(1:length(sampleNames)), function (x) { 
    colDens <- density(expression_data[,x], na.rm=T)
    max(colDens$y)
  }))
}

## Set 'x' axis range
if (! is.na(maxx)) {
  xmax <- maxx
} else {
  xmax <- max(dens$x, na.rm=T)
}

if (! is.na(minx)) {
  xmin <- minx
} else {
  xmin <- min(dens$x, na.rm=T)
}

## Y max for plotting purposes
if (! is.na(maxy)) {
  ## User supplied, use this
  densMaxY <- maxy
} else {
  densMaxY <- max(densMaxYList)
}

## per sample group density (overlay)
bitmap(outputfile, type="png16m", height=1000, width=1000, units="px")
## Set palette based on number of sample groups (or minimum of 2 required for palette() to work)
palette_size <- ifelse(length(groupNames) >= 2, length(groupNames), 2)
palette(rainbow(palette_size))
cols <- unlist(lapply(sampleGroupVector, function(x) { palette()[x] }))

### DEBUG ###
if (debug) {
  print("DEBUG: cols:")
  print(cols)
}
### END ###

## Init empty plot with xlab and ylab replicating defaults seen in an all-sample "plot(density(x))" call
plot(1, type="n", main=title, xlab=paste("N = ",dens$n, "   Bandwidth = ",round(dens$bw,5),sep=""), ylab="Density", xlim=c(xmin, xmax), ylim=c(0, densMaxY))
## Plot density curves for each individual sample

### DEBUG ###
linesDrawn <- 0
### END ###
for (sample in seq(1:length(sampleNames))) {
  lines(density(expression_data[,sample], na.rm=T), col=cols[sample], lwd=2)
  ### DEBUG ###
  if (debug && linecol > 0) {
    linesDrawn <- linesDrawn + 1
  }
  ### END ###
}
xintervals <- seq(floor(xmin),ceiling(xmax),by=0.2)
## The 'y' position calculation of minor tick marks axis was arrived at by trial and error,
## but seems to give consistent results in all cases for log2
axis(1, at=xintervals, pos=-(densMaxY / 17), cex.axis=0.6)
if (showdeciles) {
  deciles <- quantile(dens$x, probs=seq(0.1,0.9,0.1))
  abline(v=deciles,col="gray80")
}
## Legend colours automatically wrap if # groups exceeds palette entries, so output should
## match line colours above
#legend("topright", groupNames, col=1:max(sampleGroupVector), lwd=2, lty=1)
legend("topright", groupNames, col=palette(), lwd=2, lty=1)
dev.off()

### DEBUG ###
## We had a few issues with lines now being drawn due to bad logic for line colouring.
## This output will show that all samples are being drawn (or not).
## This *should* be fixed now properly but you never know.
if (debug) {
  print(paste("DEBUG: Drew",linesDrawn,"lines in overlay density plot"))
}
### END ###

## Single combined density plot
title <- paste(title, "(all)")
bitmap(paste(output_path,"/",fbase,"_single.png",sep=""), type="png16m", height=1000, width=1000, units="px")
plot(density(expression_data, na.rm=T), main=title, lwd=2, xlim=c(xmin,xmax), ylim=c(0,densMaxY))
xintervals <- seq(floor(xmin),ceiling(xmax),by=0.2)
## The 'y' position calculation of minor tick marks axis was arrived at by trial and error,
## but seems to give consistent results in all cases for log2
axis(1, at=xintervals, pos=-(densMaxY / 17), cex.axis=0.6)
if (showdeciles) {
  deciles <- quantile(dens$x, probs=seq(0.1,0.9,0.1))
  abline(v=deciles,col="gray80")
}
dev.off()

