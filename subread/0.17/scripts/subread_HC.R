#===================================
# Generic HC plot
#===================================
# Author: O.Korn

source("scripts/inc/util.R")

Usage <- function() {
   print("Usage:  R -f <script_name.R> --args -input=<dataframe.txt> -output=<output.png> [-phenofile=<sample_info.txt>] [-islog2=TRUE/FALSE] [-title=Custom HC title]")
   print("")
   print(" 'phenofile' expects 2 or 3 tab-separated columns: SAMPLE_ID  GROUP  [OPTIONAL_SAMPLE_LABEL]")
}


# Expecting input data frame
inputfile <- ARGV.get("input",T)
outputfile <- ARGV.get("output",T)
islog2 <- FALSE
title <- ARGV.get("title")
phenofile <- ""

if (is.na(ARGV.get("phenofile"))) {
   print("Warning: Phenotype file (sample name to group) not given, only sample ID labels will be used.")
   Usage()
   Sys.sleep(1)
} else {
   phenofile <- as.character(ARGV.get("phenofile"))
}

outFileBase <- sub(".png$", "", as.character(outputfile))

if (! is.na(ARGV.get("islog2"))) {
   islog2 <- as.logical(ARGV.get("islog2"))
}

## Load input data frame
## Works for data frames with or without leading TAB in header row
data <- read.table(as.character(inputfile), sep="\t", header=T, row.names=1, na.strings="", quote="", check.names=F, as.is=T)
sampleNames <- colnames(data)

## Strip .bam from sample IDs if present
##
if (length(grep(".bam", sampleNames, fixed=T))) {
   sampleNames <- gsub(".bam", "", sampleNames, fixed=T)
}

if (nchar(phenofile)) {
  if (file.exists(phenofile)) {
    phenoMap <- read.table(phenofile, sep="\t", stringsAsFactors=F, colClasses="character", check.names=F, quote="")
    if (identical(sort(as.vector(phenoMap$V1)), sort(sampleNames)) != TRUE) {
      print(paste("Error: Samples in phenotype file '",phenofile,"' do not match samples in expression data (check quantity and IDs)!",sep=""))
      print("Samples from phenoMap: ")
      print(sort(as.vector(phenoMap$V1)))
      print("Samples from data: ")
      print(sort(as.vector(sampleNames)))
      q(status=1)
    }

    ## sample ID -> group name, ordered as per data
    sampleNames <- unlist(lapply(sampleNames, function(x) { paste(x, phenoMap[phenoMap$V1 == x,]$V2, sep=" - ") }))
  }
}


# Plot sample hierarchical cluster

## Based off Lumi's plot(what="sampleRelation" ..) where rows exhibiting sd/mean > 0.1 are retained
#DE_probes <- c()
#for (probe in row.names(data)) {
#  row <- unlist(data[probe,])
#  if (is_log2 == "TRUE") {
#    if (sd(2^row) / mean(2^row) > 0.1) {
#      DE_probes <- c(DE_probes, probe)
#    }
#  } else {
#    if (sd(row) / mean(row) > 0.1) {
#      DE_probes <- c(DE_probes, probe)
#    }
#
#  }
#}
#d.t <- dist(t(data[ row.names(data) %in% DE_probes, ]))

## Now, the quick way which is orders of magnitude faster, even if it's not 
## achieved with a single apply() call.
## NOTE: cov = coefficient of variation = sd/mean
covs <- c()
if (islog2) {
  covs <- apply(data, 1, function(x) { sd(2^as.numeric(unlist(x)),na.rm=T) / mean(2^as.numeric(unlist(x)),na.rm=T) })
} else {
  covs <- apply(data, 1, function(x) { sd(as.numeric(unlist(x)),na.rm=T) / mean(as.numeric(unlist(x)),na.rm=T) })
}
covs_DE <- covs[ covs > 0.1 ]

## Truncate sample names to 35 chars (don't want ridiculously long names in plot)
sampleNames <- substr(sampleNames, 1, 35)

## default title
if (is.na(title)) {
  title <- "Sample relations"
}

d.t <- dist(t(data[ names(covs_DE), ]))
bitmap(outputfile, type="png16m", height=1000, width = 1200, units = "px")
## If there are many samples, decrease font size for sample labelling to avoid overlapping
if (length(sampleNames) >= 50) {
  ## Plot dendogram and labels with 0.5 font multiplier (so we don't affect title and y-axis font)
  ## - "cex.lab" takes no effect hence global cex=0.5 is used
  par(cex=0.5)
  plot(hclust(d.t, method="average"), xlab="", ylab="", main="", labels=sampleNames, axes=FALSE)
  ## Now plot the title at default size with slightly reduced tick label font size
  par(cex=1, cex.axis=0.8)
  title(xlab="d.t", ylab="Height", main=paste(title," based on ",length(covs_DE)," features with sd/mean > 0.1",sep=""))
  axis(2)
} else {
  plot(hclust(d.t, method="average"), main=paste(title," based on ",length(covs_DE)," features with sd/mean > 0.1",sep=""), labels=sampleNames)
}
dev.off()

