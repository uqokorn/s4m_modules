#!/bin/sh
## test_subread.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  SUBREAD_TEST_DIR="./data/test"
  SUBREAD_TEST_FASTQ="$SUBREAD_TEST_DIR/fastq"
  SUBREAD_TEST_TARGETS="$SUBREAD_TEST_DIR/targets"
  export SUBREAD_TEST_DIR SUBREAD_TEST_FASTQ SUBREAD_TEST_TARGETS
 
  ## Load functions being tested
  . ./inc/subread_funcs.sh

  ## Set up temp data / files location and populate dummy files
  mkdir "$S4M_TMP/$$"
   
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_TMP/$$"
}

## Run between tests
setUp () {
  rm -rf "$S4M_TMP/$$/"*.tab
  touch "$S4M_TMP/$$/foo.00.c.tab"
}



## TESTS ##

## Test that required binary dependencies exist.
## We're not reinventing the wheel here, so simply call the same core s4m
## function for dependency validation that would be run whenever the target
## module is invoked.
testBinaryDependencies () {
  s4m_validate_module_binary_dependencies "$UNITTEST_TARGET_MODULE"
  assertEquals 0 $?
}

## TODO: Need a way to test the Conda env, there is no "version" command currently
#test_subread_environment () {
#  s4m_exec subread::version > /dev/null 2>&1
#  assertEquals 0 $?
#}

testDryRunModeByBoolStringOrReturnStatus () {
  SUBREAD_DRYRUN="TRUE"; export SUBREAD_DRYRUN
  assertEquals "true" `is_subread_dryrun`
  is_subread_dryrun > /dev/null 2>&1
  assertEquals 0 $?
}

## Test FASTQ files found by extension
testFindFASTQFileRegex () {
  fqfiles=`ls -1 ./$SUBREAD_TEST_FASTQ/ | grep -P "\.(fastq|fq)$"`
  assertNotNull "$fqfiles"
}

## Check we find single index file (foo.00.c.tab)
testFindSubreadIndexRegexNotNull () {
  matches=`subread_find_index_file "$S4M_TMP/$$"`
  assertNotNull "$matches"
}

## It's invalid to find 2 or more index files
testMultipleSubreadIndexTypesReturnsNull () {
  touch "$S4M_TMP/$$/bar.00.b.tab"
  matches=`subread_find_index_file "$S4M_TMP/$$"`
  assertNull "$matches"
}

## It's invalid to find zero index files
testZeroSubreadIndexesReturnsNull () {
  rm -rf "$S4M_TMP/$$/"*.tab
  matches=`subread_find_index_file "$S4M_TMP/$$"`
  assertNull "$matches"
}

## Test find index by name
testIndexExistsByName () {
  idxbase="foo"
  indexdir="$S4M_TMP/$$"
  assertTrue "[ -f $indexdir/${idxbase}.00.c.tab ]"
}

## Base space is default if not specified
testBaseSpaceDefault () {
  baseorcs=""
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "base" "$baseorcs"
}

## Test we're picking up color space directive
testIsColorSpace () {
  baseorcs="color"
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "color" "$baseorcs"
}

## Colour with the 'u' this time
testIsColourSpace () {
  baseorcs="colour"
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "color" "$baseorcs"
}

## Base space is default if we get unknown value
testUnknownSpaceDefaultsToBase () {
  baseorcs="FOO"
  echo "$baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    baseorcs="color"
  else
    baseorcs="base"
  fi
  assertEquals "base" "$baseorcs"
}

## Test we have the R scripts we expect
testHaveRequiredRScripts () {
  assertTrue "[ -f ./scripts/subread_build_index.R ]"
  assertTrue "[ -f ./scripts/subread_fastq_to_bam.R ]"
  assertTrue "[ -f ./scripts/subread_feature_count_transcript.R ]"
  assertTrue "[ -f ./scripts/subread_feature_count_gene.R ]"
  assertTrue "[ -f ./scripts/inc/util.R ]"
  assertTrue "[ -f ./scripts/inc/sequtil.R ]"
}

## Test the logic that controls targets (sample) splitting to ensure the integrity
## of the coarse-grained chunking process (we assume here that the in-built
## validation process will pass or fail as intended, so we just check their return
## values)
testMultirunTargetSplittingBadSubsetInputs () {
  ## Number of splits must be 1 or greater
  nsplit=1
  subread_split_targets_subset "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$" $nsplit 1 > /dev/null 2>&1
  assertEquals 1 "$?"

  ## Invalid targets file
  subread_split_targets_subset "$SUBREAD_TEST_TARGETS/targets_not_exist.txt" "$S4M_TMP/$$" 2 1 > /dev/null 2>&1
  assertEquals 1 "$?"

  ## Bad chunk size (must be 1 or more)
  nchunk=0
  subread_split_targets_subset "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$" 2 $nchunk > /dev/null 2>&1
  assertEquals 1 "$?"

  ## Not enough samples in targets (must have at least 2 for 2 splits)
  ## Top-level function
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n1.txt" "$S4M_TMP/$$" 2 > /dev/null 2>&1
  assertEquals 1 "$?"
  ## Subset function
  subread_split_targets_subset "$SUBREAD_TEST_TARGETS/targets_n1.txt" "$S4M_TMP/$$" 2 1 > /dev/null 2>&1
  assertEquals 1 "$?"
}

## Test expected results based on given inputs
testMultirunPreAlignTargetsSplittingExpected () {
  ## 13 samples split 13 ways (expect: 1 1 1 1 1 1 1 1 1 1 1 1 1)
  mkdir -p "$S4M_TMP/$$/13by13"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by13" 13 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by13/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by13/targets_13.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by13/targets_1.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by13/targets_13.txt" | wc -l`
  assertEquals 1 "$firstn"
  assertEquals 1 "$lastn"

  ## 13 samples split 7 ways (expect: 2 2 2 2 2 2 1)
  mkdir -p "$S4M_TMP/$$/13by7"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by7" 7 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by7/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by7/targets_7.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by7/targets_1.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by7/targets_7.txt" | wc -l`
  assertEquals 2 "$firstn"
  assertEquals 1 "$lastn"

  ## 13 samples split 6 ways (expect: 3 2 2 2 2 2)
  mkdir -p "$S4M_TMP/$$/13by6"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by6" 6 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by6/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by6/targets_6.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by6/targets_1.txt" | wc -l`
  secondn=`tail -n+2 "$S4M_TMP/$$/13by6/targets_2.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by6/targets_6.txt" | wc -l`
  assertEquals 3 "$firstn"
  assertEquals 2 "$secondn"
  assertEquals 2 "$lastn"

  ## 13 samples split 5 ways (expect: 3 3 3 2 2)
  mkdir -p "$S4M_TMP/$$/13by5"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by5" 5 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by5/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by5/targets_5.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by5/targets_1.txt" | wc -l`
  thirdn=`tail -n+2 "$S4M_TMP/$$/13by5/targets_3.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by5/targets_5.txt" | wc -l`
  assertEquals 3 "$firstn"
  assertEquals 3 "$thirdn"
  assertEquals 2 "$lastn"

  ## 13 samples split 4 ways (expect: 4 3 3 3)
  mkdir -p "$S4M_TMP/$$/13by4"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by4" 4 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by4/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by4/targets_4.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by4/targets_1.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by4/targets_4.txt" | wc -l`
  assertEquals 4 "$firstn"
  assertEquals 3 "$lastn"

  ## 13 samples split 3 ways (expect: 5 4 4)
  mkdir -p "$S4M_TMP/$$/13by3"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by3" 3 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by3/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by3/targets_3.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by3/targets_1.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by3/targets_3.txt" | wc -l`
  assertEquals 5 "$firstn"
  assertEquals 4 "$lastn"

  ## 13 samples split 2 ways (expect: 7 6)
  mkdir -p "$S4M_TMP/$$/13by2"
  subread_split_targets "$SUBREAD_TEST_TARGETS/targets_n13.txt" "$S4M_TMP/$$/13by2" 2 > /dev/null 2>&1
  assertTrue "[ -f $S4M_TMP/$$/13by2/targets_1.txt ]"
  assertTrue "[ -f $S4M_TMP/$$/13by2/targets_2.txt ]"
  firstn=`tail -n+2 "$S4M_TMP/$$/13by2/targets_1.txt" | wc -l`
  lastn=`tail -n+2 "$S4M_TMP/$$/13by2/targets_2.txt" | wc -l`
  assertEquals 7 "$firstn"
  assertEquals 6 "$lastn"
}

