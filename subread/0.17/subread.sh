#!/bin/sh
##===========================================================================
## Module:   subread
## Author:   Othmar Korn
##
## Align FASTQ read libraries and output RPKM gene/transcript count table/s
##===========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##===========================================================================

## Include subread utility functions
. ./inc/subread_funcs.sh


subread_fastx_quality () {
  sfq_fastqfile="$1"
  sfq_fastxoutdir="$2"
  ## suppress detailed output (could interleave in STDOUT stream and look ugly)
  ## if this function is called in multi-process usage
  sfq_quietmode="$3"

  fbase=`subread_fastq_basename "$sfq_fastqfile"`
  sfq_statsout="$sfq_fastxoutdir/${fbase}_stats.txt"

  touch "$sfq_fastxoutdir/.${fbase}.lock"

  s4m_log "Calling fastx_quality_stats on $sfq_fastqfile.."

  ## Try WITHOUT -Q33 flag first because we know what the error will be if it's
  ## a modern Illumina/SOLiD FASTQ file - on the off chance that it's not, it'll run
  ## without error so it's cleaner this way in the context of how this tool treats
  ## default as not being Phred+33.
  if [ `is_subread_dryrun` = false ]; then
    ret=`fastx_quality_stats -i "$sfq_fastqfile" -o "$sfq_statsout" 2>&1`
    if [ $? -ne 0 ]; then
      ## ASCII encoding issue - try Q33 for Sanger Phred+33 (Illumina/SOLiD)
      echo "$ret" | grep -i "invalid quality score" > /dev/null 2>&1
      if [ -z "$sfq_quietmode" ]; then
        s4m_log "  Re-running with ASCII+33 support"
      fi
      if [ $? -eq 0 ]; then
        ret=`fastx_quality_stats -Q33 -i "$sfq_fastqfile" -o "$sfq_statsout" 2>&1`
      fi
    fi
    if [ -z "$sfq_quietmode" ]; then
      s4m_log "  done"
    fi
  fi

  if [ -z "$sfq_quietmode" ]; then
    s4m_log "Generating quality score and nucleotide distribution plots for $sfq_fastqfile.."
  fi
  sfq_library="$fbase"
  if [ `is_subread_dryrun` = false ]; then
    fastq_quality_boxplot_graph.sh -i "$sfq_statsout"  -o "$sfq_fastxoutdir/${fbase}_quality_boxplot.png" -t "$sfq_library"
    fastx_nucleotide_distribution_graph.sh -i "$sfq_statsout"  -o "$sfq_fastxoutdir/${fbase}_nucleotide_dist.png" -t "$sfq_library"
    rm -f "$sfq_fastxoutdir/.${fbase}.lock"
  fi
  if [ -z "$sfq_quietmode" ]; then
    s4m_log "  done"
  fi
}


##
## Determine if we need to rerun FASTQC based on timestamps of input FASTQ
##
## Unit Test: TODO
##
needs_fastqc_rerun () {
  nfr_fqfilesdir="$1"
  nfr_fqfiles="$2"
  nfr_outdir="$3"

  if [ ! -f "$nfr_outdir/FASTQC_quality.PDF" -o ! -f "$nfr_outdir/FASTQC_nucleotide_dist.PDF" ]; then
    echo "true"
    return
  fi

  ## If QC outputs exist, only re-run if input FASTQ files newer than output reports
  for fq in $nfr_fqfiles
  do
    if [ "$nfr_fqfilesdir/$fq" -nt "$nfr_outdir/FASTQC_quality.PDF" -o "$nfr_fqfilesdir/$fq" -nt "$nfr_outdir/FASTQC_nucleotide_dist.PDF" ]; then
      echo "true"
      return
    fi
  done
  echo "false"
}


##
## Create per-base phred quality summaries for read libraries in given input directory.
##
## NOTE: Expects FASTQ files named "*.fastq" or "*.fq"
## NOTE: If targets file given, only FASTQ files reference within will be used.
##
## Unit Test: TODO
##
subread_fastqc () {
  sf_fastqdir="$1"
  sf_outdir="$2"
  sf_targets="$3"
  sf_updatemode="$4"

  s4m_debug "Running QC on FASTQ files in directory '$sf_fastqdir', writing outputs to '$sf_outdir'"

  if [ ! -z "$sf_targets" ]; then
    validate_targets_file "$sf_targets"
    if [ $? -ne 0 ]; then
      s4m_error "Targets file '$sf_targets' missing or has invalid format!"
      return 1
    fi
    s4m_log "Using FASTQ files referenced in targets.."
    sf_fqfiles=`subread_get_fastq_names_from_targets "$sf_targets"`

  else
    s4m_log "No targets file, looking for FASTQ in input directory.."
    sf_fqfiles=`subread_get_fastq_files "$sf_fastqdir"`
  fi

  if [ -z "$sf_fqfiles" ]; then
    s4m_error "No FASTQ files found!"
    return 1
  fi

  s4m_log "Got FASTQ files:"
  s4m_log "`echo $sf_fqfiles | sed 's/^/  /g'`"

  if [ ! -z "$sf_updatemode" ]; then
    do_fastqc=`needs_fastqc_rerun "$sf_fastqdir" "$sf_fqfiles" "$sf_outdir"`
    if [ "$do_fastqc" = "false" ]; then
      s4m_log "No changes to input FASTQ since last run, will not re-run fastx_quality_stats"
      return
    fi
  fi

  for fq in $sf_fqfiles
  do
    subread_fastx_quality "$sf_fastqdir/$fq" "$sf_outdir"
  done

  s4m_log "Compiling FASTQC reports (PDF).."
  ## Compile Phred quality and nucleotide dist PDF reports
  convert "$sf_outdir"/*quality*.png "$sf_outdir"/FASTQC_quality.PDF && convert "$sf_outdir"/*nucleotide*png "$sf_outdir"/FASTQC_nucleotide_dist.PDF
}


##
## Block until all fastx processing done for each input FASTQ
##
wait_for_fastx_done () {
  wffd_outdir="$1"
  wffd_fqfiles="$2"

  wffd_numfqfiles=`echo "$wffd_fqfiles" | wc -l`

  s4m_debug "  Waiting on fastx quality report outputs for $wffd_numfqfiles FASTQ files: $wffd_fqfiles
"

  wffd_numdone=0
  ## Wait for all FASTQC outputs to be produced, check every N seconds
  while [ $wffd_numdone -lt $wffd_numfqfiles ];
  do
    sleep $SUBREAD_FASTX_WAIT_SECONDS
    for fq in $wffd_fqfiles
    do
      fbase=`subread_fastq_basename "$fq"`
      if [ -f "$wffd_outdir/.${fbase}.lock" ]; then
        s4m_debug "    '$fbase' fastx is NOT done, checking all again in $SUBREAD_FASTX_WAIT_SECONDS seconds.."
        wffd_numdone=0
        break
      else
        s4m_debug "    '$fbase' fastx is done"
        wffd_numdone=`expr $wffd_numdone + 1`
      fi
    done
  done
}


##
## Create per-base phred quality summaries for read libraries in given input directory.
## Multi-process version (sub-shell execution).
##
## NOTE: Expects FASTQ files named "*.fastq" or "*.fq"
##
## Unit Test: TODO
##
subread_fastqc_multi () {
  sfm_fastqdir="$1"
  sfm_outdir="$2"
  sfm_ncpu="$3"
  sfm_targets="$4"
  sfm_updatemode="$5"

  if [ -z "$sfm_ncpu" ]; then
    s4m_error "Must supply number of CPUs for multi-process FASTQC!"
    return 1
  fi

  s4m_debug "Running QC on FASTQ files in directory '$sfm_fastqdir', writing outputs to '$sfm_outdir'"

  if [ ! -z "$sfm_targets" ]; then
    validate_targets_file "$sfm_targets"
    if [ $? -ne 0 ]; then
      s4m_error "Targets file '$sfm_targets' missing or has invalid format!"
      return 1
    fi
    s4m_log "Using FASTQ files referenced in targets.."
    sfm_fqfiles=`subread_get_fastq_names_from_targets "$sfm_targets"`

  else
    sfm_fqfiles=`subread_get_fastq_files "$sfm_fastqdir"`
  fi

  if [ -z "$sfm_fqfiles" ]; then
    s4m_error "No FASTQ files found!"
    return 1
  fi

  s4m_log "Got FASTQ files:"
  s4m_log "`echo $sfm_fqfiles | sed 's/^/  /g'`"

  if [ ! -z "$sfm_updatemode" ]; then
    do_fastqc=`needs_fastqc_rerun "$sfm_fastqdir" "$sfm_fqfiles" "$sfm_outdir"`
    if [ "$do_fastqc" = "false" ]; then
      s4m_log "No changes to input FASTQ, will not re-run FASTQC"
      return
    fi
  fi

  sfm_numfqfiles=`echo "$sfm_fqfiles" | wc -l`
  ### DEBUG ###
  s4m_debug "Got # FASTQ files = [$sfm_numfqfiles]"
  sleep 3
  ### END DEBUG

  ## More FASTQ files than CPUs, let's chunk the processing into subset
  ## splits and process one chunk at a time until there are no more FASTQ
  ## files to process
  if [ $sfm_numfqfiles -gt $sfm_ncpu ]; then

    ## If # FASTQ files is a multiple of # CPUs, it's a simple division to work out
    ## how many batches we need to process
    modulo=`expr $sfm_numfqfiles % $sfm_ncpu`
    if [ $modulo = "0" ]; then
      totalsplits=`expr $sfm_numfqfiles \/ $sfm_ncpu`
    ## Otherwise, add one to make sure remainder of modulus is taken care of
    else
      totalsplits=`expr $sfm_numfqfiles \/ $sfm_ncpu + 1`
    fi

    nsplit=1
    ## Stage a subset of FASTQ files for processing
    while [ $nsplit -le $totalsplits ]; do
      s4m_log "Submitting FASTQ batch $nsplit of $totalsplits for quality reporting"
      sfm_start=`expr $nsplit \* $sfm_ncpu - $sfm_ncpu + 1`
      ## it doesn't matter if end pos overruns how many samples we have because
      ## sed will gracefully allow it
      sfm_end=`expr $nsplit \* $subread_ncpu`
      fqsplit=`echo "$sfm_fqfiles" | sed -n "${sfm_start},${sfm_end}p"`
      for fq in $fqsplit
      do
        ## Run per-sample fastx quality routines in sub-shells (asynchronously)
        ( subread_fastx_quality "$sfm_fastqdir/$fq" "$sfm_outdir" quiet )&
      done
      wait_for_fastx_done "$sfm_outdir" "$fqsplit"
      nsplit=`expr $nsplit + 1`
    done

  ## Have NCPU >= N samples, run all at once
  else
    for fq in $sfm_fqfiles
    do
      ## Run per-sample fastx quality routines in sub-shells (asynchronously)
      ( subread_fastx_quality "$sfm_fastqdir/$fq" "$sfm_outdir" quiet )&
    done
    wait_for_fastx_done "$sfm_outdir" "$sfm_fqfiles"
  fi

  s4m_log "Compiling FASTQC reports (PDF).."
  ## Compile Phred quality and nucleotide dist PDF reports
  convert "$sfm_outdir"/*quality*.png "$sfm_outdir"/FASTQC_quality.PDF && convert "$sfm_outdir"/*nucleotide*png "$sfm_outdir"/FASTQC_nucleotide_dist.PDF
  return $?
}


##
## Create read-aligned BAMs from FASTQ files.
##
## NOTE: Only uses FASTQ files referenced in targets file.
##
## Unit Test: TODO
##
subread_align_fastq () {
  saf_fqfiles=""
  if [ ! -f "$subread_targets" ]; then
    s4m_error "Read targets file '$subread_targets' not found!"
    return 1
  else
    saf_fqfiles=`subread_get_fastq_names_from_targets "$subread_targets"`
  fi

  ## Just grabbing the FASTQ names to print to log, we don't actually do anything further with them
  if [ -z "$saf_fqfiles" ]; then
    s4m_error "No FASTQ files found in targets!"
    return 1
  fi
  s4m_log "Using FASTQ files referenced in targets.."
  s4m_log "`echo $saf_fqfiles | sed 's/^/  /g'`"

  ## If NCPUs not given, use default threads
  if [ -z "$subread_ncpu" ]; then
    s4m_log "Warning: No NCPU given, using default NCPU=$SUBREAD_DEFAULT_NTHREADS"
    subread_ncpu=$SUBREAD_DEFAULT_NTHREADS
  fi
  if [ -z "$subread_mismatches" ]; then
    s4m_log "Warning: No mismatches given, using default #mismatches=$SUBREAD_DEFAULT_MISMATCHES"
    subread_mismatches=$SUBREAD_DEFAULT_MISMATCHES
  fi
  saf_extraflags=""
  if [ ! -z "$subread_indels" ]; then
    saf_extraflags="-indels='$subread_indels'"
  fi
  ## If flag exists, disable unique-only mapping
  if [ ! -z "$subread_multimap" ]; then
    saf_extraflags="$saf_extraflags -uniqueonly=FALSE"
  fi
  if [ ! -z "$subread_phredoffset" ]; then
    saf_extraflags="$saf_extraflags -phredoffset=$subread_phredoffset"
  fi
  if [ ! -z "$subread_nbestlocations" ]; then
    saf_extraflags="$saf_extraflags -nbestlocations=$subread_nbestlocations"
  fi
  if [ ! -z "$subread_nconsensus" ]; then
    saf_extraflags="$saf_extraflags -nconsensus=$subread_nconsensus"
  fi
  if [ ! -z "$subread_type" ]; then
    saf_extraflags="$saf_extraflags -type=$subread_type"
  fi
  if [ ! -z "$subread_fraglen1" ]; then
    saf_extraflags="$saf_extraflags -minfrag=$subread_fraglen1"
  fi
  if [ ! -z "$subread_fraglen2" ]; then
    saf_extraflags="$saf_extraflags -maxfrag=$subread_fraglen2"
  fi

  ## Choose Single run or Multi run
  ##
  ## NOTE: Multi-run launches multiple instances of the alignment R script, based on the given memory (GB).
  ##
  ## If memory is less than 20GB, then only a single instance of the alignment script is used.
  ## However for 20GB or more, then the number of instances is equal to the memory divided by 10.
  ##
  ## CPUs are shared across instances e.g. 16 CPUs, 20GB would yield 2 x 8CPU instances using 10GB RAM each.
  ##
  ret=0
  if [ -z "$subread_multi_run" -o $subread_memory -lt 20 ]; then
    call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_fastq_to_bam.R" "-fastqdir='$subread_fastqdir' -output='$subread_outdir' -indexdir='$subread_indexdir' -indexname='$subread_indexname' -targets='$subread_targets' -ncpu='$subread_ncpu' -mismatches='$subread_mismatches' $saf_extraflags"
    ret=$?
 
  else 
    ## Set the number of splited target depends on Memory and CPUs 
    if [ $subread_ncpu -lt 4 ];then
      s4m_error "Not enough CPUs selected for multi-run alignment operation (requires 4 or greater)"
      return 1
    fi
    user_split=`expr $subread_memory "/" 10`
    ## TODO: Define this as a constant somewhere
    optimum_cpu=4           
    used_cpu=`expr $subread_ncpu "/" $user_split`
    if [  $used_cpu -lt $optimum_cpu ];then
      used_cpu=$optimum_cpu
      user_split=`expr $subread_ncpu "/" $used_cpu`
    fi

    s4m_log "Splitting targets .."
    subread_split_targets "$subread_targets" "$subread_outdir" $user_split
    if [ $? -ne 0 ]; then
      return 1
    fi

    s4m_log "Validating target subsets .."
    ## Validate total rows in target subsets (should add up to original targets.txt sample count)
    ntot=0
    nfound=0
    for t in `ls -1 "$subread_outdir/"targets_*txt | grep -P "targets_[0-9]+\.txt"`; do
      nfound=`expr $nfound + 1`
      nrow=`tail -n+2 "$t" | wc -l`
      ntot=`expr $ntot + $nrow`
    done
    ntargets=`tail -n+2 "$subread_targets" | wc -l`

    if [ $ntot -ne $ntargets ]; then
      s4m_error "Failed self-validation of target splitting; subset total row count ($ntot) does not match original targets sample count ($ntargets)!"
      return 1
    fi
    if [ $nfound -ne $user_split ]; then
      s4m_error "Failed self-validation of target splitting; number of subsets produced ($nfound) does not match number of splits ($user_split)!"
      return 1
    fi
    s4m_log "  [ OK ]"

    s4m_log "Launching $split alignment instances.."
    ## Launch multi-run alignment processes
    fbase=`basename "$subread_targets" .txt`
    for i in `seq 1 $user_split`; do 
      subset_targets="$subread_outdir/${fbase}_$i.txt"
      subset_log="$subread_outdir/log_${fbase}_$i.log"
      s4m_log "  Running ${fbase}_$i.txt"
      call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_fastq_to_bam.R" "-fastqdir='$subread_fastqdir' -output='$subread_outdir' -indexdir='$subread_indexdir' -indexname='$subread_indexname' -targets='$subset_targets' -ncpu='$used_cpu' -mismatches='$subread_mismatches' $saf_extraflags" > "$subset_log" 2>&1 &
    done
    wait
   
    ret=0 
    ## Check for errors in any individual runs
    for i in `seq 1 $user_split`; do 
      grep ERROR "${subread_outdir}/log_${fbase}_$i.log" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        s4m_error "Check failed alignment log: $subread_outdir/log_${fbase}_$i.log"
        ret=1
      fi
    done
    s4m_log "END Fastq2BAM"
  fi 
  return $ret
}


##
## Build genome index in given directory and with specified index file base name.
## TODO: Test color space index building.
##
## Unit Test: TODO
##
subread_build_genome_index () {
  sbgi_indexdir="$1"
  sbgi_indexname="$2"
  sbgi_genomefasta="$3"
  ## expecting "base" or "color"
  sbgi_baseorcs="$4"
  sbgi_fullindex="$5"

  if [ ! -f "$sbgi_genomefasta" ]; then
    s4m_error "Genome FASTA file '$sbgi_genomefasta' not found!"
    return 1
  fi
  if [ -d "$sbgi_indexdir" ]; then
    if [ ! -w "$sbgi_indexdir" ]; then
      s4m_error "Genome index directory '$sbgi_indexdir' is not writeable!"
      return 1
    fi
  else
    s4m_log "Index directory '$sbgi_indexdir' does not exist, creating it now"
    mkdir -p "$sbgi_indexdir"
    if [ $? -ne 0 ]; then
      s4m_error "Failed to create directory '$sbgi_indexdir', check permissions!"
      return 1
    fi
  fi

  ## Build a full (ungapped) or normal gapped index. Default is gapped if not specified.
  if [ ! -z "$sbgi_fullindex" ]; then
    sbgi_gapped_flag="-gapped=FALSE"
  else
    sbgi_gapped_flag=""
  fi

  call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_build_index.R" "-indexdir='$sbgi_indexdir' -indexname='$sbgi_indexname' -genomefasta='$sbgi_genomefasta' -indexspace='$sbgi_baseorcs' $sbgi_gapped_flag"
  return $?
}


##
## Check for existence of genome index. If it doesn't exist, try to build it.
## If we return without error then we assume the index has been built.
##
## Unit Test: test_funcs.sh
##  
subread_check_build_genome_index () {
  scbgi_indexdir="$1"
  scbgi_indexname="$2"
  scbgi_genomefasta="$3"
  scbgi_baseorcs="$4"
  scbgi_fullindex="$5"

  if [ -z "$scbgi_indexdir" ]; then
    s4m_error "Index directory must be provided!"
    return 1
  fi
  if [ "$scbgi_indexname" = "NA" ]; then
    scbgi_indexname=""
  fi

  ## Find index file in given index directory or exit if multiple indexes found
  if [ -z "$scbgi_indexname" ]; then
    idx=`subread_find_index_file "$scbgi_indexdir"`
    if [ -z "$idx" ]; then
      s4m_error "Could not determine genome index (zero or more than one returned); please specify target index name!"
      return 1
    fi
    ## check base-space and color-space naming
    echo "$idx" | grep b.tab > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      scbgi_indexname=`basename "$idx" .00.b.tab`
    else
      scbgi_indexname=`basename "$idx" .00.c.tab`
    fi
    ## Return the found index name
    echo "$scbgi_indexname"

  ## Target index base name was given
  else
    idxbase="$scbgi_indexname"
    if [ -f "$scbgi_indexdir/${idxbase}.00.b.tab" ]; then
      s4m_log "Found base-space index '$idxbase'" 

    elif [ -f "$scbgi_indexdir/${idxbase}.00.c.tab" ]; then
      s4m_log "Found color-space index '$idxbase'" 

    ## Index doesn't exist, create it
    else
      s4m_log "Genome index '$idxbase' not found, attempting to create it now.."
      if [ -z "$scbgi_baseorcs" ]; then
        s4m_log "Base or Color-space not specified, using default 'Base' space"
      fi
      echo "$scbgi_baseorcs" | grep -i -P "color|colour" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        scbgi_baseorcs="color"
      else
        scbgi_baseorcs="base"
      fi
      subread_build_genome_index "$scbgi_indexdir" "$scbgi_indexname" "$scbgi_genomefasta" "$scbgi_baseorcs" "$scbgi_fullindex"
      return $?
    fi
  fi
}


##
## Sort and index .BAM files
## NOTE: Not currently used!
##
## Unit Test: TODO
##
samtools_sort_index_bams () {
  ssib_bamdir="$1"
  ssib_ncpu="$2"
  if [ -z "$ssib_ncpu" ]; then
    ssib_ncpu=2
  fi

  s4m_log "Sorting and indexing .BAM files in directory '$ssib_bamdir'.."

  if [ ! -d "$ssib_bamdir" ]; then
    s4m_fatal_error "Input .BAM files directory '$ssib_bamdir' not found!"
  fi

  cd "$ssib_bamdir"
  for bam in *.[Bb][Aa][Mm]
  do
    echo "  $bam"
    fbase=`echo "$bam" | sed -r -e 's|^(.*)\.bam$|\1|i'`
    samtools sort -@ $ssib_ncpu -f "$bam" "$fbase.sorted.bam"
    ## replace non-sorted .bam with sorted file
    mv "$fbase.sorted.bam" "$bam"
    samtools index "$bam"
  done
}


##
## Align FASTQ to name-sorted BAM using R/Subread.
##
## Unit Test: TODO
##
subread_fastq2bam () {
  s4m_debug "Running R/Subread alignment (to BAM) for FASTQ files in directory '$subread_fastqdir', writing outputs to '$subread_outdir'"

  validate_targets_file "$subread_targets"
  if [ $? -ne 0 ]; then
    s4m_error "Targets file '$subread_targets' missing or has invalid format!"
    return 1
  fi

  subread_indexname=`subread_check_build_genome_index "$subread_indexdir" "$subread_indexname" "$subread_genome" "$subread_indexspace"`
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  subread_align_fastq
  return $?
}


##
## Align FASTQ to name-sorted BAM using R/Subread if update needed.
##
## Unit Test: TODO
##
subread_fastq2bam_update () {
  validate_targets_file "$subread_targets"
  if [ $? -ne 0 ]; then
    s4m_error "Targets file '$subread_targets' missing or has invalid format!"
    return 1
  fi

  sf2bu_fqfiles=`subread_get_fastq_names_from_targets "$subread_targets"`
  if [ -z "$sf2bu_fqfiles" ]; then
    s4m_error "No FASTQ files in targets!"
    return 1
  fi
  
  sf2bu_do_update="false"
  for fq in $sf2bu_fqfiles
  do
    fbase=`basename "$fq" .fastq`
    bamname=`subread_get_bamname_for_fastq "$fq" "$subread_targets"`

    s4m_debug "Got bamname=[$bamname]"

    if [ ! -f "$subread_outdir/$bamname.bam" ]; then
      s4m_log "  BAM file '$subread_outdir/$bamname.bam' does not exist, will re-run all library alignments."
      sf2bu_do_update="true"
      break
    fi
    if [ "$subread_fastqdir/$fq" -nt "$subread_outdir/$bamname.bam" ]; then
      s4m_log"  BAM file '$subread_outdir/$bamname.bam' older than input FASTQ '$fq', will re-run all library alignments."
      sf2bu_do_update="true"
      break
    fi
  done

  if [ "$sf2bu_do_update" = "true" ]; then
    s4m_log "Re-running sequence alignments.."
    subread_fastq2bam
    return $?
  else
    s4m_log "No FASTQ newer than existing BAMs, skipping sequence alignment.."
  fi
}


##
## Is any file in input list newer than a target file?
##
## Unit Test: TODO
##
subread_any_file_newer_than_target () {
  safnt_filelist="$1"
  safnt_target="$2"

  for infile in $safnt_filelist
  do
    if [ "$infile" -nt "$safnt_target" ]; then
      return 0
    fi
  done

  return 1
}


##
## Wrapper for 'subread_annotate_bams()' which will only run if any input
## .bam files are newer than output directory count tables
##
subread_annotate_bams_update () {
  sabu_inputdir="$1"
  sabu_outdir="$2"
  sabu_gtf="$3"
  sabu_targets="$4"
  sabu_features="$5"
  sabu_multimap="$6"
  sabu_ignoredup="$7"
  sabu_ncpu="$8"

  validate_targets_file "$sabu_targets"
  if [ $? -ne 0 ]; then
    s4m_error "Targets file '$sabu_targets' missing or has invalid format!"
    return 1
  fi

  ## Replace comma-separated feature types with newlines so we can iterate over them
  if [ -z "$sabu_features" ]; then
    sabu_features="gene
transcript"
  else
    sabu_features=`echo "$sabu_features" | tr "," "\n"`
  fi

  ## Get list of input BAMs
  samBamFiles=`ls "$sabu_inputdir"/*.[SsBb]am`

  do_update="false"
  for feature in $sabu_features
  do
    if [ "$feature" = "gene" -o "$feature" = "transcript" ]; then
      ## If raw feature count summary table exists and is older than any input SAM/BAM file,
      ## re-run the annotation
      if [ -f "$sabu_outdir/${feature}_count_frags.txt" ]; then
        if subread_any_file_newer_than_target "$samBamFiles" "$sabu_outdir/${feature}_count_frags.txt"; then
          do_update="true"
        fi
      ## Output not found, rerun
      else
        do_update="true"
      fi
    ## Custom feature type - output files are named differently
    else
      if [ -f "$sabu_outdir/custom_count_frags_${feature}.txt" ]; then
        if subread_any_file_newer_than_target "$samBamFiles" "$sabu_outdir/custom_count_frags_${feature}.txt"; then
          do_update="true"
        fi
      ## Output not found, rerun
      else
        do_update="true"
      fi
    fi
  done

  if [ "$do_update" = "false" ]; then
    s4m_log "No SAM/BAM files newer than existing annotated counts, skipping annotation/counts.."
    return
  else
    s4m_log "Found SAM/BAM files newer than existing annotated counts (or no outputs found), re-running.."
  fi

  subread_annotate_bams "$sabu_inputdir" "$sabu_outdir" "$sabu_gtf" "$sabu_targets" "$sabu_features" "$sabu_multimap" "$sabu_ignoredup" "$sabu_ncpu"
  return $?
}


## Annotate BAMs against user-supplied GTF annnotation file.
## If input BAMs are not name-sorted, subread will re-sort by name in order to
## have paired end reads side-by-side.
##
## NOTE: Assumes either single end, or paired end - not sure what happens if mixed.
##
## Unit Test: TODO
##
subread_annotate_bams () {
  sab_inputdir="$1"
  sab_outdir="$2"
  sab_gtf="$3"
  sab_targets="$4"
  sab_features="$5"
  sab_multimap="$6"
  sab_ignoredup="$7"
  sab_ncpu="$8"

  validate_targets_file "$sab_targets"
  if [ $? -ne 0 ]; then
    s4m_error "Targets file '$sab_targets' missing or has invalid format!"
    return 1
  fi

  ## Set optional flags as required
  optflags=""
  if [ ! -z $sab_ncpu ]; then
    optflags="$optflags -ncpu=$sab_ncpu"
  fi
  if [ ! -z "$sab_multimap" ]; then
    optflags="$optflags -multimap=TRUE"
  fi
  if [ ! -z "$sab_ignoredup" -a "$sab_ignoredup" = "false" ]; then
    optflags="$optflags -ignoredup=FALSE"
  fi
  ## Work out from targets file if we're dealing with paired end or not
  ## If targets.txt file contains non-blank "ReadFile2" entries then is paired end
  targetcol=`head -1 "$sab_targets" | tr "\t" "\n" | grep -n ReadFile2 | cut -d":" -f 1`
  if [ ! -z "$targetcol" ]; then
    npaired=`cut -f $targetcol "$sab_targets" | grep -v ReadFile2 | grep -P -v "^$" | wc -l`
    if [ $npaired -gt 0 ]; then
      optflags="$optflags -ispaired=TRUE"
    fi
  fi

  ## Replace comma-separated feature types with newlines so we can iterate over them
  if [ -z "$sab_features" ]; then
    ## By default, annotate to "gene" and "transcript" levels in GTF file.
    sab_features="gene
transcript"
  else
    sab_features=`echo "$sab_features" | tr "," "\n"`
  fi

  for feature in $sab_features
  do
    if [ "$feature" = "gene" -o "$feature" = "transcript" ]; then
      ## Summarise feature counts against given GTF file
      call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_feature_count_${feature}.R" "-inputdir='$sab_inputdir' -outdir='$sab_outdir' -gtf='$sab_gtf' -targets='$sab_targets' $optflags"
    else
      ## Summarise feature counts against given GTF file - with CUSTOM target feature type (e.g. "miRNA")
      ## Default featurelevel is "gene_id" but can be overriden with "-featurelevel" flag
      call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_feature_count_custom.R" "-inputdir='$sab_inputdir' -outdir='$sab_outdir' -gtf='$sab_gtf' -featuretype='$feature' -targets='$sab_targets' $optflags"
    fi
    ret=$?
    if [ $ret -ne 0 ]; then
      return $ret
    fi
  done
  return $?
}


## Sample expression density plot (overlay by pheno groups)
##
## Unit Test: TODO
##
subread_qc_density () {
  sqd_intable="$1"
  sqd_outdir="$2"
  sqd_targets="$3"
  sqd_plotname="$4"

  ## Script expects a 2 column table of "sample [TAB] pheno", without headers, so craft a temporary one
  ## NOTE: Input targets file 2nd column MUST correspond to a phenotype group e.g. CellType or SampleType!
  samplepheno="$sqd_outdir/sample_pheno_$$.txt"
  sed '1d' "$sqd_targets" | cut -f 1-2 > "$samplepheno"

  extrasamplepheno=""
  ## If there are extra columns (after the first two, non-ReadFile[12]) which contain non-NULL values,
  ## use them as extra groups to create additional PCA plots for
  extracols=`head -1 "$sqd_targets" | tr "\t" "\n" | grep -n "" | tail -n+3 | grep -v "ReadFile"`
  if [ ! -z "$extracols" ]; then
    for col in $extracols
    do
      colindex=`echo "$col" | cut -d':' -f 1`
      factorname=`echo "$col" | cut -d':' -f 2`
      sed '1d' "$sqd_targets" | cut -f 1,$colindex > "${samplepheno}.$factorname"
      extrasamplepheno="$extrasamplepheno
${samplepheno}.$factorname"
    done
  fi

  ## Title underscores become spaces
  ## NOTE: Not used due to args issues with spaces
  #plottitle=`echo "$sqd_plotname" | sed 's|_| |g'`
  ## File name spaces become underscores
  plotname=`echo "$sqd_plotname" | sed 's| |_|g'`

  ## Data should be log2 for consistency with other plots.
  ## For subread output tables, "log2" appears in file names if data is log'd, otherwise not.
  log2flag=""
  if (echo "$sqd_intable" | grep -q -i log2); then
    log2flag="-islog2=TRUE"
  fi

  s4m_log "Creating Default density plot.."
  call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_density.R" "-input='$sqd_intable' -output='$sqd_outdir/density_$plotname.png' -phenofile='$samplepheno' -title='$plotname' $log2flag"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  rm -f "$samplepheno"

  extraret=0
  if [ ! -z "$extrasamplepheno" ]; then
    s4m_log "Creating additional density plots.."
    for extrapheno in $extrasamplepheno
    do
      factorname=`echo "$extrapheno" | tr "." "\n" | tail -1 | sed -r -e 's| |_|g'`
      s4m_log "  Density - $factorname"
      call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_density.R" "-input='$sqd_intable' -output='$sqd_outdir/density_${plotname}_${factorname}.png' -phenofile='$extrapheno' -title='$plotname-[$factorname]' $log2flag"
      ret=$?
      if [ $ret -ne 0 ]; then
        extraret=$ret
      else
        rm -f "$extrapheno"
      fi
    done
  fi

  if [ $extraret -ne 0 ]; then
    return $extraret
  fi
  return 0
}


## Sample expression PCA plot
##
## Unit Test: TODO
##
subread_qc_PCA () {
  sqp_intable="$1"
  sqp_outdir="$2"
  sqp_targets="$3"
  sqp_pcaname="$4"

  ## Script expects a 2 column table of "sample [TAB] pheno", without headers, so craft a temporary one
  ## NOTE: Input targets file 2nd column MUST correspond to a phenotype group e.g. CellType or SampleType!
  samplepheno="$sqp_outdir/sample_pheno_$$.txt"
  sed '1d' "$sqp_targets" | cut -f 1-2 > "$samplepheno"

  extrasamplepheno=""
  ## If there are extra columns (after the first two, non-ReadFile[12]) which contain non-NULL values,
  ## use them as extra groups to create additional PCA plots for
  extracols=`head -1 "$sqp_targets" | tr "\t" "\n" | grep -n "" | tail -n+3 | grep -v "ReadFile"`
  if [ ! -z "$extracols" ]; then
    for col in $extracols
    do
      colindex=`echo "$col" | cut -d':' -f 1`
      factorname=`echo "$col" | cut -d':' -f 2`
      sed '1d' "$sqp_targets" | cut -f 1,$colindex > "${samplepheno}.$factorname"
      extrasamplepheno="$extrasamplepheno
${samplepheno}.$factorname"
    done
  fi

  ## Title underscores become spaces
  ## NOTE: Not used due to args issues with spaces
  #pcatitle=`echo "$sqp_pcaname" | sed 's|_| |g'`
  ## File name spaces become underscores
  pcaname=`echo "$sqp_pcaname" | sed 's| |_|g'`

  ## Data should be log2 for consistency with other plots.
  ## For subread output tables, "log2" appears in file names if data is log'd, otherwise not.
  log2flag=""
  if (echo "$sqp_intable" | grep -q -i -v log2); then
    log2flag="-dolog2=TRUE"
  fi

  s4m_log "Creating Default PCAs.."
  call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_PCA.R" "-input='$sqp_intable' -output='$sqp_outdir/PCA_$pcaname.png' -phenofile='$samplepheno' -title='$pcaname' $log2flag"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  rm -f "$samplepheno"

  extraret=0
  if [ ! -z "$extrasamplepheno" ]; then
    s4m_log "Creating additional PCAs.."
    for extrapheno in $extrasamplepheno
    do
      factorname=`echo "$extrapheno" | tr "." "\n" | tail -1 | sed -r -e 's| |_|g'`
      s4m_log "  PCA - $factorname"
      call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_PCA.R" "-input='$sqp_intable' -output='$sqp_outdir/PCA_${pcaname}_${factorname}.png' -phenofile='$extrapheno' -title='$pcaname-[$factorname]' $log2flag"
      ret=$?
      if [ $ret -ne 0 ]; then
        extraret=$ret
      else
        rm -f "$extrapheno"
      fi
    done
  fi

  ## Set sample column header to "SampleID" in PCA *sample_data.tsv outputs
  for pcatsv in $sqp_outdir/*_sample_data.tsv
  do
    sed -i -r -e '1 s|^\t|SampleID\t|' "$pcatsv"
  done
  if [ $extraret -ne 0 ]; then
    return $extraret
  fi
  return 0
}


## Sample expression HC (Hierarchical Cluster)
##
## Unit Test: TODO
##
subread_qc_HC () {
  sqh_intable="$1"
  sqh_outdir="$2"
  sqh_targets="$3"
  sqh_hcname="$4"

  ## Script expects a 2 column table of "sample [TAB] pheno", without headers, so craft a temporary one
  ## NOTE: Input targets file 2nd column MUST correspond to a phenotype group e.g. CellType or SampleType!
  ##
  samplepheno="$sqh_outdir/sample_pheno_$$.txt"
  sed '1d' "$sqh_targets" | cut -f 1-2 > "$samplepheno"

  ## Title underscores become spaces
  ## NOTE: Not used due to args issues with spaces
  #hctitle=`echo "$sqh_hcname" | sed 's|_| |g'`
  ## File name spaces become underscores
  hcname=`echo "$sqh_hcname" | sed 's| |_|g'`

  ## Script needs to know if data is log2 or not to consistently calculate CoV for the HC distance metric.
  ## For subread output tables, "log2" appears in file names if data is log'd otherwise not.
  log2flag=""
  if (echo "$sqh_intable" | grep -q -i log2); then
    log2flag="-islog2=TRUE"
  fi

  call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_HC.R" "-input='$sqh_intable' -output='$sqh_outdir/HC_$hcname.png' -phenofile='$samplepheno' -title='$hcname' $log2flag"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  rm -f "$samplepheno"
  return 0
}


## Sample expression boxplot
##
## Unit Test: TODO
##
subread_qc_boxplot () {
  sqb_intable="$1"
  sqb_outdir="$2"
  sqb_targets="$3"
  sqb_plotname="$4"

  ## Script expects a 2 column table of "sample [TAB] pheno", without headers, so craft a temporary one
  ## NOTE: Input targets file 2nd column MUST correspond to a phenotype group e.g. CellType or SampleType!
  ##
  samplepheno="$sqb_outdir/sample_pheno_$$.txt"
  sed '1d' "$sqb_targets" | cut -f 1-2 > "$samplepheno"

  ## Title underscores become spaces
  ## NOTE: Not used due to args issues with spaces
  #plottitle=`echo "$sqb_plotname" | sed 's|_| |g'`
  ## File name spaces become underscores
  plotname=`echo "$sqb_plotname" | sed 's| |_|g'`

  ## If data isn't log2, log it before plotting boxplots (to be consistent where other plots are using log2 data)
  log2flag="-dolog2=TRUE"
  if (echo "$sqb_intable" | grep -q -i log2); then
    log2flag=""
  fi

  call_R "$subread_R" "$S4M_THIS_MODULE/scripts/subread_boxplot.R" "-input='$sqb_intable' -output='$sqb_outdir/boxplot_$plotname.png' -phenofile='$samplepheno' -title='$plotname' $log2flag"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
  rm -f "$samplepheno"
  return 0
}


## Post-count table QC - PCA, HC plots etc.
##
## Unit Test: TODO
##
subread_post_count_qc () {
  spcq_intable="$1"
  spcq_outdir="$2"
  spcq_targets="$3"
  ## Used as unique part in output QC file name and in plot titles etc.
  spcq_qcname="$4"

  if [ ! -f "$spcq_intable" ]; then
    s4m_error "QC input data table '$spcq_intable' not found!"
    return 1
  fi
  if [ ! -d "$spcq_outdir" ]; then
    s4m_error "QC output directory '$spcq_outdir' not found!"
    return 1
  fi

  if [ -z "$spcq_qcname" ]; then
    spcq_qcname="default"
  fi

  ## Boxplots
  subread_qc_boxplot "$spcq_intable" "$spcq_outdir" "$spcq_targets" "$spcq_qcname"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## Density plots
  subread_qc_density "$spcq_intable" "$spcq_outdir" "$spcq_targets" "$spcq_qcname"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## PCA
  subread_qc_PCA "$spcq_intable" "$spcq_outdir" "$spcq_targets" "$spcq_qcname"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi

  ## HC
  subread_qc_HC "$spcq_intable" "$spcq_outdir" "$spcq_targets" "$spcq_qcname"
  ret=$?
  if [ $ret -ne 0 ]; then
    return $ret
  fi
}


## Handler for "fastqc" command
fastqc_handler () {
  ## Load env that has fastqc
  if ! miniconda_activate; then
    return 1
  fi
  if [ ! -z "$subread_dryrun" -o "$subread_dryrun" = "true" ]; then
    SUBREAD_DRYRUN="TRUE"; export SUBREAD_DRYRUN
    s4m_debug "Dry-run mode ENABLED, no actions will be executed"
  fi
  ## NCPUs given, attempt multi-process execution
  if [ ! -z "$subread_ncpu" ]; then
    subread_fastqc_multi "$subread_fastqdir" "$subread_outdir" "$subread_ncpu" "$subread_targets" "$subread_updatemode"
    ret=$?
  else
    subread_fastqc "$subread_fastqdir" "$subread_outdir" "$subread_targets" "$subread_updatemode"
    ret=$?
  fi
  return $ret
}


## Handler for "fastq2bam" command
fastq2bam_handler () {
  if miniconda_activate; then
    ## If asked to "update only" then only execute if have FASTQ newer than BAM
    if [ ! -z "$subread_updatemode" ]; then
      subread_fastq2bam_update
      ret=$?
    else
      subread_fastq2bam
      ret=$?
    fi
    return $ret
  fi
  return 1
}


## Handler for "subreadindex" command
subreadindex_handler () {
  if miniconda_activate; then
    subread_check_build_genome_index "$subread_indexdir" "$subread_indexname" "$subread_genome" "$subread_indexspace" "$subread_full"
    return $?
  fi
  return 1
}


## Handler for "annotatebams" command
annotatebams_handler () {
  if miniconda_activate; then
    ## If asked to "update only" then only execute if have SAM/BAM files newer than existing counts
    if [ ! -z "$subread_updatemode" ]; then
      subread_annotate_bams_update "$subread_inputdir" "$subread_outdir" "$subread_gtf" "$subread_targets" "$subread_features" "$subread_multimap" "$subread_ignoredup" "$subread_ncpu"
      ret=$?
    else
      subread_annotate_bams "$subread_inputdir" "$subread_outdir" "$subread_gtf" "$subread_targets" "$subread_features" "$subread_multimap" "$subread_ignoredup" "$subread_ncpu"
      ret=$?
    fi
    return $ret
  fi
  return 1
}


## Handler for "qc" command
qc_handler () {
  if miniconda_activate; then
    subread_post_count_qc "$subread_input" "$subread_output" "$subread_targets" "$subread_qcname"
    return $?
  fi
  return 1
}


main () {
  ## Load "call_R()" function
  s4m_import "Rutil/Rutil.sh"
  ## Import miniconda (for automatic dependency management)
  s4m_import miniconda/lib.sh

  ### FASTQC ###
  s4m_route "fastqc" "fastqc_handler"
  ### FASTQ to aligned BAM ###
  s4m_route "fastq2bam" "fastq2bam_handler"
  ### Build subread index only ###
  s4m_route "subreadindex" "subreadindex_handler"
  ### Annotate BAMs against GTF ###
  s4m_route "annotatebams" "annotatebams_handler"
  ### Post-counts QC ###
  s4m_route "qc" "qc_handler"
}

### START ###
main
