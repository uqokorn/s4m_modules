#/bin/sh
##==========================================================================
## Module: normalize
## Author: Othmar Korn
##
## Apply common normalization methods to count or expression data.
##==========================================================================

# Include S4M functions 
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

##==========================================================================

## Produce CPM counts and print Voom mean-variance plot
do_voom_cpm () {
  dvc_inputfile="$1"
  dvc_outputfile="$2"
  dvc_targets="$3"
  dvc_phenotarget="$4"
  dvc_normmethod="$5"
  dvc_plotmeanvariance="$6"
  dvc_postnorm="$7"

  dvc_extraflags=""
  if [ ! -z "$dvc_phenotarget" ]; then
    dvc_extraflags="$dvc_extraflags -pheno_column=$dvc_phenotarget"
  fi
  if [ ! -z "$dvc_normmethod" ]; then
    dvc_extraflags="$dvc_extraflags -normmethod=$dvc_normmethod"
  fi
  if [ ! -z "$dvc_postnorm" ]; then
    dvc_extraflags="$dvc_extraflags -postnorm=$dvc_postnorm"
  fi
  if [ ! -z "$dvc_plotmeanvariance" ]; then
    dvc_extraflags="$dvc_extraflags -plotmeanvariance=$dvc_plotmeanvariance"
  else
    dvc_extraflags="$dvc_extraflags -plotmeanvariance=TRUE"
  fi

  call_R $normalize_R "$S4M_THIS_MODULE/scripts/voom.R" "-input='$dvc_inputfile' -output='$dvc_outputfile' -targets='$dvc_targets' $dvc_extraflags"
  return $?
}

## Produce RPKM counts and print Voom mean-variance plot
do_voom_rpkm () {
  dvr_inputfile="$1"
  dvr_outputfile="$2"
  dvr_targets="$3"
  dvr_genelengths="$4"
  dvr_phenotarget="$5"
  dvr_normmethod="$6"
  dvr_plotmeanvariance="$7"
  dvr_postnorm="$8"

  if [ ! -f "$dvr_genelengths" ]; then
    s4m_error "Gene lengths file must be supplied for Voom RPKM output!"
    return 1
  fi
  dvr_extraflags="-countformat=RPKM "
  if [ ! -z "$dvr_phenotarget" ]; then
    dvr_extraflags="$dvr_extraflags -pheno_column=$dvr_phenotarget"
  fi
  if [ ! -z "$dvr_normmethod" ]; then
    dvr_extraflags="$dvr_extraflags -normmethod=$dvr_normmethod"
  fi
  if [ ! -z "$dvr_postnorm" ]; then
    dvr_extraflags="$dvr_extraflags -postnorm=$dvr_postnorm"
  fi 
  if [ ! -z "$dvr_plotmeanvariance" ]; then
    dvr_extraflags="$dvr_extraflags -plotmeanvariance=$dvr_plotmeanvariance"
  else
    dvr_extraflags="$dvr_extraflags -plotmeanvariance=TRUE"
  fi

  call_R $normalize_R "$S4M_THIS_MODULE/scripts/voom.R" "-input='$dvr_inputfile' -output='$dvr_outputfile' -targets='$dvr_targets' -genelengths='$dvr_genelengths' $dvr_extraflags"
  return $?
}

## Handler for the "voom" command
voom_handler () {
  if [ "$countformat" = "RPKM" ]; then
    do_voom_rpkm "$inputfile" "$outputfile" "$targetsfile" "$genelengths" "$phenotarget" "$normmethod" "$plotmeanvariance" "$postnorm"
    ret=$?
  elif [ "$countformat" = "CPM" -o -z "$countformat" ]; then
    do_voom_cpm "$inputfile" "$outputfile" "$targetsfile" "$phenotarget" "$normmethod" "$plotmeanvariance" "$postnorm"
    ret=$?
  fi
  return $ret
}


main () {
  inputfile="$normalize_inputfile"
  outputfile="$normalize_outputfile"
  targetsfile="$normalize_targets"
  normmethod="$normalize_normmethod"
  plotmeanvariance="$normalize_plotmeanvariance"
  postnorm="$normalize_postnorm"
  phenotarget="$normalize_phenotarget"
  countformat="$normalize_countformat"
  genelengths="$normalize_genelengths"
  
  s4m_import "Rutil/Rutil.sh"
  s4m_import "miniconda/lib.sh"

  ## Activate R environment
  if ! miniconda_activate; then
    exit 1
  fi

  ## Voom mean-variance plotting and pre and/or post scaling of log2 CPM/RPKM counts
  s4m_route "voom" "voom_handler"
}

### START ###
main

