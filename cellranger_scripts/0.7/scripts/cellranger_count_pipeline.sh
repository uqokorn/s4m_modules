#!/bin/sh
DSDIR="$1"
TRANSCRIPTOME_DIR="$2"
TARGETS="$3"
NCPU="$4"
## Without "GB" ending
MEMORY="$5"

Usage () {
  echo "$0 <dataset_dir> <transcriptome_dir> <targets_file> <NCPU> <MEM>"; echo
}

if [ -z "$DSDIR" -o -z "$TRANSCRIPTOME_DIR" -o -z "$TARGETS" -o -z "$NCPU" -o -z "$MEMORY" ]; then
  Usage
  exit 1
fi

. inc/funcs.sh

PIPELINE_TEMPLATE=cellranger_count.pipeline.template
genome=`basename "$TRANSCRIPTOME_DIR"`
PROCDIR="processed.$genome"
OUTDIR="$DSDIR/source/$PROCDIR"
mkdir -p "$OUTDIR"

template_basename=`basename "$PIPELINE_TEMPLATE" .template`

instance_template=`get_instance_template "$PIPELINE_TEMPLATE" \
  %DATASET_DIR%="$DSDIR" %TRANSCRIPTOME_DIR%="$TRANSCRIPTOME_DIR" \
  %PROCESSED_OUTDIR%="$PROCDIR" \
  %TARGETS%="$TARGETS" %NCPU%=$NCPU %MEMORY%=$MEMORY`

## Write instance pipeline file
echo "$instance_template" > "$OUTDIR/$template_basename"

#export S4M_DEBUG=TRUE
## Run token-replaced pipeline
s4m pipeline::pipeline -f "$OUTDIR/$template_basename"

