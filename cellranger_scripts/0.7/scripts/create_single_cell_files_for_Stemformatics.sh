#!/bin/sh
## Generate files needed for Stemformatics single cell viz

DSDIR="$1"
GENOME="$2"
## Final QC targets (row count matches GCT cell count)
QC_TARGETS="$3"
POSTQC_DIR="$4"


Usage () {
  echo "$0 <dsdir> <genome> <qc_targets> [<post_qc_dir>]

  Note: If not given, default <post_qc_dir> chosen will be (in order of presence):

    'scanpy_residuals.filtered'
    'scanpy_residuals'
    'scanpy_norm.filtered'
    'scanpy_norm'
"
}

if [ -z "$DSDIR" -o -z "$GENOME" -o -z "$QC_TARGETS" ]; then
  Usage
  exit 1
fi

DSDIR=`readlink -f $DSDIR`
dsid=`basename "$DSDIR"`
procdir="$DSDIR/source/processed.$GENOME"

if [ -z "$POSTQC_DIR" ]; then
  if [ -d "$procdir/post_qc/scanpy_residuals.filtered" ]; then
    POSTQC_DIR="scanpy_residuals.filtered"
  elif [ -d "$procdir/post_qc/scanpy_residuals" ]; then
    POSTQC_DIR="scanpy_residuals"
  elif [ -d "$procdir/post_qc/scanpy_norm.filtered" ]; then
    POSTQC_DIR="scanpy_norm.filtered"
  elif [ -d "$procdir/post_qc/scanpy_norm" ]; then
    POSTQC_DIR="scanpy_norm"
  else
    echo "Error: No post_qc subfolder given and none were found automatically.
  Check '$procdir/post_qc' validity."
    exit 1
  fi
fi

postqc_dir="$procdir/post_qc/$POSTQC_DIR"

if [ ! -d "$postqc_dir" ]; then
  echo "Error: QC directory '$postqc_dir' not found!
" 1>&2
  exit 1
fi
if [ ! -f "$QC_TARGETS" ]; then
  echo "Error: Targets file '$QC_TARGETS' not found!
" 1>&2
  exit 1
fi

## Need to have files:
## 	pca.txt (alias for pca.tsv) [DONE]
##	umap.txt (alias for umap.tsv) [DONE]
##	samples.txt
##	sampleGroupItems.txt
##	clusters.txt
##	clusterItems.txt

### pca.txt and umap.txt ###

cd "$postqc_dir"
if [ ! -L pca.txt ]; then
  if [ ! -f pca.tsv ]; then
    echo "Error: PCA data file '$postqc_dir/pca.tsv' not found!
" 1>&2
    exit 1
  fi
  ln -s pca.tsv pca.txt
else
  echo "Info: Symlink pca.txt already exists, skipping.."
fi
if [ ! -L umap.txt ]; then
  if [ ! -f umap.tsv ]; then
    echo "Error: UMAP data file '$postqc_dir/umap.tsv' not found!
" 1>&2
    exit 1
  fi
  ln -s umap.tsv umap.txt
else
  echo "Info: Symlink umap.txt already exists, skipping.."
fi
cd $OLDPWD

### samples.txt ###
echo "Writing samples.txt.."
## Extract sample_id and sample_group from targets - but must retain same order as pca.txt
/bin/echo -e "sample_id\tgroup" > "$postqc_dir/samples.txt"
tail -n+2 "$postqc_dir/pca.txt" | cut -f 1 | while read cellid
do
  grep -P "^$cellid\t" "$QC_TARGETS" | head -1 | cut -f 1,2 >> "$postqc_dir/samples.txt"
done
echo "  done"

### sampleGroupItems.txt ###
echo "Writing sampleGroupItems.txt.."
sg_items=`tail -n+2 "$QC_TARGETS" | cut -f 2 | sort -u | tr "\n" "," | sed -r -e 's|\,$||g'`
/bin/echo -e "sampleGroup\titems" > "$postqc_dir/sampleGroupItems.txt"
/bin/echo -e "group\t$sg_items" >> "$postqc_dir/sampleGroupItems.txt"
echo "  done"

### clusters.txt ###
echo "Writing clusters.txt.."
echo "$POSTQC_DIR" | grep -i filtered > /dev/null 2>&1
## If filtered cells, remove "QC_is_cell" metadata
if [ $? -eq 0 ]; then
  qc_cols=`head -1 "$QC_TARGETS" | tr "\t" "\n" | grep -nP "^QC_" | grep -v "QC_is_cell" | cut -d':' -f 1`
else
  qc_cols=`head -1 "$QC_TARGETS" | tr "\t" "\n" | grep -nP "^QC_" | cut -d':' -f 1`
fi
cut_cols=`echo "$qc_cols" | tr "\n" "," | sed -r -e 's|\,$||'`
qc_names=`head -1 "$QC_TARGETS" | cut -f $cut_cols`
/bin/echo -e "$qc_names" > "$postqc_dir/clusters.txt"
## retain same cell order as pca.txt
tail -n+2 "$postqc_dir/pca.txt" | cut -f 1 | while read cellid
do
  grep -P "^$cellid\t" "$QC_TARGETS" | head -1 | cut -f 1,$cut_cols >> "$postqc_dir/clusters.txt"
done
echo "  done"

### clusterGroupItems.txt ###
echo "Writing clusterItems.txt.."
/bin/echo -e "cluster\titems" > "$postqc_dir/clusterItems.txt"
## one row per QC point
for qc_ind in $qc_cols
do
  qc_name=`head -1 "$QC_TARGETS" | cut -f $qc_ind`
  qc_vals=`tail -n+2 "$QC_TARGETS" | cut -f $qc_ind | sort -V -u | tr "\n" "," | sed -r -e 's|\,$||g'`
  /bin/echo -e "$qc_name\t$qc_vals" >> "$postqc_dir/clusterItems.txt"
done
echo "  done"

echo "All Done"; echo

