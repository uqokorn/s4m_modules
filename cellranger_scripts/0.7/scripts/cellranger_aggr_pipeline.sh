#!/bin/sh
PROCDIR="$1"
TARGETS="$2"
NCPU="$3"
## Without "GB" ending
MEMORY="$4"

Usage () {
  echo "$0 <processed_dir> <targets_file> <NCPU> <MEM>"; echo
}

if [ -z "$PROCDIR" -o -z "$TARGETS" -o -z "$NCPU" -o -z "$MEMORY" ]; then
  Usage
  exit 1
fi
if [ ! -d "$PROCDIR" ]; then
  echo "Error: $0: Processed dir '$PROCDIR' does not exist!
" 1>&2
  exit 1
fi

. inc/funcs.sh

PIPELINE_TEMPLATE=cellranger_aggr.pipeline.template

template_basename=`basename "$PIPELINE_TEMPLATE" .template`

OUTDIR="$PROCDIR"

instance_template=`get_instance_template "$PIPELINE_TEMPLATE" \
  %PROCESSED_OUTDIR%="$PROCDIR" \
  %TARGETS%="$TARGETS" %NCPU%=$NCPU %MEMORY%=$MEMORY`

## Write instance pipeline file
echo "$instance_template" > "$OUTDIR/$template_basename"

#export S4M_DEBUG=TRUE
## Run token-replaced pipeline
s4m pipeline::pipeline -f "$OUTDIR/$template_basename"

