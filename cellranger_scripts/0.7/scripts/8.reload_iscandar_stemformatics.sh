#!/bin/sh

DSID=XXXX
S4MHOST=%HOST%
FOLDER=`find ../ -type d -name scanpy_residuals.filtered`

export S4M_DEBUG=TRUE

s4m stemformatics_web::upload-reports \
  -t iscandar \
  -r \
  -f "$FOLDER" \
  -h "$S4MHOST" \
  -D "$DSID"

if [ $? -ne 0 ]; then
  echo "Error: 'stemformatics_web::upload-reports' returned non-zero status!
" 1>&2
  exit 1
else
  echo; echo "Done"
fi
