
##
## Get parsed / var replaced version of template (taking multiple %VAR%=<value> args
## for token replacement).
##
get_instance_template () {
  template="$1"
  shift

  sed_cmd="cat $template | sed -r "
  sed_opts=""

  while [ ! -z "$1" ];
  do
    kv="$1"
    key=`echo "$kv" | cut -d'=' -f 1`
    val=`echo "$kv" | cut -d'=' -f 2`
    sed_opts="$sed_opts -e \"s|$key|$val|g\""
    shift
  done

  ## DEBUG
  #echo "[eval $sed_cmd $sed_opts]"

  eval "$sed_cmd $sed_opts"
}

