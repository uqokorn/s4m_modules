
2019-05-05
O.Korn

INSTALLATION / SETUP:
=====================

1. Install Cell Ranger v3.0.1 and bcl2fastq v2.20

2. Install module "scpipe_scripts" (i.e. run "s4m install scpipe_scripts") - we use the same underlying
   R environment for things like t-SNE, PCA and basic post-Cell Ranger QC/single cell normalisation

3. Build human GRCh38 Ensembl v91 reference for Cell Ranger:

  ENSVER=91
  wget ftp://ftp.ensembl.org/pub/release-$ENSVER/fasta/homo_sapiens/dna/Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
  gunzip Homo_sapiens.GRCh38.dna.primary_assembly.fa.gz
  # Copy DNA FASTA to standard location ..

  ## Get full genes GTF
  wget ftp://ftp.ensembl.org/pub/release-$ENSVER/gtf/homo_sapiens/Homo_sapiens.GRCh38.$ENSVER.gtf.gz
  gunzip Homo_sapiens.GRCh38.$ENSVER.gtf.gz
  # Copy GTF to standard location ..

  ## *** the following commands should be run via PBS etc with appropriate CPU/mem ***

  ## Create the "filtered" GTF that removes things like mt-RNA
  cellranger mkgtf /input/path/to/Homo_sapiens.GRCh38.$ENSVER.gtf /output/path/to/Homo_sapiens.GRCh38.$ENSVER.filtered.gtf \
		   --attribute=gene_biotype:protein_coding \
		   --attribute=gene_biotype:lincRNA \
		   --attribute=gene_biotype:antisense \
		   --attribute=gene_biotype:IG_LV_gene \
		   --attribute=gene_biotype:IG_V_gene \
		   --attribute=gene_biotype:IG_V_pseudogene \
		   --attribute=gene_biotype:IG_D_gene \
		   --attribute=gene_biotype:IG_J_gene \
		   --attribute=gene_biotype:IG_J_pseudogene \
		   --attribute=gene_biotype:IG_C_gene \
		   --attribute=gene_biotype:IG_C_pseudogene \
		   --attribute=gene_biotype:TR_V_gene \
		   --attribute=gene_biotype:TR_V_pseudogene \
		   --attribute=gene_biotype:TR_D_gene \
		   --attribute=gene_biotype:TR_J_gene \
		   --attribute=gene_biotype:TR_J_pseudogene \
		   --attribute=gene_biotype:TR_C_gene

  ## Make genome indexes for STAR (used by Cell Ranger)

  ENSVER=91
  CELRANGER_VER=3.0.1
  OUTDIR=/output/path/to/cellranger_indexes/GRCh38.$ENSVER

  cd $OUTDIR

  ## Build gene-filtered reference
  cellranger mkref --genome=GRCh38.$ENSVER.filtered \
		   --fasta=/path/to/Homo_sapiens.GRCh38.dna.primary_assembly.fa \
		   --genes=/path/to/Homo_sapiens.GRCh38.$ENSVER.filtered.gtf \
		   --nthreads=$NCPU \
		   --memgb=$RAM \
		   --ref-version=$CELLRANGER_VER
  mv Log.out Log_GRCh38.$ENSVER.filtered.out


  ## Build unfiltered (full) reference
  cellranger mkref --genome=GRCh38.$ENSVER \
		   --fasta=/path/to/Homo_sapiens.GRCh38.dna.primary_assembly.fa \
		   --genes=/path/to/Homo_sapiens.GRCh38.$ENSVER.gtf \
		   --nthreads=$NCPU \
		   --memgb=$RAM \
		   --ref-version=$CELLRANGER_VER
  mv Log.out Log_GRCh38.$ENSVER.out



DATA PROCESSING:
================

1. Extract (mkfastq) or otherwise download/prepare Cell Ranger FASTA libraries for experiment.
   This may involve one or more samples / run IDs e.g.

  CCJYFANXX
  CCG6WANXX
  CCL9HANXX

  ## NOTE: FASTQ R1/R1 and index outputs in "outs/fastq_path" for example:

  $ tree CCJYFANXX/outputs/fastq_path

  CCJYFANXX/outs/fastq_path/
  |-- SI-GA-A6_1
  |   |-- SI-GA-A6_1_S17_L006_I1_001.fastq.gz
  |   |-- SI-GA-A6_1_S17_L006_R1_001.fastq.gz
  |   `-- SI-GA-A6_1_S17_L006_R2_001.fastq.gz
  |-- SI-GA-A6_2
  |   |-- SI-GA-A6_2_S18_L006_I1_001.fastq.gz
  |   |-- SI-GA-A6_2_S18_L006_R1_001.fastq.gz
  |   `-- SI-GA-A6_2_S18_L006_R2_001.fastq.gz
  |-- SI-GA-A6_3
  |   |-- SI-GA-A6_3_S19_L006_I1_001.fastq.gz
  |   |-- SI-GA-A6_3_S19_L006_R1_001.fastq.gz
  |   `-- SI-GA-A6_3_S19_L006_R2_001.fastq.gz
  `-- SI-GA-A6_4
      |-- SI-GA-A6_4_S20_L006_I1_001.fastq.gz
      |-- SI-GA-A6_4_S20_L006_R1_001.fastq.gz
      `-- SI-GA-A6_4_S20_L006_R2_001.fastq.gz



2. Run "cellranger count" for each input library, using filtered and unfiltered annotations
   i.e. two different outputs so we can compare things like mt-RNA vs non-mt-RNA contaminated QC.

  ## MULTIPLE LIBRARIES (REPEAT FOR EACH INPUT SAMPLE/RUN):

  OUTDIR=/path/to/cellranger_counts.filtered
  cd $OUTDIR
  ## FILTERED GTF
  transcriptome=/path/to/cellranger_indexes/GRCh38.$ENSVER.filtered
  cellranger count --jobmode=local --localmem="$MEM" --localcores="$NCPU" --nosecondary --disable-ui --id="$sid" --sample="$samples" --fastqs="$fastqdir" --transcriptome="$transcriptome"

  ## UN-FILTERED GTF
  OUTDIR=/path/to/cellranger_counts
  cd $OUTDIR
  transcriptome=/path/to/cellranger_indexes/GRCh38.$ENSVER
  cellranger count --jobmode=local --localmem="$MEM" --localcores="$NCPU" --nosecondary --disable-ui --id="$sid" --sample="$samples" --fastqs="$fastqdir" --transcriptome="$transcriptome"

  # Where:
  # $sid = sample/run ID e.g. CCJYFANXX
  # $samples = SI-GA-G1_01,SI-GA-G1_02,SI-GA-G1_03,SI-GA-G1_04 (continuing our example - use all available FASTQ data/lanes)
  # $fastqdir = /path/to/CCJYFANXX/outs/fastq_path
  # $transcriptome = Cell Ranger reference e.g. /path/to/cellranger_indexes/GRCh38.$ENSVER  or  /path/to/cellranger_indexes/GRCh38.$ENSVER.filtered


  ## SINGLE LIBRARY:

  ## FILTERED GTF
  OUTDIR=/path/to/cellranger_counts.filtered
  cd $OUTDIR
  transcriptome=/path/to/cellranger_indexes/GRCh38.$ENSVER.filtered
  ## We leave out "nosecondary" as we want to generate analysis results in web QC report
  cellranger count --jobmode=local --localmem="$MEM" --localcores="$NCPU" --disable-ui --id="$sid" --sample="$samples" --fastqs="$fastqdir" --transcriptome="$transcriptome"

  ## UN-FILTERED GTF
  OUTDIR=/path/to/cellranger_counts
  cd $OUTDIR
  transcriptome=/path/to/cellranger_indexes/GRCh38.$ENSVER
  ## We leave out "nosecondary" as we want to generate analysis results in web QC report
  cellranger count --jobmode=local --localmem="$MEM" --localcores="$NCPU" --disable-ui --id="$sid" --sample="$samples" --fastqs="$fastqdir" --transcriptome="$transcriptome"

  ## Filtered and un-filtered GTF counts
  COUNTDIRS="/path/to/cellranger_counts.filtered
/path/to/cellranger_counts"

  ## Generate full (non-MEX format) count matrices
  for countdir in $COUNTDIRS
  do
    MEXPATH=$countdir/$sid/outs/filtered_feature_bc_matrix
    cellranger mat2csv $MEXPATH "$countdir/gene_count_$sid.csv"
    ## Convert CSV to TSV
    sed -r -e 's|\,|\t|g' -e 's|\"||g' "$countdir/gene_count_$sid.csv" > "$countdir/gene_count_$sid.tsv"
  done
 

3. Merge ("cellranger aggr") multiple counts into a single matrix, output merged analysis web QC

  *** If single library, SKIP THIS STEP - WE ALREADY HAVE FINAL UMI COUNT MATRIX ***

  sids="CCJYFANXX
CCG6WANXX
CCL9HANXX"

  ## Repeate for both filtered and un-filtered GTF counts
  COUNTDIRS="/path/to/cellranger_counts.filtered
/path/to/cellranger_counts"

  for countdir in $COUNTDIRS
  do
    csvfile=$countdir/aggr_info.csv

    ## Build metadata CSV required by cellranger aggr
    echo "library_id,molecule_h5" > $csvfile.header
    ## Build the merge CSV and list of sample IDs involved
    echo $sids | while read sid
    do
      echo "$sid,$countdir/$sid/outs/molecule_info.h5" >> $csvfile.body
    done
    cat "$csvfile.header" "$csvfile.body" > "$csvfile"
    rm -f "$csvfile.header" "$csvfile.body"

    outputdesc="Merged counts (default subsampling normalization) from: $sids"
    outputdesc_nonorm="Merged counts (WITHOUT subsampling normalization) from: $sids"

    ## Aggregate WITH sub-sampling norm (correct for read count vs genome complexity bias at the cost of losing some information from larger libraries)
    cellranger aggr --jobmode=local --localmem="$MEM" --localcores="$NCPU" --disable-ui --id="merged" --csv="$csvfile" --description="$outputdesc" --normalize="mapped"

    ## Aggregate WITHOUT sub-sampling norm (i.e. raw merge, downstream processes must handle normalization e.g. using R)
    cellranger aggr --jobmode=local --localmem="$MEM" --localcores="$NCPU" --disable-ui --id="merged_nonorm" --csv="$csvfile" --description="$outputdesc_nonorm" --normalize="none"
  done


4. Arrange for transfer of pipeline outputs and all scripts used to Stemformatics team

   Counts data - for every input library/sample, the output of "cellranger count" *EXCEPT* .bam files

   e.g.

  outs
  |-- filtered_feature_bc_matrix
  |   |-- barcodes.tsv.gz
  |   |-- features.tsv.gz
  |   `-- matrix.mtx.gz
  |-- filtered_feature_bc_matrix.h5
  |-- metrics_summary.csv
  |-- molecule_info.h5
  |-- raw_feature_bc_matrix
  |   |-- barcodes.tsv.gz
  |   |-- features.tsv.gz
  |   `-- matrix.mtx.gz
  |-- raw_feature_bc_matrix.h5
  `-- web_summary.html

  Also, the CSV/TSV (non-sparse) count matrices produced in step 2)


  Merged counts data (if applicable) *BOTH NORMALISED AND NON-NORMALISED*

  outs
  |-- aggregation.csv
  |-- analysis
  |   |-- clustering
  |   |   |-- *
  |   |-- diffexp
  |   |   |-- *
  |   |-- pca
  |   |   |-- *
  |   `-- tsne
  |       |-- *
  |-- cloupe.cloupe
  |-- filtered_feature_bc_matrix
  |   |-- *
  |-- filtered_feature_bc_matrix.h5
  |-- raw_feature_bc_matrix
  |   |-- *
  |-- raw_feature_bc_matrix.h5
  |-- summary.json
  `-- web_summary.html
   

