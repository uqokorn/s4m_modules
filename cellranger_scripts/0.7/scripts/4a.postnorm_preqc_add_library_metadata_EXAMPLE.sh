#!/bin/sh
## Inject QC metadata before running post_qc
## TODO: Take QC columns to include as comma-separated argument input

GENOME="$1"
## The QC fields from high level targets to add into cell-level targets
QC_TO_INCLUDE="ExampleQCName
"

Usage () {
  echo "
  Usage: $0 <genome>
"
}

if [ -z "$GENOME" ]; then
  Usage
  exit
fi

TMPDIR=./tmp
mkdir -p $TMPDIR
RAW=../raw
PROCDIR=../processed.$GENOME
TARGETS="$RAW/targets.txt"
AGGR_INFO=$PROCDIR/counts_merged/aggr_info.csv

QC_TARGETS=$PROCDIR/normalised/targets_QC.txt
QC_TARGETS_REMOVED=$PROCDIR/normalised/targets_QC_removecells.txt

QC_FINAL="$PROCDIR/targets_QC.txt.FINAL"
QC_FINAL_REMOVED="$PROCDIR/targets_QC_removecells.txt.FINAL"

if [ ! -f "${QC_FINAL}.BAK" -a -f "$QC_FINAL" ]; then
  echo "NOTE: '$QC_FINAL' file exists, backing up to '${QC_FINAL}.BAK'"
  cp "$QC_FINAL" "${QC_FINAL}.BAK"
fi
if [ ! -f "${QC_FINAL_REMOVED}.BAK" -a -f "$QC_FINAL_REMOVED" ]; then
  echo "NOTE: '$QC_FINAL_REMOVED' file exists, backing up to '${QC_FINAL_REMOVED}.BAK'"
  cp "$QC_FINAL_REMOVED" "${QC_FINAL_REMOVED}.BAK"
fi

cp "$QC_TARGETS" "$QC_FINAL"
cp "$QC_TARGETS_REMOVED" "$QC_FINAL_REMOVED"

head -1 "$QC_FINAL" > "${QC_FINAL}.header"
head -1 "$QC_FINAL_REMOVED" > "${QC_FINAL_REMOVED}.header"
tail -n+2 "$QC_FINAL" > "${QC_FINAL}.body"
tail -n+2 "$QC_FINAL_REMOVED" > "${QC_FINAL_REMOVED}.body"

sample_ids=`tail -n+2 $TARGETS | cut -f 1`
## Add QC columns to new headers
for qc in $QC_TO_INCLUDE
do
  echo "$qc" | grep -P "^QC_" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "NOTE: Prepending 'QC_' to column name '$qc' for downstream plotting.."
    qc="QC_${qc}"
  fi
  sed -i -r -e "s|\$|\\t$qc|" "${QC_FINAL}.header"
  sed -i -r -e "s|\$|\\t$qc|" "${QC_FINAL_REMOVED}.header"
done

for sid in $sample_ids; do
  ## Get correct aggregation sample index number for appending to cell barcodes
  snum=`grep -n "$sid" "$AGGR_INFO" | head -1 | cut -d':' -f 1`
  ## account for aggr_info header
  snum=`expr $snum - 1`

  echo "Processing sample '$sid'.."

  echo "DEBUG: snum=[$snum]"
  for qc in $QC_TO_INCLUDE
  do
    echo "  DEBUG: qc=[$qc]"
    ## get index of QC field
    cutcol=`head -1 $TARGETS | tr "\t" "\n" | grep -n $qc | cut -d':' -f 1`
    echo "  DEBUG: cutcol=[$cutcol]"
    qc_val=`tail -n+2 $TARGETS | sed -n "${snum}p" | cut -f $cutcol`
    echo "  DEBUG: qc_val=[$qc_val]"
    stype=`tail -n+2 $TARGETS | sed -n "${snum}p" | cut -f 2`
    echo "  DEBUG: stype=[$stype]"
    ## *** Assumes SampleType\torig.ident ***
    sed -i -r -e "s|^(.*)($stype\\t$snum)(.*)\$|\\1\\2\\3\\t$qc_val|g" "${QC_FINAL}.body"
    sed -i -r -e "s|^(.*)($stype\\t$snum)(.*)\$|\\1\\2\\3\\t$qc_val|g" "${QC_FINAL_REMOVED}.body"
  done
done

echo "Creating final output '$QC_FINAL'.."
cat "${QC_FINAL}.header" "${QC_FINAL}.body" > "$QC_FINAL"
echo "Creating final output '$QC_FINAL_REMOVED'.."
cat "${QC_FINAL_REMOVED}.header" "${QC_FINAL_REMOVED}.body" > "$QC_FINAL_REMOVED"

## clean up temp files
rm -f "${QC_FINAL}.header" "${QC_FINAL}.body" "${QC_FINAL_REMOVED}.header" "${QC_FINAL_REMOVED}.body"

echo "All done"; echo

