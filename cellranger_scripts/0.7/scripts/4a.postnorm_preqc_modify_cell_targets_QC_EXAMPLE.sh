#!/bin/sh
## Example script to post-parse normalised cell-level QC targets in preparation for
## scanpy-based QC plotting.
##
## Add binned gene count metadata based on gene count column already present from
## Seurat pipeline

## So we use the right processed.$GENOME directory!
GENOME="$1"

Usage () {
  echo "
  Usage: $0 <genome>
"
}

if [ -z "$GENOME" ]; then
  Usage
  exit
fi

TMPDIR=./tmp
mkdir -p $TMPDIR
PROCDIR=../processed.$GENOME

## We're going to replace this targets file
QC_TARGETS="$PROCDIR/normalised/targets_QC.txt"
QC_TARGETS_REMOVECELLS="$PROCDIR/normalised/targets_QC_removecells.txt"

if [ ! -f "$QC_TARGETS" ]; then
  echo "Error: Missing QC targets file '$QC_TARGETS' - has normalisation been run?
" 1>&2
  exit 1
fi

## Make (one time) backups first
if [ ! -f "${QC_TARGETS}.BAK" -a -f "$QC_TARGETS" ]; then
  echo "NOTE: '$QC_TARGETS' file exists, backing up to '${QC_TARGETS}.BAK'"
  cp "$QC_TARGETS" "${QC_TARGETS}.BAK"
fi
if [ ! -f "${QC_TARGETS_REMOVECELLS}.BAK" -a -f "$QC_TARGETS_REMOVECELLS" ]; then
  echo "NOTE: '$QC_TARGETS_REMOVECELLS' file exists, backing up to '${QC_TARGETS_REMOVECELLS}.BAK'"
  cp "$QC_TARGETS_REMOVECELLS" "${QC_TARGETS_REMOVECELLS}.BAK"
fi

## Do custom modifications to QC targets file here

## Index of gene count column ("nFeature_RNA" and NOT gene_count which is UMI library size!)
genecol=5

files="$QC_TARGETS
$QC_TARGETS_REMOVECELLS"

for f in $files
do
  if [ ! -f "$f" ]; then
    continue
  fi
  echo "Adding binned gene counts to: '$f' .."
  cat "$f" | awk -F'\t' 'NR==1 {
    print $0"\tQC_gene_count_binned"
  }
  NR>1 {
    genecount=$7
    genebinned=""
    if (genecount < 100) {
      genebinned="under_100_genes"
    } else if (genecount < 500) {
      genebinned="under_500_genes"
    } else if (genecount < 1000) {
      genebinned="under_1000_genes"
    } else {
      genebinned="over_1000_genes"
    }
    print $0"\t"genebinned
  }' > "$f.new"
  mv "$f.new" "$f"
  echo "  done"
done

echo "All done"; echo

