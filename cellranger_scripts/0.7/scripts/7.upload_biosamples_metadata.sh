#!/bin/sh

DSID=XXXX
PSQL_HOST=%HOST_ALIAS%
SAMPLE_TYPE_ORDER="Sample1,Sample2"
SAMPLE_TYPE_GROUPS_JSON="{\"Sample1\":0,\"Sample2\":1}"
BIOSAMPLES_TSV=biosamples_metadata.tsv

if [ ! -f $BIOSAMPLES_TSV ]; then
  echo "Error: Failed to find new biosamples_metadata file '$BIOSAMPLES_TSV'!
" 1>&2
  exit 1
fi

echo "Replacing biosamples_metadata for D#$DSID in host '$PSQL_HOST'.."

which psql.$PSQL_HOST > /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo "Error: Failed to find psql wrapper 'psql.$PSQL_HOST'!
" 1>&2
  exit 1
fi

echo "  Removing existing rows.."
psql.$PSQL_HOST "delete from biosamples_metadata where ds_id=$DSID"
if [ $? -ne 0 ]; then
  echo "Error: Failed to delete existing biosamples_metadata rows!
" 1>&2
  exit 1
fi
echo "  [ OKAY ]"

echo "  Inserting new rows.."
psql.$PSQL_HOST load biosamples_metadata $BIOSAMPLES_TSV
if [ $? -ne 0 ]; then
  echo "Error: Failed to insert new biosamples_metadata rows!
" 1>&2
  exit 1
fi
echo "  [ OKAY ]"; echo

echo "  Updating sample groups and ordering.."
psql.$PSQL_HOST "update dataset_metadata set ds_value='$SAMPLE_TYPE_ORDER' where ds_id=$DSID and ds_name='sampleTypeDisplayOrder'"
if [ $? -ne 0 ]; then
  echo "Error: Failed to update dataset_metadata!
" 1>&2
  exit 1
fi
psql.$PSQL_HOST "update dataset_metadata set ds_value='$SAMPLE_TYPE_GROUPS_JSON' where ds_id=$DSID and ds_name='sampleTypeDisplayGroups'"
if [ $? -ne 0 ]; then
  echo "Error: Failed to update dataset_metadata!
" 1>&2
  exit 1
fi
echo "  [ OKAY ]"; echo


echo "All done"; echo


