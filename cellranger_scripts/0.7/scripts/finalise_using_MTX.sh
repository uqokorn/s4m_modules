#!/bin/sh
## Set up for Stemformatics loading and GCT creation
## (do not use GCT creation from s4m.sh because it will take too long for large N single cell datasets)

DSDIR="$1"
GENOME="$2"
COUNTDIR="normalised"
if [ ! -z "$3" ]; then
  COUNTDIR="$3"
fi
TARGETS="$4"

Usage () {
  echo "$0 <dsdir> <genome> <countdir>

  NOTE: default countdir is 'normalised' if not supplied
"
}

if [ -z "$DSDIR" -o -z "$GENOME" -o -z "$COUNTDIR" ]; then
  Usage
  exit 1
fi

DSDIR=`readlink -f "$DSDIR"`
DSID=`basename $DSDIR`

TARGETS_FINAL="$DSDIR/source/processed.$GENOME/targets_QC_removecells.txt.FINAL"
if [ ! -z "$TARGETS" ]; then
  TARGETS_FINAL="$TARGETS"
else
  if [ ! -f "$TARGETS_FINAL" ]; then
    TARGETS_FINAL="$DSDIR/source/processed.$GENOME/targets_QC.txt.FINAL"
    if [ ! -f "$TARGETS_FINAL" ]; then
      echo "Error: No valid targets file found!

  Looking for: '$DSDIR/source/processed.$GENOME/targets_QC_removecells.txt.FINAL' or
    '$DSDIR/source/processed.$GENOME/targets_QC.txt.FINAL'
" 1>&2
      exit 1
    fi
  fi
fi

## NOTE: This isn't actually used for anything, but the file must exist for s4m.sh purposes!
RAW_FILE="gene_count.mtx"
NORM_FILE="gene_count_normalised_log2+1.mtx"
if [ -f "$DSDIR/source/processed.$GENOME/$COUNTDIR/gene_count_normalised_log2+1_removecells.mtx" ]; then
  NORM_FILE="gene_count_normalised_log2+1_removecells.mtx"
fi

echo "Will use '$NORM_FILE' as normalized expression data.."

echo "Linking files.."
(cd "$DSDIR/source/normalized" && unlink raw_expression.mtx 2> /dev/null)
(cd "$DSDIR/source/normalized" && unlink normalized_expression.mtx 2> /dev/null)
(cd "$DSDIR/source/normalized" && ln -s "../processed.$GENOME/$COUNTDIR/$RAW_FILE" raw_expression.mtx)
(cd "$DSDIR/source/normalized" && ln -s "../processed.$GENOME/$COUNTDIR/$NORM_FILE" normalized_expression.mtx)
echo "  done"

## Create GCT file
echo "Creating GCT file.."

create_gct_from_mtx () {
  input="$1"
  output="$2"

  #DO_DEBUG=TRUE
  ./scripts/mtx_to_gct.sh "$input" "$output" $DO_DEBUG
  return $?
}

dsid=`basename "$DSDIR"`
accession="S4M-$dsid"
time create_gct_from_mtx "$DSDIR/source/normalized/normalized_expression.mtx" "$DSDIR/$accession.gct"

## Is this needed?
echo "Creating 'normalized_expression_detection.txt'.."
(
  cd "$DSDIR/source/normalized";
  tail -n+4 "$DSDIR/$accession.gct" | cut -f 1 > normalized_expression_detection.txt.labels
  tail -n+4 "$DSDIR/$accession.gct" | cut -f 3- | awk -F'\t' '{print $0"\tTRUE"}' > normalized_expression_detection.txt.body
  pr -Jtm normalized_expression_detection.txt.labels normalized_expression_detection.txt.body > normalized_expression_detection.txt
  rm normalized_expression_detection.txt.labels normalized_expression_detection.txt.body
)
echo "  done"

## Calculate Median of medians

## Assumes cell columns are ordered numerically
grep -Pv "^%" "$DSDIR/source/normalized/normalized_expression.mtx" | tail -n+2 | cut -d' ' -f 2-3 | sort -k1,1V | awk '
function median (arr) {
  len = asort(arr, arr_sorted)

  print "DEBUG: In median(): sorted array = "
  for (i=1; i<=len; i++) {
    print arr_sorted[i]","
  }
  printf "\n"

  if (len % 2 == 0) {
    mid = int(len/2)
    med = (arr_sorted[mid] + arr_sorted[mid+1]) / 2.0
    #printf "DEBUG: In median(): med=" med "\n"
    return med
  } else {
    mid = int(len/2)+1
    med = arr_sorted[mid]
    #printf "DEBUG: In median(): med=" med "\n"
    return med
  }
}
BEGIN {
  ORS=""
  ## Temp arrays
  split("", cell_vals)
  ## Global arrays
  split("", cell_ind)
  split("", cell_counter)
  split("", cell_medians)
  curr_cell = 0
}
{
  #printf "DEBUG: curr_cell = [" curr_cell "]\n"
  if ($1 > curr_cell) {
    ## calculate median of previous cell
    if (curr_cell > 0) {
      split("", valarr)
      for (i=1; i<=cell_counter[curr_cell]; i++) {
        valarr[i] = cell_vals[curr_cell,i]
      }
      med = median(valarr)
      cell_medians[curr_cell] = med
      printf "Median of cell " curr_cell ": " med "\n"
      ## reset temp arrays
      split("", cell_vals)
    }
    curr_cell = $1
  }
  if ($1 in cell_ind) {
    cell_counter[$1] += 1
    valind = cell_counter[$1]
    #printf "DEBUG: (exist) cell_vals["$1","valind"] = "$2 "\n"
    cell_vals[$1,valind] = $2
  } else {
    cell_ind[$1] = "true"
    cell_counter[$1] = 1
    #printf "DEBUG: (new) cell_vals["$1",1] = "$2 "\n"
    cell_vals[$1,1] = $2
  }
}
END {
  #printf "DEBUG: curr_cell = [" curr_cell "]\n"
  ## calculate median of final cell 
  split("", valarr)
  for (i=1; i<=cell_counter[curr_cell]; i++) {
    valarr[i] = cell_vals[curr_cell,i]
  }
  med = median(valarr)
  cell_medians[curr_cell] = med
  printf "Median of cell " curr_cell ": " med "\n"
  ## Now do med of medians
  med_of_med = median(cell_medians)
  printf "\nMedian_of_medians: " med_of_med "\n"
}' > "$DSDIR/source/normalized/median.log"

grep -F Median_of_medians "$DSDIR/source/normalized/median.log" | cut -d':' -f 2 | tr -d " " \
  > "$DSDIR/source/normalized/MEDIAN_ABOVE_THRESHOLD"

## Biosamples metadata for Stemformatics upload (not using manual annotation for large N cells)

## Must match chip_type in datasets table

CHIP_TYPE=`grep chip_type "$DSDIR/METASTORE" | cut -d'=' -f 2`
if [ -z "$CHIP_TYPE" -o $? -ne 0 ]; then
  echo "Error: Failed to determine chip type from METASTORE!
" 1>&2
  exit 1
fi
SPECIES=`grep species_long "$DSDIR/METASTORE" | cut -d'=' -f 2`
if [ -z "$SPECIES" -o $? -ne 0 ]; then
  echo "Error: Failed to determine species from METASTORE!
" 1>&2
  exit 1
fi


echo "Creating 'biosamples_metadata.tsv.."
rm -f "$DSDIR/biosamples_metadata.tsv"

tail -n+2 "$TARGETS_FINAL" | while read line
do
  sid=`echo "$line" | cut -f 1`
  group=`echo "$line" | cut -f 2`
  /bin/echo -e "$CHIP_TYPE\t$sid\tAge\tNULL\t$DSID
$CHIP_TYPE\t$sid\tCell Type\tNULL\t$DSID
$CHIP_TYPE\t$sid\tCell line\tNULL\t$DSID
$CHIP_TYPE\t$sid\tDay\tNULL\t$DSID
$CHIP_TYPE\t$sid\tDevelopmental stage\tNULL\t$DSID
$CHIP_TYPE\t$sid\tDisease state\tNULL\t$DSID
$CHIP_TYPE\t$sid\tFACS profile\tNULL\t$DSID
$CHIP_TYPE\t$sid\tFinal cell type\tNULL\t$DSID
$CHIP_TYPE\t$sid\tGeneric sample type\tSample Type\t$DSID
$CHIP_TYPE\t$sid\tGeneric sample type long\tSample Type\t$DSID
$CHIP_TYPE\t$sid\tGenetic modification\tSample Type\t$DSID
$CHIP_TYPE\t$sid\tLabelling\tNULL\t$DSID
$CHIP_TYPE\t$sid\tLineGraphGroup\tNULL\t$DSID
$CHIP_TYPE\t$sid\tMedia\tNULL\t$DSID
$CHIP_TYPE\t$sid\tOrganism\t$SPECIES\t$DSID
$CHIP_TYPE\t$sid\tParental cell type\tNULL\t$DSID
$CHIP_TYPE\t$sid\tReplicate\t1\t$DSID
$CHIP_TYPE\t$sid\tReplicate Group ID\t$sid\t$DSID
$CHIP_TYPE\t$sid\tReprogramming method\tNULL\t$DSID
$CHIP_TYPE\t$sid\tSample Description\t$sid\t$DSID
$CHIP_TYPE\t$sid\tSample Type\t$group\t$DSID
$CHIP_TYPE\t$sid\tSample name\t$sid\t$DSID
$CHIP_TYPE\t$sid\tSample name long\t$sid\t$DSID
$CHIP_TYPE\t$sid\tSample type long\t$group\t$DSID
$CHIP_TYPE\t$sid\tSex\tNULL\t$DSID
$CHIP_TYPE\t$sid\tSource Name\t$sid\t$DSID
$CHIP_TYPE\t$sid\tTissue/organism part\tNULL\t$DSID
$CHIP_TYPE\t$sid\tproject_msc_set\tNULL\t$DSID
$CHIP_TYPE\t$sid\tproject_msc_type\tNULL\t$DSID
$CHIP_TYPE\t$sid\tproject_msc_why\tNULL\t$DSID" >> "$DSDIR/biosamples_metadata.tsv"
done
echo "  done"

## Refresh biosamples_metadata.txt while we're here
echo "Refreshing biosamples_metadata.txt.."
cut -f 1-4 "$DSDIR/biosamples_metadata.tsv" > "$DSDIR/biosamples_metadata.txt.2"
cut -f 5 "$DSDIR/biosamples_metadata.tsv" > "$DSDIR/biosamples_metadata.txt.1"
pr -Jtm "$DSDIR/biosamples_metadata.txt.1" "$DSDIR/biosamples_metadata.txt.2" > "$DSDIR/biosamples_metadata.txt"
rm -f "$DSDIR"/biosamples_metadata.txt.[12]
echo "  done"

echo "All Done"; echo
