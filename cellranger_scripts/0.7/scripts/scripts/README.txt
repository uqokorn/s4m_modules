
postnorm_qc_seurat.R.NEEDS_DEBUGGING:
	It kinda works but there's a weird issue where QC annotation is somehow not used or
	corrupted when loading the saved seuratDat object from a Seurat normalisation run,
	causing weird clustering of cells in PCA and/or UMAP.

	We are using 'scanpy_umap' which does essentially the same thing, so we don't
	*need* this Seurat based script (scanpy_umap is faster anyway) - but leaving here in
	case we want to do anything with this in future.
