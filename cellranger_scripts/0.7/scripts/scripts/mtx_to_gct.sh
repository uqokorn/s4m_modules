#!/bin/sh

input_mtx="$1"
gct_out="$2"
debugmode="$3"


Usage () {
  echo "$0 <input_MTX> <output_GCT>"
  echo
}


if [ -z "$input_mtx" -o -z "$gct_out" ]; then
  Usage
  exit 1
fi
if [ ! -z "$debugmode" ]; then
  DO_DEBUG=TRUE
fi

echo "#1.2" > ${gct_out}.header
#ROWNAMES=`grep -F "%ROWNAMES" $input_mtx | cut -f 2-`
#COLNAMES=`grep -F "%COLNAMES" $input_mtx | cut -f 2-`

echo "Processing GCT header.."
nrow_ncol=`grep -F -v "%" $input_mtx | head -1 | awk 'NR==1 {
  print $1"\\t"$2
}' | tee -a ${gct_out}.header`
nrow=`echo "$nrow_ncol" | cut -f 1`
ncol=`echo "$nrow_ncol" | cut -f 2`

if [ "$DO_DEBUG" = "TRUE" ]; then
  echo "DEBUG: Got nrow=[$nrow]" 1>&2
  echo "DEBUG: Got ncol=[$ncol]" 1>&2
fi

/bin/echo -n -e "NAME\tDESCRIPTION\t" >> ${gct_out}.header
grep -F "%COLNAMES" $input_mtx | cut -f 2- >> ${gct_out}.header

echo "  done"

echo "Expanding sparse values (*** this may take some time ***) .."

sort_tmpdir=""
sort --help | grep temporary-directory > /dev/null 2>&1
if [ $? -eq 0 ]; then
  sort_tmpdir="-T `dirname $gct_out`"
fi

if [ "$DO_DEBUG" = "TRUE" ]; then
  echo "DEBUG: First 40 sorted MTX values:" 1>&2
  grep -F -v "%" $input_mtx | tail -n+2 | sort -k1,2V $sort_tmpdir | head -40 1>&2
  echo 1>&2
fi

## sort has -T flag?
grep -F -v "%" $input_mtx | tail -n+2 | sort -k1,2V $sort_tmpdir | awk -v _NROW=$nrow -v _NCOL=$ncol 'BEGIN{
  ORS = ""
  firstrow = 0
  firstcol = 0
  lastrow = 0
  lastcol = 0
}
{
  row = $1
  col = $2 

  if (firstcol == 0) {
    firstcol = col
  }
  if (firstrow == 0) {
    firstrow = row
    ## zero pad up until the start row
    for (i=1; i<firstrow; i++) {
      for (j=1; j<=_NCOL; j++) {
        print "0"
        if (j < _NCOL)
          print "\t"
      }
      print "\n"
    }
    lastrow = row
  }

  if (row > lastrow) {
    ## Finish zero padding previous partial row
    if (lastcol < _NCOL) {
      for (j=lastcol+1; j<=_NCOL; j++) {
        print "0"
        if (j < _NCOL)
          print "\t"
      }
    }
    print "\n"
    ## if there are more rows between last and current, we have to zero-pad them
    for (i=lastrow+1; i<row; i++) {
      for (j=1; j<=_NCOL; j++) {
        print "0"
        if (j < _NCOL)
          print "\t"
      }
      print "\n"
    }
    lastrow = row
    lastcol = 0
  }

  ## Do current row - print up to the non-zero value (row:col)
  if (lastrow == row) {
    for (j=lastcol+1; j<col; j++) {
      print "0"
      if (j < _NCOL)
        print "\t"
    }
    print $3
    if (col < _NCOL)
      print "\t"
    lastcol = col
  }
};
END {
  ## Finish zero padding previous partial row
  if (lastcol < _NCOL) {
    for (j=lastcol+1; j<=_NCOL; j++) {
      print "0"
      if (j < _NCOL)
        print "\t"
    }
  }
  print "\n" 
  ## if there are more rows between last and EOF, we have to zero-pad them
  for (i=lastrow+1; i<=_NROW; i++) {
    for (j=1; j<=_NCOL; j++) {
      print "0"
      if (j < _NCOL)
        print "\t"
    }
    print "\n"
  }
}' > ${gct_out}.values
echo "  done"


echo "Processing labels (row names).."
grep -F "%ROWNAMES" $input_mtx | cut -f 2- | tr "\t" "\n" | awk '{print $1"\tNA"}' > ${gct_out}.labels
echo "  done"

nrow_values=`wc -l ${gct_out}.values | cut -d' ' -f 1`
nrow_labels=`wc -l ${gct_out}.labels | cut -d' ' -f 1`

if [ "$DO_DEBUG" = "TRUE" ]; then
  echo; echo "DEBUG: Num lines in ${gct_out}.values = $nrow_values" 1>&2
  echo "DEBUG: Num lines in ${gct_out}.labels = $nrow_labels" 1>&2
fi

ret=0
if [ $nrow_values -eq $nrow_labels ]; then
  echo "Merging row labels and data.."
  pr -Jtm ${gct_out}.labels ${gct_out}.values | cat ${gct_out}.header - > $gct_out
  ret=$?
  ## If in debug mode, don't clean up part files
  if [ "$DO_DEBUG" != "TRUE" ]; then
    rm -f ${gct_out}.header ${gct_out}.labels ${gct_out}.values
  fi
  echo "  done"
else
  echo "Error: Mismatch in line count between labels and data rows!
" 1>&2
  ret=1
fi

if [ $ret -eq 0 ]; then
  echo "All done"; echo
fi
exit $ret
