#!/bin/sh
PROCDIR="$1"
TARGETS="$2"

Usage () {
  echo "$0 <processed_dir> <targets_file>"; echo
}

if [ -z "$PROCDIR" -o -z "$TARGETS" ]; then
  Usage
  exit 1
fi
if [ ! -d "$PROCDIR" ]; then
  echo "Error: $0: Processed dir '$PROCDIR' does not exist!
" 1>&2
  exit 1
fi

. inc/funcs.sh

OUTDIR="$PROCDIR"

## get sample IDs from targets
tail -n+2 "$TARGETS" > "$OUTDIR/.targets.txt.body"

echo "Running 'mat2csv' on individual sample counts.."
while read line
do
  sid=`echo "$line" | cut -f 1`
  ## path to feature count MEX folder ("filtered" = good cellular barcodes)
  mexpath="$PROCDIR/counts/$sid/outs/filtered_feature_bc_matrix"

  cellranger mat2csv "$mexpath" "$OUTDIR/counts/gene_count_$sid.csv"
  if [ $? -ne 0 ]; then
    echo "Error: 'cellranger mat2csv' returned non-zero status for sample: $sid
" 1>&2
    exit 1
  fi
  ## csv to tsv
  cat "$OUTDIR/counts/gene_count_$sid.csv" | sed -r -e 's|\,|\t|g' -e 's|\"||g' > "$OUTDIR/counts/gene_count_$sid.txt"
done < "$OUTDIR/.targets.txt.body"

if [ -d "$PROCDIR/counts_merged" ]; then
  echo; echo "Running 'mat2csv' on MERGED sample counts.."
  mexpath="$PROCDIR/counts_merged/merged/outs/filtered_feature_bc_matrix"
  cellranger mat2csv "$mexpath" "$OUTDIR/counts_merged/gene_count_merged.csv"
  if [ $? -ne 0 ]; then
    echo "Error: 'cellranger mat2csv' returned non-zero status for merged input!
" 1>&2
    exit 1
  fi
  ## csv to tsv
  cat "$OUTDIR/counts_merged/gene_count_merged.csv" | sed -r -e 's|\,|\t|g' -e 's|\"||g' > "$OUTDIR/counts_merged/gene_count_merged.txt"

  ## NO-NORM
  mexpath="$PROCDIR/counts_merged/merged_nonorm/outs/filtered_feature_bc_matrix"
  if [ -d "$mexpath" ]; then
    cellranger mat2csv "$mexpath" "$OUTDIR/counts_merged/gene_count_merged_nonorm.csv"
    if [ $? -ne 0 ]; then
      echo "Error: 'cellranger mat2csv' returned non-zero status for merged (no-norm) input!
  " 1>&2
      exit 1
    fi
    ## csv to tsv
    cat "$OUTDIR/counts_merged/gene_count_merged_nonorm.csv" | sed -r -e 's|\,|\t|g' -e 's|\"||g' > "$OUTDIR/counts_merged/gene_count_merged_nonorm.txt"
  fi
fi

echo; echo "All done"
