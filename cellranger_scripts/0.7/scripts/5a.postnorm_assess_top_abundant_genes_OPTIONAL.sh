#!/bin/sh
## Assess the degree to which top most abundant genes in each library contribute
## to overall expression (by proportion)

DSID=XXXX
DSDIR=/path/to/data/datasets/$DSID
GENOME=%GENOME%
PROCDIR="$DSDIR/source/processed.$GENOME"
RAWFILE="$DSDIR/source/custom/gene_count.txt"
## e.g. Ensembl_ID\tEntrez_ID\tSymbol\tDescription\tChromosome\tStart\tEnd\tStrand 
ANNOTFILE=annotation.tsv
TARGETS="$DSDIR/source/raw/targets_cells.txt"
OUTDIR="$PROCDIR/qc_gene_abundance"
mkdir -p "$OUTDIR"

## Load env with a suitable R version
eval `s4m miniconda::load_environment -m 'cellranger_scripts/0.7' | tail -1`
R_BIN=`which R`

cd scripts
R --slave -f ./assess_top_abundant_genes.R --args -input="$RAWFILE" -outdir="$OUTDIR" -targets="$TARGETS"
if [ $? -ne 0 ]; then
  echo "Error: R script 'assess_top_abundant_genes.R' exited with non-zero status!"
  exit 1
fi
cd $OLDPWD

echo "Annotating.."
## attempt to annotate output top genes TSV table
head -1 "$OUTDIR/topgenes_data.tsv" > "$OUTDIR/.topgenes_header"
head -1 "$ANNOTFILE" | cut -f 3,5-9 > "$OUTDIR/.annot_header"
pr -Jtm "$OUTDIR/.topgenes_header" "$OUTDIR/.annot_header" > "$OUTDIR/topgenes_data_annotated.tsv"
tail -n+2 "$OUTDIR/topgenes_data.tsv" | while read line
do
  ensid=`echo "$line" | cut -f 2`
  ensannot=`grep -F "$ensid" "$ANNOTFILE" | cut -f 3,5-9`
  /bin/echo -e "$line\t$ensannot"
done >> "$OUTDIR/topgenes_data_annotated.tsv"

echo "All Done"; echo

