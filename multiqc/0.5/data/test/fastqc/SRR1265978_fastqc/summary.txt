PASS	Basic Statistics	SRR1265978.fastq
PASS	Per base sequence quality	SRR1265978.fastq
WARN	Per tile sequence quality	SRR1265978.fastq
PASS	Per sequence quality scores	SRR1265978.fastq
FAIL	Per base sequence content	SRR1265978.fastq
PASS	Per sequence GC content	SRR1265978.fastq
PASS	Per base N content	SRR1265978.fastq
PASS	Sequence Length Distribution	SRR1265978.fastq
PASS	Sequence Duplication Levels	SRR1265978.fastq
PASS	Overrepresented sequences	SRR1265978.fastq
PASS	Adapter Content	SRR1265978.fastq
FAIL	Kmer Content	SRR1265978.fastq
