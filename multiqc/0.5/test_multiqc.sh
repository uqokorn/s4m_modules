#!/bin/sh
## test_multiqc.sh: For "unittest" module execution
## To be executed inside module directory

## Set up test environment before any tests are run
oneTimeSetUp () {
  ## Load functions being tested

  S4M_MULTIQC_TMPDIR="$S4M_TMP/$$"
  ## Set up temp data / files location and populate dummy files
  mkdir -p "$S4M_MULTIQC_TMPDIR"
}

## Run after all tests finished
oneTimeTearDown () {
  rm -rf "$S4M_MULTIQC_TMPDIR"
}

## Run between tests (uncomment if needed)
#setUp () {
#}


## TESTS ##

test_multiqc_environment () {
  s4m_exec multiqc::version > /dev/null 2>&1
  assertEquals 0 $?
}

## Run MultiQC over FASTQC output
test_multiqc_fastqc () {
  s4m_exec multiqc::multiqc -i ./data/test/fastqc -o "$S4M_MULTIQC_TMPDIR/"
  assertEquals 0 $?
}
