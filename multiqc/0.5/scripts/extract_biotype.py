#!/usr/bin/env python

__author__ = "Isaac Virshup"

import pandas as pd
import re
import gzip
import json
from pathlib import Path
from collections import OrderedDict

ID_BIOTYPE_RE = re.compile(r"^.+\sgene_id \"(\w+)\";\s.*\sgene_biotype\s\"([^\s]+)\".+$")
IDNAME = "GeneID"

def extract_ids(infile):
    """Returns generator yielding tuples of (ensembl_id, biotype) from a gtf file"""
    infile = str(infile)
    with open(infile) as f:
        for line in f:
            matches = ID_BIOTYPE_RE.findall(line)
            if len(matches) == 0:
                pass
            elif len(matches) == 1:
                yield matches[0]
            else:
                raise Exception("Found multiple matches!\n Line:\t{}\nFound:\t{}".format(line, matches))

def to_long(data, targets):
    """Converts count data from wide to long."""
    if data.columns.name == None:
        data = data.copy()
        sampleid = targets.columns[0]
        data.columns.name = sampleid
    else:
        sampleid = data.columns.name
    if data.index.name == None:
        data.index.name = IDNAME
    longdf = data.stack().reset_index(name="Counts")
    longdf = longdf.merge(targets, on=sampleid, how="left")
    return longdf

def to_plot_dict(df):
    out = OrderedDict()
    for i in df.itertuples():
        d = i._asdict()
        k = d.pop("Index")
        out[k] = d
    return out

def to_plot_dict(df):
    out = OrderedDict()
    for i in df.itertuples():
        d = i._asdict()
        k = d.pop("Index")
        out[k] = d
    return out

def main():
    from argparse import ArgumentParser
    import datetime

    timestamped = lambda x: "[{}] {}".format(str(datetime.datetime.now()), str(x))

    parser = ArgumentParser(description="Annotates data by biotype, outputs MultiQC custom graph json.")
    parser.add_argument("gene_counts", help="Gene count table (e.g. gene_count_frags.txt)", type=Path)
    parser.add_argument("targets", help="Targets file", type=Path)
    parser.add_argument("gtf", help="Relevant gtf", type=Path)
    parser.add_argument("output_path", help="Where to place output json.", type=Path)
    parser.add_argument("-g", "--group_by", help="Column in targets to group samples by.", default="SampleID", type=str)

    args = parser.parse_args()

    print(timestamped("Received arguments:\n\t") + "\n\t".join(["{}:\t{}".format(k, str(v)) for k,v in args.__dict__.items()]))
    print(timestamped("Reading..."))

    data = pd.read_table(args.gene_counts)
    t = pd.read_table(args.targets)
    t["SampleID"] = t["SampleID"].astype(str) # In case it's read as integers
    long_data = to_long(data, t)
    assert args.group_by in t.columns, "Error: Argument `--group_by` must be a column in the targets file."
    if not str(args.output_path).endswith("_mqc.json"):
        Warning("Output file will not be found or parsed by multiqc unless it ends in '_mqc.json'")

    print(timestamped("Reading gtf (long step)..."))

    biotype_table = pd.DataFrame(extract_ids(args.gtf), columns=[IDNAME, "Biotype"])
    biotype_table.drop_duplicates(inplace=True)
    # Make sure no GeneID maps to multiple biotypes
    assert all(biotype_table.groupby(IDNAME)["Biotype"].apply(lambda x: len(x.unique())) == 1)
    biotype_table.set_index("GeneID", inplace=True)

    print(timestamped("Done reading GTF. Formatting json..."))

    # Annotating long dataframe with biotypes
    tidydf = pd.merge(long_data, biotype_table, left_on=IDNAME, right_index=True, how="left");
    # Summazing data by biotype, `group_by` variable
    plotdf = tidydf.groupby([args.group_by, "Biotype"], as_index=False)["Counts"].sum().pivot(index="SampleID", columns="Biotype", values="Counts")
    plotdf = plotdf[plotdf.mean().sort_values(ascending=False).index]

    # Formatting data for multiqc
    section_data = {
        "id": "biotype_bar_plot",
        "section_name": "Gene biotype distribution (all types)",
        "plot_type": "bargraph",
        "pconfig": {
            "title": "Biotype distribution",
            "ylab": args.group_by
        },
        "data": to_plot_dict(plotdf)
    }

    with args.output_path.open("w") as f:
        json.dump(section_data, f, indent=1, default=lambda x: x.item())

if __name__ == '__main__':
    main()
