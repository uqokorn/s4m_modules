#/bin/sh
# Module: multiqc
# Author: O.Korn, I.Virshup

# Include S4M functions
for i in `ls $S4M_SCRIPT_HOME/inc/*.sh`; do . $i; done

## ============================================================================


## This is a fix for Python3 'Click' library used by MultiQC - needs suitable LANG env to work properly
## TODO: Do we need this any more?
##
fix_env () {
  echo "$LANG" | grep -i "utf\-8" > /dev/null 2>&1
  if [ $? -ne 0 -o -z "$LANG" ]; then
    export LANG=en_US.UTF-8
  fi
}


do_multiqc () {
  dm_inputdir="$1"
  dm_outdir="$2"

  fix_env
  multiqc --version 2>/dev/null
  ## NOTE: Always forcibly overwrite existing outputs, disable interactive plots with >500 samples
  multiqc --force --cl_config "plots_flat_numseries: 500" -o "$dm_outdir" "$dm_inputdir"
  return $?
}


## Generate MultiQC report
multiqc_handler () {
  do_multiqc "$multiqc_inputdir" "$multiqc_outdir"
}


## Return version of MultiQC installed
multiqc_version_handler () {

  fix_env
  multiqc --version 2>/dev/null
  echo
  return $?
}


## Run scripts generating custom content
multiqc_biotype_qc_handler () {

  fix_env
  ## Sum samples by biotype
  if [ -z "$multiqc_group_by" ]; then
    python3 -u ./scripts/extract_biotype.py "$multiqc_gene_counts" "$multiqc_targets" "$multiqc_gtf" "$multiqc_output_path"
  else
    python3 -u ./scripts/extract_biotype.py "$multiqc_gene_counts" "$multiqc_targets" "$multiqc_gtf" "$multiqc_output_path" -g "$multiqc_group_by"
  fi
  return $?
}



main () {

  ## Import miniconda (for automatic dependency management)
  s4m_import miniconda/lib.sh

  if ! miniconda_activate; then
    exit 1
  fi

  s4m_route multiqc    multiqc_handler
  s4m_route version    multiqc_version_handler
  s4m_route biotype_qc multiqc_biotype_qc_handler

}

### START ###
main
